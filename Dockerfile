# Pull base image from docker.io registry, which is default registry.
From tomcat:8-jre8-alpine
EXPOSE 8080
# Let's copy our WAR to our newly created Tomcat image
ADD psp_beckend.war /usr/local/tomcat/webapps/

ARG ZK_URL=zk.docker.dev:2181
ARG ZK_NODE=BE_Prod

RUN mkdir /usr/local/tomcat/webapps/psp_beckend &&\
    unzip /usr/local/tomcat/webapps/psp_beckend.war -d /usr/local/tomcat/webapps/psp_beckend &&\
    sed -i 's/\/opt\/tomcat-pspb/\/usr\/local\/tomcat/g' /usr/local/tomcat/webapps/psp_beckend/WEB-INF/classes/logback.xml &&\
    sed -i "s/BE_Prod/${ZK_NODE}/g" /usr/local/tomcat/webapps/psp_beckend/WEB-INF/classes/psp_beckend_config.properties &&\
    sed -i "s/zookeeper1.pdc.games24x7.com:2182,zookeeper2.pdc.games24x7.com:2183,zookeeper3.pdc.games24x7.com:2182/${ZK_URL}/g" /usr/local/tomcat/webapps/psp_beckend/WEB-INF/classes/psp_beckend_config.properties

