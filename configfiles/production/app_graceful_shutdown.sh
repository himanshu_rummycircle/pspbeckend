#kill the app gracefully
pgrep -f tomcat-pspb | xargs kill

#wait for 2 minutes for the app to shut down
sleep 120;

apppath="/opt/tomcat-pspb/logs"
bpath="/backup/catalina/pspb-logs"

if [ ! -d $bpath ]; then
        mkdir -p $bpath
fi

Date=`date +%d-%m-%Y-%H.%M.%S`
#move the live log
mv -v $apppath/pspbeckend.* $bpath
mv -v $apppath/catalina* $bpath
mv -v $apppath/localhost_access_log* $bpath  

#Make tar for all file inside backup
tar -czvf  $bpath/PSP-BE.live.$Date.tar.gz $bpath

#sync archived logs to s3
/usr/local/scripts/copytos3-backup.sh
