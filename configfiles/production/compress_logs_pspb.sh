#!/bin/bash
# Shell script for compressing TourDaemon logs 
# Cleanup interval 15 days
# Version 2.02 (Modified Sep 01, 2017)(Meghanand)
# Change log.
# Fixes :- Web server missing logs issue.
# Improvement :- Backup folder structure 

apppath="/opt/tomcat-pspb/logs/"
filepath="/opt/tomcat-pspb/files/"
compress="/usr/bin/gzip"
ydate=$(date --date '1 day ago' +'%Y-%m-%d')
ddate=$(date --date '60 day ago' +'%Y-%m-%d')
dtnow=($date)
bpath="/backup/catalina/pspb-logs/$ydate"
cmp_log="/var/log/games24x7/pspb_logs_compression.log"



if [ ! -d /var/log/games24x7 ]; then
        echo "Creating : /var/log/games24x7"
        mkdir -p /var/log/games24x7
fi

# Recording time
echo "---------------" >> $cmp_log
echo `date` >> $cmp_log
echo "---------------" >> $cmp_log

if [ ! -d $bpath ]; then
        echo "Creating : $bpath"
        mkdir -p $bpath
fi



mv -v $apppath/catalina.$ydate* $bpath >> $cmp_log 2>&1
mv -v $apppath/localhost.$ydate* $bpath >> $cmp_log 2>&1
mv -v $apppath/manager.$ydate* $bpath >> $cmp_log 2>&1
mv -v $apppath/host-manager.$ydate* $bpath  >> $cmp_log 2>&1
mv -v $apppath/localhost_access_log.$ydate* $bpath  >> $cmp_log 2>&1
mv -v $apppath/pspbeckend.$ydate* $bpath   >> $cmp_log 2>&1
mv -v $filepath/*.csv $bpath   >> $cmp_log 2>&1
$compress -rv $bpath >> $cmp_log 2>&1
echo "Done"

# Delete old logs (60 days )
echo "Cleaning up logs :$ddate"
rm -rfv /backup/catalina/pspb-logs/$ddate* >> $cmp_log 2>&1 &&
echo "Done"
