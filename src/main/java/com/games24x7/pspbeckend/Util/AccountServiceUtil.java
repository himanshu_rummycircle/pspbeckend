package com.games24x7.pspbeckend.Util;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response.Status;

import org.apache.commons.collections4.ListUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.constants.Globals;
import com.games24x7.pspbeckend.mq.CMmqFactory;
import com.games24x7.pspbeckend.pojo.AccountStatus;
import com.games24x7.pspbeckend.pojo.BlockAccountStatusData;
import com.games24x7.pspbeckend.pojo.BulkStatusChangeData;
import com.games24x7.pspbeckend.pojo.CloseAccountStatusData;
import com.games24x7.pspbeckend.pojo.CloseAndMergeData;
import com.games24x7.pspbeckend.pojo.CloseAndRefundData;
import com.games24x7.pspbeckend.pojo.MergeAccount;
import com.games24x7.pspbeckend.pojo.Refund;
import com.games24x7.pspbeckend.pojo.UnblockUserData;
import com.games24x7.pspbeckend.pojo.UserData;
import com.games24x7.pspbeckend.pojo.UserKYCData;
import com.games24x7.pspbeckend.webclient.UserServiceClient;
import com.google.gson.Gson;

@Component
public class AccountServiceUtil {
    public static final Logger logger = LoggerFactory.getLogger(AccountServiceUtil.class);

    @Autowired
    Configuration config;

    @Autowired
    CMmqFactory mq;
    @Autowired
    UserServiceClient usClient;

    @Autowired
    PlayerUpdateUtil puUtil;

    @Autowired
    PSPCommentsUtil pspCommentsUtil;
    @Autowired
    BlockedUsersMappingUtil blockedUsersMappingUtil;

    private JSONArray getUserData(List<Long> userIds) {
        String url = config.get(Constants.UMS_ACC_STATUS_URL);

        url = url.replace("USERIDS", getEncodedUrl(userIds));
        logger.debug("URL ************* " + url);
        JSONArray result = usClient.getCall(url.trim());
        logger.debug("result ****** " + result);

        return result;
    }

    public void getherUserData(Object obj, int key) {
        int targetSize = config.getInt(Constants.ACC_CLOSURE_BATCH_SIZE, 100);
        if (key == Globals.CLOSE_AND_MERGE) {
            CloseAndMergeData data = (CloseAndMergeData) obj;
            getherUserDataForCloseAndMerge(data, key, targetSize);

        } else if (key == Globals.CLOSE_AND_REFUND) {
            CloseAndRefundData ref = (CloseAndRefundData) obj;
            getherUserDataForCloseAndRefund(ref, key, targetSize);

        } else if (key == Globals.BULK_ACCOUNT_STATUS_CHANGE) {
            BulkStatusChangeData ref = (BulkStatusChangeData) obj;
            getherUserDataForBulkAccountStatusChange(ref, key, targetSize);
        }
    }

    /**
     * @param ref
     * @param key
     * @param targetSize
     */
    private void getherUserDataForBulkAccountStatusChange(BulkStatusChangeData data, int key, int targetSize) {
        List<List<UserData>> list = ListUtils.partition(data.getUserData(), targetSize);

        list.forEach(mergeBatchList -> {
            List<Long> fromUserIds = data.getUserIdList(mergeBatchList);
            JSONArray result = getUserData(fromUserIds);
            setUserData(result, key, mergeBatchList);
        });

        logger.debug("Account Status Gethered **** " + data);
    }

    /**
     * @param ref
     * @param key
     * @param targetSize
     */
    private void getherUserDataForCloseAndRefund(CloseAndRefundData ref, int key, int targetSize) {
        List<List<Refund>> list = ListUtils.partition(ref.getRefundList(), targetSize);
        list.forEach(refundBatch -> {
            List<Long> fromUserIds = ref.getRefundUserIdList(refundBatch);
            JSONArray result = getUserData(fromUserIds);
            setUserData(result, key, refundBatch);
        });

        logger.debug("Account Status Refund Gethered **** " + ref.getRefundList());
    }

    /**
     * @param data
     * @param targetSize
     * @param key
     */
    private void getherUserDataForCloseAndMerge(CloseAndMergeData data, int key, int targetSize) {
        List<List<MergeAccount>> list = ListUtils.partition(data.getMergeAccData(), targetSize);

        list.forEach(mergeBatchList -> {
            List<Long> fromUserIds = data.getMergeUserList(mergeBatchList, true);
            JSONArray result = getUserData(fromUserIds);
            setUserData(result, key, mergeBatchList);
        });

        logger.debug("Account Status Gethered **** " + data.getMergeAccData());
    }

    private void setUserData(JSONArray result, int key, List<?> list) {

        if (result != null) {
            if (result.length() == list.size()) {
                Map<Long, Integer> resultMap = getResultMap(result);
                for (int i = 0; i < list.size(); i++) {
                    if (key == Globals.CLOSE_AND_MERGE) {
                        MergeAccount acc = (MergeAccount) list.get(i);
                        acc.setAccountStatus(resultMap.get(acc.getFromUserId()));

                    } else if (key == Globals.CLOSE_AND_REFUND) {
                        Refund acc = (Refund) list.get(i);
                        acc.setAccountStatus(resultMap.get(acc.getUserId()));
                    } else if (key == Globals.BULK_ACCOUNT_STATUS_CHANGE) {
                        UserData acc = (UserData) list.get(i);
                        acc.setAccountStatus(resultMap.get(acc.getUserId()));
                    }
                }
            } else {
                List<Long> reqUserId = getRequestUserIds(list, key);
                getNonExistanceUserList(reqUserId, result);
            }
        } else {
            List<Long> reqUserId = getRequestUserIds(list, key);
            throw new ServiceException(Constants.USERS_NOT_IN_DB + reqUserId, Status.BAD_REQUEST);
        }

    }

    /**
     * @param result
     * @return
     */
    private Map<Long, Integer> getResultMap(JSONArray result) {
        Map<Long, Integer> resultMap = new HashMap<>();
        if (result != null) {
            for (int i = 0; i < result.length(); i++) {

                resultMap.put(result.getJSONObject(i).getLong(Constants.USER_ID),
                        result.getJSONObject(i).getInt(Constants.ACCOUNT_STATUS));
            }
        }
        return resultMap;
    }

    /**
     * @param list
     * @param key
     * @return
     */
    private List<Long> getRequestUserIds(List<?> list, int key) {
        List<Long> reqUserId = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (key == Globals.CLOSE_AND_MERGE) {
                MergeAccount acc = (MergeAccount) list.get(i);
                reqUserId.add(acc.getFromUserId());

            } else if (key == Globals.CLOSE_AND_REFUND) {
                Refund acc = (Refund) list.get(i);
                reqUserId.add(acc.getUserId());
            } else if (key == Globals.BULK_ACCOUNT_STATUS_CHANGE) {
                UserData acc = (UserData) list.get(i);
                reqUserId.add(acc.getUserId());
            }
        }
        return reqUserId;
    }

    public void updateAcctStatusForFailedMergeAcc(CloseAndMergeData data) {
        String[] status = config.get(Constants.ALL_ACC_STATUS).split(",");
        Arrays.asList(status).forEach(accStatus -> {
            List<MergeAccount> list = data.getMergeAccForAccStatus(data.getFailedMergeAcc(),
                    Integer.parseInt(accStatus));
            logger.debug("updateAcctStatusForFailedMergeAcc size " + list.size() + " : " + list);
            if (!list.isEmpty()) {
                AccountStatus stat = updateAccountStatus(data.getMergeUserList(list, true), Integer.parseInt(accStatus),
                        data.getAgentId());
                // postAccStatusUpdateActivity(stat, 0, data,
                // data.getFileName(), data.getAgentId(), data.getAgent());

            }
        });
    }

    public AccountStatus updateAccountStatus(List<Long> list, int accStatus, long agentId) {
        AccountStatus status = new AccountStatus();
        List<Long> failedList = new ArrayList<>();
        List<Long> sucessList = new ArrayList<>();
        int targetSize = config.getInt(Constants.ACC_CLOSURE_BATCH_SIZE, 100);
        List<List<Long>> output = ListUtils.partition(list, targetSize);
        output.forEach(userList -> {
            JSONObject obj = getUpdateAccStatusJson(userList, accStatus, agentId);
            logger.debug("JSON for ACCstatus Update : " + obj.toString());
            String result = usClient.postCall(config.get(Constants.BULK_ACC_CLOSURE_URL), obj.toString(), "PUT");
            logger.debug("result from put call " + result);
            if (result != null) {
                if (result.trim().length() > Globals.ZERO) {
                    JSONObject respJson = new JSONObject(result);
                    if (respJson.has("failedList")) {
                        failedList.addAll(convertJSONArrayToList(respJson.getJSONArray(Constants.FAILED_LIST)));
                        sucessList.addAll(convertJSONArrayToList(respJson.getJSONArray(Constants.SUCESS_LIST)));
                    }
                } else {
                    sucessList.addAll(userList);
                }
            } else {
                status.setFailedUserIds(userList);
            }
        });
        status.setFailedUserIds(failedList);
        status.setSucessfulUserIds(sucessList);
        logger.debug("After ACC STATUS update :" + status.toString());
        return status;
    }

    private List<Long> convertJSONArrayToList(JSONArray jArray) {
        List<Long> list = new ArrayList<>();
        if (jArray != null) {
            for (int i = 0; i < jArray.length(); i++) {
                list.add(jArray.getLong(i));
            }
        }
        return list;
    }

    private JSONObject getUpdateAccStatusJson(List<Long> userList, int accStatus, long agentId) {
        JSONObject obj = new JSONObject();
        JSONObject value = new JSONObject();
        value.put(Constants.ACCOUNT_STATUS, accStatus);
        value.put(Constants.USER_LIST, new JSONArray(userList.toArray()));

        JSONObject context = new JSONObject();
        context.put(Constants.AGENT_ID, agentId);
        context.put(Constants.SOURCE, Constants.SOURCE_PSP_BECKEND);

        obj.put(Constants.VALUE, value);
        obj.put(Constants.CONTEXT, context);

        return obj;
    }

    /**
     * 
     * @param userList
     * @param status
     * @return
     */
    public JSONArray validateAccountStatus(List<Long> userList, String status) {

        JSONArray result = null;
        int targetSize = config.getInt(Constants.MERGE_BATCH_SIZE, 100);
        List<List<Long>> output = ListUtils.partition(userList, targetSize);
        logger.debug("output *** " + output);
        String url = config.get(Constants.UMS_URL);
        url = url.replace("ACC_STATUS", status);
        for (List<Long> list : output) {
            logger.debug("LIST  *** " + list);
            url = url.replace("USERIDS", getEncodedUrl(list));
            logger.debug("URL ************* " + url);
            result = usClient.getCall(url.trim());
            logger.debug("result ****** " + result);
            if (result != null) {
                return result;
            }
        }
        return result;

    }

    /**
     * @param list
     * @return
     */
    private String getEncodedUrl(List<Long> list) {
        String encodedURL = null;
        try {
            encodedURL = java.net.URLEncoder.encode(list.toString().substring(1, list.toString().length() - 1),
                    "UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.error(e.getMessage(), e);
        }
        return encodedURL;
    }

    public void closeAccount(Object obj, int key) {
        List<Long> closeuserIdList = null;
        long agentId = 0;
        String fileName = null;
        String agentName = null;
        if (key == Globals.CLOSE_AND_MERGE) {
            CloseAndMergeData data = (CloseAndMergeData) obj;
            closeuserIdList = data.getMergeUserList(data.getMergeAccData(), true);
            agentId = data.getAgentId();
            fileName = data.getFileName();
            agentName = data.getAgent();
        }
        if (key == Globals.CLOSE_AND_REFUND) {
            CloseAndRefundData data = (CloseAndRefundData) obj;
            closeuserIdList = data.getRefundUserIdList(data.getRefundList());
            agentId = data.getAgentId();
            fileName = data.getFilename();
            agentName = data.getAgent();
        }
        AccountStatus status = updateAccountStatus(closeuserIdList, Globals.ACCOUNT_STATUS_CLOSED, agentId);
        postAccStatusUpdateActivity(status, key, obj, fileName, agentId, agentName);

    }

    private void postAccStatusUpdateActivity(AccountStatus status, int key, Object obj, String fileName, long agentId,
            String agentName) {
        if (key == Globals.CLOSE_AND_MERGE) {
            CloseAndMergeData data = (CloseAndMergeData) obj;
            data.setFailedAccStatusChanged(status.getFailedUserIds());
            if (!status.getFailedUserIds().isEmpty()) {
                logger.debug(
                        "Merge Data Before " + data.getMergeAccData() + " : size: " + data.getMergeAccData().size());
                data.setMergeAccData(data.getMergeAccAfterRemoval(data, status.getFailedUserIds()));
                logger.debug("Merge Data After removal " + data.getMergeAccData() + " : size: "
                        + data.getMergeAccData().size());
            }
        }

    }

    public void sendAccountStatusCmMessage(List<Long> sucessfulUserIds, List<Long> failedUserIds, String filename,
            long agentId, String agentName) {
        logger.info(
                "Acc Status Changed SucessFully for :" + sucessfulUserIds.size() + " failed :" + failedUserIds.size());
        mq.publishToCM(mq.getAccStatusChangeMessage(sucessfulUserIds, failedUserIds, filename, agentId, agentName));

    }

    /**
     * @param userList
     */
    public void validateUserExistance(List<Long> userList) {

        JSONArray result = null;
        int targetSize = config.getInt(Constants.MERGE_BATCH_SIZE, 100);
        List<List<Long>> output = ListUtils.partition(userList, targetSize);
        for (List<Long> list : output) {

            result = getUserData(list);
            logger.debug("result validateUserExistance :" + result);
            if (result != null) {
                if (result.length() != list.size()) {
                    getNonExistanceUserList(list, result);
                }
            } else {
                throw new ServiceException(Constants.USERS_NOT_IN_DB + list, Status.BAD_REQUEST);
            }
        }
    }

    /**
     * @param list
     * @param result
     */
    private void getNonExistanceUserList(List<Long> list, JSONArray result) {
        Map<Long, Integer> map = getResultMap(result);
        List<Long> userexistList = new ArrayList<>(map.keySet());
        list.removeAll(userexistList);
        throw new ServiceException(Constants.USERS_NOT_IN_DB + list, Status.BAD_REQUEST);
    }

    public String getAccStatusName(int prevStatus) {
        String status = "ACTIVE";
        if (prevStatus == 1) {
            status = "NEW";
        } else if (prevStatus == 2) {
            status = "PHONE VERIFIED";
        } else if (prevStatus == 3) {
            status = "ID VERIFIED";
        } else if (prevStatus == 4) {
            status = "KYC VERIFIED";
        } else if (prevStatus == 5) {
            status = "BLOCKED";
        } else if (prevStatus == 6) {
            status = "CLOSED";
        } else if (prevStatus == 11) {
            status = "Under Review";
        } else if (prevStatus == 12) {
            status = "ID Rejected";
        } else if (prevStatus == 13) {
            status = "Address Proof/KYC Rejected";
        }
        return status;
    }

    private UserKYCData getUserData(long userId) {
        UserKYCData user = null;
        try {
            String url = config.get("ums.user.kyc.url");
            url = url.replace("USERID", String.valueOf(userId));
            logger.debug("URL ************* " + url);
            String result = usClient.getStringResGetCall(url.trim());
            logger.debug("result ****** " + result);
            if (result != null) {
                user = new Gson().fromJson(result, UserKYCData.class);
            } else {
                throw new ServiceException(Constants.INVALID_USER, Status.INTERNAL_SERVER_ERROR,
                        MTTSettlementConstant.US_CALL_FAILURE_CODE);
            }
        } catch (Exception e) {
            throw new ServiceException(Constants.GENERIC_ERROR_MESSAGE_PRINT,
                    Status.INTERNAL_SERVER_ERROR.getStatusCode(), e, MTTSettlementConstant.US_CALL_FAILURE_CODE);
        }

        return user;
    }

    private void updateAccStatus(long userId, String comment, int blockheaderId, String blockheadermessage,
            int accStatus, String additionalInfo, JSONObject context) {
        try {
            JSONObject obj = new JSONObject();
            JSONObject value = new JSONObject();
            value.put(Constants.ACCOUNT_STATUS, accStatus);
            value.put(Constants.BLOCKHEADER_ID, blockheaderId);
            value.put(Constants.BLOCK_HEADER_MSG, blockheadermessage);
            obj.put(Constants.VALUE, value);

            context.put(Constants.SOURCE, Constants.PSPBS);
            context.put(Constants.ADDITIONALINFO, additionalInfo);
            context.put(Constants.COMMENTS, comment);
            obj.put(Constants.CONTEXT, context);
            String resp = usClient.postCall(config.getString("ums.accstatus.update.url") + userId, obj.toString(),
                    "POST");
            logger.debug("response after userStatus update {}", resp);
            if (resp == null) {
                throw new ServiceException(Constants.ACCSTATUS_UPDATE_DB_CALL_ERROR,
                        Status.INTERNAL_SERVER_ERROR.getStatusCode(), MTTSettlementConstant.US_CALL_FAILURE_CODE);
            }
        } catch (Exception e) {
            throw new ServiceException(Constants.ACCSTATUS_UPDATE_DB_CALL_ERROR,
                    Status.INTERNAL_SERVER_ERROR.getStatusCode(), e, MTTSettlementConstant.US_CALL_FAILURE_CODE);
        }

    }

    @Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    public void processBlockUser(BlockAccountStatusData obj, JSONObject context) {
        try {
            UserKYCData user = getUserData(obj.getUserId());
            String prevAccStatus = getAccStatusName(user.getAccount_status());
            if (!Constants.UMS.equalsIgnoreCase(context.getString(Constants.SOURCE)))
                updateAccStatus(obj.getUserId(), obj.getComment(), obj.getBlockheaderid(), obj.getMessagetouser(),
                        Globals.ACC_STATUS_BLOCKED, null, context);

            puUtil.insertIntoPlayerUpddatesForBlockUserHeaderData(obj.getUserId(), obj.getAgentId(), obj.getComment(),
                    obj.getHeader(), prevAccStatus, obj.getPspdata(),context.getString(Constants.SOURCE));
            long plUpdateId = puUtil.insertIntoPlayerUpddatesForBlockUser(obj, prevAccStatus, obj.getPspdata(),context.getString(Constants.SOURCE));
            pspCommentsUtil.savePSPComments(plUpdateId, obj.getPspdata());
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(Constants.BLOCK_USER_PROCESS_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                    e, Constants.BLOCK_USER_PROCESS_ERROR_CODE);
        }

    }

    @Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    public void processCloseUser(CloseAccountStatusData obj, JSONObject context) {
        try {
            UserKYCData user = getUserData(obj.getUserId());
            String prevAccStatus = getAccStatusName(user.getAccount_status());
            if (!Constants.UMS.equalsIgnoreCase(context.getString(Constants.SOURCE)))
                updateAccStatus(obj.getUserId(), obj.getComment(), obj.getBlockheaderid(), obj.getMessagetouser(),
                        Globals.ACCOUNT_STATUS_CLOSED, null, context);

            puUtil.insertIntoPlayerUpddatesForBlockUserHeaderData(obj.getUserId(), obj.getAgentId(), obj.getComment(),
                    obj.getHeader(), prevAccStatus, obj.getPspdata(),context.getString(Constants.SOURCE));
            long plUpdateId = puUtil.insertIntoPlayerUpddatesForCloseUser(obj, prevAccStatus, obj.getPspdata(),context.getString(Constants.SOURCE));
            pspCommentsUtil.savePSPComments(plUpdateId, obj.getPspdata());
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(Constants.CLOSE_USER_PROCESS_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                    e, Constants.CLOSE_USER_PROCESS_ERROR_CODE);
        }
    }

    @Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    public void unBlockUser(UnblockUserData obj, JSONObject context) {
        try {
            String prevAccStatus = getAccStatusName(obj.getPrevStatus());
            long plUpdateId = puUtil.insertIntoPlayerUpddatesForUnBlock(obj, prevAccStatus, obj.getPspdata(),context.getString(Constants.SOURCE));
            if (plUpdateId != 0) {
                pspCommentsUtil.savePSPComments(plUpdateId, obj.getPspdata());
            }

        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(Constants.UNBLOCK_USER_PROCESS_ERROR,
                    Status.INTERNAL_SERVER_ERROR.getStatusCode(), e, Constants.UNBLOCK_USER_PROCESS_ERROR_CODE);
        }
    }

}
