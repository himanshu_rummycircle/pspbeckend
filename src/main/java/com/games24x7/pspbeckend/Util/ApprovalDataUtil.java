package com.games24x7.pspbeckend.Util;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response.Status;

import org.apache.commons.collections4.ListUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Globals;
import com.games24x7.pspbeckend.dao.ApprovalDAO;
import com.games24x7.pspbeckend.pojo.ApprovalData;
import com.games24x7.pspbeckend.pojo.ApprovalDetailsDummy;
import com.games24x7.pspbeckend.pojo.CloseAndMergeData;
import com.games24x7.pspbeckend.pojo.MergeAccount;
import com.games24x7.pspbeckend.pojo.RequestedDetails;

@Component
public class ApprovalDataUtil {
    private static final Logger logger = LoggerFactory.getLogger(ApprovalDataUtil.class);

    @Autowired
    ApprovalDAO dao;

    @Autowired
    Configuration config;

    @Autowired
    ThreadPoolExecutorUtil util;

    @Autowired
    FundTransferUtil fsUtil;

    public CloseAndMergeData getApprovalData(CloseAndMergeData data) {
        List<ApprovalData> approvalList = new ArrayList<>();
        List<MergeAccount> mergeAccList = new ArrayList<>();
        int targetSize = config.getInt(Constants.APPROVAL_BATCH_SIZE, 100);
        List<Long> largeList = data.getFromAccountList();
        List<List<Long>> output = ListUtils.partition(largeList, targetSize);
        for (List<Long> list : output) {

            JSONArray result = fsUtil.getDepositAndWithdrawalAmt(list);

            logger.debug("UserMergeAccMap Keys *** " + data.getFromUserMergeAccMap().keySet());
            if (result != null) {

                List<Long> userListFromResp = new ArrayList<>();
                for (int i = 0; i < result.length(); i++) {
                    JSONObject jsonObject = result.getJSONObject(i);
                    long fromUid = jsonObject.getLong(Constants.USER_ID);
                    long toUid = data.getUserMap().get(jsonObject.getLong(Constants.USER_ID));
                    double deposit = getDeposit(jsonObject);
                    double withdrawable = getWithdrawable(jsonObject);

                    logger.debug("User : " + fromUid + " : " + deposit + " : to :" + toUid);
                    if ((deposit + withdrawable) > config.getLong(Constants.APPROVAL_GENERATION_AMOUNT, 2000)) {
                        ApprovalData approvalData = new ApprovalData(fromUid, toUid, deposit, withdrawable);
                        approvalList.add(approvalData);
                    } else {
                        MergeAccount mergeAccount = data.getFromUserMergeAccMap().get(fromUid);
                        logger.debug("mergeAccount *** " + mergeAccount);
                        mergeAccount.setDeposit(deposit);
                        mergeAccount.setWithdrawable(withdrawable);
                        mergeAccList.add(mergeAccount);

                    }
                    userListFromResp.add(fromUid);

                }
                checkInValidUsers(list, userListFromResp);

            } else {
                throw new ServiceException("These Users are not Present In DB " + list, Status.BAD_REQUEST);
            }
            logger.debug("Result *** " + result);

        }
        logger.info("mergedata size " + mergeAccList.size() + " approvaldata size " + approvalList.size());
        data.setMergeAccData(mergeAccList);
        data.setApprovalData(approvalList);
        logger.debug("Approval Data List *** " + approvalList);
        return data;
    }

    /**
     * @param list
     * @param userListFromResp
     */
    private void checkInValidUsers(List<Long> list, List<Long> userListFromResp) {
        if (list.size() != userListFromResp.size()) {
            list.removeAll(userListFromResp);
            logger.debug("Invalid from users *** " + list);
            throw new ServiceException("These Users are not Present In DB " + list, Status.BAD_REQUEST);
        }
    }

    /**
     * @param jsonObject
     * @return
     */
    private double getWithdrawable(JSONObject jsonObject) {
        double withdrawable = 0;
        if (!jsonObject.isNull(Constants.WITHDRAWABLE_AMOUNT))
            withdrawable = jsonObject.getDouble(Constants.WITHDRAWABLE_AMOUNT);
        return withdrawable;
    }

    /**
     * @param jsonObject
     * @return
     */
    private double getDeposit(JSONObject jsonObject) {
        double deposit = 0;
        if (!jsonObject.isNull(Constants.DEPOSIT_AMOUNT))
            deposit = jsonObject.getDouble(Constants.DEPOSIT_AMOUNT);
        return deposit;
    }

    public void generateApproval(CloseAndMergeData data) {

        logger.debug("List size ApprovalDetailsDummy Start : ");
        List<ApprovalDetailsDummy> list = new ArrayList<>();
        data.getApprovalData().forEach(approvalData -> {
            ApprovalDetailsDummy dummy = getApprovalDetailsData(approvalData, data.getAgent());
            list.add(dummy);
        });
        int targetSize = config.getInt(Constants.APPROVAL_BATCH_SIZE, 100);
        List<List<ApprovalDetailsDummy>> output = ListUtils.partition(list, targetSize);

        logger.debug("List size ApprovalDetailsDummy : " + output.size());
        output.forEach(dummy -> {
            dao.generateApprovalId(dummy);
            dao.generateDataShowLogAndAccMergingLog(dummy);
        });
        for (int i = 0; i < list.size(); i++) {
            data.getApprovalData().get(i).setApprovalId(list.get(i).getApprovdetailsid());
            logger.debug("Generated ApprovalIds : " + list.get(i).getApprovdetailsid());
        }

    }

    private ApprovalDetailsDummy getApprovalDetailsData(ApprovalData approvalData, String agent) {

        ApprovalDetailsDummy dummy = new ApprovalDetailsDummy();
        dummy.setActionId(Globals.MERGE_ACCOUNTS);
        dummy.setRequestor(agent);
        dummy.setPlayerId(approvalData.getFromUserId());
        dummy.setRequestorComment("Account Merge");
        String data = "from userId :" + approvalData.getFromUserId() + ",to userId:" + approvalData.getToUserId()
                + ",trnsfrdamt:" + approvalData.getDeposit() + ", transfrWithdral: " + approvalData.getWithdrawable();
        dummy.setRequestedData(data);
        dummy.setLoginId(String.valueOf(approvalData.getFromUserId()));
        RequestedDetails details = new RequestedDetails();
        details.setAmount(approvalData.getWithdrawable());
        details.setLoginId(String.valueOf(approvalData.getToUserId()));

        dummy.setRequestedDetails(details);
        return dummy;
    }

}
