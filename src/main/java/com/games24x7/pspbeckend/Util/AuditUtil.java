package com.games24x7.pspbeckend.Util;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.audit.entities.EventData;
import com.games24x7.frameworks.kafka.core.KafkaMessagePublisher;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.RiskConstant;
import com.games24x7.pspbeckend.pojo.CloseAndMergeData;
import com.games24x7.pspbeckend.pojo.MergeAccount;
import com.games24x7.pspbeckend.pojo.PSPComments;
import com.games24x7.pspbeckend.pojo.PlayerUpdates;
import com.games24x7.pspbeckend.pojo.PlayersRiskCertificate;
import com.games24x7.pspbeckend.pojo.RiskCertificate;

@Component
public class AuditUtil {
    private static final Logger logger = LoggerFactory.getLogger(AuditUtil.class);
    @Autowired
    ThreadPoolExecutorUtil util;

    @Autowired
    KafkaMessagePublisher messagePublisher;

    public void auditAccountMerging(CloseAndMergeData data) {
        util.getPlayerUpdateThreadpoolexecutor().execute(() -> processAccMergingAudit(data));
    }

    private void processAccMergingAudit(CloseAndMergeData data) {
        data.getMergeAccData().forEach(acc -> {
            EventData event = getAccMergingEvent(acc, data.getAgentId());
            sendCustomAuditMessageToKafka(event);

        });
    }

    public void sendCustomAuditMessageToKafka(EventData event) {
        JSONObject json = new JSONObject(event);
        try {
            messagePublisher.publishKafkaMessage("customAuditTopic", json.toString());
            logger.debug("sendCustomAuditMessageToKafka {} ", json.toString());
        } catch (Exception e) {
            logger.error("Error while sending message to AuditService ", e);
        }

    }

    private EventData getAccMergingEvent(MergeAccount sucessAcc, long agentId) {
        EventData event = new EventData();
        Map<String, Object> value = new HashMap<>();
        value.put(Constants.FROMUSERID, sucessAcc.getFromUserId());
        value.put(Constants.TOUSERID, sucessAcc.getToUserId());
        value.put(Constants.AGENT_ID, agentId);
        value.put(Constants.DEPOSIT_AMOUNT, sucessAcc.getDeposit());
        value.put(Constants.COMMENT, sucessAcc.getComment());
        value.put(Constants.WITHDRAWABLE_AMOUNT, sucessAcc.getWithdrawable());
        value.put(Constants.ACCOUNTSTATUS, sucessAcc.getAccountStatus());
        value.put(Constants.SUCCESS, sucessAcc.isSucess());
        event.setType(Constants.ACC_MERGING);
        event.setValue(value);
        return event;
    }

    public void playerUpdateAuditLog(List<PlayerUpdates> list) {
        util.getPlayerUpdateThreadpoolexecutor().execute(() -> {
            list.forEach(playerUpdate -> {
                try {
                    EventData event = getPlayerUpdateEvent(playerUpdate);
                    sendCustomAuditMessageToKafka(event);
                    EventData pspPlayerUpdateEvent = getPlayerUpdateEvent(playerUpdate, null, null, null, null,
                            Constants.PSP);
                    sendCustomAuditMessageToKafka(pspPlayerUpdateEvent);
                } catch (Exception e) {
                    logger.error("Error while sending playerUpdate audit log", e);
                }

            });
        });
    }

    public EventData getPlayerUpdateEvent(PlayerUpdates playerUpdate) {
        EventData event = new EventData();
        Map<String, Object> value = new HashMap<>();
        value.put(Constants.USERID, playerUpdate.getPlayerUserId());
        value.put(Constants.AGENT_ID, playerUpdate.getAgentId());
        value.put(Constants.FIELD_UPDATED, playerUpdate.getFieldUpdated());
        value.put(Constants.COMMENT, playerUpdate.getComment());
        value.put(Constants.PREV_DATA, playerUpdate.getPreviousData());
        value.put(Constants.TXNID, playerUpdate.getTxnId());
        value.put(Constants.ADDITIONAL_INFO, playerUpdate.getAdditionalInfo());
        value.put(Constants.UPDATE_DATE, playerUpdate.getUpdateDate());
        event.setType(Constants.ACC_CLOSURE);
        event.setValue(value);
        return event;
    }

    public void publishToAudit(String offlineSettlementTopic, JSONObject jsonObject) {
        try {
            messagePublisher.publishKafkaMessage(offlineSettlementTopic, jsonObject.toString());
        } catch (Exception e) {
            logger.error("Error while sending message to publishToAudit ", e);
        }
    }

    public void publishToAuditPlayerRiskCertificate(PlayersRiskCertificate riskPayload) {

        sendCustomAuditMessageToKafka(getPlayerRiskCertificateUpdateEvent(riskPayload));
    }

    private EventData getPlayerRiskCertificateUpdateEvent(PlayersRiskCertificate riskPayload) {
        EventData event = new EventData();
        Map<String, Object> value = new HashMap<>();
        value.put(Constants.USERID, riskPayload.getUserId());
        value.put(Constants.AGENT_ID, riskPayload.getAgentId());
        value.put(Constants.COMMENT, riskPayload.getComment());
        value.put(Constants.RISK_CERTIFICATE_NAMES, riskPayload.getRiskCertificateNames());
        value.put(Constants.CONTACT_TYPE, riskPayload.getContactType().toUpperCase());
        value.put(Constants.CONTACT_DIRECTION, riskPayload.getContactDirection().toUpperCase());
        value.put(Constants.UPDATE_TIME, System.currentTimeMillis());
        value.put(Constants.RISK_STATUS, riskPayload.getRiskStatus());
        value.put(Constants.RISK_STATUS_COMMENT, riskPayload.getRiskStatusComment());
        if (riskPayload.getReasonForDowngrade() != null && riskPayload.getReasonForDowngrade().length() > 0) {
            value.put(Constants.REASON_FOR_DOWNGRADE, riskPayload.getReasonForDowngrade());
        } else {
            value.put(Constants.REASON_FOR_DOWNGRADE, Constants.NA);
        }
        if (riskPayload.getInteractionId() != null && riskPayload.getInteractionId().length() > 0) {
            value.put(Constants.INTERACTION_ID, riskPayload.getInteractionId());
        } else {
            value.put(Constants.INTERACTION_ID, Constants.NA);
        }

        event.setType(Constants.PLAYER_RISK_CERTIFICATE_AUDIT);
        event.setValue(value);
        return event;
    }

    public void publishToAuditRiskCertificate(RiskCertificate riskPayload, long certId) {
        EventData event = new EventData();
        Map<String, Object> value = new HashMap<>();
        value.put(Constants.USERID, riskPayload.getId());
        value.put(Constants.AGENT_ID, riskPayload.getAgentId());
        value.put(Constants.RISK_CERTIFICATE_NAMES, riskPayload.getRiskCertificateName());
        value.put(Constants.UPDATE_TIME, System.currentTimeMillis());
        value.put(Constants.RISK_STATUS, riskPayload.getRiskStatus());
        value.put(RiskConstant.IS_ACTIVE_JSON, riskPayload.isActive());

        event.setType(Constants.RISK_CERTIFICATE_AUDIT);
        event.setValue(value);
        sendCustomAuditMessageToKafka(event);
    }

    public EventData getPlayerUpdateEvent(PlayerUpdates playerUpdate, Integer docType, String documentId,
            Boolean verified, PSPComments pspComments, String source) {
        if (pspComments == null) {
            pspComments = new PSPComments();
        }

        return getPspplayerupdates(playerUpdate.getAgentId(), playerUpdate.getPlayerUserId(),
                playerUpdate.getFieldUpdated(), playerUpdate.getComment(), playerUpdate.getPreviousData(),
                playerUpdate.getAdditionalInfo(), docType, documentId, verified, pspComments.getContact_direction(),
                pspComments.getContact_type(), pspComments.getCmr_interaction_id(), pspComments.getGameType(),
                pspComments.getIssue_catagory(), source);

    }

    public EventData getPspplayerupdates(Long agentId, Long userId, Integer changedField, String comment,
            String prevData, String additionalInfo, Integer docType, String documentId, Boolean verified,
            String contactDirection, String contactType, String cmrInteractionId, String gameType, String issueCatagory,
            String source) {

        EventData event = new EventData();
        Map<String, Object> value = new HashMap<>();
        value.put(Constants.USERID, userId);
        value.put(Constants.AGENT_ID, agentId);
        value.put(Constants.FIELD_UPDATED, changedField);
        value.put(Constants.COMMENT, comment);
        value.put(Constants.PREV_DATA, prevData);
        value.put(Constants.ADDITIONAL_INFO, additionalInfo);
        value.put(Constants.UPDATE_DATE, new Timestamp(System.currentTimeMillis()));
        if (docType != null)
            value.put(Constants.DOCTYPE, docType);
        if (documentId != null)
            value.put(Constants.DOCUMENT_ID, documentId);
        if (verified != null)
            value.put(Constants.DOC_VERIFIED, verified);

        if (contactDirection != null && contactDirection.trim().length() > 0)
            value.put(Constants.CONTACT_DIRECTION, contactDirection);
        if (contactType != null && contactType.trim().length() > 0)
            value.put(Constants.CONTACT_TYPE, contactType);
        if (gameType != null && gameType.trim().length() > 0)
            value.put(Constants.GAME_TYPE, gameType);
        if (cmrInteractionId != null && cmrInteractionId.trim().length() > 0)
            value.put(Constants.CMR_INTERACTION_ID, cmrInteractionId);
        if (issueCatagory != null && issueCatagory.trim().length() > 0)
            value.put(Constants.ISSUE_CATAGORY, issueCatagory);
        if (source != null && source.trim().length() > 0)
            value.put(Constants.SOURCE, source);

        event.setType(Constants.PSP_PLAYER_UPDATES);
        event.setValue(value);
        return event;
    }

    public EventData getAuditMessageForUpdateBank(long userId, JSONObject payload) {
        EventData event = new EventData();
        Map<String, Object> value = new HashMap<>();
        value.put(Constants.USERID, userId);
        value.put(Constants.AGENT_NAME, payload.get(Constants.AGENT_NAME));
        value.put(Constants.ACCOUNT_NUMBER, payload.get(Constants.ACCOUNT_NUMBER));
        value.put(Constants.IFSC_CODE, payload.get(Constants.IFSC_CODE));
        value.put(Constants.UPDATE_DATE, System.currentTimeMillis());
        event.setType(Constants.BANK_DETAIL_UPDATE_BY_AGENT);
        event.setValue(value);
        return event;
    }
}
