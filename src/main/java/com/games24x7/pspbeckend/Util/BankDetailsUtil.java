package com.games24x7.pspbeckend.Util;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.webclient.WithdrawServiceClient;

@Component
public class BankDetailsUtil {
	private static final Logger logger = LoggerFactory.getLogger(BankDetailsUtil.class);

	@Autowired
	WithdrawServiceClient wsClient;
	@Autowired
	Configuration config;

	public JSONObject getBankDetailForUser(long userId) {
		JSONObject result = null;

		try {
			String url = config.get("withdrawService.url") + Constants.WS_GET_BANK_DETAILS_URL + userId;
			logger.debug("getBankDetailForUser {}: URL {}", userId, url);
			String response = wsClient.getStringResGetCall(url);
			if (response != null)
				result = new JSONObject(response);
		} catch (ServiceException e) {
			throw e;
		} catch (Exception e) {
			throw new ServiceException(Constants.WS_CALL_FAILURE_MSG, Status.INTERNAL_SERVER_ERROR, e,
					Constants.WS_CALL_FAILURE_CODE);
		}
		return result;
	}

	public void updateBankDetailForUser(long userId, JSONObject payload) {
		try {
			String url = config.get("withdrawService.url") + Constants.WS_UPDATE_BANK_DETAILS_URL + userId;
			logger.debug("updateBankDetailForUser {}. PAYLOAD: {}", url, payload);
			wsClient.postCall(url, payload);
		} catch (ServiceException e) {
			throw e;
		} catch (Exception e) {
			throw new ServiceException(Constants.WS_CALL_FAILURE_MSG, Status.INTERNAL_SERVER_ERROR, e,
					Constants.WS_CALL_FAILURE_CODE);
		}
	}

}
