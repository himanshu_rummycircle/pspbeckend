package com.games24x7.pspbeckend.Util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.pspbeckend.dao.BlockUsersMappingDao;

@Component
public class BlockedUsersMappingUtil {
    private static final Logger logger = LoggerFactory.getLogger(BlockedUsersMappingUtil.class);

    @Autowired
    BlockUsersMappingDao blockUsersMappingDao;

    public void saveBlockedUsers(long userId, int headerId, String header, String message) {
        blockUsersMappingDao.saveBlockUserData(userId, headerId, header, message);
    }
}
