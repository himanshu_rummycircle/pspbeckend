package com.games24x7.pspbeckend.Util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.pojo.BulkStatusChangeData;
import com.games24x7.pspbeckend.pojo.UserData;
import com.sun.jersey.core.header.FormDataContentDisposition;

@Component
public class BulkStatusChangeUtil {
    private static final Logger logger = LoggerFactory.getLogger(BulkStatusChangeUtil.class);
    @Autowired
    Configuration config;
    @Autowired
    FileValidationsUtil fileValidation;

    public BulkStatusChangeData generateData(FormDataContentDisposition fileDetail, String agentName, long agentId) {

        String uploadedFileLocation = config.get(Constants.FILE_LOCATION) + fileDetail.getFileName();
        java.nio.file.Path pathToFile = Paths.get(uploadedFileLocation);

        BulkStatusChangeData bulkStatusChangeData = new BulkStatusChangeData();
        List<UserData> list = new ArrayList<>();

        String line = "";
        String cvsSplitBy = Constants.CSV_SPLIT_BY;
        try (BufferedReader br = Files.newBufferedReader(pathToFile)) {
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] data = line.split(cvsSplitBy);
                String comment = "";
                if (data.length >= 2 && data[1] != null) {
                    if (data[1].length() >= 254)
                        comment = data[1].substring(0, 254);
                    else
                        comment = data[1];
                }

                UserData refund = new UserData(Long.parseLong(data[0]), " [Bulk Blocked Account] " + comment);
                list.add(refund);

            }
            bulkStatusChangeData.setUserData(list);
            bulkStatusChangeData.setAgentId(agentId);
            bulkStatusChangeData.setAgent(agentName);
            bulkStatusChangeData.setFilename(fileDetail.getFileName());
        } catch (Exception e) {
            throw new ServiceException(Constants.FILE_DATA_ERROR_MESSAGE, Status.INTERNAL_SERVER_ERROR, e,
                    Constants.ERROR_IN_PROCESSING_REQUEST_CODE);
        }
        return bulkStatusChangeData;

    }

    public void basicValidations(String agentName, FormDataContentDisposition fileDetail,
            InputStream uploadedInputStream, Long agentId) {
        fileValidation.agentInputValidation(agentName, agentId);
        fileValidation.fileValidation(fileDetail, uploadedInputStream);
    }

    public void dataValidation(BulkStatusChangeData data) {
        if (data.getUserData().isEmpty()) {
            throw new ServiceException("File has no entries to process.", Status.BAD_REQUEST);
        } else if (!data.getUserData().isEmpty()
                && data.getUserData().size() > config.getInt(Constants.ALLOWED_ENTRY)) {
            logger.debug("List size is more  " + data.getUserData().size());
            throw new ServiceException("File has more entries than allowed.", Status.BAD_REQUEST);
        } else if (data.getAccountStatus() == null || data.getAccountStatus() <= 0) {
            logger.debug("getAccountStatus() " + data.getAccountStatus());
            throw new ServiceException(Constants.INVALID_ACCOUNT_STATUS_MESSAGE, Status.BAD_REQUEST);
        } else if (!config.get(Constants.VALID_ACCOUNT_STATUS, Constants.ALLOWED_ACCOUNT_STATUS)
                .contains(data.getAccountStatus().toString())) {
            logger.debug("AccountStatus() " + data.getAccountStatus());
            throw new ServiceException(Constants.INVALID_ACCOUNT_STATUS_MESSAGE + "And Should Be one of these "
                    + Constants.ALLOWED_ACCOUNT_STATUS, Status.BAD_REQUEST);
        }
    }

}
