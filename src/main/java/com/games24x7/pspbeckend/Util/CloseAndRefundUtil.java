package com.games24x7.pspbeckend.Util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Globals;
import com.games24x7.pspbeckend.pojo.CloseAndRefundData;
import com.games24x7.pspbeckend.pojo.Refund;
import com.sun.jersey.core.header.FormDataContentDisposition;

@Component
public class CloseAndRefundUtil {
    private static final Logger logger = LoggerFactory.getLogger(CloseAndRefundUtil.class);
    @Autowired
    Configuration config;
    @Autowired
    FileValidationsUtil fileValidation;
    

    public CloseAndRefundData refundDataGeneration(FormDataContentDisposition fileDetail, String agentName,
            long agentId) {

        String uploadedFileLocation = config.get(Constants.FILE_LOCATION) + fileDetail.getFileName();
        java.nio.file.Path pathToFile = Paths.get(uploadedFileLocation);

        CloseAndRefundData refundData = new CloseAndRefundData();
        List<Refund> list = new ArrayList<>();

        String line = "";
        String cvsSplitBy = Constants.CSV_SPLIT_BY;
        try (BufferedReader br = Files.newBufferedReader(pathToFile)) {
            int i = 0;
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] data = line.split(cvsSplitBy);

                i++;
                if (i != 1) {
                    Refund refund = new Refund(Long.parseLong(data[0]), data[1]);
                    list.add(refund);

                }

            }
            refundData.setRefundList(list);
            refundData.setAgentId(agentId);
            refundData.setAgent(agentName);
            refundData.setFilename(fileDetail.getFileName());
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return refundData;

    }

    public void basicValidations(String agentName, FormDataContentDisposition fileDetail,
            InputStream uploadedInputStream, Long agentId) {
        fileValidation.agentInputValidation(agentName, agentId);
        fileValidation.fileValidation(fileDetail, uploadedInputStream);
        fileValidation.validateFileHeader(fileDetail, Globals.CLOSE_AND_REFUND);
    }

    public void refundDataValidation(CloseAndRefundData refundData) {
        if (refundData.getRefundList().isEmpty()) {
            throw new ServiceException("File has no entries to process.", Status.BAD_REQUEST);
        }
        if (!refundData.getRefundList().isEmpty()
                && refundData.getRefundList().size() > config.getInt(Constants.ALLOWED_ENTRY)) {
            logger.debug("List size is more  " + refundData.getRefundList().size());
            throw new ServiceException("File has more entries than allowed.", Status.BAD_REQUEST);
        }
    }

}
