package com.games24x7.pspbeckend.Util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.stream.Stream;

import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Globals;
import com.sun.jersey.core.header.FormDataContentDisposition;

@Component
public class FileValidationsUtil {
    private static final Logger logger = LoggerFactory.getLogger(FileValidationsUtil.class);

    @Autowired
    Configuration config;
    private static final String FILE_HEADER_DO_NOT_MATCH = " File Headers do not match .";

    public void fileValidation(FormDataContentDisposition fileDetail, InputStream uploadedInputStream) {

        if (fileDetail == null) {
            throw new ServiceException("Please upload a csv file before submitting ", Status.BAD_REQUEST);
        }

        String uploadedFileLocation = config.get(Constants.FILE_LOCATION) + fileDetail.getFileName();

        // File validations
        String ext = FilenameUtils.getExtension(fileDetail.getFileName());
        if (!ext.equalsIgnoreCase(config.get("allowedFileExtention", Constants.CSV))) {
            logger.debug("File Uploaded with extention -- " + ext);
            throw new ServiceException("Please upload a csv file before submitting " + fileDetail.getFileName(),
                    Status.BAD_REQUEST);
        }

        File filefolder = new File(config.get(Constants.FILE_LOCATION));
        if (!filefolder.isDirectory()) {
            throw new ServiceException("Folder for file Uplaod is not proper : " + config.get(Constants.FILE_LOCATION),
                    Status.INTERNAL_SERVER_ERROR);
        }

        File f = new File(uploadedFileLocation);
        if (f.exists() && !f.isDirectory()) {
            logger.debug("File Exists-- " + uploadedFileLocation);
            throw new ServiceException("File uploaded already exists.", Status.BAD_REQUEST);
        }

        // save it
        writeToFile(uploadedInputStream, uploadedFileLocation);
    }

    // save uploaded file to new location
    private void writeToFile(InputStream uploadedInputStream, String uploadedFileLocation) {

        try (OutputStream out = new FileOutputStream(new File(uploadedFileLocation));) {

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = uploadedInputStream.read(bytes)) != -1) {

                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();
        } catch (IOException e) {

            throw new ServiceException("Error while writing the file , file name : " + uploadedFileLocation,
                    Status.INTERNAL_SERVER_ERROR, e);
        }

    }

    /**
     * 
     * @param fileDetail
     * @param key
     */
    public void validateFileHeader(FormDataContentDisposition fileDetail, int key) {
        String cvsSplitBy = Constants.CSV_SPLIT_BY;
        String uploadedFileLocation = config.get(Constants.FILE_LOCATION) + fileDetail.getFileName();
        java.nio.file.Path pathToFile = Paths.get(uploadedFileLocation);
        try (Stream<String> stream = Files.lines(pathToFile);) {
            Optional<String> option = stream.findFirst();
            String firstLine = option.isPresent() ? firstLine = option.get() : null;
            logger.debug("firstLine = " + firstLine);
            String[] data = firstLine != null ? firstLine.split(cvsSplitBy) : new String[0];

            switch (key) {
            case Globals.CLOSE_AND_MERGE:
                if (!(Constants.FROM_USERID_HEADER.equals(data[0]) && Constants.TO_USERID_HEADER.equals(data[1])
                        && Constants.COMMENTS.equals(data[2]))) {
                    throw new ServiceException(FILE_HEADER_DO_NOT_MATCH, Status.BAD_REQUEST);
                }
                break;
            case Globals.CLOSE_AND_REFUND:
                if (!(Constants.USER_ID.equals(data[0]) && Constants.COMMENTS.equals(data[1]))) {
                    throw new ServiceException(FILE_HEADER_DO_NOT_MATCH, Status.BAD_REQUEST);
                }
                break;

            default:
                throw new ServiceException("Invalid Option Selected.", Status.BAD_REQUEST);
            }

        } catch (Exception e) {
            throw new ServiceException(FILE_HEADER_DO_NOT_MATCH, Status.BAD_REQUEST, e);
        }

    }

    public void agentInputValidation(String agentName, Long agentId) {
        if (agentName == null || agentName.trim().length() <= 0 || "".equalsIgnoreCase(agentName)) {
            throw new ServiceException("AgentName is missing.", Status.BAD_REQUEST);

        }
        if (agentId == null || agentId <= 0) {
            throw new ServiceException("AgentId mandatory  & can not be 0.", Status.BAD_REQUEST);
        }
    }

}
