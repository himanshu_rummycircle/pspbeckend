package com.games24x7.pspbeckend.Util;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.webclient.UserServiceClient;

@Component
public class FraudServiceUtil {
    private static final Logger logger = LoggerFactory.getLogger(FraudServiceUtil.class);

    @Autowired
    UserServiceClient usClient;
    @Autowired
    Configuration config;

    public JSONObject getFraudDetailForDevice(String deviceId, int status) {
        JSONObject result = null;

        try {
            String url = config.get("ums.fraud.device.url");
            StringBuilder umsurl = new StringBuilder(url);
            umsurl.append(Constants.STATUS);
            umsurl.append("=");
            umsurl.append(status);

            umsurl.append("&");
            umsurl.append(Constants.DEVICE_ID);
            umsurl.append("=");
            umsurl.append(deviceId);
            logger.debug("REQUET URL ************* {}", umsurl);
            String response = usClient.getStringResGetCall(umsurl.toString().trim());
            logger.debug("RESULT ****** {}", response);
            if (response != null)
                result = new JSONObject(response);
            else
                result = new JSONObject();

        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(MTTSettlementConstant.US_CALL_FAILURE_MSG,
                    Status.INTERNAL_SERVER_ERROR.getStatusCode(), e, MTTSettlementConstant.US_CALL_FAILURE_CODE);
        }
        return result;
    }

    public JSONObject getFraudDetailForStatus(int status, int limit, int offset) {
        JSONObject result = null;

        try {
            String url = config.get("ums.fraud.device.url");
            StringBuilder umsurl = new StringBuilder(url);
            umsurl.append(Constants.STATUS);
            umsurl.append("=");
            umsurl.append(status);
            umsurl.append("&");
            umsurl.append(Constants.LIMIT);
            umsurl.append("=");
            umsurl.append(limit == 0 ? config.getInt("fraud.limit", 10) : limit);
            if (offset > 0) {
                umsurl.append("&");
                umsurl.append(Constants.OFFSET);
                umsurl.append("=");
                umsurl.append(offset);
            }
            logger.debug("REQUET URL ************* {}", umsurl);
            String response = usClient.getStringResGetCall(umsurl.toString().trim());
            logger.debug("RESULT ****** {}", response);
            if (response != null)
                result = new JSONObject(response);
            else
                result = new JSONObject();

        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(MTTSettlementConstant.US_CALL_FAILURE_MSG,
                    Status.INTERNAL_SERVER_ERROR.getStatusCode(), e, MTTSettlementConstant.US_CALL_FAILURE_CODE);
        }
        return result;
    }

    public void updateFraudDeviceStatus(int status, String agent, String deviceId) {
        try {
            String url = config.get("ums.fraud.device.update.url");
            url = url.replace(Constants.DEVICE_ID, deviceId);
            StringBuilder umsurl = new StringBuilder(url);
            umsurl.append(Constants.CURRENT_STATUS);
            umsurl.append("=");
            umsurl.append(status);
            umsurl.append("&");
            umsurl.append(Constants.AGENT);
            umsurl.append("=");
            umsurl.append(agent);

            logger.debug("REQUET URL ************* {}", umsurl);
            String response = usClient.postCall(umsurl.toString().trim(), "", "POST");
            logger.debug("RESULT ****** {}", response);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(MTTSettlementConstant.US_CALL_FAILURE_MSG,
                    Status.INTERNAL_SERVER_ERROR.getStatusCode(), e, MTTSettlementConstant.US_CALL_FAILURE_CODE);
        }

    }

}
