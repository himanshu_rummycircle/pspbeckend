package com.games24x7.pspbeckend.Util;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.ListUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.fund_transfer_util.CreditAccountRequest;
import com.games24x7.frameworks.fund_transfer_util.DebitAccountRequest;
import com.games24x7.frameworks.fund_transfer_util.GlobalTxn;
import com.games24x7.frameworks.fund_transfer_util.PlayerAccountKey;
import com.games24x7.frameworks.fund_transfer_util.PlayerBuckets;
import com.games24x7.frameworks.fund_transfer_util.TransferContext;
import com.games24x7.frameworks.fund_transfer_util.TransferPayload;
import com.games24x7.frameworks.fund_transfer_util.TransferTxn;
import com.games24x7.frameworks.fund_transfer_util.TransferValue;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Globals;
import com.games24x7.pspbeckend.pojo.CloseAndMergeData;
import com.games24x7.pspbeckend.pojo.MergeAccount;
import com.games24x7.pspbeckend.webclient.FundServiceClient;
import com.google.gson.Gson;

@Component
public class FundTransferUtil {
    private static final Logger logger = LoggerFactory.getLogger(FundTransferUtil.class);
    @Autowired
    FundServiceClient fsClient;
    

    @Autowired
    Configuration config;
    @Autowired
    ThreadPoolExecutorUtil util;

    public void processPayload(CloseAndMergeData data) {
        int targetSize = config.getInt(Constants.MERGE_BATCH_SIZE, 50);
        Gson gson = new Gson();
        List<List<MergeAccount>> output = ListUtils.partition(data.getMergeAccData(), targetSize);
        for (List<MergeAccount> list : output) {
            TransferPayload payload = createPayload(list, data.getAgentId());
            // JSONObject payload = createMergeAccPayload(list,
            // accMain.getAgentId());

            String jsonPayload = gson.toJson(payload);
            // String jsonPayload = payload.toString();
            logger.debug("jsonPayload *** " + jsonPayload);
            String result = fsClient.postCall(config.get(Constants.ACC_MERGE_URL), jsonPayload, "POST");

            if (result != null) {
                processRequest(result,list);
            }
            else{
                list.forEach(acc ->{acc.setSucess(false);} );
            }
        }
    }

   public void processRequest(String result, List<MergeAccount> list) {

        JSONObject payloadResJson = new JSONObject(result);
        if (payloadResJson.has(Constants.PARTIAL_FAILURE)
                && payloadResJson.getBoolean(Constants.PARTIAL_FAILURE)) {
            JSONArray transArr = payloadResJson.getJSONArray(Constants.TRANSACTIONS);
            for (int i = 0; i < transArr.length(); i++) {
                JSONObject transObj = transArr.getJSONObject(i);
                MergeAccount acc = list.get(i);
                if (transObj.getInt(Constants.STATUS_CODE) == Globals.SUCESS_CODE) {
                    acc.setSucess(true);

                } else {
                    acc.setSucess(false);
                }
            }
        }

    
    }

    private TransferPayload createPayload(List<MergeAccount> list, long agentId) {
        List<TransferTxn> transactions = new ArrayList<>();
        GlobalTxn globalTransaction = createGlobalTransaction(Globals.MERGE_ACCOUNTS_TXN_TYPE);
        for (int i = 0; i < list.size(); i++) {

            DebitAccountRequest debitAccounts = createDebitAccount(list.get(i));
            CreditAccountRequest creditAccounts = createCreditAccount(list.get(i));
            TransferTxn transaction = TransferTxn.buildTransferTxnForCash(globalTransaction, debitAccounts,
                    creditAccounts);

            transactions.add(transaction);
        }

        return createTransferPayload(transactions, agentId);

    }

    private TransferPayload createTransferPayload(List<TransferTxn> transactions, long agentId) {
        TransferValue value = TransferValue.buildTransferValue(false, transactions);
        TransferContext context = TransferContext.buildTransferContext(agentId, null, Constants.SOURCE_PSP_BECKEND,
                Constants.USER_ACTION);
        return TransferPayload.buildTransferPayload(value, context);
    }

    private CreditAccountRequest createCreditAccount(MergeAccount mergeAccount) {
        double deposit = mergeAccount.getDeposit();
        double withdrawable = mergeAccount.getWithdrawable();
        double nonwithdrawable = 0;
        long toUserId = mergeAccount.getToUserId();
        PlayerBuckets subaccountAmounts = PlayerBuckets.buildPlayerBuckets(deposit, withdrawable, nonwithdrawable);
        PlayerAccountKey key = PlayerAccountKey.buildPlayerKey(toUserId);
        return CreditAccountRequest.buildPlayerCreditAccount(key, null, subaccountAmounts);
    }

    private DebitAccountRequest createDebitAccount(MergeAccount mergeAccount) {
        double deposit = mergeAccount.getDeposit();
        double withdrawable = mergeAccount.getWithdrawable();
        double nonwithdrawable = 0;
        long fromUserId = mergeAccount.getFromUserId();
        PlayerBuckets subaccountAmounts = PlayerBuckets.buildPlayerBuckets(deposit, withdrawable, nonwithdrawable);
        PlayerAccountKey key = PlayerAccountKey.buildPlayerKey(fromUserId);
        return DebitAccountRequest.buildPlayerDebitRequest(key, null, subaccountAmounts);
    }

    private GlobalTxn createGlobalTransaction(int txnType) {
        return GlobalTxn.buildGlobalTxn(txnType);
    }

    public JSONArray getDepositAndWithdrawalAmt(List<Long> list) {
        String encodedURL = null;
        String url = config.get(Constants.FS_URL);
        try {
            encodedURL = java.net.URLEncoder.encode(list.toString().substring(1, list.toString().length() - 1),
                    "UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.error(e.getMessage(), e);
        }
        url = url.replace("USERIDS", encodedURL);

        return fsClient.getCall(url.trim());
    }

}
