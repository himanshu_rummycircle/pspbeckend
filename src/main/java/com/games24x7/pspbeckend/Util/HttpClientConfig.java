package com.games24x7.pspbeckend.Util;

import java.io.IOException;

import javax.annotation.PreDestroy;

import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.pspbeckend.constants.Constants.ClientConstants;

/**
 * 
 */
@org.springframework.context.annotation.Configuration
public class HttpClientConfig {

    private static final Logger logger = LoggerFactory.getLogger(HttpClientConfig.class);

    private CloseableHttpClient client;

    @Autowired
    private Configuration config;

    @Bean(name = "pspbehttpclient")
    public CloseableHttpClient getGstigHttpClient() throws Exception {
        try {
            client = HttpClients.custom().setConnectionManager(getConnectionManager())
                    .setDefaultRequestConfig(getRequestConfig()).setKeepAliveStrategy(getKeepAliveStrategy()).build();
            logger.debug("initializing pspbehttpclient bean");
            return client;
        } catch (Exception ex) {
            logger.error("Exception in pspbehttpclient", ex);
            throw ex;
        }
    }

    @SuppressWarnings("restriction")
    @PreDestroy
    public void cleanUp() throws IOException {
        try {
            client.close();
        } catch (Exception ex) {
            logger.error("Exception in cleanUp", ex);
        }

    }

    private PoolingHttpClientConnectionManager getConnectionManager() {
        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(config.getInt(ClientConstants.MAX_TOTAL_CONN,
                ClientConstants.MAX_TOTAL_CONN_DEFAULT));
        cm.setDefaultMaxPerRoute(config.getInt(ClientConstants.MAX_CONN_PER_ROUTE,
                ClientConstants.MAX_CONN_PER_ROUTE_DEFAULT));
        return cm;
    }

    private ConnectionKeepAliveStrategy getKeepAliveStrategy() {
        return new ConnectionKeepAliveStrategy() {
            public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
                return config.getLong(ClientConstants.KEEPALIVE_CONN_TIMEOUT,
                        ClientConstants.DEFAULT_TIMEOUT);
            }
        };
    }

    private RequestConfig getRequestConfig() {
        return RequestConfig
                .custom()
                .setConnectTimeout(
                        config.getInt(ClientConstants.CONNECT_TIMEOUT,
                                ClientConstants.DEFAULT_TIMEOUT))
                .setSocketTimeout(
                        config.getInt(ClientConstants.SOCKET_TIMEOUT,
                                ClientConstants.DEFAULT_TIMEOUT))
                .setConnectionRequestTimeout(
                        config.getInt(ClientConstants.CONNECTREQ_TIMEOUT,
                                ClientConstants.DEFAULT_TIMEOUT)).build();
    }

}
