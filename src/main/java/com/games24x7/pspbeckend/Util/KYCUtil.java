package com.games24x7.pspbeckend.Util;

import java.sql.Timestamp;

import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.constants.Globals;
import com.games24x7.pspbeckend.dao.BlockUsersMappingDao;
import com.games24x7.pspbeckend.dao.IdentityInfoDao;
import com.games24x7.pspbeckend.mq.CMmqFactory;
import com.games24x7.pspbeckend.mq.NotifierMQFactory;
import com.games24x7.pspbeckend.pojo.DepositLimitData;
import com.games24x7.pspbeckend.pojo.IdfyData;
import com.games24x7.pspbeckend.pojo.IdkycStatusData;
import com.games24x7.pspbeckend.pojo.KYCData;
import com.games24x7.pspbeckend.pojo.UserKYCData;
import com.games24x7.pspbeckend.pojo.UserUploadLog;
import com.games24x7.pspbeckend.webclient.AvsClient;
import com.games24x7.pspbeckend.webclient.LimitServiceClient;
import com.games24x7.pspbeckend.webclient.UserServiceClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component
public class KYCUtil {
    private static final Logger logger = LoggerFactory.getLogger(KYCUtil.class);
    @Autowired
    PlayerUpdateUtil puUtil;
    @Autowired
    IdentityInfoDao identityInfoDao;

    @Autowired
    UserPreferedLanguageUtil userPreferedLanguageUtil;
    @Autowired
    PSPCommentsUtil pspCommentsUtil;
    @Autowired
    BlockUsersMappingDao blockUsersMappingDao;
    @Autowired
    LimitServiceClient limitClient;
    @Autowired
    UserServiceClient usClient;
    @Autowired
    AvsClient avsClient;
    @Autowired
    Configuration config;

    @Autowired
    AccountServiceUtil accServiceUtil;
    @Autowired
    CMmqFactory cm;
    @Autowired
    NotifierMQFactory notifier;

    public void processKycVerified(KYCData obj, JSONObject context) {
        try {

            UserKYCData user = getUserData(obj);
            IdkycStatusData idkycData = getIDKycStatusData(obj.getUserId(), context);
            if (idkycData != null) {
                if (idkycData.getKyc_verified() == 0)
                    idkycData.setKyc_verified(1);
                idkycData.setUpdate_time(new Timestamp(System.currentTimeMillis()));
                idkycData.setKyc_verification_status(Globals.KYC_ACCEPTED);
                idkycData.setId_verified(1);
                idkycData.setId_verification_status(Globals.KYC_ACCEPTED);
            }
            String additionalInfo = "";
            int accStatus = getAccountStatusToUpdate(user, isOldUser(user), idkycData);

            updateUserStatus(obj.getUserId(), accStatus, Globals.KYC_ACCEPTED, context);
            DepositLimitData depositData = null;
            int userPreferedLanguage = userPreferedLanguageUtil.getUserPreferedLanguage(obj.getUserId());
            if (!isCallFromAVS(context)) {
                updateIDKYCStatus(idkycData, context);
                depositData = updateDepositLimit(obj.getUserId(), user.getAccount_status(), obj.getStatusComment(),
                        context, accStatus);
                String notifiermessage = getNotifierObj(obj, user, depositData, userPreferedLanguage, true);
                sendMessageToNotifier(notifiermessage, obj.getUserId());

            } else {
                logger.debug("before Idfy insert call...");
                insertIdfyData(obj.getUserId(), obj.getData(), context);
                sendOCREmail(user.getEmail(), obj.getData(), context.getInt(MTTSettlementConstant.CHANNELID));
                depositData = getDepositLimit(obj.getUserId());
            }
            double currDepositLimit = depositData != null ? depositData.getCurrentLimit() : 0;
            sendKYCUpdateEmail(Globals.KYC_ACCEPTED, user.getFirstname(), userPreferedLanguage, user.getEmail(),
                    currDepositLimit, context.getInt(MTTSettlementConstant.CHANNELID));
            sendSms(Globals.KYC_ACCEPTED, user);

            additionalInfo = checkIfDocTypeChanged(obj.getDocType(), obj.getUserId(), context);

            insertIntoLogTables(obj, user.getAccount_status(), additionalInfo, Globals.FIELD_UPDATE_KYC_VERIFIED, true,
                    context);

        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(Constants.KYC_PROCESSING_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(), e,
                    Constants.KYC_PROCESS_FAILURE_CODE);
        }

    }

    public void processKycRejected(KYCData obj, JSONObject context) {
        try {
            UserKYCData user = getUserData(obj);
            IdkycStatusData idkycData = getIDKycStatusData(obj.getUserId(), context);
            if (idkycData != null) {
                idkycData.setUpdate_time(new Timestamp(System.currentTimeMillis()));
                idkycData.setKyc_verification_status(Globals.KYC_REJECTED_STATUS);
            }

            String additionalInfo = "";
            updateUserStatus(obj.getUserId(), null, Globals.KYC_REJECTED_STATUS, context);
            DepositLimitData depositData = getDepositLimit(obj.getUserId());
            double currDepositLimit = depositData != null ? depositData.getCurrentLimit() : 0;
            int userPreferedLanguage = userPreferedLanguageUtil.getUserPreferedLanguage(obj.getUserId());
            sendKYCUpdateEmail(Globals.KYC_REJECTED_STATUS, user.getFirstname(), userPreferedLanguage, user.getEmail(),
                    currDepositLimit, context.getInt(MTTSettlementConstant.CHANNELID));
            additionalInfo = checkIfDocTypeChanged(obj.getDocType(), obj.getUserId(), context);
            insertIntoLogTables(obj, user.getAccount_status(), additionalInfo, Globals.FIELD_UPDATE_KYC_REJECTED, false,
                    context);

            if (!isCallFromAVS(context)) {
                updateIDKYCStatus(idkycData, context);
                updateUserKYCData(obj.getStatusComment(), obj.getUserId(), context);
                String notifiermessage = getNotifierObj(obj, user, depositData, userPreferedLanguage, false);
                sendMessageToNotifier(notifiermessage, obj.getUserId());
            }
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(Constants.KYC_REJECT_PROCESSING_ERROR,
                    Status.INTERNAL_SERVER_ERROR.getStatusCode(), e, Constants.KYC_REJECT_PROCESS_FAILURE_CODE);
        }

    }

    public void processKycReVerified(KYCData obj, JSONObject context) {
        try {

            IdkycStatusData idkycData = new IdkycStatusData();
            idkycData.setKyc_verified(1);
            idkycData.setUpdate_time(new Timestamp(System.currentTimeMillis()));
            idkycData.setKyc_verification_status(Globals.KYC_ACCEPTED);
            idkycData.setId_verified(1);
            idkycData.setId_verification_status(Globals.KYC_ACCEPTED);
            idkycData.setUser_id(obj.getUserId());

            String additionalInfo = "";

            updateUserStatus(obj.getUserId(), null, Globals.KYC_ACCEPTED, context);
            updateIDKYCStatus(idkycData, context);

            insertIntoLogTables(obj, Globals.KYC_STATUS, additionalInfo, Globals.FIELD_UPDATE_KYC_RE_VERIFIED, true,
                    context);

        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(Constants.KYC_REVERIFICATION_PROCESSING_ERROR,
                    Status.INTERNAL_SERVER_ERROR.getStatusCode(), e, Constants.KYC_REVERIFICATION_FAILURE_CODE);
        }

    }

    private void updateUserKYCData(String reason, Long userId, JSONObject context) {
        try {
            JSONObject obj = new JSONObject();
            JSONObject value = new JSONObject();

            value.put(Constants.REJECTION_TYPE, "PSP-Generic");
            value.put(Constants.REJECTION_REASON, reason);
            value.put(Constants.USER_ID, userId);
            obj.put(Constants.VALUE, value);
            obj.put(Constants.CONTEXT, context);
            logger.debug("KYC DATA update call BODY {}", obj.toString());
            String resp = avsClient.postCall(config.getString("avs.update.user.kyc.data.url"), obj.toString(), "POST");

            logger.debug("KYC DATA update call RESPONSE BODY {}", resp);

        } catch (Exception e) {
            throw new ServiceException(Constants.KYC_USER_DATA_UPDATE_ERROR,
                    Status.INTERNAL_SERVER_ERROR.getStatusCode(), e, Constants.KYC_USER_DATA_UPDATE_ERROR_FAILURE_CODE);
        }
    }

    private void sendOCREmail(String email, IdfyData data, int channelId) {
        cm.publishToCM(cm.getOCREmailMessage(email, data, channelId));
    }

    public boolean isCallFromAVS(JSONObject context) {
        if (context.has(Constants.SOURCE) && !context.isNull(Constants.SOURCE)
                && context.getString(Constants.SOURCE).equals(Constants.AVS)) {
            return true;
        } else
            return false;
    }

    private boolean isOldUser(UserKYCData user) {
        return user.getKyc_status() == 0 ? true : false;
    }

    private int getAccountStatusToUpdate(UserKYCData user, boolean isoldUser, IdkycStatusData idkycData) {
        int accStatus = 0;
        if (user.getAccount_status() < Globals.KYC_STATUS) {
            accStatus = Globals.KYC_STATUS;
        } else if (user.getAccount_status() == Globals.ACCOUNT_STATUS_CLOSED
                || user.getAccount_status() == Globals.ACC_STATUS_BLOCKED) {
            accStatus = Globals.KYC_STATUS;

            if (!isoldUser) {
                accStatus = Globals.KYC_STATUS;
            }
            if ((idkycData != null && idkycData.getId_verification_status() == 4)) {
                accStatus = Globals.KYC_STATUS;
            }
        } else {
            accStatus = user.getAccount_status();
        }
        return accStatus;
    }

    private DepositLimitData getDepositLimit(Long userId) {
        DepositLimitData data = null;
        try {
            String url = config.getString("limitservice.get.deposit.limit.url");
            url = url.replace("USERID", String.valueOf(userId));
            String res = limitClient.getStringResGetCall(url);
            if (res != null) {
                data = new GsonBuilder().setDateFormat("yyyy-MM-dd hh:mm:ss").create().fromJson(res,
                        DepositLimitData.class);
            }
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(Constants.GET_LIMIT_SERVICE_DB_CALL_ERROR,
                    Status.INTERNAL_SERVER_ERROR.getStatusCode(), e, Constants.LIMIT_SERVICE_GER_CALL_FAILURE_CODE);
        }

        return data;
    }

    private String checkIfDocTypeChanged(Integer curDocType, long userId, JSONObject context) {
        String additionalInfo = "";
        UserUploadLog data = getUserUploadLogData(userId);
        if (data != null && (curDocType != data.getDoc_type())) {
            changeLastUploadedDocType(curDocType, data.getId(), context);
            additionalInfo = additionalInfo + "KYC document type changed from "
                    + getDocTypeStringForCommentsDisplay(data.getDoc_type()) + " to "
                    + getDocTypeStringForCommentsDisplay(curDocType) + ". ";
        }
        return additionalInfo;
    }

    private void insertIntoLogTables(KYCData obj, int prevAccStatus, String additionalInfo, int fieldChanged,
            boolean verified, JSONObject context) {
        
        long playerUpdateId = puUtil.insertIntoPlayerUpdatesForKYCUpdate(obj,
                accServiceUtil.getAccStatusName(prevAccStatus), additionalInfo, fieldChanged, verified,
                obj.getPspdata(),context.getString(Constants.SOURCE));
        saveIdentityInfo(playerUpdateId, obj, context);
        pspCommentsUtil.savePSPComments(playerUpdateId, obj.getPspdata());
        if (prevAccStatus > Globals.KYC_STATUS && Globals.FIELD_UPDATE_KYC_VERIFIED == fieldChanged)
            blockUsersMappingDao.deleteUserMapping(obj.getUserId());
    }

    private UserUploadLog getUserUploadLogData(long userId) {
        UserUploadLog data = null;
        try {
            String url = config.getString("avs.get.userupload.log");
            url = url.replace("USERID", String.valueOf(userId));
            JSONArray res = avsClient.getCall(url);
            logger.debug("Response for USERUPLOAD LOG {}", res);
            if (res != null && res.length() > 0) {
                data = new GsonBuilder().setDateFormat("yyyy-MM-dd hh:mm:ss").create().fromJson(res.get(0).toString(),
                        UserUploadLog.class);
            } else if (res == null) {
                throw new ServiceException(Constants.USER_UPLOAD_LOG_ERROR,
                        Status.INTERNAL_SERVER_ERROR.getStatusCode(), Constants.USER_UPLOAD_LOG_FAILURE_CODE);
            }

        } catch (Exception e) {
            throw new ServiceException(Constants.USER_UPLOAD_LOG_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(), e,
                    Constants.USER_UPLOAD_LOG_FAILURE_CODE);
        }

        return data;
    }

    private void saveIdentityInfo(long playerUpdateId, KYCData obj, JSONObject context) {

        try {
            JSONObject jsonobj = new JSONObject();
            JSONObject value = new JSONObject();
            value.put(Constants.PLAYER_UPDATE_ID, playerUpdateId);
            value.put(Constants.UPDATED_BY, obj.getAgentId());
            value.put(Constants.ID_NO, (obj.getData() == null || obj.getData().getDocumentId() == null) ? null
                    : obj.getData().getDocumentId());
            value.put(Constants.ID_TYPE, obj.getDocType());
            value.put(Constants.USER_ID, obj.getUserId());
            jsonobj.put(Constants.VALUE, value);
            jsonobj.put(Constants.CONTEXT, context);
            logger.debug("saveIdentityInfo insert call BODY {}", jsonobj.toString());
            String resp = avsClient.postCall(config.getString("avs.insert.identityInfo.url"), jsonobj.toString(),
                    "POST");

            logger.debug("saveIdentityInfo insert RESPONSE BODY {}", resp);
            if (resp == null) {
                throw new ServiceException(Constants.IDENTITY_INFO_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                        Constants.IDENTITY_INFO_ERROR_FAILURE_CODE);
            }
        } catch (Exception e) {
            throw new ServiceException(Constants.IDENTITY_INFO_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(), e,
                    Constants.IDENTITY_INFO_ERROR_FAILURE_CODE);
        }

    }

    private IdkycStatusData getIDKycStatusData(Long userId, JSONObject context) {
        IdkycStatusData idkyc = null;
        try {
            String url = config.get("avs.get.ID.kyc.url");
            url = url.replace("USERID", String.valueOf(userId));
            logger.debug("URL ************* " + url);
            String result = avsClient.getStringResGetCall(url.trim());
            if (result != null) {
                idkyc = new GsonBuilder().setDateFormat("yyyy-MM-dd hh:mm:ss").create().fromJson(result,
                        IdkycStatusData.class);
                if (idkyc.getUser_id() == 0)
                    return null;
            }
            logger.debug("RESPONSE DATA FOR IDKYCSTATUS {}", idkyc);

        } catch (Exception e) {
            throw new ServiceException(Constants.ID_KYC_GET_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(), e,
                    Constants.ID_KYC_GET_CALL_FAILURE_CODE);
        }

        return idkyc;
    }

    private void sendKYCUpdateEmail(int kycStatus, String firstName, int userPrefLanguage, String email,
            double currDepositLimit, int channelId) {
        if (Globals.KYC_ACCEPTED == kycStatus) {
            cm.publishToCM(
                    cm.getKYCUpdateEmailMessage(firstName, userPrefLanguage, email, currDepositLimit, channelId));
        } else if (Globals.KYC_REJECTED_STATUS == kycStatus) {
            cm.publishToCM(cm.getKYCRejectEmailMessage(firstName, userPrefLanguage, email, channelId));
        }
    }

    private UserKYCData getUserData(KYCData obj) {
        UserKYCData user = null;
        try {
            String url = config.get("ums.user.kyc.url");
            url = url.replace("USERID", String.valueOf(obj.getUserId()));
            logger.debug("URL ************* " + url);
            String result = usClient.getStringResGetCall(url.trim());
            logger.debug("result ****** " + result);
            if (result != null) {
                user = new Gson().fromJson(result, UserKYCData.class);
            } else {
                throw new ServiceException(Constants.INVALID_USER, Status.INTERNAL_SERVER_ERROR,
                        MTTSettlementConstant.US_CALL_FAILURE_CODE);
            }
        } catch (Exception e) {
            throw new ServiceException(Constants.KYC_PROCESSING_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(), e,
                    MTTSettlementConstant.US_CALL_FAILURE_CODE);
        }

        return user;
    }

    private void insertIdfyData(Long userId, IdfyData idfyData, JSONObject context) {
        try {
            String jsonString = new Gson().toJson(idfyData);
            JSONObject obj = new JSONObject();
            obj.put(Constants.VALUE, new JSONObject(jsonString));
            obj.put(Constants.CONTEXT, context);
            String res = usClient.postCall(config.getString("ums.idfydata.url") + userId, obj.toString(), "POST");
            logger.debug("IDFY UPDATE CALL RESPONSE {}", res);
            if (res == null) {
                throw new ServiceException(Constants.IDFY_DB_CALL_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                        Constants.IDFY_DB_CALL_ERROR_CODE);
            }
        } catch (Exception e) {
            throw new ServiceException(Constants.IDFY_DB_CALL_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(), e,
                    Constants.IDFY_DB_CALL_ERROR_CODE);
        }

    }

    private void updateIDKYCStatus(IdkycStatusData idkycData, JSONObject context) {
        try {
            if (idkycData != null) {
                JSONObject obj = new JSONObject();
                JSONObject value = new JSONObject();
                value.put(Constants.ID_VERIFICATION_STATUS, idkycData.getId_verification_status());
                value.put(Constants.KYC_VERIFICATION_STATUS, idkycData.getKyc_verification_status());
                value.put(Constants.ID_VERIFIED, idkycData.getId_verified());
                value.put(Constants.KYC_VERIFIED, idkycData.getKyc_verified());
                value.put(Constants.USER_ID, idkycData.getUser_id());
                obj.put(Constants.VALUE, value);
                obj.put(Constants.CONTEXT, context);
                logger.debug("IDKYCSTATUS update call BODY {}", obj.toString());
                String resp = avsClient.postCall(config.getString("avs.update.idkycstaus.url"), obj.toString(), "POST");

                logger.debug("IDKYCSTATUS update RESPONSE BODY {}", resp);
                if (resp == null) {
                    throw new ServiceException(Constants.ID_KYC_UPDATE_ERROR,
                            Status.INTERNAL_SERVER_ERROR.getStatusCode(), Constants.ID_KYC_UPDATE_ERROR_FAILURE_CODE);
                }
            }

        } catch (Exception e) {
            throw new ServiceException(Constants.ID_KYC_UPDATE_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(), e,
                    Constants.ID_KYC_UPDATE_ERROR_FAILURE_CODE);
        }
    }

    private void changeLastUploadedDocType(Integer curDocType, long id, JSONObject context) {
        try {
            JSONObject obj = new JSONObject();
            JSONObject value = new JSONObject();
            value.put(Constants.DOC_TYPE, curDocType);
            value.put(Constants.ID, id);
            obj.put(Constants.VALUE, value);
            obj.put(Constants.CONTEXT, context);
            logger.debug("Changeuploadlog update call BODY {}", obj.toString());
            String resp = avsClient.postCall(config.getString("avs.update.useruploadlog.url"), obj.toString(), "POST");
            logger.debug("Changeuploadlog update RESPONSE BODY {}", resp);
            if (resp == null) {
                throw new ServiceException(Constants.USER_UPLOAD_UPDATE_LOG_ERROR,
                        Status.INTERNAL_SERVER_ERROR.getStatusCode(), Constants.USER_UPLOAD_UPDATE_LOG_FAILURE_CODE);
            }
        } catch (Exception e) {
            throw new ServiceException(Constants.USER_UPLOAD_UPDATE_LOG_ERROR,
                    Status.INTERNAL_SERVER_ERROR.getStatusCode(), e, Constants.USER_UPLOAD_UPDATE_LOG_FAILURE_CODE);
        }
    }

    private void sendSms(int kycStatus, UserKYCData user) {
        String smsString = "";
        if (Globals.KYC_STATUS == kycStatus) {
            if (!isOldUser(user)) {
                smsString = config.getString("NEW_KYC_DOC_ACCEPTED_SMS");
            } else {
                smsString = config.getString("KYC_DOC_ACCEPTED_SMS");
            }
        } else if (Globals.KYC_STATUS_REJECTED == kycStatus) {
            if (!isOldUser(user)) {
                smsString = config.getString("NEW_KYC_REJECTED");
            } else {
                smsString = config.getString("ONE_OR_BOTH_DOC_REJECTED_SMS");
            }

        }
        smsString = smsString.replaceAll("#name#",
                user.getFirstname() != null ? user.getFirstname() : user.getLoginid());
        smsString = smsString.replaceAll("#Verification_type#", "KYC Proof");
        smsString = smsString.replaceAll("#have#", "has");
        cm.publishToCM(cm.getKYCsmsMessage(user, smsString));
    }

    private DepositLimitData updateDepositLimit(long userId, int prevAccountStatus, String comment, JSONObject context,
            int accountStatus) {
        DepositLimitData data = null;
        try {
            JSONObject obj = new JSONObject();
            JSONObject value = new JSONObject();
            value.put(Constants.ACCOUNTSTATUS, accountStatus);
            value.put(Constants.COMMENT, comment);
            value.put(Constants.USERID, userId);
            value.put(Constants.DEPOSIT_LIMIT_REFERENCE, 13);
            value.put(Constants.PREV_ACCOUNT_STATUS, prevAccountStatus);
            obj.put(Constants.VALUE, value);
            obj.put(Constants.CONTEXT, context);
            logger.debug("Deposit Limit Body {}", obj.toString());
            String result = limitClient.postCall(config.getString("limitservice.post.deposit.limit.url"),
                    obj.toString(), "POST");
            logger.debug("Deposit Limit Response {}", result);
            if (result != null) {
                data = new GsonBuilder().setDateFormat("yyyy-MM-dd hh:mm:ss").create().fromJson(result,
                        DepositLimitData.class);
            }
        } catch (Exception e) {
            throw new ServiceException(Constants.UPDATE_LIMIT_SERVICE_DB_CALL_ERROR,
                    Status.INTERNAL_SERVER_ERROR.getStatusCode(), e, Constants.LIMIT_SERVICE_CALL_FAILURE_CODE);
        }
        return data;
    }

    private void updateUserStatus(long userId, Integer accStatus, int kycStatus, JSONObject context) {
        try {
            JSONObject obj = new JSONObject();
            JSONObject value = new JSONObject();
            if (accStatus != null)
                value.put(Constants.ACCOUNT_STATUS, accStatus);
            value.put(Constants.KYC_STATUS, kycStatus);
            obj.put(Constants.VALUE, value);
            obj.put(Constants.CONTEXT, context);
            String resp = usClient.postCall(config.getString("ums.user.update.url") + userId, obj.toString(), "PUT");
            logger.debug("response after userStatus update {}", resp);
            if (resp == null) {
                throw new ServiceException(Constants.KYC_UPDATE_DB_CALL_ERROR,
                        Status.INTERNAL_SERVER_ERROR.getStatusCode(), MTTSettlementConstant.US_CALL_FAILURE_CODE);
            }
        } catch (Exception e) {
            throw new ServiceException(Constants.KYC_UPDATE_DB_CALL_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                    e, MTTSettlementConstant.US_CALL_FAILURE_CODE);
        }

    }

    private String getDocTypeStringForCommentsDisplay(int docType) {

        switch (docType) {
        case Globals.PAN_CARD:
            return "PAN Card";
        case Globals.PASSPORT:
            return "Indian Passport";
        case Globals.DRIVING_LICENCE:
            return "Driving License";
        case Globals.VOTER_ID:
            return "Voter ID-Card";
        case Globals.AADHAR_CARD:
            return "Aadhar Card";
        case Globals.RATION_CARD:
            return "Ration Card";
        case Globals.LAST_3_MNTHS_BANK_STMT:
            return "Last 3 Months Bank Statement";
        case Globals.OTHERS:
            return "Others";
        case Globals.Elec_bill_N_PAN:
            return "Electricity Bill & PAN Card";
        case Globals.PHONE_BILL_N_PAN:
            return "Telephone/Mobile Bill & PAN Card";
        case Globals.BANK_ACCOUNT_STMNT_N_PAN:
            return "Bank Account Statement & PAN Card";
        case Globals.BANK_PASSBOOK_n_PAN:
            return "Bank Passbook & PAN Card";
        case Globals.RATION_N_PAN_CARD:
            return "Ration Card & PAN Card";
        default:
            return "Others";
        }

    }

    private void sendMessageToNotifier(String notifiermessage, Long userId) {
        try {
            notifier.publishToNotifier(notifier.getNotifierMQMessage(notifiermessage, userId));
        } catch (Exception e) {
            logger.error("ERROR while sendMessageToNotifier {}", e);
        }

    }

    private String getNotifierObj(KYCData obj, UserKYCData user, DepositLimitData depositData, int userPreferedLanguage,
            boolean kycVerified) {
        JSONObject notifierObj = new JSONObject();
        notifierObj.put(Constants.USERID, obj.getUserId());
        notifierObj.put(Constants.EMAIL, user.getEmail());
        notifierObj.put(Constants.FIRST_NAME, user.getFirstname());
        notifierObj.put(Constants.VERIFICATION_FOR, 3);
        notifierObj.put(Constants.ID_ACCEPT_STATUS, kycVerified ? true : false);
        notifierObj.put(Constants.KYC_ACCEPT_STATUS, kycVerified ? true : false);
        notifierObj.put(Constants.PREV_STATUS, user.getAccount_status());
        notifierObj.put(Constants.RPS_ID_VERIFICATION, 0);
        notifierObj.put(Constants.RPS_KYC_VERIFICATION, 0);
        notifierObj.put(Constants.PREV_DEPOSIT_LIMIT, depositData.getPrevMaxLimit());
        notifierObj.put(Constants.CURRENT_DEPOSIT_LIMIT, depositData.getMaxLimit());
        notifierObj.put(Constants.REASON_FOR_KYCREJECTION, obj.getStatusComment());
        notifierObj.put(Constants.IDVERIFIED, kycVerified ? 1 : 0);
        notifierObj.put(Constants.USER_PREFERED_LANGUAGE, userPreferedLanguage);
        notifierObj.put(Constants.REJECTION_TYPE, kycVerified ? Constants.SUCCESS : "PSP-Generic");
        logger.debug("Message to notifier {} ", notifierObj.toString());
        return notifierObj.toString();
    }

}
