package com.games24x7.pspbeckend.Util;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.mq.CMmqFactory;
import com.games24x7.pspbeckend.pojo.KnownDevice;
import com.games24x7.pspbeckend.webclient.UserServiceClient;

@Component
public class KnownDevicesUtil {
    private static final Logger logger = LoggerFactory.getLogger(KnownDevicesUtil.class);

    @Autowired
    Configuration config;

    @Autowired
    CMmqFactory mq;
    @Autowired
    UserServiceClient usClient;

    public KnownDevice getObject(String payload) {
        KnownDevice knownDevice = null;
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        try {
            knownDevice = mapper.readValue(payload, KnownDevice.class);
        } catch (Exception ex) {
            throw new ServiceException("Bad Payload", Status.BAD_REQUEST.getStatusCode());
        }
        return knownDevice;
    }
    public void basicValidations(KnownDevice knownDevice) {
        logger.debug("Validating knownDevice object "+knownDevice);
        agentValidation(knownDevice.getAgentName(), knownDevice.getAgentId());
        actionValidation(knownDevice.getAction());
        userValidation(knownDevice.getUserId());
        deviceValidation(knownDevice.getDeviceId());
    }
    
    private void agentValidation(String agentName, Long agentId) {
        if (agentName == null || agentName.trim().length() <= 0 || "".equalsIgnoreCase(agentName)) {
            throw new ServiceException("AgentName is missing.", Status.BAD_REQUEST);

        }
        if (agentId == null || agentId <= 0) {
            throw new ServiceException("AgentId mandatory  & can not be 0.", Status.BAD_REQUEST);
        }
    }

    private void actionValidation(String action) {
        if (action == null || action.trim().length() <= 0 || "".equalsIgnoreCase(action)) {
            throw new ServiceException("Action is missing.", Status.BAD_REQUEST);

        }
    }

    private void userValidation(Long userId) {
        if (userId== null || userId <= 0 ) {
            throw new ServiceException("UserId is missing.", Status.BAD_REQUEST);
        }
    }

    private void deviceValidation(String deviceId) {
        if (deviceId == null || deviceId.trim().length() <= 0 || "".equalsIgnoreCase(deviceId)) {
            throw new ServiceException("Device id is missing.", Status.BAD_REQUEST);

        }
    }

    public String getListURL(long userId) {
        return config.get(Constants.USER_SERVICE_URL)+Constants.KNOWN_DEVICES_URL_PATH+userId;
    }
    
    public String authenticateDeviceURL(long userId) {
        return config.get(Constants.USER_SERVICE_URL)+Constants.KNOWN_DEVICES_URL_PATH+Constants.KNOWN_DEVICES_APPROVAL_URL_PATH+userId;
    }

    public JSONObject getKnownDevicesRequestBody(String deviceId, int channelId, long agentId) {
        JSONObject value = new JSONObject();
        value.put("channel_id", channelId);
        value.put("deviceId", deviceId);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("value", value);
        jsonObject.put("context", getContext(agentId));
        return jsonObject;
    }
    
    private JSONObject getContext(long agentId) {
        JSONObject context = new JSONObject();
        context.put("source", "psp");
        context.put("agentId", agentId);
        return context;
    }

    public String removeDevice(long userId) {
        return config.get(Constants.USER_SERVICE_URL)+Constants.KNOWN_DEVICES_URL_PATH+userId;
    }
}
