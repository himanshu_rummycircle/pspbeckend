package com.games24x7.pspbeckend.Util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Globals;
import com.games24x7.pspbeckend.pojo.CloseAndMergeData;
import com.games24x7.pspbeckend.pojo.MergeAccount;
import com.sun.jersey.core.header.FormDataContentDisposition;

@Component
public class MergeAndCloseUtil {
    private static final Logger logger = LoggerFactory.getLogger(MergeAndCloseUtil.class);
    @Autowired
    Configuration config;
    @Autowired
    FileValidationsUtil fileValidation;

    @Autowired
    AccountServiceUtil accServiceUtil;

    public CloseAndMergeData mergeDataGeneration(FormDataContentDisposition fileDetail, String agentName,
            long agentId) {

        String uploadedFileLocation = config.get(Constants.FILE_LOCATION) + fileDetail.getFileName();
        java.nio.file.Path pathToFile = Paths.get(uploadedFileLocation);

        CloseAndMergeData mergeData = new CloseAndMergeData();
        Map<Long, Long> userMap = new HashMap<>();
        Map<Long, MergeAccount> userMergeAccMap = new HashMap<>();
        Map<Long, MergeAccount> touserMergeAccMap = new HashMap<>();

        String line = "";
        String cvsSplitBy = Constants.CSV_SPLIT_BY;
        try (BufferedReader br = Files.newBufferedReader(pathToFile)) {
            int i = 0;
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] data = line.split(cvsSplitBy);

                i++;

                if (i != 1) {

                    long fromUid = Long.parseLong(data[0]);
                    long toUid = Long.parseLong(data[1]);
                    String comment = data[2];
                    MergeAccount account = new MergeAccount(fromUid, toUid, comment);
                    userMap.put(fromUid, toUid);
                    if (!userMergeAccMap.containsKey(fromUid)) {
                        userMergeAccMap.put(fromUid, account);
                    } else {
                        throw new ServiceException("UserId " + fromUid + " is present in "
                                + Constants.FROM_USERID_HEADER + " multiple times in the file .", Status.BAD_REQUEST);
                    }

                    if (!touserMergeAccMap.containsKey(toUid)) {
                        touserMergeAccMap.put(toUid, account);
                    } else {
                        throw new ServiceException("UserId " + toUid + " is present in " + Constants.TO_USERID_HEADER
                                + " multiple times in the file.", Status.BAD_REQUEST);
                    }

                }
                if (i > config.getInt(Constants.ALLOWED_ENTRY)) {
                    throw new ServiceException("File has more entries than allowed.", Status.BAD_REQUEST);
                }

            }
            mergeData.setUserMap(userMap);
            mergeData.setFromUserMergeAccMap(userMergeAccMap);
            mergeData.setAgent(agentName);
            mergeData.setAgentId(agentId);
            mergeData.setFileName(fileDetail.getFileName());
            mergeData.setToUserMergeAccMap(touserMergeAccMap);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException("Some Error with File Data ", Status.INTERNAL_SERVER_ERROR, e);
        }
        return mergeData;

    }

    public void basicValidations(String agentName, FormDataContentDisposition fileDetail,
            InputStream uploadedInputStream, Long agentId) {

        fileValidation.agentInputValidation(agentName, agentId);
        fileValidation.fileValidation(fileDetail, uploadedInputStream);
        fileValidation.validateFileHeader(fileDetail, Globals.CLOSE_AND_MERGE);

    }

    public void mergeAccValidations(CloseAndMergeData data) {

        if ( data.getFromUserMergeAccMap().keySet().isEmpty()) {
            throw new ServiceException("File has no entries to process.", Status.BAD_REQUEST);
        }

        accServiceUtil.validateUserExistance(data.getToAccountList());
        accServiceUtil.validateUserExistance(data.getFromAccountList());

        JSONArray accountstatus = accServiceUtil.validateAccountStatus(data.getToAccountList(),
                config.get(Constants.ACC_STATUS));
        if (accountstatus != null) {
            List<Long> list = new ArrayList<>();
            for (int i = 0; i < accountstatus.length(); i++) {
                list.add(accountstatus.getJSONObject(i).getLong(Constants.USER_ID));
            }
            throw new ServiceException("These users account are already closed or blocked . " + list.toString(),
                    Status.BAD_REQUEST);
        }

    }

}
