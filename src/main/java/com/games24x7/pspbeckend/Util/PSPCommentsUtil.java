package com.games24x7.pspbeckend.Util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.pspbeckend.dao.PSPCommentsDao;
import com.games24x7.pspbeckend.pojo.PSPComments;

@Component
public class PSPCommentsUtil {
    private static final Logger logger = LoggerFactory.getLogger(PSPCommentsUtil.class);

    @Autowired
    PSPCommentsDao pspCommentsDao;

    public void savePSPComments(long playerUpdateId, PSPComments pspComments) {
        if (pspComments == null) {
            pspComments = new PSPComments();
        }
        pspComments.setP_id(playerUpdateId);
        pspCommentsDao.insertIntoPspComments(pspComments);
    }
}
