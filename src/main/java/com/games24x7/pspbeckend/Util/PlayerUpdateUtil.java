package com.games24x7.pspbeckend.Util;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.audit.entities.EventData;
import com.games24x7.frameworks.config.Configuration;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Globals;
import com.games24x7.pspbeckend.dao.PlayerUpdatesDAO;
import com.games24x7.pspbeckend.pojo.BlockAccountStatusData;
import com.games24x7.pspbeckend.pojo.BulkStatusChangeData;
import com.games24x7.pspbeckend.pojo.CloseAccountStatusData;
import com.games24x7.pspbeckend.pojo.CloseAndMergeData;
import com.games24x7.pspbeckend.pojo.CloseAndRefundData;
import com.games24x7.pspbeckend.pojo.KYCData;
import com.games24x7.pspbeckend.pojo.PSPComments;
import com.games24x7.pspbeckend.pojo.PlayerUpdates;
import com.games24x7.pspbeckend.pojo.UnblockUserData;

@Component
public class PlayerUpdateUtil {
    private static final Logger logger = LoggerFactory.getLogger(PlayerUpdateUtil.class);
    @Autowired
    Configuration config;
    @Autowired
    ThreadPoolExecutorUtil util;

    @Autowired
    PlayerUpdatesDAO dao;

    @Autowired
    AuditUtil auditUtil;

    public void insertIntoPlayerUpdates(Object obj, int action) {

        List<PlayerUpdates> playerUpdatesList = getPlayerUpdatesList(obj, action);
        logger.debug("playerUpdatesList *** " + playerUpdatesList);
        if (playerUpdatesList != null) {
            int targetSize = config.getInt(Constants.PLAYER_UPDATE_BATCH_SIZE, 100);
            List<List<PlayerUpdates>> output = ListUtils.partition(playerUpdatesList, targetSize);
            output.forEach(playeUpdate -> {
                insertPlayerUpdates(playeUpdate);
                auditUtil.playerUpdateAuditLog(playeUpdate);
            });
        }
    }

    /**
     * @param obj
     * @param action
     * @return
     */
    private List<PlayerUpdates> getPlayerUpdatesList(Object obj, int action) {
        List<PlayerUpdates> playerUpdatesList = null;
        switch (action) {
        case Globals.ACCOUNT_CLOSED_PLAYER_UPDATES:
            playerUpdatesList = getPlayerUpdateListForAccountClose((CloseAndMergeData) obj);
            break;
        case Globals.ACCOUNT_MERGE_PLAYER_UPDATES:
            playerUpdatesList = getPlayerUpdateListForMergeAcc((CloseAndMergeData) obj);
            break;
        case Globals.CLOSE_AND_REFUND:
            playerUpdatesList = getPlayerUpdateListForCloseAndRefund((CloseAndRefundData) obj);
            break;
        case Globals.BULK_ACCOUNT_STATUS_CHANGE:
            playerUpdatesList = getPlayerUpdateListForBulkAccountSatatus((BulkStatusChangeData) obj);
            break;
        default:
            break;
        }
        return playerUpdatesList;

    }

    /**
     * @param obj
     * @return
     */
    private List<PlayerUpdates> getPlayerUpdateListForBulkAccountSatatus(BulkStatusChangeData obj) {
        List<PlayerUpdates> playerUpdatesList = new ArrayList<>();
        obj.getUserData().forEach(refund -> {
            String additionalInfo = "Kindly check your email for further information. ";
            playerUpdatesList.add(getPlayerUpdates(obj.getAgentId(), refund.getComments(),
                    Globals.ACCOUNT_STATUS_BLOCKED, refund.getUserId(), String.valueOf(refund.getAccountStatus()),
                    new Timestamp(System.currentTimeMillis()), additionalInfo));
        });
        return playerUpdatesList;
    }

    private List<PlayerUpdates> getPlayerUpdateListForCloseAndRefund(CloseAndRefundData obj) {
        List<PlayerUpdates> playerUpdatesList = new ArrayList<>();
        obj.getRefundList().forEach(refund -> {
            String additionalInfo = "Bulk Account Close and Refund by PSP ";
            playerUpdatesList
                    .add(getPlayerUpdates(obj.getAgentId(), refund.getComments(), Globals.ACCOUNT_CLOSED_PLAYER_UPDATES,
                            refund.getUserId(), String.valueOf(refund.getAccountStatus()),
                            new Timestamp(System.currentTimeMillis()), additionalInfo));
        });
        return playerUpdatesList;
    }

    private List<PlayerUpdates> getPlayerUpdateListForMergeAcc(CloseAndMergeData accMain) {
        List<PlayerUpdates> playerUpdatesList = new ArrayList<>();

        accMain.getSucessfulMergeAcc().forEach(mergeAcc -> {
            String comment = "UserId " + mergeAcc.getFromUserId() + " is merged with " + mergeAcc.getToUserId()
                    + " By Agent " + accMain.getAgent();
            String additionalInfo = "Bulk Account Merging by PSP ";
            String previousData = "deposit :" + mergeAcc.getDeposit() + ":withdraw :" + mergeAcc.getWithdrawable();
            playerUpdatesList.add(getPlayerUpdates(accMain.getAgentId(), comment, Globals.ACCOUNT_MERGE_PLAYER_UPDATES,
                    mergeAcc.getFromUserId(), previousData + " was debited", new Timestamp(System.currentTimeMillis()),
                    additionalInfo));

            playerUpdatesList.add(getPlayerUpdates(accMain.getAgentId(), comment, Globals.ACCOUNT_MERGE_PLAYER_UPDATES,
                    mergeAcc.getToUserId(), previousData + " was credited", new Timestamp(System.currentTimeMillis()),
                    additionalInfo));
        });
        return playerUpdatesList;
    }

    private void insertPlayerUpdates(List<PlayerUpdates> playeUpdate) {

        util.getPlayerUpdateThreadpoolexecutor().execute(() -> {
            try {
                logger.debug("Thread name :: " + Thread.currentThread().getName());
                dao.insertIntoPlayerUpdatesDB(playeUpdate);
            } catch (Exception e) {
                logger.error("Exception while inserting PlayerUpdates", e);
            }

        });

    }

    private List<PlayerUpdates> getPlayerUpdateListForAccountClose(CloseAndMergeData data) {
        List<PlayerUpdates> playerUpdatesList = new ArrayList<>();

        data.getMergeAccData().forEach(mergeAcc -> {
            logger.debug(" Lambda merge " + mergeAcc.getFromUserId() + " : To :: " + mergeAcc.getToUserId());
            String comment = "UserId " + mergeAcc.getFromUserId() + " is merged with " + mergeAcc.getToUserId()
                    + " By Agent " + data.getAgent();
            String additionalInfo = "Bulk Account Merging by PSP ";
            playerUpdatesList.add(getPlayerUpdates(data.getAgentId(), comment, Globals.ACCOUNT_CLOSED_PLAYER_UPDATES,
                    mergeAcc.getFromUserId(), String.valueOf(mergeAcc.getAccountStatus()),
                    new Timestamp(System.currentTimeMillis()), additionalInfo));
        });
        return playerUpdatesList;
    }

    private PlayerUpdates getPlayerUpdates(long agentId, String comment, int fieldUpdated, long playerUserId,
            String previousData, Timestamp updateDate, String additionalInfo) {
        return new PlayerUpdates(playerUserId, agentId, fieldUpdated, previousData, comment, updateDate, additionalInfo,
                0);

    }

    public long insertIntoPlayerUpdatesForKYCUpdate(KYCData obj, String previousData, String additionalInfo,
            int fieldUpdated, boolean verified, PSPComments pspComments, String source) {
        PlayerUpdates plud = getPlayerUpdates(obj.getAgentId(), obj.getStatusComment(), fieldUpdated, obj.getUserId(),
                previousData, new Timestamp(System.currentTimeMillis()), additionalInfo);

        String docId = (obj.getData() == null || obj.getData().getDocumentId() == null) ? null
                : obj.getData().getDocumentId();
        return insertIntoPlayerUpdatesAndPerformAudit(plud, obj.getDocType(), docId, verified,
                pspComments,source);

    }

    private long insertIntoPlayerUpdatesAndPerformAudit(PlayerUpdates plud, Integer docType, String docId,
            Boolean verified, PSPComments pspComments, String source) {
        long playerUpdateId = dao.insertIntoPlayerUpdatesDB(plud);
        util.getPlayerUpdateThreadpoolexecutor().execute(() -> {
            EventData event = auditUtil.getPlayerUpdateEvent(plud, docType, docId, verified, pspComments,source);
            auditUtil.sendCustomAuditMessageToKafka(event);
        });
        return playerUpdateId;
    }

    public long insertIntoPlayerUpddatesForBlockUser(BlockAccountStatusData obj, String prevAccStatus,
            PSPComments pspComments, String source) {

        PlayerUpdates pludate = getPlayerUpdates(obj.getAgentId(), obj.getComment(), Globals.ACCOUNT_STATUS_BLOCKED,
                obj.getUserId(), prevAccStatus, new Timestamp(System.currentTimeMillis()), "N/A");
        return insertIntoPlayerUpdatesAndPerformAudit(pludate, null, null, null, pspComments,source);

    }

    public long insertIntoPlayerUpddatesForBlockUserHeaderData(long userId, long agentId, String comment, String header,
            String prevAccStatus, PSPComments pspComments, String source) {

        PlayerUpdates plud = getPlayerUpdates(agentId, comment, Globals.ACCOUNT_STATUS_BLOCKED_HEADER_CHANGE, userId,
                header, new Timestamp(System.currentTimeMillis()), "N/A");
        return insertIntoPlayerUpdatesAndPerformAudit(plud, null, null, null, pspComments,source);
    }

    public long insertIntoPlayerUpddatesForCloseUser(CloseAccountStatusData obj, String prevAccStatus,
            PSPComments pspComments,String source) {
        PlayerUpdates pludate = getPlayerUpdates(obj.getAgentId(), obj.getComment(),
                Globals.ACCOUNT_STATUS_CHANGED_TO_CLOSED, obj.getUserId(), prevAccStatus,
                new Timestamp(System.currentTimeMillis()), "N/A");
        return insertIntoPlayerUpdatesAndPerformAudit(pludate, null, null, null, pspComments,source);
    }

    public long insertIntoPlayerUpddatesForUnBlock(UnblockUserData obj, String prevAccStatus, PSPComments pspComments,String source) {
        PlayerUpdates pludate = null;
        switch (obj.getStatus()) {
        case 1:
            pludate = getPlayerUpdates(obj.getAgentId(), obj.getComment(), Globals.ACCOUNT_STATUS_CHANGED_TO_NEW,
                    obj.getUserId(), prevAccStatus, new Timestamp(System.currentTimeMillis()), "N/A");
            break;
        case 2:
            pludate = getPlayerUpdates(obj.getAgentId(), obj.getComment(),
                    Globals.ACCOUNT_STATUS_CHANGED_TO_PHONE_VERIFIED, obj.getUserId(), prevAccStatus,
                    new Timestamp(System.currentTimeMillis()), "N/A");
            break;
        case 4:
            pludate = getPlayerUpdates(obj.getAgentId(), obj.getComment(),
                    Globals.ACCOUNT_STATUS_CHANGED_TO_KYC_VERIFIED, obj.getUserId(), prevAccStatus,
                    new Timestamp(System.currentTimeMillis()), "N/A");
            break;

        default:
            logger.debug("UNBLOCKING for Staus {} is not allowed ", obj.getStatus());
            break;
        }

        if (pludate == null) {
            return 0;
        }

        return insertIntoPlayerUpdatesAndPerformAudit(pludate, null, null, null, pspComments,source);
    }

}
