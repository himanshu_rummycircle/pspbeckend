package com.games24x7.pspbeckend.Util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import javax.annotation.Resource;
import javax.ws.rs.core.Response.Status;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.frameworks.serviceutil.httpclient.CustomHTTPClient;
import com.games24x7.frameworks.serviceutil.httpclient.Retriable;
import com.games24x7.frameworks.serviceutil.httpclient.RetryConfig;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Globals;
import com.games24x7.pspbeckend.pojo.WebResponse;

@Component
public class RestCoreUtil {
    private static final Logger logger = LoggerFactory.getLogger(RestCoreUtil.class);
    @Autowired
    Configuration config;

    @Resource(name = "pspbehttpclient")
    CloseableHttpClient httpClient;

    @Autowired
    private CustomHTTPClient customHttpClient;
    private static final String WAS_NOT_EXECUTED = " was not executed .";
    private static final String EXCEPTION_IN_REST_CALL = " Exception in rest call for :";
    private static final String COUNT = " count :";

    public String getCall(String resturl) {
        int count = Globals.ZERO;
        int maxTries = config.getInt(Constants.MAX_RETRIES);

        String res = null;

        while (count < maxTries) {
            try {

                URL url = new URL(resturl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setConnectTimeout(10000);
                conn.setRequestMethod("GET");
                logger.debug("Response Code : " + conn.getResponseCode());
                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    logger.debug("HTTP_OK :" + count + " : " + maxTries);
                    res = getResponse(conn);
                    closeConnection(conn);
                    return res;
                } else if (conn.getResponseCode() == HttpURLConnection.HTTP_NOT_FOUND) {
                    logger.debug("HTTP_NOT_FOUND :" + count + " : " + maxTries);
                    closeConnection(conn);
                    throw new ServiceException(resturl + WAS_NOT_EXECUTED, Status.NOT_FOUND);
                }

                count++;
                if (count == maxTries) {
                    closeConnection(conn);
                    throw new ServiceException(resturl + WAS_NOT_EXECUTED, Status.INTERNAL_SERVER_ERROR);
                }
                closeConnection(conn);
                logger.debug("count1 :" + count + " : " + maxTries);

            } catch (ServiceException e) {
                logger.info(EXCEPTION_IN_REST_CALL + resturl + COUNT + count + e);
                throw e;
            } catch (Exception e) {
                logger.info(EXCEPTION_IN_REST_CALL + resturl + COUNT + count + e);
                throw new ServiceException(resturl + WAS_NOT_EXECUTED, Status.INTERNAL_SERVER_ERROR);
            }
        }
        return res;

    }

    /**
     * Method for a GET call to a REST service.
     * 
     * @param uri
     * @return
     */
    public WebResponse callGet(String uri, RetryConfig retryConfig) throws Exception {
        HttpGet request = new HttpGet(uri);
        return executeHttpRequest(request, retryConfig);
    }

    public WebResponse callPost(String uri, JSONObject payload, RetryConfig retryConfig) throws Exception {
        HttpPost request = new HttpPost(uri);
        StringEntity entity = new StringEntity(payload.toString(), ContentType.APPLICATION_JSON);
        request.setEntity(entity);
        return executeHttpRequest(request, retryConfig);
    }

    public WebResponse callPut(String uri, JSONObject payload, RetryConfig retryConfig) throws Exception {
        HttpPut request = new HttpPut(uri);
        StringEntity entity = new StringEntity(payload.toString(), ContentType.APPLICATION_JSON);
        request.setEntity(entity);
        return executeHttpRequest(request, retryConfig);
    }

    /**
     * Method for executing any HTTP method, converting HTTP response to POJO
     * and returning it. Uses CloseableHttpResponse client. Creates new client
     * for every request. Internally closes both client and response before
     * returning.
     * 
     * @param request
     * @return
     */
    public WebResponse executeHttpRequest(final HttpUriRequest request, RetryConfig retryConfig) throws Exception {
        WebResponse webResponse = new WebResponse();
        CloseableHttpResponse httpRes = null;
        try {
            httpRes = (CloseableHttpResponse) customHttpClient.executeRequest(retryConfig, new Retriable() {
                public Object execute(Map<String, String> headers) throws ClientProtocolException, IOException {
                    for (Map.Entry<String, String> header : headers.entrySet()) {
                        request.addHeader(header.getKey(), header.getValue());
                    }
                    return httpClient.execute(request);
                }
            });
            if (httpRes != null) {
                webResponse.setStatusCode(httpRes.getStatusLine().getStatusCode());
                webResponse.setPayload(EntityUtils.toString(httpRes.getEntity()));
            }
        } finally {
            if (httpRes != null) {
                try {
                    httpRes.close();
                } catch (Exception e) {
                    logger.error("Exception in executeHttpRequest finally ", e);
                }
            }
        }
        return webResponse;
    }

    /**
     * @param conn
     */
    private void closeConnection(HttpURLConnection conn) {
        if (conn != null) {
            conn.disconnect();
        }
    }

    /**
     * @param conn
     * @return result
     * @throws Exception
     */
    private String getResponse(HttpURLConnection conn) {
        String resp = null;
        try (BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));) {

            StringBuilder sb = new StringBuilder("");
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                sb.append(inputLine);
            }

            resp = sb.toString();
            logger.debug("Response from call : " + resp);
        } catch (Exception e) {
            throw new ServiceException("Rest call Response Exception ", Status.INTERNAL_SERVER_ERROR, e);
        }
        return resp;
    }

    public String postCall(String resturl, String body, String requestMethod) {
        String resp = null;
        int count = Globals.ZERO;
        int maxTries = config.getInt(Constants.MAX_RETRIES);
        while (count < maxTries) {
            try {
                URL url = new URL(resturl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setConnectTimeout(10000);
                conn.setDoOutput(true);
                conn.setRequestMethod(requestMethod);
                conn.setRequestProperty("Content-Type", "application/json");

                OutputStream os = conn.getOutputStream();
                os.write(body.getBytes());
                os.flush();
                int responseCode = conn.getResponseCode();
                logger.debug("POST Response Code :: " + responseCode);
                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK
                        || conn.getResponseCode() == HttpURLConnection.HTTP_NO_CONTENT) {
                    logger.debug("HTTP_OK :" + count + " : " + maxTries);
                    resp = getResponse(conn);
                    closeConnection(conn);
                    return resp;

                }
                count++;
                if (count == maxTries) {
                    closeConnection(conn);
                    logger.debug("last retry " + resturl + " count :" + count);
                    throw new ServiceException(resturl + " was not executed last retry .", Status.INTERNAL_SERVER_ERROR,
                            responseCode);
                }

                closeConnection(conn);
                logger.debug("count1 :" + count + " : " + maxTries);

            } catch (ServiceException e) {
                logger.info("Service exception catched in rest call for :" + resturl + " count :" + count + e);
                throw new ServiceException(resturl + " was not executed last retry throwing .",
                        Status.INTERNAL_SERVER_ERROR, e.getBusinessErrorCode());
            } catch (Exception e) {
                logger.info("Exception in rest call for :" + resturl + " count :" + count + e);
                throw new ServiceException(resturl + " was not executed .", Status.INTERNAL_SERVER_ERROR);
            }
        }
        return resp;
    }

    public String deleteCall(String resturl, String body) {
        String resp = null;
        int count = Globals.ZERO;
        int maxTries = config.getInt(Constants.MAX_RETRIES);
        while (count < maxTries) {
            try {
                URL url = new URL(resturl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setConnectTimeout(10000);
                conn.setDoOutput(true);
                conn.setRequestMethod("DELETE");
                conn.setRequestProperty("Content-Type", "application/json");

                OutputStream os = conn.getOutputStream();
                os.write(body.getBytes());
                os.flush();
                int responseCode = conn.getResponseCode();
                logger.debug("POST Response Code :: " + responseCode);
                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    logger.debug("HTTP_OK :" + count + " : " + maxTries);
                    resp = getResponse(conn);
                    closeConnection(conn);
                }
                count++;
                if (count == maxTries) {
                    closeConnection(conn);
                    logger.debug("last retry " + resturl + COUNT + count);
                    throw new ServiceException(resturl + " was not executed last retry .", Status.INTERNAL_SERVER_ERROR,
                            responseCode);
                }

                closeConnection(conn);
                logger.debug("count1 :" + count + " : " + maxTries);

            } catch (ServiceException e) {
                logger.info("Service exception catched in rest call for :" + resturl + COUNT + count + e);
                throw new ServiceException(resturl + " was not executed last retry throwing .",
                        Status.INTERNAL_SERVER_ERROR, e.getBusinessErrorCode());
            } catch (Exception e) {
                logger.info(EXCEPTION_IN_REST_CALL + resturl + " count :" + count + e);
                throw new ServiceException(resturl + WAS_NOT_EXECUTED, Status.INTERNAL_SERVER_ERROR);
            }
        }
        return resp;
    }

}
