package com.games24x7.pspbeckend.Util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.RiskConstant;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;
import com.games24x7.pspbeckend.dao.RiskCertificateDAO;
import com.games24x7.pspbeckend.pojo.PlayersRiskCertificate;
import com.games24x7.pspbeckend.pojo.RiskCertificate;
import com.games24x7.pspbeckend.pojo.UpdateRiskCertificate;
import com.games24x7.pspbeckend.webclient.UserServiceClient;

@Component
public class RiskCertificateHelper {

    private static final Logger logger = LoggerFactory.getLogger(RiskCertificateHelper.class);

    @Autowired
    RiskCertificateDAO ceriticateDAO;

    @Autowired
    Configuration config;

    @Autowired
    UserServiceClient userServiceClient;

    public RiskCertificate jsonToRiskCertificatePayload(JSONObject jsonPayload) {

        if (jsonPayload.length() == 0) {
            throw new ServiceException(WebConstant.NO_PAYLOAD, Status.BAD_REQUEST, WebConstant.EMPTY_OR_NULL_PAYLOAD);
        }
        RiskCertificate riskCertificate = null;
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        try {
            riskCertificate = mapper.readValue(jsonPayload.toString(), RiskCertificate.class);
        } catch (Exception jse) {
            throw new ServiceException(WebConstant.IMPROPER_INPUT_PAYLOAD, Status.BAD_REQUEST, jse,
                    WebConstant.IMPROPER_INPUT_PAYLOAD_CODE);
        }
        return riskCertificate;

    }

    public void validateCreateInputPayload(RiskCertificate riskPayload) {
        validateRiskCertificateName(riskPayload);
        validateRiskStatus(riskPayload.getRiskStatus());
        validateAgent(riskPayload.getAgentId());
        riskPayload.setActive(true);
    }

    public void validateRiskStatusComment(String riskStatusComment) {
        if ((riskStatusComment == null) || (riskStatusComment != null && riskStatusComment.isEmpty())) {
            throw new ServiceException(RiskConstant.EMPTY_RISK_STATUS_COMMENT_VALUE, Status.BAD_REQUEST,
                    RiskConstant.EMPTY_RISK_STATUS_COMMENT_VALUE_CODE);
        }
    }

    public void validateRiskStatus(String riskStatus) {
        if ((riskStatus == null) || (riskStatus != null && riskStatus.isEmpty())) {
            throw new ServiceException(RiskConstant.EMPTY_RISK_STATUS_VALUE, Status.BAD_REQUEST,
                    RiskConstant.EMPTY_RISK_STATUS_VALUE_CODE);
        }
        if (!(ifValidRiskStatusValue(riskStatus))) {
            throw new ServiceException(RiskConstant.INVALID_RISK_STATUS_VALUE, Status.BAD_REQUEST,
                    RiskConstant.INVALID_RISK_STATUS_VALUE_CODE);
        }
    }

    boolean ifValidRiskStatusValue(String riskStatus) {
        try {
            if (ceriticateDAO.checkIfValidRiskStatus(riskStatus)) {
                return true;
            }
        } catch (Exception e) {
            logger.error("Exception while validating the risk status , Error {}", e);
            throw new ServiceException(RiskConstant.DATABASE_OPERATION_ERROR, Status.BAD_REQUEST, e,
                    RiskConstant.DATABASE_OPERATION_ERROR_CODE);
        }
        return false;
    }

    public void validateRiskCertificateName(RiskCertificate payload) {

        if ((payload.getRiskCertificateName() == null)
                || (payload.getRiskCertificateName() != null && payload.getRiskCertificateName().isEmpty())) {
            throw new ServiceException(RiskConstant.EMPTY_CERTIFICATE_NAME, Status.BAD_REQUEST,
                    RiskConstant.EMPTY_CERTIFICATE_NAME_CODE);
        }
        String trimmedName = payload.getRiskCertificateName().replaceAll("\\s+", "");
        if ((trimmedName.length() > RiskConstant.HUNDRED)) {
            throw new ServiceException(RiskConstant.MAX_CHAR_LIMIT_EXCEEDED, Status.BAD_REQUEST,
                    RiskConstant.MAX_CHAR_LIMIT_EXCEEDED_CODE);
        }

    }

    public void validateAgent(String agentId) {
        if ((agentId == null) || (agentId != null && agentId.isEmpty())) {
            throw new ServiceException(RiskConstant.MISSING_AGENT, Status.BAD_REQUEST, RiskConstant.MISSING_AGENT_CODE);
        }
    }

    public void validateAgentGroup(RiskCertificate payload) {
        if ((payload.getAgentGroup() == null)
                || (payload.getAgentGroup() != null && payload.getAgentGroup().isEmpty())) {
            throw new ServiceException(RiskConstant.MISSING_AGENT_GROUP, Status.BAD_REQUEST,
                    RiskConstant.MISSING_AGENT_GROUP_CODE);
        }
    }

    public JSONArray getRiskListInJSONArray(List<RiskCertificate> riskList, HashMap<Integer, String> riskMap) {
        JSONArray jsonArray = new JSONArray();
        if (riskList != null && riskList.size() > 0) {
            JSONObject jsonObject = null;
            for (RiskCertificate risk : riskList) {
                jsonObject = new JSONObject();
                jsonObject.put(RiskConstant.RISK_ID_JSON, risk.getId());
                jsonObject.put(RiskConstant.RISK_CERTIFICATE_NAME_JSON, risk.getRiskCertificateName());
                jsonObject.put(Constants.RISK_STATUS, riskMap.get(risk.getRiskStatusType()));
                jsonObject.put(RiskConstant.IS_ACTIVE_JSON, risk.isActive());

                jsonArray.put(jsonObject);
            }
        }
        return jsonArray;
    }

    public UpdateRiskCertificate jsonToUpdateRiskCertificatePayload(JSONObject jsonObject) {
        if (jsonObject.length() == 0) {
            throw new ServiceException(WebConstant.NO_PAYLOAD, Status.BAD_REQUEST, WebConstant.EMPTY_OR_NULL_PAYLOAD);
        }
        UpdateRiskCertificate riskCertificate = null;
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        try {
            riskCertificate = mapper.readValue(jsonObject.toString(), UpdateRiskCertificate.class);
        } catch (Exception jse) {
            logger.error("Exception while converting JSON to POJO (jsonToUpdateRiskCertificatePayload).", jse);
            throw new ServiceException(WebConstant.IMPROPER_INPUT_PAYLOAD, Status.BAD_REQUEST,
                    WebConstant.IMPROPER_INPUT_PAYLOAD_CODE);
        }
        return riskCertificate;
    }

    public UpdateRiskCertificate validateUpdateRiskCertificateInput(JSONObject jsonObject) {
        UpdateRiskCertificate updateRiskCertificate = jsonToUpdateRiskCertificatePayload(jsonObject);
        logger.info("riskPayload " + updateRiskCertificate.toString());
        validateRiskIds(updateRiskCertificate.getRiskCertificateIds());
        validateActivateStatus(jsonObject);
        validateAgent(updateRiskCertificate.getAgentId());
        return updateRiskCertificate;
    }

    public void validateActivateStatus(JSONObject jsonObject) {
        logger.info(" is active " + jsonObject.toString());
        if (!jsonObject.has(RiskConstant.ACTIVATE)) {
            throw new ServiceException(RiskConstant.ACTIVATE_DATA_NOTFOUND, Status.BAD_REQUEST,
                    RiskConstant.ACTIVATE_DATA_NOTFOUND_CODE);
        } else if (!(jsonObject.get(RiskConstant.ACTIVATE).toString().equalsIgnoreCase(RiskConstant.TRUE)
                || jsonObject.get(RiskConstant.ACTIVATE).toString().equalsIgnoreCase(RiskConstant.FALSE))) {
            throw new ServiceException(RiskConstant.INVALID_ACTIVATE_VALUE_FOUND, Status.BAD_REQUEST,
                    RiskConstant.INVALID_ACTIVATE_VALUE_FOUND_ERR_CODE);
        }
    }

    public void validateRiskIds(String riskCertificateIds) {
        List<String> riskIdList = checkRiskIdValues(riskCertificateIds);
        if (riskIdList.size() != totalUniqueRiskIds(riskCertificateIds)) {
            throw new ServiceException(RiskConstant.LIST_DISTINCT_RISKID_NOT_MATCHING, Status.BAD_REQUEST,
                    RiskConstant.LIST_DISTINCT_RISKID_NOT_MATCHING_ERR_CODE);
        }
    }

    public List<String> checkRiskIdValues(String riskCertificateIds) {

        List<String> riskIdList = null;
        if (riskCertificateIds != null && riskCertificateIds.length() > 0) {
            riskIdList = Arrays.asList(riskCertificateIds.split(","));
            for (String riskIdValue : riskIdList) {
                try {
                    if (Long.parseLong(riskIdValue) <= 0) {
                        throw new ServiceException(RiskConstant.INVALID_RISK_ID + " : " + riskIdValue,
                                Status.BAD_REQUEST, RiskConstant.INVALID_RISK_ID_CODE);
                    }
                } catch (Exception nfe) {
                    throw new ServiceException(RiskConstant.INVALID_RISK_ID + " : " + riskIdValue, Status.BAD_REQUEST,
                            nfe, RiskConstant.INVALID_RISK_ID_CODE);
                }
            }
        } else {
            logger.error("Risk id is null");
            throw new ServiceException(RiskConstant.RISKIDS_EMPTY, Status.BAD_REQUEST,
                    RiskConstant.RISKIDS_EMPTY_ERROR_CODE);
        }
        logger.info("riskIds.length() " + riskIdList.size());
        return riskIdList;
    }

    int totalUniqueRiskIds(String riskIdString) {
        return ceriticateDAO.distinctRiskId(riskIdString);
    }

    public PlayersRiskCertificate validatePlayersRiskCertificateInput(JSONObject jsonObject) {
        PlayersRiskCertificate playerRiskCert = jsonToPlayersRiskCertificatePayload(jsonObject);
        logger.info("riskPayload PlayersRiskCertificate {} ", playerRiskCert.toString());
        validateRiskNameValue(playerRiskCert.getRiskCertificateNames());
        validateUserId(playerRiskCert);
        validateContactType(playerRiskCert);
        validateContactDirection(playerRiskCert);
        validateComment(playerRiskCert);
        validateRiskStatus(playerRiskCert.getRiskStatus());
        validateRiskStatusComment(playerRiskCert.getRiskStatusComment());
        validateAgent(playerRiskCert.getAgentId());
        return playerRiskCert;
    }

    public void validateRiskNameValue(String riskCertificateNames) {
        List<String> riskNameList = null;
        StringBuilder strRiskName = new StringBuilder();
        if (riskCertificateNames != null && riskCertificateNames.length() > 0) {
            riskNameList = Arrays.asList(riskCertificateNames.split(","));
            for (String riskNameValue : riskNameList) {
                if (riskNameValue.length() == 0) {
                    throw new ServiceException(RiskConstant.INVALID_RISK_NAME, Status.BAD_REQUEST,
                            RiskConstant.INVALID_RISK_NAME_CODE);
                }
                if (strRiskName.length() > 0) {
                    strRiskName.append(",'" + riskNameValue + "'");
                } else {
                    strRiskName.append("'" + riskNameValue + "'");
                }
            }
        } else {
            logger.error("Risk name is empty ");
            throw new ServiceException(RiskConstant.RISK_NAME_EMPTY, Status.BAD_REQUEST,
                    RiskConstant.RISK_NAME_EMPTY_ERROR_CODE);
        }
        List<Long> riskCertificateIds = ceriticateDAO.getRiskCertificateName(strRiskName.toString());
        if (riskNameList.size() != riskCertificateIds.size()) {
            throw new ServiceException(RiskConstant.LIST_DISTINCT_RISKNAME_NOT_MATCHING, Status.BAD_REQUEST,
                    RiskConstant.LIST_DISTINCT_RISKNAME_NOT_MATCHING_ERR_CODE);
        }
    }

    void validateUserId(PlayersRiskCertificate playerRiskCert) {
        if (playerRiskCert.getUserId() <= 0) {
            throw new ServiceException(RiskConstant.INVALID_USERID, Status.BAD_REQUEST,
                    RiskConstant.INVALID_USERID_ERRCODE);
        }
    }

    void validateComment(PlayersRiskCertificate playerRiskCert) {
        if ((playerRiskCert.getComment() == null)
                || (playerRiskCert.getComment() != null && playerRiskCert.getComment().isEmpty())) {
            throw new ServiceException(RiskConstant.MISSING_COMMENT, Status.BAD_REQUEST,
                    RiskConstant.MISSING_COMMENT_ERRCODE);
        }
    }

    void validateContactDirection(PlayersRiskCertificate playerRiskCert) {
        if ((playerRiskCert.getContactDirection() == null)
                || (playerRiskCert.getContactDirection() != null && playerRiskCert.getContactDirection().isEmpty())) {
            throw new ServiceException(RiskConstant.MISSING_CONTACT_DIRECTION, Status.BAD_REQUEST,
                    RiskConstant.MISSING_CONTACT_DIRECTION_ERRCODE);
        }
        String validContactDirection = config.get(RiskConstant.VALID_CONTARCT_DIRECTION, "In/Out,In,Out,Out/In");
        logger.info("validContractType " + validContactDirection.toUpperCase());
        List<String> validContractDirectionList = Arrays.asList(validContactDirection.toUpperCase().split(","));
        logger.info("validContractTypeList " + validContractDirectionList.toString());
        // logger.info("playerRiskCert.getContactType()
        // "+playerRiskCert.getContactType().toUpperCase());
        if (!validContractDirectionList.contains(playerRiskCert.getContactDirection().toUpperCase())) {
            throw new ServiceException(RiskConstant.INVALID_CONTACT_DIRECTION, Status.BAD_REQUEST,
                    RiskConstant.INVALID_CONTACT_DIRECTION_ERRCODE);
        }
    }

    void validateContactType(PlayersRiskCertificate playerRiskCert) {
        if ((playerRiskCert.getContactType() == null)
                || (playerRiskCert.getContactType() != null && playerRiskCert.getContactType().isEmpty())) {
            throw new ServiceException(RiskConstant.MISSING_CONTACT_TYPE, Status.BAD_REQUEST,
                    RiskConstant.MISSING_CONTACT_TYPE_ERRCODE);
        }
        String validContractType = config.get(RiskConstant.VALID_CONTARCT_TYPE, "Email,Phone,Email/Phone,Phone/Email");
        logger.info("validContractType " + validContractType.toUpperCase());
        List<String> validContractTypeList = Arrays.asList(validContractType.toUpperCase().split(","));
        logger.info("validContractTypeList " + validContractTypeList.toString());
        logger.info("playerRiskCert.getContactType() " + playerRiskCert.getContactType().toUpperCase());
        if (!validContractTypeList.contains(playerRiskCert.getContactType().toUpperCase())) {
            throw new ServiceException(RiskConstant.INVALID_CONTACT_TYPE, Status.BAD_REQUEST,
                    RiskConstant.INVALID_CONTACT_TYPE_ERRCODE);
        }
    }

    public PlayersRiskCertificate jsonToPlayersRiskCertificatePayload(JSONObject jsonObject) {
        if (jsonObject.length() == 0) {
            throw new ServiceException(WebConstant.NO_PAYLOAD, Status.BAD_REQUEST, WebConstant.EMPTY_OR_NULL_PAYLOAD);
        }
        PlayersRiskCertificate playerRiskCertificate = null;
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        try {
            playerRiskCertificate = mapper.readValue(jsonObject.toString(), PlayersRiskCertificate.class);
        } catch (Exception jse) {
            logger.error("Exception while converting JSON to POJO (jsonToUpdateRiskCertificatePayload).", jse);
            throw new ServiceException(WebConstant.IMPROPER_INPUT_PAYLOAD, Status.BAD_REQUEST,
                    WebConstant.IMPROPER_INPUT_PAYLOAD_CODE);
        }
        return playerRiskCertificate;
    }

    public void validateGetRiskCertificateInput(int riskType, long offset, Set<Integer> validRiskStatusSet) {
        if (riskType < 0 || validateRiskType(riskType, validRiskStatusSet)) {
            throw new ServiceException(WebConstant.INVALID_RISK_TYPE, Status.BAD_REQUEST,
                    WebConstant.INVALID_RISK_TYPE_CODE);
        }
        validateOffset(offset);
    }

    public boolean validateRiskType(int riskType, Set<Integer> validRiskStatusSet) {
        if (validRiskStatusSet.size() > 0 && validRiskStatusSet.contains(riskType)) {
            return false;
        }
        return true;
    }

    public void validateOffset(long offset) {
        if (offset < 0) {
            throw new ServiceException(RiskConstant.INVALID_OFFSET, Status.BAD_REQUEST,
                    RiskConstant.INVALID_OFFSET_ERRCODE);
        }
    }

}
