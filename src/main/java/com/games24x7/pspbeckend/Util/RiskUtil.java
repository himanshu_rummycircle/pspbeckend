/**
 * 
 */
package com.games24x7.pspbeckend.Util;

import java.text.SimpleDateFormat;

import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.webclient.AuditServiceClient;
import com.games24x7.pspbeckend.webclient.UserServiceClient;

/**
 * 
 */
@Component
public class RiskUtil {
    private static final Logger logger = LoggerFactory.getLogger(RiskUtil.class);
    @Autowired
    AuditServiceClient auditclient;
    @Autowired
    UserServiceClient usClient;

    @Autowired
    Configuration config;

    /**
     * @param userId
     * @return
     */
    public String getRiskStatus(long userId) {

        String url = config.get(Constants.UMS_RISK_STATUS_URL);
        url = url.replace("USERID", String.valueOf(userId));
        logger.debug("URL ************* " + url);
        String result = usClient.getStringResGetCall(url.trim());
        logger.debug("result ****** " + result);

        if (result != null) {
            JSONObject obj = new JSONObject(result);
            return obj.get("risk_status").toString();
        } else {
            throw new ServiceException(Constants.INVALID_USER, Status.INTERNAL_SERVER_ERROR,
                    MTTSettlementConstant.US_CALL_FAILURE_CODE);
        }
    }

    /**
     * 
     * @param userId
     * @return
     */
    public JSONArray getRiskLog(long userId) {
        String url = config.get(MTTSettlementConstant.AUDIT_SERVICE_URL)
                + config.get(MTTSettlementConstant.AUDIT_SERVICE_RISK_PREFIX,
                        MTTSettlementConstant.AUDIT_SERVICE_IN_PREFIX)
                + userId + config.get(MTTSettlementConstant.AUDIT_SERVICE_RISK_SUFFIX,
                        MTTSettlementConstant.AUDIT_SERVICE_IN_SUFFIX);

        JSONArray arr = auditclient.getCall(url);
        logger.debug("Result from Audit Log :", arr);
        JSONArray result = new JSONArray();
        for (int i = 0; arr != null && i < arr.length(); i++) {
            JSONObject obj = arr.getJSONObject(i);
            JSONObject newObj = new JSONObject();

            newObj.put("date", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    .format(Long.parseLong(obj.getJSONObject(Constants.UPDATE_TIME).getString("$numberLong"))));
            newObj.put(Constants.RISK_CERTIFICATE_NAMES, obj.get(Constants.RISK_CERTIFICATE_NAMES));
            newObj.put(Constants.CONTACT_TYPE, obj.get(Constants.CONTACT_TYPE));
            newObj.put(Constants.CONTACT_DIRECTION, obj.get(Constants.CONTACT_DIRECTION));
            newObj.put(Constants.INTERACTION_ID,
                    obj.has(Constants.INTERACTION_ID) ? obj.get(Constants.INTERACTION_ID) : Constants.NA);
            newObj.put(Constants.COMMENT, obj.get(Constants.COMMENT));
            newObj.put(Constants.RISK_STATUS, obj.has(Constants.RISK_STATUS_COMMENT)
                    ? obj.get(Constants.RISK_STATUS_COMMENT) : obj.get(Constants.RISK_STATUS));
            newObj.put(Constants.REASON_FOR_DOWNGRADE,
                    obj.has(Constants.REASON_FOR_DOWNGRADE) ? obj.get(Constants.REASON_FOR_DOWNGRADE) : Constants.NA);
            newObj.put(Constants.AGENT_ID, obj.get(Constants.AGENT_ID));
            result.put(newObj);
        }
        logger.debug("Final Result from Audit Log :", result);
        return result;

    }

}
