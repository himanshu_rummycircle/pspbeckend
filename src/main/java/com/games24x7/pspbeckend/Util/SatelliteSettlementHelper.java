package com.games24x7.pspbeckend.Util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.eds.events.CurrentWithdrawable;
import com.games24x7.frameworks.eds.events.Event;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.frameworks.serviceutil.httpclient.RetryConfig;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.dao.SatelliteSettlementDAO;
import com.games24x7.pspbeckend.mq.SecondaryMQFactory;
import com.games24x7.pspbeckend.pojo.OfflinePrize;
import com.games24x7.pspbeckend.pojo.OfflineSettlementMoneyInfo;
import com.games24x7.pspbeckend.pojo.OfflineSettlementPayload;
import com.games24x7.pspbeckend.pojo.SortByRank;
import com.games24x7.pspbeckend.pojo.WebResponse;
import com.games24x7.pspbeckend.webclient.FundServiceClient;
import com.games24x7.pspbeckend.webclient.UserServiceClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component
public class SatelliteSettlementHelper {

    private static final Logger logger = LoggerFactory.getLogger(SatelliteSettlementHelper.class);

    @Autowired
    SatelliteSettlementDAO settlementDAO;

    @Autowired
    Configuration config;

    @Autowired
    UserServiceClient userServiceClient;

    @Autowired
    FundServiceClient fundServiceClient;

    @Autowired
    SecondaryMQFactory secondaryMQ;

    @Autowired
    AuditUtil audit;

    HashMap<Integer, String> tournamentStatus = new HashMap<Integer, String>();

    private static final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private static final Gson GSON = new GsonBuilder().setDateFormat(DATE_PATTERN).serializeNulls().create();

    public OfflineSettlementMoneyInfo validateInputData(OfflineSettlementPayload payload) {
        OfflineSettlementMoneyInfo moneyInfo = null;
        if (payload != null) {
            checkForAllowedTListZK(payload.getTournamentId());
            Map<String, Object> tournInfoPrizeAmount = settlementDAO.getTournamentInfo(payload.getTournamentId());
            validateTournamentProperties(payload, tournInfoPrizeAmount);
            Double totalPrizePool = ((BigDecimal) tournInfoPrizeAmount.get(MTTSettlementConstant.PRIZE_POOL))
                    .doubleValue();
            moneyInfo = validatePrizeListUsersAmountAndReturnMoneyInfo(payload, totalPrizePool);
            validateAgent(payload);
            validateAgentGroup(payload);
            validateComment(payload);
        } else {
            throw new ServiceException(MTTSettlementConstant.NO_PAYLOAD, Status.BAD_REQUEST,
                    MTTSettlementConstant.EMPTY_OR_NULL_PAYLOAD);
        }
        return moneyInfo;
    }

    /**
     * Validate genuine user id Validate prize positioning Validate amounts
     * Amount of higher position should be more then that of lower position.
     * 
     * @param payload
     */
    OfflineSettlementMoneyInfo validatePrizeListUsersAmountAndReturnMoneyInfo(OfflineSettlementPayload payload,
            double totalPrizePool) {
        if (payload.getPrizelist() == null || (payload.getPrizelist() != null && payload.getPrizelist().size() == 0)) {
            throw new ServiceException(MTTSettlementConstant.MISSING_PRIZE_LIST, Status.BAD_REQUEST,
                    MTTSettlementConstant.MISSING_PRIZE_LIST_CODE);
        }
        return validatePrizeListProperties(payload, totalPrizePool);
    }

    OfflineSettlementMoneyInfo validatePrizeListProperties(OfflineSettlementPayload payload, double totalPrizePool) {
        List<OfflinePrize> prizelist = payload.getPrizelist();
        double currentMaxAmount = 0.0D;
        double totalPrizeAmountToDistribute = 0.0D;
        String allUserIds = "";
        HashSet<Long> userSet = new HashSet<Long>();
        try {
            checkForThreeDecimal(prizelist);
            logger.info("prizeList " + prizelist.toString());
            Collections.sort(prizelist, new SortByRank());
            for (int i = 0; i < prizelist.size(); i++) {
                validateUserId(prizelist.get(i).getUserId());
                validateRanks(prizelist.get(i).getRank(), prizelist.size());
                validatePrizeAmount(prizelist.get(i).getPrizeAmount());
                if (currentMaxAmount > 0) {
                    validatingAmountAsPerTheRank(currentMaxAmount, prizelist.get(i).getPrizeAmount());
                }
                currentMaxAmount = prizelist.get(i).getPrizeAmount();
                totalPrizeAmountToDistribute = totalPrizeAmountToDistribute + prizelist.get(i).getPrizeAmount();
                if (allUserIds.length() > 0) {
                    allUserIds = allUserIds + "," + prizelist.get(i).getUserId();
                } else {
                    allUserIds = prizelist.get(i).getUserId() + "";
                }
                userSet.add(prizelist.get(i).getUserId());
            }
        } catch (ServiceException se) {
            throw se;
        } catch (Exception e) {
            throw new ServiceException(MTTSettlementConstant.EMPTY_CELLS_ERROR, Status.BAD_REQUEST,
                    MTTSettlementConstant.EMPTY_CELLS_ERROR_CODE);
        }

        if (userSet.size() != prizelist.size()) {
            throw new ServiceException(MTTSettlementConstant.UNIQUE_USERID_COUNT_FAILED, Status.BAD_REQUEST,
                    MTTSettlementConstant.UNIQUE_USERID_COUNT_FAILED_CODE);
        }
        if (getValidUserListSize(allUserIds) != prizelist.size()) {
            throw new ServiceException(MTTSettlementConstant.INVALID_USERIDS_FOUND, Status.BAD_REQUEST,
                    MTTSettlementConstant.INVALID_USERIDS_FOUND_CODE);
        }
        if (totalPrizePool == 0) {
            throw new ServiceException(MTTSettlementConstant.GUARANTEED_AMOUNT_ZERO, Status.BAD_REQUEST,
                    MTTSettlementConstant.GUARANTEED_AMOUNT_ZERO_CODE);
        }
        logger.info("totalPrizePool " + totalPrizePool + " AND totalPrizeAmount " + totalPrizeAmountToDistribute);
        if (totalPrizePool != totalPrizeAmountToDistribute) {
            throw new ServiceException(MTTSettlementConstant.PRIZE_AMOUNT_NOT_EQUAL, Status.BAD_REQUEST,
                    MTTSettlementConstant.PRIZE_AMOUNT_NOT_EQUAL_CODE);
        }
        return new OfflineSettlementMoneyInfo(totalPrizeAmountToDistribute, totalPrizePool,
                validateAndReturnQualifierContribution(payload.getTournamentId()));
    }

    void checkForThreeDecimal(List<OfflinePrize> prizelist) {
        MTTSettlementConstant.threeDecimalFormatter.setRoundingMode(RoundingMode.HALF_UP);
        for (int u = 0; u < prizelist.size(); u++) {
            if (prizelist.get(u).getPrizeAmount() != null) {
                prizelist.get(u).setPrizeAmount(Double.valueOf(
                        MTTSettlementConstant.threeDecimalFormatter.format(prizelist.get(u).getPrizeAmount())));
            } else {
                throw new ServiceException(MTTSettlementConstant.AMOUNT_VALUE_NOT_FOUND, Status.BAD_REQUEST,
                        MTTSettlementConstant.AMOUNT_VALUE_NOT_FOUND_CODE);
            }

        }

    }

    double validateAndReturnQualifierContribution(long tournamentId) {
        double totalPrizeQualifierContribution = 0D;
        try {
            totalPrizeQualifierContribution = settlementDAO.getTournamentQualifierContribution(tournamentId);
        } catch (Exception e) {
            throw new ServiceException(MTTSettlementConstant.ZERO_PRIZE_ALLOCATED, Status.BAD_REQUEST,
                    MTTSettlementConstant.ZERO_PRIZE_ALLOCATED_CODE);
        }
        logger.info("Total Prize " + totalPrizeQualifierContribution);
        if (totalPrizeQualifierContribution <= 0) {
            throw new ServiceException(MTTSettlementConstant.ZERO_PRIZE_ALLOCATED, Status.BAD_REQUEST,
                    MTTSettlementConstant.ZERO_PRIZE_ALLOCATED_CODE);
        }

        return totalPrizeQualifierContribution;

    }

    void validatingAmountAsPerTheRank(double currentMaxAmount, double prizeAmount) {
        if (currentMaxAmount < prizeAmount) {
            throw new ServiceException(MTTSettlementConstant.HIGHER_POSITION_LOWER_AMOUNT, Status.BAD_REQUEST,
                    MTTSettlementConstant.HIGHER_POSITION_LOWER_AMOUNT_CODE);
        }
    }

    void validatePrizeAmount(double prizeAmount) {
        if (prizeAmount < 0) {
            throw new ServiceException(MTTSettlementConstant.INVALID_PRIZE_AMOUNT_VALUE + prizeAmount,
                    Status.BAD_REQUEST, MTTSettlementConstant.INVALID_PRIZE_AMOUNT_VALUE_CODE);
        }
    }

    void validateRanks(int rank, int totalPrize) {
        if (rank <= 0 || rank > totalPrize) {
            throw new ServiceException(MTTSettlementConstant.INVALID_RANK_VALUE + rank, Status.BAD_REQUEST,
                    MTTSettlementConstant.INVALID_RANK_VALUE_CODE);
        }
    }

    void validateUserId(long userId) {
        if (userId <= 0) {
            throw new ServiceException(MTTSettlementConstant.INVALID_USER_ID + userId, Status.BAD_REQUEST,
                    MTTSettlementConstant.INVALID_USER_ID_CODE);
        }
    }

    int getValidUserListSize(String allUserIds) {
        logger.info("ALL USER IDS " + allUserIds);
        String url = config.get(MTTSettlementConstant.USER_SERVICE_URL) + MTTSettlementConstant.USER_SERVICE_IN_PREFIX
                + allUserIds + MTTSettlementConstant.USER_SERVICE_IN_SUFFIX;
        RetryConfig retryConfig = RetryConfig.getStandardRetryConfigForPutPost();
        WebResponse usersData = null;
        logger.info("URL :: " + url);
        usersData = userServiceClient.getCall(url, retryConfig);
        return new JSONArray(usersData.getPayload()).length();
    }

    public void validateComment(OfflineSettlementPayload payload) {
        if (payload.getComment() == null || (payload.getComment() != null && payload.getComment().isEmpty())) {
            throw new ServiceException(MTTSettlementConstant.MISSING_COMMENT, Status.BAD_REQUEST,
                    MTTSettlementConstant.MISSING_COMMENT_CODE);
        }
        String commentTrimmed = payload.getComment().replaceAll("\\s+", "");
        if (commentTrimmed.length() < MTTSettlementConstant.TEN) {
            throw new ServiceException(MTTSettlementConstant.MISSING_COMMENT, Status.BAD_REQUEST,
                    MTTSettlementConstant.MISSING_COMMENT_CODE);
        }
    }

    /**
     * Validates agent id.
     *
     * @param agentId
     *            the agent id
     */
    public void validateAgent(OfflineSettlementPayload payload) {
        logger.info("payload " + payload.toString());
        if ((payload.getAgentId() == null) || (payload.getAgentId() != null && payload.getAgentId().isEmpty())) {
            throw new ServiceException(MTTSettlementConstant.MISSING_AGENT, Status.BAD_REQUEST,
                    MTTSettlementConstant.MISSING_AGENT_CODE);
        }
    }

    public void validateAgentGroup(OfflineSettlementPayload payload) {
        logger.info("payload " + payload.toString());
        if ((payload.getAgentGroup() == null)
                || (payload.getAgentGroup() != null && payload.getAgentGroup().isEmpty())) {
            throw new ServiceException(MTTSettlementConstant.MISSING_AGENT_GROUP, Status.BAD_REQUEST,
                    MTTSettlementConstant.MISSING_AGENT_GROUP_CODE);
        }
    }

    /**
     * Tournament id should have these Characteristics 1) Exist in DB 2)
     * Tournament should be Satellite in nature. 3) Satellite tournament in
     * cancel state. 4) Not in Completed State.
     * 
     * @param payload
     */
    void validateTournamentProperties(OfflineSettlementPayload payload, Map<String, Object> torunamentInfoMap) {
        if (payload.getTournamentId() == null || (payload.getTournamentId() != null && payload.getTournamentId() <= 0
                || String.valueOf(payload.getTournamentId()).length() > MTTSettlementConstant.MAX_LENGTH_TID)) {
            throw new ServiceException(MTTSettlementConstant.TOURNAMENT_NUMBER_ERROR, Status.BAD_REQUEST,
                    MTTSettlementConstant.TOURNAMENT_NUMBER_ERROR_CODE);
        }

        if (torunamentInfoMap != null) {
            int tournamentType = (int) torunamentInfoMap.get(MTTSettlementConstant.TOURNAMENT_TYPE);
            if (MTTSettlementConstant.SATELLITE_PARENT_TOURNAMENT != tournamentType) {
                throw new ServiceException(MTTSettlementConstant.TOURNAMENT_INVALID_TYPE_ERROR, Status.BAD_REQUEST,
                        MTTSettlementConstant.TOURNAMENT_INVALID_TYPE_ERROR_CODE);
            }
            int tournamentStatus = (int) torunamentInfoMap.get(MTTSettlementConstant.TOURNAMENT_STATUS);
            validateTournamentState(tournamentStatus);
        } else {
            throw new ServiceException(MTTSettlementConstant.NO_DATA_FOR_THIS_TOURNAMENTID, Status.BAD_REQUEST,
                    MTTSettlementConstant.NO_DATA_FOR_THIS_TOURNAMENTID_CODE);
        }
    }

    void checkForAllowedTListZK(long tournamentId) {
        // Check if this is there in ZK.
        String allowedTIdOfflineSettlementList = config.get(MTTSettlementConstant.ALLOWED_OFFLINE_SETTLEMENT_LIST);
        if (allowedTIdOfflineSettlementList != null && allowedTIdOfflineSettlementList.length() > 0) {
            List<String> tIdList = Arrays.asList(allowedTIdOfflineSettlementList.split(","));
            if (!tIdList.contains(tournamentId + "")) {
                throw new ServiceException(MTTSettlementConstant.OFFLINE_SETTLEMENT_NOT_PERMITTED, Status.BAD_REQUEST,
                        MTTSettlementConstant.OFFLINE_SETTLEMENT_NOT_PERMITTED_CODE);
            }
        } else {
            throw new ServiceException(MTTSettlementConstant.OFFLINE_SETTLEMENT_NOT_PERMITTED, Status.BAD_REQUEST,
                    MTTSettlementConstant.OFFLINE_SETTLEMENT_NOT_PERMITTED_CODE);
        }

    }

    void validateTournamentState(int tStatus) {
        if (tStatus == MTTSettlementConstant.COMPLETED_STATUS) {
            throw new ServiceException(MTTSettlementConstant.TOURNAMENT_COMPLETED_STATE_ERROR, Status.BAD_REQUEST,
                    MTTSettlementConstant.TOURNAMENT_COMPLETED_STATE_ERROR_CODE);
        } else if (tStatus != MTTSettlementConstant.INPROGRESS_STATUS) {
            throw new ServiceException(MTTSettlementConstant.TOURNAMENT_NOTIN_INPROGRESS_STATE_ERROR,
                    Status.BAD_REQUEST, MTTSettlementConstant.TOURNAMENT_NOTIN_CANCELLED_STATE_ERROR_CODE);
        }
    }

    public OfflineSettlementPayload jsonToOfflineSettlementPayload(JSONObject jsonPayload) {
        if (jsonPayload.length() == 0) {
            throw new ServiceException(MTTSettlementConstant.NO_PAYLOAD, Status.BAD_REQUEST,
                    MTTSettlementConstant.EMPTY_OR_NULL_PAYLOAD);
        }
        OfflineSettlementPayload offlinePayload = null;
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        try {
            offlinePayload = mapper.readValue(jsonPayload.toString(), OfflineSettlementPayload.class);
        } catch (Exception jse) {
            logger.error("Exception while converting JSON to POJO (OfflineSettlementPayload).", jse);
            throw new ServiceException(MTTSettlementConstant.IMPROPER_INPUT_PAYLOAD, Status.BAD_REQUEST,
                    MTTSettlementConstant.IMPROPER_INPUT_PAYLOAD_CODE);
        }
        return offlinePayload;
    }

    public void publishEdsMessageAndAuditLogs(OfflineSettlementPayload payload) {
        List<OfflinePrize> prizelist = payload.getPrizelist();
        try {
            for (int i = 0; i < prizelist.size(); i++) {
                secondaryMQ.publishToSecondaryMQ(prepareEDSMsg(prizelist.get(i).getUserId()));
                audit.publishToAudit(MTTSettlementConstant.OFFLINE_SETTLEMENT_TOPIC,
                        new JSONObject(preparedAuditData(prizelist.get(i), payload.getTournamentId()).toString()));
            }
        } catch (Exception jse) {
            logger.error("Exception while publishing data to EDS and Audit.", jse);
        }
    }

    String preparedAuditData(OfflinePrize prize, Long tournamentId) {
        JSONObject json = new JSONObject(prize);
        json.put(MTTSettlementConstant.UPDATE_TIME, System.currentTimeMillis());
        json.put(Constants.USERID, prize.getUserId());
        json.put(MTTSettlementConstant.ENTITY_TYPE, MTTSettlementConstant.OFFLINE_SETTLEMENT_LOG);
        json.put(MTTSettlementConstant.TOURNAMENT_ID, tournamentId);
        logger.info("Audit logs : " + json.toString());
        StringBuffer msg = new StringBuffer();
        msg.append("{");
        msg.append("\"entityType\"");
        msg.append(MTTSettlementConstant.COLON);
        msg.append('"' + MTTSettlementConstant.OFFLINE_SETTLEMENT_LOG + '"');
        msg.append(MTTSettlementConstant.COMMA);
        msg.append("\"entity\"");
        msg.append(MTTSettlementConstant.COLON);
        msg.append(json.toString());
        msg.append("}");
        return msg.toString();
    }

    public Event prepareEDSMsg(long userId) {
        CurrentWithdrawable currenWithdrawable = new CurrentWithdrawable();
        JSONObject currentWithdrawableJSON = getPlayerAccountJson(userId);
        logger.info("Fund object = " + currentWithdrawableJSON.toString());
        if (currentWithdrawableJSON != null) {
            currenWithdrawable.setUserId(userId);
            currenWithdrawable.setDepositAmt(currentWithdrawableJSON.getDouble(MTTSettlementConstant.DEPOSIT));
            currenWithdrawable.setWithdrawAmt(currentWithdrawableJSON.getDouble(MTTSettlementConstant.WITHDRAWABLE));
            currenWithdrawable
                    .setNonWithdrawAmt(currentWithdrawableJSON.getDouble(MTTSettlementConstant.NON_WITHDRAWABLE));
            currenWithdrawable.setEventType(MTTSettlementConstant.CURRENT_WITHDRAWABLE_EVENT_TYPE);
            currenWithdrawable.setChannelId(0);
        }
        return currenWithdrawable;
    }

    JSONObject getPlayerAccountJson(long userId) {
        JSONObject playerAccount = null;
        try {
            StringBuilder builder = new StringBuilder();
            builder.append(config.get(MTTSettlementConstant.FS_BASE_URL));
            builder.append("/").append(MTTSettlementConstant.ACCOUNT_RC_PLAYERACCOUNT).append("/");
            builder.append(userId).append(MTTSettlementConstant.PLAYER_ACCOUNT_DEFAULT_PROJECTION);
            playerAccount = fundServiceClient.getAccount(builder.toString(),
                    RetryConfig.getStandardRetryConfigForGet());
        } catch (Exception ex) {
            logger.error("Unable to get player account json for user " + userId, ex);
        }
        return playerAccount;
    }

    public static <T> String formatJson(T object) {
        return GSON.toJson(object);
    }

}
