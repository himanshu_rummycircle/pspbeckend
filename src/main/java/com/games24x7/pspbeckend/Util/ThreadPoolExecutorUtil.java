package com.games24x7.pspbeckend.Util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.pspbeckend.constants.Constants;

@Component
public class ThreadPoolExecutorUtil {
    private ExecutorService approvalPoolExecutor;
    private ExecutorService playerUpdatePoolExecutor;

    @Autowired
    Configuration config;

    @PostConstruct
    public void init() {
        approvalPoolExecutor = new ThreadPoolExecutor(config.getInt(Constants.APPROVAL_CORE_POOL_SIZE, 3),
                config.getInt(Constants.APPROVAL_MAX_POOL_SIZE, 10), 2, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());
        playerUpdatePoolExecutor = new ThreadPoolExecutor(config.getInt(Constants.PL_UPDATE_CORE_POOL_SIZE, 6),
                config.getInt(Constants.PL_UPDATE_MAX_POOL_SIZE, 10), 2, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());
    }

    public ExecutorService getApprovalThreadpoolexecutor() {
        return approvalPoolExecutor;
    }

    public ExecutorService getPlayerUpdateThreadpoolexecutor() {
        return playerUpdatePoolExecutor;
    }

}
