package com.games24x7.pspbeckend.Util;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.frameworks.serviceutil.httpclient.RetryConfig;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.constants.Constants.SessionServiceConstants;
import com.games24x7.pspbeckend.constants.Constants.UMSConstants;
import com.games24x7.pspbeckend.pojo.WebResponse;
import com.games24x7.pspbeckend.webclient.SessionServiceClient;
import com.games24x7.pspbeckend.webclient.UserServiceClient;

@Component
public class UpdateMobileUtil {

    private static final Logger logger = LoggerFactory.getLogger(UpdateMobileUtil.class);

    @Autowired
    UserServiceClient userServiceClient;

    @Autowired
    SessionServiceClient sessionServiceClient;

    @Autowired
    Configuration config;

    public WebResponse updateMobile(long userId, JSONObject payload) {
        WebResponse webResponse = userServiceClient.post(
                config.getString(Constants.USER_SERVICE_URL) + UMSConstants.UPDATE_MOBILE_URL + userId, payload,
                RetryConfig.getStandardRetryConfigForPutPost());
        logger.debug("Response after updateMobile {}", webResponse);
        if (webResponse == null) {
            throw new ServiceException(Constants.UPDATE_MOBILE_UMS_CALL_ERROR,
                    Status.INTERNAL_SERVER_ERROR.getStatusCode(), MTTSettlementConstant.US_CALL_FAILURE_CODE);
        }
        return webResponse;
    }

    public void deleteAllSessionsOfUser(long userId) {
        JSONObject json = new JSONObject();
        json.put(SessionServiceConstants.USER_ID, userId);
        WebResponse webResponse = sessionServiceClient.postCall(
                config.getString(SessionServiceConstants.SESSION_SESSION_URL)
                        + SessionServiceConstants.DELETE_ALL_SESSIONS_URL,
                json, RetryConfig.getStandardRetryConfigForPutPost());
        logger.debug("Response after deleteAllSessionsOfUser {}", webResponse);
    }
}
