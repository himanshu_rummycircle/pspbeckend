package com.games24x7.pspbeckend.Util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.pspbeckend.dao.UserLangPrefernceDao;

@Component
public class UserPreferedLanguageUtil {
    private static final Logger logger = LoggerFactory.getLogger(UserPreferedLanguageUtil.class);

    @Autowired
    UserLangPrefernceDao userlandPrefDao;

    public int getUserPreferedLanguage(Long userId) {
        try {
            Integer langPreference = userlandPrefDao.getUserLanguagePrefence(userId);
            return (langPreference != null && langPreference > 0) ? langPreference : 1;
        } catch (Exception e) {
            logger.error("ERROR fetching data UserPreferedLanguage {}", e);
        }
        return 1;
    }
}
