package com.games24x7.pspbeckend.constants;

public final class Globals {
    public static final Integer MERGE_ACCOUNTS = 4;
    public static final Integer MERGE_ACCOUNTS_TXN_TYPE = 38;
    public static final int ACCOUNT_CLOSED_PLAYER_UPDATES = 5;
    public static final int ACCOUNT_MERGE_PLAYER_UPDATES = 18;
    public static final int SUCESS_CODE = 0;
    public static final int ACCOUNT_STATUS_CLOSED = 6;
    public static final int ACC_STATUS_BLOCKED = 5;
    public static final int CLOSE_AND_REFUND = 2;
    public static final int CLOSE_AND_MERGE = 1;
    public static final int ZERO = 0;
    public static final int ACCOUNT_STATUS_BLOCKED = 4;
    public static final int ACCOUNT_STATUS_CHANGED_TO_CLOSED = 5;
    public static final int ACCOUNT_STATUS_CHANGED_TO_NEW = 3;
    public static final int ACCOUNT_STATUS_CHANGED_TO_PHONE_VERIFIED = 14;
    public static final int ACCOUNT_STATUS_CHANGED_TO_KYC_VERIFIED = 16;
    public static final int ACCOUNT_STATUS_BLOCKED_HEADER_CHANGE = 33;
    public static final int FIELD_UPDATE_KYC_RE_VERIFIED = 45;
    public static final int BULK_ACCOUNT_STATUS_CHANGE = 3;
    public static final int KYC_STATUS = 4;
    public static final int ID_VERIFIED = 3;
    public static final int FIELD_UPDATE_KYC_VERIFIED = 16;
    public static final int KYC_STATUS_REJECTED = 13;
    public static final int FIELD_UPDATE_KYC_REJECTED = 42;
    public static final int KYC_REJECTED_STATUS = 3;
    public static final int KYC_REVISION_UNDER_REVIEW = 5;
    public static final int KYC_ACCEPTED = 4;

    public static final int PAN_CARD = 1;
    public static final int PASSPORT = 2;
    public static final int DRIVING_LICENCE = 3;
    public static final int VOTER_ID = 4;
    public static final int AADHAR_CARD = 5;
    public static final int RATION_CARD = 6;
    public static final int OTHERS = 7;
    public static final int LAST_3_MNTHS_BANK_STMT = 8;
    public static final int Elec_bill_N_PAN = 9;
    public static final int PHONE_BILL_N_PAN = 10;
    public static final int BANK_ACCOUNT_STMNT_N_PAN = 11;
    public static final int BANK_PASSBOOK_n_PAN = 12;
    public static final int RATION_N_PAN_CARD = 13;
    public static final int WHITELISTED_DEVICE_STATUS = 2;
    

    public static final int RGP_BAD_REQUEST = 530;

}