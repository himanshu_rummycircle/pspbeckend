package com.games24x7.pspbeckend.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.frameworks.serviceutil.annotations.ServiceAnnotation;
import com.games24x7.pspbeckend.Util.WebServiceUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.RiskConstant;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;
import com.games24x7.pspbeckend.service.AccountService;

@Controller
@Path("/account")
public class AccountStatusController {
    private static final Logger logger = LoggerFactory.getLogger(AccountStatusController.class);

    @Autowired
    public AccountService service;

    /**
     * 
     * @param payload
     * 
     *            { "context": { "agentId": 12345, "source": "UMS/PSP",
     *            "channelId": 1, "transactionId": "abcd1234" }, "value": {
     *            "userId":123,"status": 5, "comment": "test
     *            commentss","blockheaderid":9,
     *            "header":"others","messagetouser":"Kindly contact RummyCircle
     *            support team for further
     *            information","pspdata":{"department":"department","issue_catagory":"issue_catagory"}
     *            } }
     * @return
     */
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @MonitorCalls
    @ServiceAnnotation(apiIdentifier = Constants.PROCESS_BLOCK_USER, loggingRequired = true)
    @Path("/block")
    public Response blockUser(String payload) {
        try {
            if (WebServiceUtil.isNullOrEmpty(payload)) {
                throw new ServiceException(WebConstant.NO_PAYLOAD, Status.BAD_REQUEST.getStatusCode(),
                        WebConstant.EMPTY_OR_NULL_PAYLOAD);
            }
            JSONObject payloadJson = new JSONObject(payload);
            if (!payloadJson.has(Constants.VALUE)) {
                throw new ServiceException(Constants.VALUE_MISSING_FROM_PAYLOAD_MSG, Status.BAD_REQUEST.getStatusCode(),
                        Constants.VALUE_MISSING_FROM_PAYLOAD);
            }
            if (!payloadJson.has(Constants.CONTEXT)) {
                throw new ServiceException(Constants.CONTEXT_MISSING_FROM_PAYLOAD_MSG,
                        Status.BAD_REQUEST.getStatusCode(), Constants.CONTEXT_MISSING_FROM_PAYLOAD);
            }
            JSONObject value = payloadJson.getJSONObject(Constants.JSON_PAYLOAD_VALUE);
            JSONObject context = payloadJson.getJSONObject(Constants.CONTEXT);

            service.blockUser(value, context);
            return Response.status(Status.OK)
                    .entity(new JSONObject().put(RiskConstant.STATUS, Constants.SUCCESS).toString()).build();
        } catch (JSONException exception) {
            throw new ServiceException(WebConstant.JSON_FORMAT_EXCEPTION, Status.BAD_REQUEST.getStatusCode(), exception,
                    WebConstant.INVALID_JSON_FORMAT);
        } catch (ServiceException se) {
            throw se;
        } catch (Exception e) {
            throw new ServiceException(WebConstant.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(), e,
                    WebConstant.GENERIC_ERROR_CODE);
        }
    }

    /**
     * 
     * @param payload
     * 
     *            { "context": { "agentId": 12345, "source": "UMS/PSP",
     *            "channelId": 1, "transactionId": "abcd1234" }, "value": {
     *            "userId":123,"status": 6, "comment": "test
     *            commentss","blockheaderid":9,
     *            "header":"others","messagetouser":"Kindly contact RummyCircle
     *            support team for further
     *            information","pspdata":{"department":"department","issue_catagory":"issue_catagory"}
     *            } }
     * @return
     */
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @MonitorCalls
    @ServiceAnnotation(apiIdentifier = Constants.PROCESS_CLOSE_USER, loggingRequired = true)
    @Path("/close")
    public Response closeUser(String payload) {
        try {
            if (WebServiceUtil.isNullOrEmpty(payload)) {
                throw new ServiceException(WebConstant.NO_PAYLOAD, Status.BAD_REQUEST.getStatusCode(),
                        WebConstant.EMPTY_OR_NULL_PAYLOAD);
            }
            JSONObject payloadJson = new JSONObject(payload);
            if (!payloadJson.has(Constants.VALUE)) {
                throw new ServiceException(Constants.VALUE_MISSING_FROM_PAYLOAD_MSG, Status.BAD_REQUEST.getStatusCode(),
                        Constants.VALUE_MISSING_FROM_PAYLOAD);
            }
            if (!payloadJson.has(Constants.CONTEXT)) {
                throw new ServiceException(Constants.CONTEXT_MISSING_FROM_PAYLOAD_MSG,
                        Status.BAD_REQUEST.getStatusCode(), Constants.CONTEXT_MISSING_FROM_PAYLOAD);
            }
            JSONObject value = payloadJson.getJSONObject(Constants.JSON_PAYLOAD_VALUE);
            JSONObject context = payloadJson.getJSONObject(Constants.CONTEXT);

            service.closeUser(value, context);
            return Response.status(Status.OK)
                    .entity(new JSONObject().put(RiskConstant.STATUS, Constants.SUCCESS).toString()).build();
        } catch (JSONException exception) {
            throw new ServiceException(WebConstant.JSON_FORMAT_EXCEPTION, Status.BAD_REQUEST.getStatusCode(), exception,
                    WebConstant.INVALID_JSON_FORMAT);
        } catch (ServiceException se) {
            throw se;
        } catch (Exception e) {
            throw new ServiceException(WebConstant.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(), e,
                    WebConstant.GENERIC_ERROR_CODE);
        }
    }

    /**
     * 
     * @param payload
     *            { "context": { "agentId": 12345, "source": "UMS/PSP",
     *            "channelId": 1, "transactionId": "abcd1234" }, "value": {
     *            "userId": 123, "status": 1/2/4, "comment": "test commentss",
     *            "prevStatus":1, "pspdata": { "department": "department",
     *            "issue_catagory": "issue_catagory" } } }
     * @return
     */
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @MonitorCalls
    @ServiceAnnotation(apiIdentifier = Constants.PROCESS_UNBLOCK_USER, loggingRequired = true)
    @Path("/unblock")
    public Response unblockUser(String payload) {
        try {
            if (WebServiceUtil.isNullOrEmpty(payload)) {
                throw new ServiceException(WebConstant.NO_PAYLOAD, Status.BAD_REQUEST.getStatusCode(),
                        WebConstant.EMPTY_OR_NULL_PAYLOAD);
            }
            JSONObject payloadJson = new JSONObject(payload);
            if (!payloadJson.has(Constants.VALUE)) {
                throw new ServiceException(Constants.VALUE_MISSING_FROM_PAYLOAD_MSG, Status.BAD_REQUEST.getStatusCode(),
                        Constants.VALUE_MISSING_FROM_PAYLOAD);
            }
            if (!payloadJson.has(Constants.CONTEXT)) {
                throw new ServiceException(Constants.CONTEXT_MISSING_FROM_PAYLOAD_MSG,
                        Status.BAD_REQUEST.getStatusCode(), Constants.CONTEXT_MISSING_FROM_PAYLOAD);
            }
            JSONObject value = payloadJson.getJSONObject(Constants.JSON_PAYLOAD_VALUE);
            JSONObject context = payloadJson.getJSONObject(Constants.CONTEXT);

            service.unBlockUser(value, context);
            return Response.status(Status.OK)
                    .entity(new JSONObject().put(RiskConstant.STATUS, Constants.SUCCESS).toString()).build();
        } catch (JSONException exception) {
            throw new ServiceException(WebConstant.JSON_FORMAT_EXCEPTION, Status.BAD_REQUEST.getStatusCode(), exception,
                    WebConstant.INVALID_JSON_FORMAT);
        } catch (ServiceException se) {
            throw se;
        } catch (Exception e) {
            throw new ServiceException(WebConstant.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(), e,
                    WebConstant.GENERIC_ERROR_CODE);
        }
    }
}
