package com.games24x7.pspbeckend.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.frameworks.serviceutil.annotations.ServiceAnnotation;
import com.games24x7.pspbeckend.Util.WebServiceUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.RiskConstant;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;
import com.games24x7.pspbeckend.pojo.Alert;
import com.games24x7.pspbeckend.service.AlertService;
import com.google.gson.Gson;

@Component
@Path("/alert")
public class AlertsController {
    private static final Logger logger = LoggerFactory.getLogger(AlertsController.class);

    @Autowired
    public AlertService service;

    /**
     * 
     * @param payload
     *            { "value": { "user_id": 200, "event_type": 4, "status": 1,
     *            "owner_id": 0, "reference": 0, "start_date": null,
     *            "close_date": null, "followup_date":null, "creation_date":
     *            "2018-11-16 11:28:44", "created_by": 0, "last_update_date":
     *            null, "last_updatedby": null, "comment": null,
     *            "withdraw_id":0, "additional_info":null, }, "context": {
     *            "channelId": null, "source": "PSP", "transactionId":
     *            "abcd1234", "agentId": 12345 } }
     * @return
     */
    @POST
    @Path("/" + Constants.CREATE_ALERT)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ServiceAnnotation(apiIdentifier = Constants.CREATE_ALERT, loggingRequired = true)
    @MonitorCalls
    public Response createAlert(String payload) {

        try {
            if (WebServiceUtil.isNullOrEmpty(payload)) {
                throw new ServiceException(WebConstant.NO_PAYLOAD, Status.BAD_REQUEST.getStatusCode(),
                        WebConstant.EMPTY_OR_NULL_PAYLOAD);
            }
            JSONObject value = new JSONObject(payload).getJSONObject(Constants.JSON_PAYLOAD_VALUE);

            long alertId = service.createAlert(value);
            return Response.status(Status.OK).entity(new JSONObject().put(Constants.ALERT_ID, alertId).toString())
                    .build();
        } catch (JSONException exception) {
            throw new ServiceException(WebConstant.JSON_FORMAT_EXCEPTION, Status.BAD_REQUEST.getStatusCode(), exception,
                    WebConstant.INVALID_JSON_FORMAT);
        } catch (ServiceException se) {
            throw se;
        } catch (Exception e) {
            throw new ServiceException(WebConstant.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(), e,
                    WebConstant.GENERIC_ERROR_CODE);
        }

    }

    /**
     * 
     * @param alertId
     * @param payload
     * 
     *            { "value": { "user_id": 200, "event_type": 4, "status": 2,
     *            "owner_id": 0, "reference": 0, "start_date": null,
     *            "close_date": null, "followup_date":null, "creation_date":
     *            "2018-11-16 11:28:44", "created_by": 0, "last_update_date":
     *            null, "last_updatedby": null, "comment": null,
     *            "withdraw_id":0, "additional_info":null, }, "context": {
     *            "channelId": null, "source": "PSP", "transactionId":
     *            "abcd1234", "agentId": 12345 } }
     * @return
     */

    @PUT
    @Path("/" + Constants.UPDATE_ALERT + "/{alertId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ServiceAnnotation(apiIdentifier = Constants.UPDATE_ALERT, loggingRequired = true)
    @MonitorCalls
    public Response updateAlert(@PathParam("alertId") long alertId, String payload) {

        try {
            if (alertId <= 0) {
                throw new ServiceException(WebConstant.INVALID_ALERTID_MSG, Status.BAD_REQUEST.getStatusCode(),
                        WebConstant.INVALID_ALERT_ID_ERROR_CODE);
            }
            if (WebServiceUtil.isNullOrEmpty(payload)) {
                throw new ServiceException(WebConstant.NO_PAYLOAD, Status.BAD_REQUEST.getStatusCode(),
                        WebConstant.EMPTY_OR_NULL_PAYLOAD);
            }
            JSONObject value = new JSONObject(payload).getJSONObject(Constants.JSON_PAYLOAD_VALUE);

            service.updateAlert(alertId, value);
            return Response.status(Status.OK).entity(new JSONObject().put(Constants.SUCCESS, true).toString()).build();
        } catch (JSONException exception) {
            throw new ServiceException(WebConstant.JSON_FORMAT_EXCEPTION, Status.BAD_REQUEST.getStatusCode(), exception,
                    WebConstant.INVALID_JSON_FORMAT);
        } catch (ServiceException se) {
            throw se;
        } catch (Exception e) {
            throw new ServiceException(WebConstant.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(), e,
                    WebConstant.GENERIC_ERROR_CODE);
        }

    }

    /**
     * 
     * @param alertId
     * @param payload
     * 
     *            { "value": {
     * 
     *            "last_updatedby": null, "comment": null
     * 
     *            }, "context": { "channelId": null, "source": "PSP",
     *            "transactionId": "abcd1234", "agentId": 12345 } }
     * @return
     */
    @PUT
    @Path("/" + Constants.UPDATE_ALERT_COMMENT + "/{alertId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ServiceAnnotation(apiIdentifier = Constants.UPDATE_ALERT_COMMENT, loggingRequired = true)
    @MonitorCalls
    public Response updateAlertComment(@PathParam("alertId") long alertId, String payload) {

        try {
            if (alertId <= 0) {
                throw new ServiceException(WebConstant.INVALID_ALERTID_MSG, Status.BAD_REQUEST.getStatusCode(),
                        WebConstant.INVALID_ALERT_ID_ERROR_CODE);
            }
            if (WebServiceUtil.isNullOrEmpty(payload)) {
                throw new ServiceException(WebConstant.NO_PAYLOAD, Status.BAD_REQUEST.getStatusCode(),
                        WebConstant.EMPTY_OR_NULL_PAYLOAD);
            }
            JSONObject value = new JSONObject(payload).getJSONObject(Constants.JSON_PAYLOAD_VALUE);

            service.updateAlertComment(alertId, value);
            return Response.status(Status.OK).entity(new JSONObject().put(Constants.SUCCESS, true).toString()).build();
        } catch (JSONException exception) {
            throw new ServiceException(WebConstant.JSON_FORMAT_EXCEPTION, Status.BAD_REQUEST.getStatusCode(), exception,
                    WebConstant.INVALID_JSON_FORMAT);
        } catch (ServiceException se) {
            throw se;
        } catch (Exception e) {
            throw new ServiceException(WebConstant.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(), e,
                    WebConstant.GENERIC_ERROR_CODE);
        }

    }

    @GET
    @Path("/" + Constants.GET_ALERT + "/{alertId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ServiceAnnotation(apiIdentifier = Constants.GET_ALERT, loggingRequired = true)
    @MonitorCalls
    public Response getAlertData(@PathParam("alertId") long alertId) {
        try {
            if (alertId <= 0) {
                throw new ServiceException(WebConstant.INVALID_ALERTID_MSG, Status.BAD_REQUEST.getStatusCode(),
                        WebConstant.INVALID_ALERT_ID_ERROR_CODE);
            }
            Alert alert = service.getAlert(alertId);
            String jsonString = new Gson().toJson(alert);

            return Response.status(Status.OK).entity(jsonString).build();
        } catch (JSONException exception) {
            throw new ServiceException(WebConstant.JSON_FORMAT_EXCEPTION, Status.BAD_REQUEST.getStatusCode(), exception,
                    WebConstant.INVALID_JSON_FORMAT);
        } catch (ServiceException se) {
            throw se;
        } catch (Exception e) {
            throw new ServiceException(WebConstant.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                    WebConstant.GENERIC_ERROR_CODE);
        }

    }
}
