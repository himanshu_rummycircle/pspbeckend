package com.games24x7.pspbeckend.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.frameworks.serviceutil.annotations.ServiceAnnotation;
import com.games24x7.pspbeckend.Util.WebServiceUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;
import com.games24x7.pspbeckend.service.BankDetailsService;

@Controller
@Path("/")
public class BankDetailsController {
	private static final Logger logger = LoggerFactory.getLogger(BankDetailsController.class);

	@Autowired
	BankDetailsService bankDetailsService;

    /**
     * This API is used to GET bank details
     * 
     * @return
     */
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	@MonitorCalls
	@ServiceAnnotation(apiIdentifier = Constants.GET_USER_BANK_DETAILS, loggingRequired = true)
	@Path("/getbankdetails/{" + Constants.USER_ID + "}")
	public Response getUserBankDetails(@PathParam(Constants.USER_ID) long userId) {
		try {
			JSONObject jsonObj = bankDetailsService.getUserBankDetails(userId);
			if (jsonObj != null) {
				return Response.status(Status.OK).entity(jsonObj.toString()).build();
			} else {
				throw new ServiceException(Constants.NO_DATA_FOUND, Status.NOT_FOUND, Constants.NO_DATA_FOUND_ERR_CODE);
			}
		} catch (ServiceException se) {
			throw se;
		} catch (Exception e) {
			throw new ServiceException(WebConstant.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR, e,
					WebConstant.GENERIC_ERROR_CODE);
		}
	}

	/**
     * This API is used to update player bank details
     * 
     * @param payload
     *            { "value":{"agentName" : "Amitab", "ifscCode":"HDFC253252",
     *            "accountNumber":"873823678237" }, "context":{"source":"PSP"}} 
     * @return
     */
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@POST
	@MonitorCalls
	@ServiceAnnotation(apiIdentifier = Constants.UPDATE_USER_BANK_DETAILS, loggingRequired = true)
	@Path("/updatebankdetails/{" + Constants.USER_ID + "}")
	public Response updateUserBankDetails(@PathParam(Constants.USER_ID) long userId, String payload) {
		try {
			if (WebServiceUtil.isNullOrEmpty(payload)) {
				throw new ServiceException(WebConstant.NO_PAYLOAD, Status.BAD_REQUEST,
						WebConstant.EMPTY_OR_NULL_PAYLOAD);
			}
			JSONObject payloadJson = new JSONObject(payload);
			bankDetailsService.updateUserBankDetails(userId, payloadJson);
			return Response.status(Status.OK).build();
		} catch (JSONException se) {
			throw new ServiceException(WebConstant.JSON_FORMAT_EXCEPTION, Status.BAD_REQUEST, se,
					WebConstant.INVALID_JSON_FORMAT);
		} catch (ServiceException se) {
			throw se;
		} catch (Exception e) {
			throw new ServiceException(WebConstant.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR, e,
					WebConstant.GENERIC_ERROR_CODE);
		}
	}
}
