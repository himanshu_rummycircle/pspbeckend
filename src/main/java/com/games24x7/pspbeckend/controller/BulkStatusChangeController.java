package com.games24x7.pspbeckend.controller;

import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.serviceutil.annotations.ServiceAnnotation;
import com.games24x7.pspbeckend.Util.BulkStatusChangeUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Globals;
import com.games24x7.pspbeckend.pojo.BulkStatusChangeData;
import com.games24x7.pspbeckend.service.AccountService;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

/**
 * API for bulkstatuschange.
 */
@Component
@Path("/bulkstatuschange")
public class BulkStatusChangeController {
    private static final Logger logger = LoggerFactory.getLogger(CloseAndMergeAccountController.class);

    @Autowired
    BulkStatusChangeUtil util;

    @Autowired
    AccountService accService;

    /**
     * This API updates account status for multiple users in file. Uploaded file
     * should be in CSV format. First column in the file contains the userIds
     * and the 2nd column contains the comment to be added.
     * 
     * @param uploadedInputStream
     *            :uploaded File stream
     * @param agentName
     *            :Empty or Null Values are not allowed
     * @param agentId
     *            :only positive values are allowed
     * @param accountStatus
     *            :only positive values are allowed
     * @param fileDetail
     *            :uploaded File
     * @return Response
     */
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_HTML)
    @ServiceAnnotation(apiIdentifier = Constants.BULK_STATUS_CHANGE, loggingRequired = true)
    @MonitorCalls
    public Response changeAccountStatus(@FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("name") String agentName, @FormDataParam("agentId") Long agentId,
            @FormDataParam("accountStatus") Integer accountStatus,
            @FormDataParam("file") FormDataContentDisposition fileDetail) {
        logger.info("Api starts :" + agentName + " : agent Id :" + agentId + " file " + fileDetail);
        util.basicValidations(agentName, fileDetail, uploadedInputStream, agentId);
        BulkStatusChangeData data = util.generateData(fileDetail, agentName, agentId);
        data.setAccountStatus(accountStatus);

        util.dataValidation(data);
        accService.changeBulkAccountStatus(data, Globals.BULK_ACCOUNT_STATUS_CHANGE);

        return Response.status(Status.OK).entity(fileDetail.getFileName() + " has been processed successfully.")
                .build();

    }

}
