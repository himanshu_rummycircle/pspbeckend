package com.games24x7.pspbeckend.controller;

import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.serviceutil.annotations.ServiceAnnotation;
import com.games24x7.pspbeckend.Util.MergeAndCloseUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Globals;
import com.games24x7.pspbeckend.pojo.CloseAndMergeData;
import com.games24x7.pspbeckend.service.AccountService;
import com.games24x7.pspbeckend.service.ApprovalService;
import com.games24x7.pspbeckend.service.FundTransferService;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Component
@Path("/closeandmerge")
public class CloseAndMergeAccountController {
    private static final Logger logger = LoggerFactory.getLogger(CloseAndMergeAccountController.class);

    @Autowired
    ApprovalService approvalService;
    @Autowired
    Configuration config;
    @Autowired
    FundTransferService fService;
    @Autowired
    AccountService accService;
    @Autowired
    MergeAndCloseUtil util;

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_HTML)
    @ServiceAnnotation(apiIdentifier = Constants.CLOSE_AND_MERGE, loggingRequired = true)
    @MonitorCalls
    public Response closeAndMerge(@FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("name") String agentName, @FormDataParam("agentId") Long agentId,
            @FormDataParam("file") FormDataContentDisposition fileDetail) {
        util.basicValidations(agentName, fileDetail, uploadedInputStream, agentId);
        CloseAndMergeData data = util.mergeDataGeneration(fileDetail, agentName, agentId);
        util.mergeAccValidations(data);

        data = approvalService.generateApprovalAndMergeData(data);
        approvalService.processApproval(data);

        accService.processAccountClosure(data, Globals.CLOSE_AND_MERGE);
        logger.debug("After Closure MERGE - DATA **** " + data.getMergeAccData().size());

        fService.processAccountMerging(data);
        logger.info("SucessfulMergeAcc size : " + data.getSucessfulMergeAcc().size());
        return Response.status(Status.OK)
                .entity(fileDetail.getFileName()
                        + " uploaded for processing. You would be informed via email when processing completes.")
                .build();

    }
}
