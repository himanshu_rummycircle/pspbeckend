package com.games24x7.pspbeckend.controller;

import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.serviceutil.annotations.ServiceAnnotation;
import com.games24x7.pspbeckend.Util.CloseAndRefundUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Globals;
import com.games24x7.pspbeckend.pojo.CloseAndRefundData;
import com.games24x7.pspbeckend.service.AccountService;
import com.games24x7.pspbeckend.service.RefundService;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Component
@Path("/closeandrefund")
public class CloseAndRefundController {
    private static final Logger logger = LoggerFactory.getLogger(CloseAndMergeAccountController.class);

    @Autowired
    Configuration config;

    @Autowired
    CloseAndRefundUtil util;

    @Autowired
    RefundService refService;

    @Autowired
    AccountService accService;

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_HTML)
    @ServiceAnnotation(apiIdentifier = Constants.CLOSE_AND_REFUND, loggingRequired = true)
    @MonitorCalls
    public Response closeAndRefund(@FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("name") String agentName, @FormDataParam("agentId") Long agentId,
            @FormDataParam("file") FormDataContentDisposition fileDetail) {
        logger.info("closeAndRefund starts :" + agentName + " : agent Id :" + agentId + " file " + fileDetail);
        util.basicValidations(agentName, fileDetail, uploadedInputStream, agentId);
        CloseAndRefundData refundData = util.refundDataGeneration(fileDetail, agentName, agentId);

        util.refundDataValidation(refundData);
        accService.processAccountClosure(refundData, Globals.CLOSE_AND_REFUND);

        refService.processRefund(refundData);
        return Response.status(Status.OK).entity(fileDetail.getFileName() + " SucessFully Uploaded .").build();

    }

}
