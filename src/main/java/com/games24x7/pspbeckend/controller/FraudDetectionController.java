package com.games24x7.pspbeckend.controller;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.frameworks.serviceutil.annotations.ServiceAnnotation;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.RiskConstant;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;
import com.games24x7.pspbeckend.service.FraudService;

@Controller
@Path("/fraud")
public class FraudDetectionController {
    private static final Logger logger = LoggerFactory.getLogger(FraudDetectionController.class);

    @Autowired
    public FraudService service;

    /**
     * @queryparams status ( 1: Blacklist, 2: whitelist), limit (optional - For
     *              pagination) ,offset (optional - For pagination)
     * 
     *              or
     * 
     *              status ( 1: Blacklist, 2: whitelist), deviceId
     * 
     * @return
     */
    @Produces(MediaType.APPLICATION_JSON)
    @ServiceAnnotation(apiIdentifier = Constants.GET_FRAUD_DEVICE, loggingRequired = true)
    @MonitorCalls
    @GET
    @Path("/getdevice")
    public Response getFraudDevices(@QueryParam(Constants.STATUS) int status, @QueryParam(Constants.LIMIT) int limit,
            @QueryParam(Constants.OFFSET) int offset, @QueryParam(Constants.DEVICE_ID) String deviceId) {
        try {
            JSONObject jsonObj = service.getFraudDevicesInfo(status, limit, offset, deviceId);
            return Response.status(Status.OK).entity(jsonObj.toString()).build();
        } catch (ServiceException se) {
            throw se;
        } catch (Exception e) {
            throw new ServiceException(WebConstant.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(), e,
                    WebConstant.GENERIC_ERROR_CODE);
        }
    }

    /**
     * 
     * @queryparam status
     * @param agentId
     * @param deviceId
     * @return
     */

    @Produces(MediaType.APPLICATION_JSON)
    @ServiceAnnotation(apiIdentifier = Constants.UPDATE_FRAUD_DEVICE, loggingRequired = true)
    @MonitorCalls
    @POST
    @Path("/updatedevice/{" + Constants.DEVICE_ID + "}")
    public Response updateDevice(@QueryParam(Constants.STATUS) int status, @QueryParam(Constants.AGENT_ID) String agent,
            @PathParam(Constants.DEVICE_ID) String deviceId) {
        try {
            service.updateFraudDevice(status, agent, deviceId);
            return Response.status(Status.OK)
                    .entity(new JSONObject().put(RiskConstant.STATUS, Constants.SUCCESS).toString()).build();
        } catch (ServiceException se) {
            throw se;
        } catch (Exception e) {
            throw new ServiceException(WebConstant.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(), e,
                    WebConstant.GENERIC_ERROR_CODE);
        }
    }
}
