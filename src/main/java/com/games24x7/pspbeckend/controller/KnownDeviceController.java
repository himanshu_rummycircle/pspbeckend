package com.games24x7.pspbeckend.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.serviceutil.annotations.ServiceAnnotation;
import com.games24x7.pspbeckend.Util.KnownDevicesUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.pojo.KnownDevice;
import com.games24x7.pspbeckend.service.KnownDevicesService;

@Component
@Path("/knownDevices")
public class KnownDeviceController {
    private static final Logger logger = LoggerFactory.getLogger(KnownDeviceController.class);

    @Autowired
    KnownDevicesUtil util;
    @Autowired
    KnownDevicesService knownDevicesService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ServiceAnnotation(apiIdentifier = Constants.KNOWN_DEVICES)
    public Response knownDevices(String payload) {
        logger.debug("received Object " + payload);
        KnownDevice knownDevice = util.getObject(payload);
        util.basicValidations(knownDevice);
        String response = knownDevicesService.processKnowDevices(knownDevice);
        logger.debug("received response " + response);
        if (response != null && !"".equals(response))
            return Response.status(Status.OK).entity(response).build();
        else
            return Response.status(Status.NOT_FOUND).entity("No devices Available").build();

    }

}
