package com.games24x7.pspbeckend.controller;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.frameworks.serviceutil.annotations.ServiceAnnotation;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.service.SatelliteTournamentService;

@Path("/mtt")
@Produces("application/json")
@Consumes("application/json")
@Service
public class MTTOfflineSatelliteTournamentController {
    private static final Logger logger = LoggerFactory.getLogger(MTTOfflineSatelliteTournamentController.class);

    @Autowired
    SatelliteTournamentService satelliteService;

    @POST
    @Path("/" + MTTSettlementConstant.OFFLINE_SETTLEMENT)
    @ServiceAnnotation(apiIdentifier = MTTSettlementConstant.OFFLINE_SETTLEMENT, loggingRequired = true)
    public Response mttOfflineSettlement(String payload) throws Exception {
        JSONObject jsonObject = null;
        try {
            if (payload == null || payload.trim().equals("")) {
                throw new ServiceException(MTTSettlementConstant.NO_PAYLOAD, Status.BAD_REQUEST,
                        MTTSettlementConstant.EMPTY_OR_NULL_PAYLOAD);
            }
            jsonObject = new JSONObject(payload);
            String response = satelliteService.processOfflineSatelliteSettlement(jsonObject);
            return Response.status(Status.OK).entity(response).build();
        } catch (JSONException exception) {
            throw customServiceException(new ServiceException(MTTSettlementConstant.JSON_FORMAT_EXCEPTION, Status.BAD_REQUEST,
                    MTTSettlementConstant.INVALID_JSON_FORMAT));
        } catch (ServiceException se) {
            throw customServiceException(se); 
        } catch (Exception e) {
            logger.error("Error while processing data : " + e.getMessage(), e);
            throw customServiceException( new ServiceException(MTTSettlementConstant.GENERIC_ERROR,
                    Status.INTERNAL_SERVER_ERROR,  MTTSettlementConstant.GENERIC_ERROR_CODE));
        }
    }

    private ServiceException customServiceException(ServiceException exception) {
        Map<String, Object> paramsMap= new HashMap<String,Object>();
        paramsMap.put(Constants.SUCCESS, false);
        exception.setParamsMap(paramsMap);
        return exception;
    }

}
