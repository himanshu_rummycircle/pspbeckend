package com.games24x7.pspbeckend.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.frameworks.serviceutil.annotations.ServiceAnnotation;
import com.games24x7.pspbeckend.Util.WebServiceUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.RiskConstant;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;
import com.games24x7.pspbeckend.service.PlayerUpdateService;

@Component
@Path("/playerupdate")
public class PlayerUpdatesController {
    private static final Logger logger = LoggerFactory.getLogger(PlayerUpdatesController.class);

    @Autowired
    public PlayerUpdateService service;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ServiceAnnotation(apiIdentifier = Constants.PROCESS_PLAYER_UPDATES_AUDIT, loggingRequired = true)
    @MonitorCalls
    public Response pushdatatoaudit(String payload) {
        try {
            if (WebServiceUtil.isNullOrEmpty(payload)) {
                throw new ServiceException(WebConstant.NO_PAYLOAD, Status.BAD_REQUEST.getStatusCode(),
                        WebConstant.EMPTY_OR_NULL_PAYLOAD);
            }
            JSONObject payloadJson = new JSONObject(payload);
            if (!payloadJson.has(Constants.VALUE)) {
                throw new ServiceException(Constants.VALUE_MISSING_FROM_PAYLOAD_MSG, Status.BAD_REQUEST.getStatusCode(),
                        Constants.VALUE_MISSING_FROM_PAYLOAD);
            }
            if (!payloadJson.has(Constants.CONTEXT)) {
                throw new ServiceException(Constants.CONTEXT_MISSING_FROM_PAYLOAD_MSG,
                        Status.BAD_REQUEST.getStatusCode(), Constants.CONTEXT_MISSING_FROM_PAYLOAD);
            }
            JSONObject value = payloadJson.getJSONObject(Constants.JSON_PAYLOAD_VALUE);
            JSONObject context = payloadJson.getJSONObject(Constants.CONTEXT);

            service.processAuditData(value, context);
            return Response.status(Status.OK)
                    .entity(new JSONObject().put(RiskConstant.STATUS, Constants.SUCCESS).toString()).build();
        } catch (JSONException exception) {
            throw new ServiceException(WebConstant.JSON_FORMAT_EXCEPTION, Status.BAD_REQUEST.getStatusCode(), exception,
                    WebConstant.INVALID_JSON_FORMAT);
        } catch (ServiceException se) {
            throw se;
        } catch (Exception e) {
            throw new ServiceException(WebConstant.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(), e,
                    WebConstant.GENERIC_ERROR_CODE);
        }

    }

}
