package com.games24x7.pspbeckend.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.frameworks.serviceutil.annotations.ServiceAnnotation;
import com.games24x7.pspbeckend.Util.WebServiceUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.RiskConstant;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;
import com.games24x7.pspbeckend.service.RGPService;

/**
 * This controller is responsible for all the risk related APIS.
 */
@Component
@Path("/rgp")
public class RGPController {
	private static final Logger logger = LoggerFactory.getLogger(RGPController.class);

	private static final String GET_RGP_STATUS = "/getStatus/{" + Constants.USERID + "}";
	private static final String GET_RGP_STATUS_CHANGE_LOG = "/getStatusChangeLog/{" + Constants.USERID + "}";
	private static final String UPDATE_RGP_STATUS = "/updateStatus/{" + Constants.USERID + "}";

	@Autowired
	RGPService rgpService;

	/**
	 * 
	 * @return Response received from RGP service (Contains Current RGP & Valid RGP
	 *         Status)
	 * @throws ServiceException
	 */
	@GET
	@Path(GET_RGP_STATUS)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ServiceAnnotation(apiIdentifier = GET_RGP_STATUS, loggingRequired = true)
	@MonitorCalls
	public Response getRGPStatus(@PathParam(Constants.USERID) long userId) {
		if (userId <= 0) {
			throw new ServiceException(Constants.MTTSettlementConstant.INVALID_USER_ID + userId,
					Status.BAD_REQUEST.getStatusCode(), Constants.MTTSettlementConstant.INVALID_USER_ID_CODE);
		}
		String response = rgpService.getRGPStatus(userId);
		if (response != null) {
			return Response.status(Status.OK).entity(response).build();
		} else {
			return Response.status(Status.NOT_FOUND).build();
		}
	}

	/**
	 * 
	 * @return Response received from Audit service (Contains Change logs of user
	 *         RGP Status)
	 * @throws ServiceException
	 */
	@GET
	@Path(GET_RGP_STATUS_CHANGE_LOG)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ServiceAnnotation(apiIdentifier = GET_RGP_STATUS_CHANGE_LOG, loggingRequired = true)
	@MonitorCalls
	public Response getRGPStatusChangeLog(@PathParam(Constants.USERID) long userId) {
		if (userId <= 0) {
			throw new ServiceException(Constants.MTTSettlementConstant.INVALID_USER_ID + userId,
					Status.BAD_REQUEST.getStatusCode(), Constants.MTTSettlementConstant.INVALID_USER_ID_CODE);
		}
		String response = rgpService.getRGPStatusChangeLog(userId);
		if (response != null) {
			return Response.status(Status.OK).entity(response).build();
		} else {
			return Response.status(Status.NOT_FOUND).build();
		}
	}

	/**
	 * This API Updates the RGP Status of the user
	 * 
	 * @param payload { "value":{ "changeStatusTo": 2, "contactType": "EMAIL",
	 *                "contactDirection": "In", "comments": "Testing" }, "context":{
	 *                "source":"PSP", "channelId":0 }
	 * @return
	 */
	@POST
	@Path(UPDATE_RGP_STATUS)
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ServiceAnnotation(apiIdentifier = UPDATE_RGP_STATUS, loggingRequired = true)
	@MonitorCalls
	public Response updateRGPStatus(@PathParam(Constants.USERID) long userId, String payload) {
		JSONObject jsonObject = null;
		try {
			if (userId <= 0) {
				throw new ServiceException(Constants.MTTSettlementConstant.INVALID_USER_ID + userId,
						Status.BAD_REQUEST.getStatusCode(), Constants.MTTSettlementConstant.INVALID_USER_ID_CODE);
			}
			if (WebServiceUtil.isNullOrEmpty(payload)) {
				throw new ServiceException(WebConstant.NO_PAYLOAD, Status.BAD_REQUEST.getStatusCode(),
						WebConstant.EMPTY_OR_NULL_PAYLOAD);
			}
			jsonObject = new JSONObject(payload);
			rgpService.updateRGPStatus(userId, jsonObject);
			return Response.status(Status.OK).build();
		} catch (JSONException exception) {
			logger.info(RiskConstant.JSON_EXCEPTION, exception);
			throw new ServiceException(WebConstant.JSON_FORMAT_EXCEPTION, Status.BAD_REQUEST.getStatusCode(), exception,
					WebConstant.INVALID_JSON_FORMAT);
		} catch (ServiceException se) {
			throw se;
		} catch (Exception e) {
			logger.error(Constants.GENERIC_ERROR_MESSAGE_PRINT + e.getMessage(), e);
			throw new ServiceException(WebConstant.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(),
					WebConstant.GENERIC_ERROR_CODE);
		}

	}
}
