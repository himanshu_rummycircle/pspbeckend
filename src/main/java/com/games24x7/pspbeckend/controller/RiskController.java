package com.games24x7.pspbeckend.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.frameworks.serviceutil.annotations.ServiceAnnotation;
import com.games24x7.pspbeckend.Util.WebServiceUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.RiskConstant;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;
import com.games24x7.pspbeckend.service.RiskService;

/**
 * This controller is responsible for all the risk related APIS.
 */
@Component
@Path("/risk")
public class RiskController {

    private static final Logger logger = LoggerFactory.getLogger(RiskController.class);

    private static final String GET_RISK_STATUS_LOGS_AND_CERTIFICATES = "/getRiskStatusRiskLogAndCertificates";

    @Autowired
    RiskService riskService;

    /**
     * This API is used to Create a risk certificate
     * 
     * @param payload
     *            { "agentId" : 99, "riskCertificateName":"certificate2017",
     *            "riskStatus":"Not Risky" }
     * @return
     */
    @POST
    @Path("/" + RiskConstant.CREATE_CERTIFICATE)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ServiceAnnotation(apiIdentifier = RiskConstant.CREATE_CERTIFICATE, loggingRequired = true)
    @MonitorCalls
    public Response createRiskCertificate(String payload) {

        JSONObject jsonObject = null;
        try {
            if (WebServiceUtil.isNullOrEmpty(payload)) {
                throw new ServiceException(WebConstant.NO_PAYLOAD, Status.BAD_REQUEST.getStatusCode(),
                        WebConstant.EMPTY_OR_NULL_PAYLOAD);
            }
            jsonObject = new JSONObject(payload);
            String response = riskService.createRiskCertificate(jsonObject);
            return Response.status(Status.OK).entity(response).build();
        } catch (JSONException exception) {
            logger.info(RiskConstant.JSON_EXCEPTION, exception);
            throw new ServiceException(WebConstant.JSON_FORMAT_EXCEPTION, Status.BAD_REQUEST.getStatusCode(), exception,
                    WebConstant.INVALID_JSON_FORMAT);
        } catch (ServiceException se) {
            throw se;
        } catch (Exception e) {
            logger.error(Constants.GENERIC_ERROR_MESSAGE_PRINT + e.getMessage(), e);
            throw new ServiceException(WebConstant.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(),e,
                    WebConstant.GENERIC_ERROR_CODE);
        }

    }

    /**
     * This API gets all the Risk Certificates on the basis of riskType.
     * riskType value can be 0- InActive, 1- Active, 2-All
     * 
     * @param riskType
     * @param offset
     * @return
     */
    @GET
    @Path("/" + RiskConstant.GET_CERTIFICATE + "/{riskType}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ServiceAnnotation(apiIdentifier = RiskConstant.GET_CERTIFICATE, loggingRequired = true)
    @MonitorCalls
    public Response getRiskCertificate(@PathParam("riskType") int riskType, @QueryParam("offset") long offset) {
        try {
            JSONArray jsonRiskArray = riskService.getRiskCertificate(riskType, offset);
            return Response.status(Status.OK).entity(jsonRiskArray.toString()).build();
        } catch (JSONException exception) {
            logger.info(RiskConstant.JSON_EXCEPTION, exception);
            throw new ServiceException(WebConstant.JSON_FORMAT_EXCEPTION, Status.BAD_REQUEST.getStatusCode(), exception,
                    WebConstant.INVALID_JSON_FORMAT);
        } catch (ServiceException se) {
            throw se;
        } catch (Exception e) {
            logger.error(Constants.GENERIC_ERROR_MESSAGE_PRINT + e.getMessage(), e);
            throw new ServiceException(WebConstant.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                    WebConstant.GENERIC_ERROR_CODE);
        }

    }

    /**
     * This API Updates the risk Certificates to Active or Inactive
     * 
     * @param payload
     *            { "agentId" : 99, "riskCertificateIds":"11,14",
     *            "activate":"true" }
     * 
     *            activate value can either be true or false.
     * @return
     */
    @POST
    @Path("/" + RiskConstant.UPDATE_CERTIFICATE)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ServiceAnnotation(apiIdentifier = RiskConstant.UPDATE_CERTIFICATE, loggingRequired = true)
    @MonitorCalls
    public Response updateRiskCertificate(String payload) {
        JSONObject jsonObject = null;
        try {
            if (WebServiceUtil.isNullOrEmpty(payload)) {
                throw new ServiceException(WebConstant.NO_PAYLOAD, Status.BAD_REQUEST.getStatusCode(),
                        WebConstant.EMPTY_OR_NULL_PAYLOAD);
            }
            jsonObject = new JSONObject(payload);
            JSONArray jsonRiskArray = riskService.updateRiskCertificate(jsonObject);
            return Response.status(Status.OK).entity(jsonRiskArray.toString()).build();
        } catch (JSONException exception) {
            logger.info(RiskConstant.JSON_EXCEPTION, exception);
            throw new ServiceException(WebConstant.JSON_FORMAT_EXCEPTION, Status.BAD_REQUEST.getStatusCode(), exception,
                    WebConstant.INVALID_JSON_FORMAT);
        } catch (ServiceException se) {
            throw se;
        } catch (Exception e) {
            logger.error(Constants.GENERIC_ERROR_MESSAGE_PRINT + e.getMessage(), e);
            throw new ServiceException(WebConstant.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                    WebConstant.GENERIC_ERROR_CODE);
        }

    }

    /**
     * 
     * @param payload
     *            sample json { "riskCertificateNames": "riskCertificateNames",
     *            "contactType": "this.refs.contype.value", "contactDirection":
     *            "this.refs.condir.value", "interactionId":
     *            "this.refs.cmrid.value", "comment": "this.refs.cmnt.value",
     *            "riskStatus": "Risky", "userId": "200", "agentId":
     *            "agentLoginId", "reasonForDowngrade":
     *            "this.refs.downgradeValue.value", "riskStatusComment":
     *            "riskCertificateValue"
     * 
     * 
     * 
     *            }
     * @return
     */
    @POST
    @Path("/" + RiskConstant.UPDATE_PLAYERS_STATUS)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ServiceAnnotation(apiIdentifier = RiskConstant.UPDATE_PLAYERS_STATUS, loggingRequired = true)
    @MonitorCalls
    public Response updatePlayersRiskStatusAndAuditLogCreation(String payload) {
        JSONObject jsonObject = null;
        try {
            if (WebServiceUtil.isNullOrEmpty(payload)) {
                throw new ServiceException(WebConstant.NO_PAYLOAD, Status.BAD_REQUEST.getStatusCode(),
                        WebConstant.EMPTY_OR_NULL_PAYLOAD);
            }
            jsonObject = new JSONObject(payload);
            String response = riskService.updatePlayersRiskStatus(jsonObject);
            return Response.status(Status.OK).entity(response).build();
        } catch (JSONException exception) {
            logger.info(RiskConstant.JSON_EXCEPTION, exception);
            throw new ServiceException(WebConstant.JSON_FORMAT_EXCEPTION, Status.BAD_REQUEST.getStatusCode(), exception,
                    WebConstant.INVALID_JSON_FORMAT);
        } catch (ServiceException se) {
            throw se;
        } catch (Exception e) {
            logger.error(Constants.GENERIC_ERROR_MESSAGE_PRINT + e.getMessage(), e);
            throw new ServiceException(WebConstant.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                    WebConstant.GENERIC_ERROR_CODE);
        }

    }

    /**
     * 
     * @param payloadString
     * 
     *            { "value": { "userId": 200,"riskStatus":true,"riskLogs":true,
     *            "riskCertificates":true }, "context": { "channelId": null,
     *            "source": "PSP", "transactionId": "abcd1234", "agentId":12345
     *            } }
     * @return
     * @throws ServiceException
     */
    @POST
    @Path(GET_RISK_STATUS_LOGS_AND_CERTIFICATES)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ServiceAnnotation(apiIdentifier = Constants.RISK_STATUS_AND_LOG, loggingRequired = true)
    @MonitorCalls
    public Response getRiskStatusLogsAndCertificates(String payloadString) throws ServiceException {

        try {
            if (WebServiceUtil.isNullOrEmpty(payloadString)) {
                throw new ServiceException(WebConstant.NO_PAYLOAD, Status.BAD_REQUEST.getStatusCode(),
                        Constants.PAYLOAD_NULL_OR_EMPTY_CODE);
            }
            JSONObject payloadJson = new JSONObject(payloadString);
            if (!payloadJson.has(Constants.VALUE)) {
                throw new ServiceException(Constants.VALUE_MISSING_FROM_PAYLOAD_MSG, Status.BAD_REQUEST.getStatusCode(),
                        Constants.VALUE_MISSING_FROM_PAYLOAD);
            }
            JSONObject value = payloadJson.getJSONObject(Constants.VALUE);
            if (value.has(Constants.USERID)) {
                long userId = value.getLong(Constants.USERID);
                boolean riskStatus = false, riskLogs = false, riskCertificates = false;
                if (value.has(Constants.RISK_STATUS))
                    riskStatus = value.getBoolean(Constants.RISK_STATUS);
                if (value.has(Constants.JSON_RISK_LOGS))
                    riskLogs = value.getBoolean(Constants.JSON_RISK_LOGS);
                if (value.has(Constants.JSON_RISK_CERTIFICATES))
                    riskCertificates = value.getBoolean(Constants.JSON_RISK_CERTIFICATES);
                riskService.loadRiskCertifiatesInfo();
                return Response.status(Status.OK).entity(
                        riskService.fetchRiskStatusLogsAndCertificates(userId, riskStatus, riskLogs, riskCertificates))
                        .build();
            } else {
                throw new ServiceException(Constants.USER_ID_MISSING_FROM_PAYLOAD_MSG,
                        Status.BAD_REQUEST.getStatusCode(), Constants.USER_ID_MISSING_FROM_PAYLOAD);
            }

        } catch (ServiceException se) {
            throw se;
        } catch (JSONException jse) {
            throw new ServiceException(Constants.IMPROPER_PAYLOAD_MSG, Status.BAD_REQUEST.getStatusCode(), jse,
                    Constants.GENERIC_IMPROPER_PAYLOAD_CODE);
        } catch (Exception e) {
            throw new ServiceException(Constants.OTHER_ERROR_MSG, Status.INTERNAL_SERVER_ERROR.getStatusCode(), e,
                    Constants.ERROR_IN_PROCESSING_REQUEST_CODE);
        }

    }

}
