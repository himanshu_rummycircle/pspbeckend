/**
 * 
 */
package com.games24x7.pspbeckend.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.frameworks.serviceutil.annotations.ServiceAnnotation;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.RiskConstant;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;
import com.games24x7.pspbeckend.service.MobileService;

/**
 * 
 */
@Component
@Path("/user")
public class SearchViaMobileController {
    private static final Logger logger = LoggerFactory.getLogger(SearchViaMobileController.class);

    @Autowired
    MobileService service;

    /**
     * This API is used to fetch userId for the provided Mobile Number. 1)
     * Mobile Verified & Active Account then Single User will display 2) Mobile
     * verified & Non-Active Account then sort by Last login user 3) Mobile
     * non-verified & Active account then show active users sort by Highest club
     * then by last login. 4) Mobile non-verified & Non-Active Account then show
     * Blocked or Closed account sort by last login.
     * 
     * @param mobileNo
     * @return {"userId" :1523}
     */
    @GET
    @Path("/" + Constants.GET_USER_MOBILE + "/{mobileNo}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ServiceAnnotation(apiIdentifier = Constants.GET_USER_MOBILE, loggingRequired = true)
    @MonitorCalls
    public Response getUserDatails(@PathParam("mobileNo") long mobileNo) {
        try {
            long userId = service.getUserData(mobileNo);
            JSONObject json = new JSONObject();
            json.put("userId", userId);
            return Response.status(Status.OK).entity(json.toString()).build();
        } catch (ServiceException se) {
            throw se;
        } catch (Exception e) {
            logger.error(Constants.GENERIC_ERROR_MESSAGE_PRINT + e.getMessage(), e);
            throw new ServiceException(WebConstant.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                    WebConstant.GENERIC_ERROR_CODE);
        }

    }

}
