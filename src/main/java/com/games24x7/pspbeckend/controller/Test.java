package com.games24x7.pspbeckend.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.exception.ServiceException;

@Component
@Path("/test")
public class Test {
    private static final Logger logger = LoggerFactory.getLogger(Test.class);

    @GET
    @Produces("text/html")
    public Response getStartingPage() {
        String output = "<h1>Hello World!<h1>" + "<p>RESTful Service is running ... <br>Ping @ " + new Date().toString()
                + "</p<br>";
        throw new ServiceException("This is a text ex ", Status.BAD_REQUEST);
        // return Response.status(200).entity(output).build();
    }

    @GET()
    @Path("/swagger")
    @Produces({ MediaType.APPLICATION_JSON })
    public InputStream viewHome2() {

        try {
            File f = new File(getClass().getClassLoader().getResource("/swagger.json").getFile());
            return new FileInputStream(f);
        } catch (FileNotFoundException e) {
            logger.error("", e);
        }
        return null;
    }
    @GET()
    @Path("/detailedswagger")
    @Produces({ MediaType.APPLICATION_JSON })
    public InputStream viewswagger() {

        try {
            File f = new File(getClass().getClassLoader().getResource("/modifiedswagger.json").getFile());
            return new FileInputStream(f);
        } catch (FileNotFoundException e) {
            logger.error("", e);
        }
        return null;
    }

    @GET
    @Path("/index")
    @Produces({ MediaType.TEXT_HTML })
    public InputStream viewHome() {
        try

        {
            File f = new File(getClass().getClassLoader().getResource("/swaggerUI.html").getFile());
            return new FileInputStream(f);
        } catch (

        FileNotFoundException e)

        {
            logger.error("", e);
        }
        return null;
    }

}
