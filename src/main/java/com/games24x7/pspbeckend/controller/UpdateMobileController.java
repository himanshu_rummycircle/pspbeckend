package com.games24x7.pspbeckend.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.frameworks.serviceutil.annotations.ServiceAnnotation;
import com.games24x7.pspbeckend.Util.WebServiceUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;
import com.games24x7.pspbeckend.pojo.WebResponse;
import com.games24x7.pspbeckend.service.UpdateMobileService;

@Controller
@Path("/updateMobile")
public class UpdateMobileController {

    private static final Logger logger = LoggerFactory.getLogger(UpdateMobileController.class);

    @Autowired
    UpdateMobileService updateMobileService;

    /**
     * This API is used to update user's mobile number
     * 
     * @param userId
     * @param payload
     *            {"context":{"source":"PSP"},"value":{"notifyType":4,"loginid":"ABCD",
     *            "notifyTypeUpdatedSucessfully":2,"mobile":7598214203,
     *            "email":"_tester_Dv5elyCe@email.com"}}
     * @return
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @MonitorCalls
    @ServiceAnnotation(apiIdentifier = Constants.UPDATE_MOBILE_API, loggingRequired = true)
    @Path("/{userId}")
    public Response updateMobile(@PathParam("userId") long userId, String payload) {
        try {
            if (WebServiceUtil.isNullOrEmpty(payload)) {
                throw new ServiceException(WebConstant.NO_PAYLOAD, Status.BAD_REQUEST.getStatusCode(),
                        WebConstant.EMPTY_OR_NULL_PAYLOAD);
            }
            JSONObject payloadJson = new JSONObject(payload);
            WebResponse webResponse = updateMobileService.updateMobile(userId, payloadJson);
            return Response.status(webResponse.getStatusCode()).entity(webResponse.getPayload()).build();
        } catch (JSONException se) {
            throw new ServiceException(WebConstant.JSON_FORMAT_EXCEPTION, Status.BAD_REQUEST.getStatusCode(), se,
                    WebConstant.INVALID_JSON_FORMAT);
        } catch (ServiceException se) {
            throw se;
        } catch (Exception e) {
            throw new ServiceException(WebConstant.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(), e,
                    WebConstant.GENERIC_ERROR_CODE);
        }
    }

}
