package com.games24x7.pspbeckend.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.RecoverableDataAccessException;
import org.springframework.dao.TransientDataAccessException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Repository;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.pspbeckend.constants.Constants.DBConstant;
import com.games24x7.pspbeckend.mapper.AlertMapper;
import com.games24x7.pspbeckend.pojo.Alert;

@Repository
public class AlertDao {
    private static final Logger logger = LoggerFactory.getLogger(AlertDao.class);
    @Autowired
    SqlSessionTemplate sqlSessionMaster;

    @Retryable(include = { RecoverableDataAccessException.class,
            TransientDataAccessException.class }, maxAttempts = DBConstant.DB_RETRY_COUNT_DEFAULT, backoff = @Backoff(delay = DBConstant.DEFAULT_RETRY_PAUSE))
    @MonitorCalls(methodName = "createAlertDB")
    public long createAlert(Alert alert) {
        sqlSessionMaster.getMapper(AlertMapper.class).createAlert(alert);
        return alert.getAlert_id();
    }

    @Retryable(include = { RecoverableDataAccessException.class,
            TransientDataAccessException.class }, maxAttempts = DBConstant.DB_RETRY_COUNT_DEFAULT, backoff = @Backoff(delay = DBConstant.DEFAULT_RETRY_PAUSE))
    @MonitorCalls(methodName = "UpdateAlertDB")
    public long updateAlert(long alertId, Alert alert) {
        return sqlSessionMaster.getMapper(AlertMapper.class).updateAlert(alertId, alert);
    }

    @Retryable(include = { RecoverableDataAccessException.class,
            TransientDataAccessException.class }, maxAttempts = DBConstant.DB_RETRY_COUNT_DEFAULT, backoff = @Backoff(delay = DBConstant.DEFAULT_RETRY_PAUSE))
    @MonitorCalls(methodName = "UpdateAlertCommentDB")
    public long updateAlertComment(long alertId, Alert alert) {
        return sqlSessionMaster.getMapper(AlertMapper.class).updateAlertComment(alertId, alert);
    }

    @Retryable(include = { RecoverableDataAccessException.class,
            TransientDataAccessException.class }, maxAttempts = DBConstant.DB_RETRY_COUNT_DEFAULT, backoff = @Backoff(delay = DBConstant.DEFAULT_RETRY_PAUSE))
    @MonitorCalls(methodName = "getAlertDB")
    public Alert getAlertData(long alertId) {
        return sqlSessionMaster.getMapper(AlertMapper.class).getAlert(alertId);
    }

}
