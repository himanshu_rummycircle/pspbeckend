package com.games24x7.pspbeckend.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.games24x7.pspbeckend.mapper.ApprovalMapper;
import com.games24x7.pspbeckend.pojo.ApprovalDetailsDummy;
import com.games24x7.pspbeckend.pojo.RequestedDetails;

@Repository
public class ApprovalDAO {
    private static final Logger logger = LoggerFactory.getLogger(ApprovalDAO.class);

    @Autowired
    SqlSessionTemplate sqlSessionMasterBatch;

    public void generateApprovalId(List<ApprovalDetailsDummy> list) {
        SqlSession session = sqlSessionMasterBatch.getSqlSessionFactory().openSession(ExecutorType.BATCH);
        list.forEach(dummy -> {
            session.getMapper(ApprovalMapper.class).insertIntoApprovalDetails(dummy);
        });
        session.flushStatements();
        session.commit();
        session.close();
        logger.debug("Res Batch After : " + list.get(0).getId());
        list.forEach(dummy -> {
            dummy.getRequestedDetails().setApprovdetailsid(dummy.getId());
            dummy.setApprovdetailsid(dummy.getId());
        });

    }

    public void generateDataShowLogAndAccMergingLog(List<ApprovalDetailsDummy> list) {
        List<RequestedDetails> reqDetails = new ArrayList<>();

        SqlSession session = sqlSessionMasterBatch.getSqlSessionFactory().openSession(ExecutorType.BATCH);
        list.forEach(dummy -> {
            session.getMapper(ApprovalMapper.class).insertIntoDataShowLog(dummy);
            logger.debug("Res Batch : " + dummy.getId());
            reqDetails.add(dummy.getRequestedDetails());
        });

        reqDetails.forEach(reqDet -> {
            session.getMapper(ApprovalMapper.class).insertIntoProcessrefundAccMergrLog(reqDet);
        });
        session.flushStatements();
        session.commit();
        session.close();
        logger.debug("Res Batch After generateDataShowLog: " + list.get(0).getId());
    }

}
