package com.games24x7.pspbeckend.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.games24x7.pspbeckend.mapper.BlockUsersMappingMapper;

@Repository
public class BlockUsersMappingDao {
    private static final Logger logger = LoggerFactory.getLogger(BlockUsersMappingDao.class);
    @Autowired
    SqlSessionTemplate sqlSessionMaster;

    public long deleteUserMapping(long userId) {
        return sqlSessionMaster.getMapper(BlockUsersMappingMapper.class).deleteFromBlockUsersMapping(userId);
    }

    public void saveBlockUserData(long userId, int headerId, String header, String message) {
        sqlSessionMaster.getMapper(BlockUsersMappingMapper.class).saveBlockUsersMappingData(userId, headerId, message);
    }
}
