package com.games24x7.pspbeckend.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.games24x7.pspbeckend.mapper.IdentityInfoMapper;
import com.games24x7.pspbeckend.pojo.IdentityInfo;

@Repository
public class IdentityInfoDao {
    private static final Logger logger = LoggerFactory.getLogger(IdentityInfoDao.class);
    @Autowired
    SqlSessionTemplate sqlSessionMaster;

    public long insertIntoIdentityInfo(IdentityInfo obj) {
        sqlSessionMaster.getMapper(IdentityInfoMapper.class).insertIntoIdentityInfo(obj);
        return obj.getId();
    }


}
