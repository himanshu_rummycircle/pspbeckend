package com.games24x7.pspbeckend.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.games24x7.pspbeckend.mapper.PSPCommentsMapper;
import com.games24x7.pspbeckend.pojo.PSPComments;

@Repository
public class PSPCommentsDao {
    private static final Logger logger = LoggerFactory.getLogger(PSPCommentsDao.class);
    @Autowired
    SqlSessionTemplate sqlSessionMaster;

    public long insertIntoPspComments(PSPComments obj) {
        sqlSessionMaster.getMapper(PSPCommentsMapper.class).insertIntoPspComments(obj);
        return obj.getId();
    }
}
