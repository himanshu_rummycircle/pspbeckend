package com.games24x7.pspbeckend.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.games24x7.pspbeckend.mapper.PlayerUpdatesMapper;
import com.games24x7.pspbeckend.pojo.PlayerUpdates;

@Repository
public class PlayerUpdatesDAO {
    private static final Logger logger = LoggerFactory.getLogger(PlayerUpdatesDAO.class);
    @Autowired
    SqlSessionTemplate sqlSessionMaster;

    public void insertIntoPlayerUpdatesDB(List<PlayerUpdates> list) {
        logger.debug("PlayerUpdate insert batch size  : " + list.size());
        sqlSessionMaster.getMapper(PlayerUpdatesMapper.class).playerUpdateInsertion(list);
    }

    public long insertIntoPlayerUpdatesDB(PlayerUpdates obj) {
        sqlSessionMaster.getMapper(PlayerUpdatesMapper.class).insertIntoPlayerUpdates(obj);
        return obj.getId();
    }
}
