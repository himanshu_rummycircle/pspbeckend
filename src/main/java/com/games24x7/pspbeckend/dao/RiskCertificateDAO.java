package com.games24x7.pspbeckend.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.core.Response.Status;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.RecoverableDataAccessException;
import org.springframework.dao.TransientDataAccessException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.RiskCertificateHelper;
import com.games24x7.pspbeckend.constants.Constants.DBConstant;
import com.games24x7.pspbeckend.constants.Constants.RiskConstant;
import com.games24x7.pspbeckend.mapper.RiskCertificateMapper;
import com.games24x7.pspbeckend.pojo.RiskCertificate;
import com.games24x7.pspbeckend.pojo.RiskType;
import com.games24x7.pspbeckend.pojo.UpdateRiskCertificate;

@Repository
public class RiskCertificateDAO {

    private static final Logger logger = LoggerFactory.getLogger(RiskCertificateHelper.class);
    
    @Autowired
    SqlSessionTemplate sqlSessionMaster;

    @Autowired
    SqlSessionTemplate sqlSessionMasterBatch;

    @Autowired
    Configuration config;

    @Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = DBConstant.DB_RETRY_COUNT_DEFAULT, backoff = @Backoff(delay = DBConstant.DEFAULT_RETRY_PAUSE))
    public boolean checkIfValidRiskStatus(String riskStatus) {
        boolean isValid = false;
        int countRishStatus = sqlSessionMaster.getMapper(RiskCertificateMapper.class).checkIfValidStatus(riskStatus);
        if (countRishStatus > 0) {
            isValid = true;
        }
        return isValid;
    }

    @Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = DBConstant.DB_RETRY_COUNT_DEFAULT, backoff = @Backoff(delay = DBConstant.DEFAULT_RETRY_PAUSE))
    @MonitorCalls(methodName="createRiskCertificateDB")
    public void createRiskCertificate(RiskCertificate riskPayload) {
        sqlSessionMaster.getMapper(RiskCertificateMapper.class).createRiskCertificate(riskPayload);
    }


    public HashMap<String, Integer> getRiskMap(List<RiskType> rType) {
        HashMap<String, Integer> riskMap = new HashMap<String, Integer>();
        for (RiskType risk : rType) {
            riskMap.put(risk.getRiskName(), risk.getRiskId());
        }
        return riskMap;
    }

    @Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = DBConstant.DB_RETRY_COUNT_DEFAULT, backoff = @Backoff(delay = DBConstant.DEFAULT_RETRY_PAUSE))
    @MonitorCalls(methodName="getRiskCertificateFromDB")
    public List<RiskCertificate> getRiskCertificate(int riskType, long offset) {
        List<RiskCertificate> riskList = null;
        long limit = config.getLong(DBConstant.QUERY_LIMIT, DBConstant.QUERY_LIMIT_DEFAULT);
        if (riskType == RiskConstant.ALL_RISK_TYPE) {
            riskList = sqlSessionMaster.getMapper(RiskCertificateMapper.class).getRiskCertificatesAll(limit,
                    offset);
        } else {
            riskList = sqlSessionMaster.getMapper(RiskCertificateMapper.class).getRiskCertificates(riskType,
                    limit, offset);
        }
        return riskList;
    }

    @Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = DBConstant.DB_RETRY_COUNT_DEFAULT, backoff = @Backoff(delay = DBConstant.DEFAULT_RETRY_PAUSE))
    public int distinctRiskId(String riskIdString) {
        return  sqlSessionMaster.getMapper(RiskCertificateMapper.class).distinctRiskId(riskIdString);
    }
    @MonitorCalls(methodName="updateRiskCertificateDB")
    @Transactional
    public void updateRiskCertificate(List<Long> riskIdList, Boolean activate) {
        SqlSession session = sqlSessionMasterBatch.getSqlSessionFactory().openSession(ExecutorType.BATCH);
        riskIdList.forEach(riskId -> {
            if(activate==true){
                session.getMapper(RiskCertificateMapper.class).updateRiskCertificate(riskId, activate,false);
            }else{
                session.getMapper(RiskCertificateMapper.class).updateRiskCertificate(riskId, activate,true);
            }
            
        });
        session.flushStatements();
        session.commit();
        session.close();
    }

    @Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = DBConstant.DB_RETRY_COUNT_DEFAULT, backoff = @Backoff(delay = DBConstant.DEFAULT_RETRY_PAUSE))
    public List<Long> getRiskCertificateName(String riskCertificateIds) {
        return sqlSessionMaster.getMapper(RiskCertificateMapper.class).getRiskCertificateName(
                riskCertificateIds);
    }
    @Transactional(isolation = Isolation.READ_COMMITTED,rollbackFor = Exception.class)
    public void updateRiskCertificateInDB(UpdateRiskCertificate riskPayload) {

        int batchSize = config.getInt(RiskConstant.BATCH_UPDATE, RiskConstant.DEFAULT_BATCH_UPDATE);
        List<String> riskIdList = Arrays.asList(riskPayload.getRiskCertificateIds().split(","));
        List<Long> riskIdTemp = new ArrayList<Long>();
        try {
            for (int i = 0; i < riskIdList.size(); i++) {
                riskIdTemp.add(Long.parseLong(riskIdList.get(i)));
                if (i == (riskIdList.size() - 1) || riskIdList.size() == batchSize) {
                    updateRiskCertificate(riskIdTemp, riskPayload.getActivate());
                    riskIdTemp = new ArrayList<Long>();
                }
            }
        } catch (Exception ex) {
            logger.error("Exception while doing DB operation updateRiskDB " + ex);
            throw new ServiceException(RiskConstant.DATABASE_OPERATION_ERROR, Status.BAD_REQUEST, ex,
                    RiskConstant.DATABASE_OPERATION_ERROR_CODE);
        }

    }

}
