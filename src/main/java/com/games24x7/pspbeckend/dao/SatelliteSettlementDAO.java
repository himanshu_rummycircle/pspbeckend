package com.games24x7.pspbeckend.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.RecoverableDataAccessException;
import org.springframework.dao.TransientDataAccessException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.pspbeckend.constants.Constants.DBConstant;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.mapper.SatelliteSettlementMapper;
import com.games24x7.pspbeckend.pojo.OfflinePrize;
import com.games24x7.pspbeckend.pojo.OfflineSettlement;
import com.games24x7.pspbeckend.pojo.OfflineSettlementPayload;

@Repository
public class SatelliteSettlementDAO {

    private static final Logger logger = LoggerFactory.getLogger(SatelliteSettlementDAO.class);
    @Autowired
    SqlSessionTemplate sqlSessionMaster;

    @Autowired
    SqlSessionTemplate sqlSessionMasterBatch;

    @Autowired
    Configuration config;

    @Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = DBConstant.DB_RETRY_COUNT_DEFAULT, backoff = @Backoff(delay = DBConstant.DEFAULT_RETRY_PAUSE))
    public Map<String, Object> getTournamentInfo(long tournamentId) {
        return sqlSessionMaster.getMapper(SatelliteSettlementMapper.class).getTournamentInfo(tournamentId);
    }

    @Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = DBConstant.DB_RETRY_COUNT_DEFAULT, backoff = @Backoff(delay = DBConstant.DEFAULT_RETRY_PAUSE))
    public double getTournamentQualifierContribution(long tournamentId) {
        return sqlSessionMaster.getMapper(SatelliteSettlementMapper.class).getTournamentQualifierContribution(tournamentId);
    }

    @Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = DBConstant.DB_RETRY_COUNT_DEFAULT, backoff = @Backoff(delay = DBConstant.DEFAULT_RETRY_PAUSE))
    public void insertTempTableIfNotThere() {
        sqlSessionMaster.getMapper(SatelliteSettlementMapper.class).insertTempTableIfNotThere();
    }

    @Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = DBConstant.DB_RETRY_COUNT_DEFAULT, backoff = @Backoff(delay = DBConstant.DEFAULT_RETRY_PAUSE))
    public void insertPlayersSettlementDetailsTempAndInPlayAccount(List<OfflineSettlement> tempPrizelist) {
        SqlSession session = sqlSessionMasterBatch.getSqlSessionFactory().openSession(ExecutorType.BATCH);
        tempPrizelist.forEach(prize -> {
            session.getMapper(SatelliteSettlementMapper.class).insertPlayersSettlementDetailsTemp(prize);
            session.getMapper(SatelliteSettlementMapper.class).insertInPlayAccountPlayers(prize);
        });
        session.flushStatements();
        session.commit();
        session.close(); 
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void processOfflineSettlement(OfflineSettlementPayload payload, double prizePool) {
        insertTempTableIfNotThere();
        insertDataInTempTable(payload);
        sqlSessionMaster.getMapper(SatelliteSettlementMapper.class)
                .callOfflineSettlementSP(payload.getTournamentId(), prizePool);
    }
    @Retryable(include = { RecoverableDataAccessException.class, TransientDataAccessException.class }, maxAttempts = DBConstant.DB_RETRY_COUNT_DEFAULT, backoff = @Backoff(delay = DBConstant.DEFAULT_RETRY_PAUSE))
    public void insertDataInTempTable(OfflineSettlementPayload payload) {
        logger.debug("In insertDataInTempTable().");
        List<OfflinePrize> prizelist = payload.getPrizelist();
        List<OfflineSettlement> tempPrizelist = new ArrayList<OfflineSettlement>();
        double tdsLimit = Double.parseDouble(config.get(MTTSettlementConstant.TDS_NET_WINNINGS_LIMIT));
        double tdsValue = Double.parseDouble(config.get(MTTSettlementConstant.TDS_PERCENT_VALUE));

        for (int i = 0; i < prizelist.size(); i++) {
            tempPrizelist.add(new OfflineSettlement(prizelist.get(i).getUserId(), prizelist.get(i).getRank(),
                    prizelist.get(i).getPrizeAmount(), payload.getTournamentId(), MTTSettlementConstant.MINUS_ONE,
                    tdsLimit, tdsValue));
            if (i == (prizelist.size() - 1) || tempPrizelist.size() == 1000) {
                insertPlayersSettlementDetailsTempAndInPlayAccount(tempPrizelist);
                tempPrizelist = new ArrayList<OfflineSettlement>();
            }
        }
    }
}
