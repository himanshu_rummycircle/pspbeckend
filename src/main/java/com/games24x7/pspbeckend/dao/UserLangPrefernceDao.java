package com.games24x7.pspbeckend.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.games24x7.pspbeckend.mapper.IdentityInfoMapper;
import com.games24x7.pspbeckend.mapper.UserLangPrefernceMapper;
import com.games24x7.pspbeckend.pojo.IdentityInfo;

@Repository
public class UserLangPrefernceDao {
    private static final Logger logger = LoggerFactory.getLogger(UserLangPrefernceDao.class);
    @Autowired
    SqlSessionTemplate sqlSessionMaster;

    public Integer getUserLanguagePrefence(long userId) {
        return sqlSessionMaster.getMapper(UserLangPrefernceMapper.class).getUserLanguagePrefence(userId);
    }
}
