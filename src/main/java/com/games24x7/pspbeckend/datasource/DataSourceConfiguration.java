/**
 * 
 */
package com.games24x7.pspbeckend.datasource;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSourceConfiguration {

    @Autowired
    private com.games24x7.frameworks.config.Configuration config;

    @Bean(name = { "masterDataSource" })
    public DataSource getMasterDataSource() {
        BasicDataSource master = new BasicDataSource();
        master.setDriverClassName(config.get("master.db.driver", "com.mysql.jdbc.Driver"));
        master.setUsername(config.get("master.db.user", "root"));
        master.setPassword(config.get("master.db.passwd", "root"));
        master.setUrl(config.get("master.db.url", "jdbc:mysql://localhost:3306/games24x7v2"));

        master.setMinIdle(config.getInt("master.db.pool.minidleconnections", 2));
        master.setMaxIdle(config.getInt("master.db.pool.maxidleconnections", 5));
        master.setMinEvictableIdleTimeMillis(config.getInt("master.db.pool.minEvictableIdleTimeMillis", 30000));
        master.setInitialSize(config.getInt("master.db.pool.initialconnections", 4));
        master.setMaxTotal(config.getInt("master.db.pool.maxactiveconnections", 10));
        master.setMaxWaitMillis(config.getInt("master.db.pool.maxwaitforconnectionmillis", 25000));

        master.setDefaultAutoCommit(Boolean.parseBoolean(config.get("master.db.autocommit", "false")));
        return master;
    }

    /*@Bean(name = { "activeSlaveDataSource" })
    public DataSource getActiveSlaveDataSource() {
        BasicDataSource activeSlave = new BasicDataSource();
        activeSlave.setDriverClassName(config.get("activeslave.db.driver", "com.mysql.jdbc.Driver"));
        activeSlave.setUsername(config.get("activeslave.db.user", "root"));
        activeSlave.setPassword(config.get("activeslave.db.passwd", "root"));
        activeSlave.setUrl(config.get("activeslave.db.url", "jdbc:mysql://localhost:3306/games24x7v2"));

        activeSlave.setMinIdle(config.getInt("activeslave.db.pool.minidleconnections", 2));
        activeSlave.setMaxIdle(config.getInt("activeslave.db.pool.maxidleconnections", 10));
        activeSlave
                .setMinEvictableIdleTimeMillis(config.getInt("activeslave.db.pool.minEvictableIdleTimeMillis", 30000));
        activeSlave.setInitialSize(config.getInt("activeslave.db.pool.initialconnections", 2));
        activeSlave.setMaxTotal(config.getInt("activeslave.db.pool.maxactiveconnections", 10));
        activeSlave.setMaxWaitMillis(config.getInt("activeslave.db.pool.maxwaitforconnectionmillis", 25000));

        activeSlave.setDefaultAutoCommit(Boolean.parseBoolean(config.get("activeslave.db.autocommit", "false")));
        return activeSlave;
    }

    @Bean(name = { "passiveSlaveDataSource" })
    public DataSource getPassiveSlaveDataSource() {
        BasicDataSource passiveSlave = new BasicDataSource();
        passiveSlave.setDriverClassName(config.get("passiveslave.db.driver", "com.mysql.jdbc.Driver"));
        passiveSlave.setUsername(config.get("passiveslave.db.user", "root"));
        passiveSlave.setPassword(config.get("passiveslave.db.passwd", "root"));
        passiveSlave.setUrl(config.get("passiveslave.db.url", "jdbc:mysql://localhost:3306/games24x7v2"));

        passiveSlave.setMinIdle(config.getInt("passiveslave.db.pool.minidleconnections", 2));
        passiveSlave.setMaxIdle(config.getInt("passiveslave.db.pool.maxidleconnections", 5));
        passiveSlave.setMinEvictableIdleTimeMillis(config.getInt("passiveslave.db.pool.maxidleconnections", 30000));
        passiveSlave.setInitialSize(config.getInt("passiveslave.db.pool.initialconnections", 2));
        passiveSlave.setMaxTotal(config.getInt("passiveslave.db.pool.maxactiveconnections", 5));
        passiveSlave.setMaxWaitMillis(config.getInt("passiveslave.db.pool.maxwaitforconnectionmillis", 25000));

        passiveSlave.setDefaultAutoCommit(Boolean.parseBoolean(config.get("passiveslave.db.autocommit", "false")));
        return passiveSlave;
    }*/

}