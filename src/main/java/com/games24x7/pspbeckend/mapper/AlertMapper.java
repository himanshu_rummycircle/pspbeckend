package com.games24x7.pspbeckend.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.games24x7.pspbeckend.pojo.Alert;

public interface AlertMapper {
    @Insert("insert into alerts(user_id,event_type,status,owner_id,reference,"
            + "resolution,start_date,close_date,followup_date, creation_date,created_by,"
            + "last_update_date,last_updatedby,comment,withdraw_id,additional_info) "
            + " values(#{user_id},#{event_type},#{status},#{owner_id},#{reference},#{resolution} "
            + ",#{start_date},#{close_date},#{followup_date},#{creation_date},#{created_by}"
            + ",#{last_update_date},#{last_updatedby},#{comment},#{withdraw_id},#{additional_info})")

    @Options(useGeneratedKeys = true, keyProperty = "alert_id")
    long createAlert(Alert alert);

    @Update("update alerts set user_id = #{alert.user_id}, "
            + "event_type = #{alert.event_type}, status = #{alert.status}, "
            + "owner_id = #{alert.owner_id}, reference=#{alert.reference}, resolution=#{alert.resolution} ,"
            + "start_date = #{alert.start_date}, close_date=#{alert.close_date}, followup_date=#{alert.followup_date} ,"
            + "creation_date = #{alert.creation_date}, created_by= #{alert.created_by}, last_update_date= #{alert.last_update_date} ,"
            + "last_updatedby = #{alert.last_updatedby}, comment= #{alert.comment}, withdraw_id= #{alert.withdraw_id} ,additional_info= #{alert.additional_info} "
            + "where alert_id = #{alertId} ")
    long updateAlert(@Param("alertId") long alertId, @Param("alert") Alert alert);

    @Update("update alerts set comment= #{alert.comment},last_updatedby = #{alert.last_updatedby},last_update_date= now(),status= #{alert.status}  where alert_id = #{alertId} ")
    long updateAlertComment(@Param("alertId") long alertId, @Param("alert") Alert alert);

    @Select("select * from alerts where alert_id=#{alertId}")
    Alert getAlert(long alertId);

}
