package com.games24x7.pspbeckend.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;

import com.games24x7.pspbeckend.pojo.ApprovalDetailsDummy;
import com.games24x7.pspbeckend.pojo.OfflinePrize;
import com.games24x7.pspbeckend.pojo.RequestedDetails;

public interface ApprovalMapper {

    @Insert("insert into approval_details(actionid,clevel,mlevel,status ,requestor,updatedate,"
            + "generatedate,playerid,playerloginid ) "
            + " values(#{actionId},#{clevel},#{mlevel},#{status},#{requestor},#{updateDate},#{generateDate},#{playerId},#{loginId})")

    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    public long insertIntoApprovalDetails(ApprovalDetailsDummy dummy);

    @Insert("insert into a_process_refund_with_acc_merge_log (approvdetailsid, amount, merged_login_id) "
            + " values(#{approvdetailsid},#{amount},#{loginId})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    public long insertIntoProcessrefundAccMergrLog(RequestedDetails requestedDetails);

    @Insert("insert into a_data_show_log (approvdetailsid, datashown, requestorcomments) "
            + " values(#{approvdetailsid},#{requestedData},#{requestorComment})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    public long insertIntoDataShowLog(ApprovalDetailsDummy dummy);
 
}
