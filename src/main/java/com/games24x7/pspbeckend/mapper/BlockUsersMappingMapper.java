package com.games24x7.pspbeckend.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

public interface BlockUsersMappingMapper {
    @Delete("delete from block_users_mapping where user_id = #{userId} ")
    long deleteFromBlockUsersMapping(long userId);

    @Insert("insert into block_users_mapping(user_id, header_id , message, updatetime) values ("
            + " #{userId}, #{headerId}, #{message},now()) on duplicate key update "
            + " header_id=#{headerId}, message=#{message}")
    public void saveBlockUsersMappingData(@Param("userId") long userId, @Param("headerId") long headerId,
            @Param("message") String message);

}
