package com.games24x7.pspbeckend.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;

import com.games24x7.pspbeckend.pojo.IdentityInfo;

public interface IdentityInfoMapper {

    @Insert("insert into identity_info (user_id, id_type, id_no,updated_by,update_date,player_update_id) "
            + " values(#{user_id},#{id_type},#{id_no},#{updated_by},#{update_date},#{player_update_id})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    long insertIntoIdentityInfo(IdentityInfo info);


}
