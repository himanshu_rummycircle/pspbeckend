package com.games24x7.pspbeckend.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.games24x7.pspbeckend.pojo.IdentityInfo;
import com.games24x7.pspbeckend.pojo.PSPComments;

public interface PSPCommentsMapper {

    @Insert("insert into psp_comments (p_id, department, issue_catagory,contact_type,cmr_interaction_id,contact_direction,gameType) "
            + " values(#{p_id},#{department},#{issue_catagory},#{contact_type},#{cmr_interaction_id},#{contact_direction},#{gameType})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    long insertIntoPspComments(PSPComments obj);
}
