package com.games24x7.pspbeckend.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;

import com.games24x7.pspbeckend.pojo.PlayerUpdates;

public interface PlayerUpdatesMapper {
    @Insert("insert into player_updates (player_user_id, agent_id, field_updated,comment,previous_data,update_date,txn_id,additional_info) "
	    + " values(#{playerUserId},#{agentId},#{fieldUpdated},#{comment},#{previousData},#{updateDate},#{txnId},#{additionalInfo})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    long insertIntoPlayerUpdates(PlayerUpdates playeUpdate);
    
    
  
    
    @Insert({
        "<script>",
        "INSERT INTO player_updates(player_user_id, agent_id, field_updated,comment,previous_data,update_date,txn_id,additional_info)",
        "VALUES" +  
            "<foreach item='playerUpdate' collection='list' open='' separator=',' close=''>" +
                "(" +
                    "#{playerUpdate.playerUserId},",
                    "#{playerUpdate.agentId},",
                    "#{playerUpdate.fieldUpdated},",
                    "#{playerUpdate.comment},",
                    "#{playerUpdate.previousData},",
                    "#{playerUpdate.updateDate},",
                    "#{playerUpdate.txnId},",
                    "#{playerUpdate.additionalInfo}" +
                ")" +
            "</foreach>",
    "</script>"})
    long playerUpdateInsertion(@Param("list") List<PlayerUpdates> theCollection);

}
