package com.games24x7.pspbeckend.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.games24x7.pspbeckend.pojo.RiskCertificate;
import com.games24x7.pspbeckend.pojo.RiskType;

public interface RiskCertificateMapper {

    @Select("SELECT count(1) as count FROM  risk_type   where risk_type_desc= BINARY #{riskStatus}")
    public int checkIfValidStatus(@Param("riskStatus") String riskStatus);

    @Insert("insert into risk_certificates (name, risk_type_id, is_active) "
    	    + " values(#{riskCertificateName},#{riskStatusType},1)")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
	public void createRiskCertificate(RiskCertificate riskPayload);
 
    @Select("SELECT id as id , name as riskCertificateName , risk_type_id as riskStatusType , is_active as isActive from risk_certificates order by 1 DESC limit #{limit} offset #{offset}")
    public List<RiskCertificate> getRiskCertificatesAll(@Param("limit")long limit, @Param("offset")long offset);
    
    @Select("SELECT id as id , name as riskCertificateName , risk_type_id as riskStatusType , is_active as isActive from risk_certificates where is_active = #{isActiveValue} order by 1 DESC limit #{limit} offset #{offset} ")
    public List<RiskCertificate> getRiskCertificates(@Param("isActiveValue") int isActiveValue,@Param("limit")long limit,  @Param("offset") long offset);

    @Select("SELECT count(1) as count FROM  risk_certificates  where id in (${riskIdString})")
    public int distinctRiskId(@Param("riskIdString") String riskIdString);

    @Insert("update risk_certificates set is_active = #{activate} where id= #{riskId} and is_active=#{oppActivate}")
    public void updateRiskCertificate(@Param("riskId")Long riskId, @Param("activate")Boolean activate,@Param("oppActivate")Boolean oppActivate);

    @Select("SELECT id from risk_certificates where name in (${riskCertificateNames})")
    public List<Long> getRiskCertificateName(@Param("riskCertificateNames") String riskCertificateNames);

}
