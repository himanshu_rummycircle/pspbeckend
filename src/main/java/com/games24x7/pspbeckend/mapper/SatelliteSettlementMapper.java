package com.games24x7.pspbeckend.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.StatementType;

import com.games24x7.pspbeckend.pojo.OfflineSettlement;

public interface SatelliteSettlementMapper {


    @Select("SELECT t.status as status , td.tournament_type as tournamentType , td.total_prize_pool as prizePool FROM trnlist t,table_details td where t.template_id =td.id and t.id= #{tournamentId}")
    public Map<String,Object> getTournamentInfo(@Param("tournamentId") long tournamentId);

    @Select("select amount from in_play_account where trnmnt_id = #{tournamentId} and user_id = -2")
    public double getTournamentQualifierContribution(@Param("tournamentId") long tournamentId);

    @Insert("create temporary table IF NOT EXISTS tempOfflineMTTPromo(tourn_id bigint,user_id bigint,table_id bigint,winner_amount decimal(15,3),ticket_id int,rank_id int,is_cash_player int,tds_limit decimal(15,3),tds_value decimal(15,3))")
    public void insertTempTableIfNotThere();

    @Insert("insert into tempOfflineMTTPromo(tourn_id,user_id,table_id,winner_amount,ticket_id,rank_id,is_cash_player,tds_limit,tds_value) values"
            + "(#{prize.tournamentId},#{prize.userId},#{prize.tableId},#{prize.prizeAmount},0,#{prize.rank},1,#{prize.tdsLimit},#{prize.tdsValue})")
    public void insertPlayersSettlementDetailsTemp(@Param("prize") OfflineSettlement prize);

    @Select(value= "{ CALL offline_settlement( #{tournamentId,mode=IN, jdbcType=INTEGER},#{prizePool,mode=IN, jdbcType=DOUBLE})}")
    @Options(statementType = StatementType.CALLABLE)
    public Object callOfflineSettlementSP(@Param("tournamentId") long tournamentId , @Param("prizePool") double prizePool);

    @Insert("insert ignore into in_play_account(trnmnt_id,user_id,deposit_amt,amount,game_type,withdrawable,non_withdrawable) values "
            + "(#{prize.tournamentId},#{prize.userId},0,0,4,0,0)")
    public void insertInPlayAccountPlayers(@Param("prize") OfflineSettlement prize);


}
