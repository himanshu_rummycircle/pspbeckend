package com.games24x7.pspbeckend.mapper;

import org.apache.ibatis.annotations.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.games24x7.pspbeckend.pojo.IdentityInfo;

public interface UserLangPrefernceMapper {
    @Select("select lang_id from user_language_preference  where user_id = #{userId}")
    Integer getUserLanguagePrefence(long userId);

}
