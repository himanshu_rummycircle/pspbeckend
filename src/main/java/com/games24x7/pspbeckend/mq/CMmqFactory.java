package com.games24x7.pspbeckend.mq;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.pojo.ApprovalData;
import com.games24x7.pspbeckend.pojo.CloseAndMergeData;
import com.games24x7.pspbeckend.pojo.IdfyData;
import com.games24x7.pspbeckend.pojo.MergeAccount;
import com.games24x7.pspbeckend.pojo.UserKYCData;
import com.rabbitmq.client.Address;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

@Component
public class CMmqFactory {
    private static final Logger logger = LoggerFactory.getLogger(CMmqFactory.class);
    private Channel mqChannel;
    @Autowired
    Configuration config;

    @PostConstruct
    public void init() {
        try {

            List<Address> list = new ArrayList<>();
            ConnectionFactory factory = new ConnectionFactory();

            if (config.get(Constants.CM_MQ_IP).contains(Constants.CSV_SPLIT_BY)) {
                String[] hosts = config.get(Constants.CM_MQ_IP).split(Constants.CSV_SPLIT_BY);
                for (int i = 0; i < hosts.length; i++) {
                    if (hosts[i].contains(Constants.COLON)) {
                        String[] hostports = hosts[i].split(Constants.COLON);
                        Address add = new Address(hostports[0], Integer.valueOf(hostports[1]));
                        list.add(add);
                    } else {
                        Address add = new Address(hosts[i], Integer.parseInt(config.get(Constants.CM_MQ_PORT)));
                        list.add(add);
                    }
                }

            } else {
                list.add(new Address(config.get(Constants.CM_MQ_IP)));
                factory.setPort(Integer.parseInt(config.get(Constants.CM_MQ_PORT)));
            }

            factory.setUsername(config.get(Constants.CM_MQ_USERNAME));
            factory.setPassword(config.get(Constants.CM_MQ_PASSWD));
            factory.setAutomaticRecoveryEnabled(true);
            factory.setConnectionTimeout(3000);
            Connection connection = factory.newConnection(list);
            mqChannel = connection.createChannel();
            mqChannel.queueDeclare(config.get(Constants.CM_MQ_QUEUENAME), false, false, false, null);

            logger.info(" MQ initialization done. channel:" + mqChannel + ", mq.ip:" + config.get(Constants.CM_MQ_IP)
                    + ", exchange.name:" + config.get(Constants.CM_MQ_EXCHANGENAME) + ",queue name: "
                    + config.get(Constants.CM_MQ_QUEUENAME) + " , routing key :"
                    + config.get(Constants.CM_MQ_ROUTINGKEY) + ", host:" + factory.getHost() + ", port:"
                    + factory.getPort() + ", username:" + factory.getUsername() + ", passwd:" + factory.getPassword());
        } catch (Exception e) {
            logger.error("Exception while initializing  mq.", e);
        }
    }

    public void publishToCM(String message) {
        try {
            mqChannel.basicPublish("", config.get(Constants.CM_MQ_ROUTINGKEY), null, message.getBytes());
            logger.info("Message sent to CM :" + message);
        } catch (Exception e) {
            logger.error("Exception while publishing event to CM, message: " + message, e);
        }
    }

    public String getCmMergingMQMessage(CloseAndMergeData data) {
        JSONObject jsonObject = new JSONObject();
        try {

            if (!data.getSucessfulMergeAcc().isEmpty()) {
                jsonObject.put(Constants.SUCCESS, getJsonArray(data.getSucessfulMergeAcc()));
            }
            if (!data.getFailedMergeAcc().isEmpty()) {
                jsonObject.put(Constants.FAILURE, getJsonArray(data.getFailedMergeAcc()));
            }

            jsonObject.put(Constants.TYPE, Constants.ACC_MERGING);
            jsonObject.put(Constants.FILE_NAME, data.getFileName());
            jsonObject.put(Constants.AGENT_ID, data.getAgentId());
            jsonObject.put(Constants.AGENT_NAME, data.getAgent());
            jsonObject.put(Constants.FILE_SIZE, data.getUserMap().keySet().size());
        } catch (Exception e) {
            logger.error("Exception while creating message, message: ", e);
        }
        return jsonObject.toString();

    }

    private JSONArray getJsonArray(List<MergeAccount> mergeList) {
        JSONArray value = new JSONArray();

        mergeList.forEach(acc -> {
            JSONObject obj = new JSONObject();
            obj.put(Constants.FROMUSERID, acc.getFromUserId());
            obj.put(Constants.TOUSERID, acc.getToUserId());
            value.put(obj);
        });

        return value;
    }

    public String getCmApprovalMQMessage(CloseAndMergeData data) {
        JSONObject jsonObject = new JSONObject();
        try {
            if (!data.getApprovalData().isEmpty()) {
                jsonObject.put(Constants.SUCCESS, getApprovalJsonArray(data.getApprovalData()));
                jsonObject.put(Constants.TYPE, Constants.APPROVAL);
                jsonObject.put(Constants.FILE_NAME, data.getFileName());
                jsonObject.put(Constants.AGENT_ID, data.getAgentId());
                jsonObject.put(Constants.AGENT_NAME, data.getAgent());
                jsonObject.put(Constants.FILE_SIZE, data.getUserMap().keySet().size());
            }

        } catch (Exception e) {
            logger.error("Exception while creating Approval message, message: ", e);
        }
        return jsonObject.toString();
    }

    private JSONArray getApprovalJsonArray(List<ApprovalData> approvalData) {
        JSONArray value = new JSONArray();

        approvalData.forEach(acc -> {
            JSONObject obj = new JSONObject();
            obj.put(Constants.FROMUSERID, acc.getFromUserId());
            obj.put(Constants.TOUSERID, acc.getToUserId());
            obj.put(Constants.APPROVAL_ID, acc.getApprovalId());
            value.put(obj);
        });

        return value;
    }

    public String getFailedMessage(Throwable e, String fileName, String agent) {
        JSONObject obj = new JSONObject();
        obj.put(Constants.EXCEPTION, e);
        obj.put(Constants.FILE_NAME, fileName);
        obj.put(Constants.TYPE, Constants.ERROR);
        obj.put(Constants.AGENT_NAME, agent);
        return obj.toString();
    }

    public String getAccStatusChangeMessage(List<Long> successlist, List<Long> failedList, String fileName,
            long agentId, String agentName) {
        int filesize = 0;
        JSONObject obj = new JSONObject();
        if (successlist != null) {
            obj.put(Constants.SUCCESS, new JSONArray(successlist.toArray()));
            filesize = filesize + successlist.size();
        }
        if (failedList != null) {
            obj.put(Constants.FAILURE, new JSONArray(failedList.toArray()));
            filesize = filesize + failedList.size();
        }
        obj.put(Constants.FILE_NAME, fileName);
        obj.put(Constants.TYPE, Constants.ACC_STATUS_CHANGE);
        obj.put(Constants.AGENT_ID, agentId);
        obj.put(Constants.AGENT_NAME, agentName);
        obj.put(Constants.FILE_SIZE, filesize);
        return obj.toString();
    }

    public String getKYCsmsMessage(UserKYCData user, String smsString) {

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put(Constants.TYPE, Constants.SMS);
            jsonObject.put(Constants.MOBILE_NUMBER, user.getMobile_no());
            jsonObject.put(Constants.SMS_TYPE, 1);
            jsonObject.put(Constants.MESSAGE, smsString);
        } catch (Exception e) {
            logger.error("Exception while creating message, message: ", e);
        }
        return jsonObject.toString();
    }

    public String getKYCUpdateEmailMessage(String firstName, int userPrefLanguage, String email,
            double currDepositLimit, int channelId) {
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put(Constants.TYPE, Constants.KYC_EMAIL);
            jsonObject.put(Constants.FIRST_NAME, firstName);
            jsonObject.put(Constants.VERIFICATION_FOR, 2);
            jsonObject.put(Constants.KYC_ACCEPT_STATUS, true);
            jsonObject.put(Constants.RPS_KYC_VERIFICATION, 0);
            jsonObject.put(Constants.USER_PREFERED_LANGUAGE, userPrefLanguage);
            jsonObject.put(Constants.EMAIL, email);
            jsonObject.put(Constants.CURRENT_DEPOSIT_LIMIT, currDepositLimit);
            jsonObject.put(MTTSettlementConstant.CHANNELID, channelId);
        } catch (Exception e) {
            logger.error("Exception while creating message, message: ", e);
        }
        return jsonObject.toString();
    }

    public String getKYCRejectEmailMessage(String firstName, int userPrefLanguage, String email, int channelId) {
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put(Constants.TYPE, Constants.KYC_EMAIL);
            jsonObject.put(Constants.FIRST_NAME, firstName);
            jsonObject.put(Constants.VERIFICATION_FOR, 2);
            jsonObject.put(Constants.KYC_ACCEPT_STATUS, false);
            jsonObject.put(Constants.RPS_KYC_VERIFICATION, 0);
            jsonObject.put(Constants.USER_PREFERED_LANGUAGE, userPrefLanguage);
            jsonObject.put(Constants.EMAIL, email);
            jsonObject.put(MTTSettlementConstant.CHANNELID, channelId);
        } catch (Exception e) {
            logger.error("Exception while creating message, message: ", e);
        }
        return jsonObject.toString();
    }

    public String getOCREmailMessage(String email, IdfyData data, int channelId) {
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put(Constants.TYPE, Constants.OCR_EMAIL);
            jsonObject.put(Constants.NAME, data.getFullname());
            jsonObject.put(Constants.DOCUMENTID, data.getDocumentId());
            jsonObject.put(Constants.YOB, data.getYob() != null ? data.getYob() : data.getDob());
            jsonObject.put(Constants.STATE, data.getState());
            jsonObject.put(Constants.EMAIL, email);
            jsonObject.put(MTTSettlementConstant.CHANNELID, channelId);
        } catch (Exception e) {
            logger.error("Exception while creating message, message: ", e);
        }
        return jsonObject.toString();
    }

}
