package com.games24x7.pspbeckend.mq;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.pspbeckend.constants.Constants;
import com.rabbitmq.client.Address;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

@Component
public class NotifierMQFactory {

    private static final Logger logger = LoggerFactory.getLogger(CMmqFactory.class);
    private Channel mqChannel;
    @Autowired
    Configuration config;

    @PostConstruct
    public void init() {
        try {

            List<Address> list = new ArrayList<>();
            ConnectionFactory factory = new ConnectionFactory();

            if (config.get(Constants.NOTIFIER_MQ_IP).contains(Constants.CSV_SPLIT_BY)) {
                String[] hosts = config.get(Constants.NOTIFIER_MQ_IP).split(Constants.CSV_SPLIT_BY);
                for (int i = 0; i < hosts.length; i++) {
                    if (hosts[i].contains(Constants.COLON)) {
                        String[] hostports = hosts[i].split(Constants.COLON);
                        Address add = new Address(hostports[0], Integer.valueOf(hostports[1]));
                        list.add(add);
                    } else {
                        Address add = new Address(hosts[i], Integer.parseInt(config.get(Constants.NOTIFIER_MQ_PORT)));
                        list.add(add);
                    }
                }

            } else {
                list.add(new Address(config.get(Constants.NOTIFIER_MQ_IP)));
                factory.setPort(Integer.parseInt(config.get(Constants.NOTIFIER_MQ_PORT)));
            }

            factory.setUsername(config.get(Constants.NOTIFIER_MQ_USERNAME));
            factory.setPassword(config.get(Constants.NOTIFIER_MQ_PASSWD));
            factory.setAutomaticRecoveryEnabled(true);
            factory.setConnectionTimeout(3000);
            Connection connection = factory.newConnection(list);
            mqChannel = connection.createChannel();
            // mqChannel.queueDeclare(config.get(Constants.NOTIFIER_MQ_QUEUENAME),
            // false, false, false, null);
            mqChannel.exchangeDeclare(Constants.NOTIFIER_MQ_EXCHANGENAME, BuiltinExchangeType.FANOUT, false, false,
                    null);

            logger.info(" Notifier MQ initialization done. channel:" + mqChannel + ", mq.ip:"
                    + config.get(Constants.NOTIFIER_MQ_IP) + ", exchange.name:"
                    + config.get(Constants.NOTIFIER_MQ_EXCHANGENAME) + ",queue name: "
                    + config.get(Constants.NOTIFIER_MQ_QUEUENAME) + " , routing key :"
                    + config.get(Constants.NOTIFIER_MQ_ROUTINGKEY) + ", host:" + factory.getHost() + ", port:"
                    + factory.getPort() + ", username:" + factory.getUsername() + ", passwd:" + factory.getPassword());
        } catch (Exception e) {
            logger.error("Exception while initializing Notifier mq.", e);
        }
    }

    public void publishToNotifier(String message) {
        try {
            mqChannel.basicPublish(config.get(Constants.NOTIFIER_MQ_EXCHANGENAME),
                    config.get(Constants.NOTIFIER_MQ_ROUTINGKEY), null, message.getBytes());
            logger.info("Message sent to Notifier : {}", message);
        } catch (Exception e) {
            logger.error("Exception while publishing event to CM, message: " + message, e);
        }
    }

    public String getNotifierMQMessage(String notifiermessage, Long userId) {
        JSONObject notifierJson = new JSONObject();
        try {

            String uniqueIdentifier = System.currentTimeMillis() + "" + userId;
            notifierJson.put("bannerStartTime", System.currentTimeMillis());
            notifierJson.put("messageType", "addbanner");
            notifierJson.put("zone", "101");
            notifierJson.put("validity", TimeUnit.DAYS.toMillis(7));
            notifierJson.put("message", notifiermessage);
            notifierJson.put("priority", 1);
            notifierJson.put("userId", userId);
            notifierJson.put("campaignStartDate", System.currentTimeMillis());
            notifierJson.put("uniqueIdentifier", uniqueIdentifier);
        } catch (Exception e) {
            logger.error("Exception while creating message, message: ", e);
        }
        return notifierJson.toString();

    }

}
