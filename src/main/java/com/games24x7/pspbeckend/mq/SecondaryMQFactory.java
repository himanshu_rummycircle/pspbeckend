package com.games24x7.pspbeckend.mq;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.eds.events.Event;
import com.games24x7.frameworks.eds.events.GsonTemplate;
import com.games24x7.pspbeckend.Util.SatelliteSettlementHelper;
import com.games24x7.pspbeckend.constants.Constants;
import com.rabbitmq.client.Address;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

@Component
public class SecondaryMQFactory {
    private static final Logger logger = LoggerFactory.getLogger(SecondaryMQFactory.class);
    private Channel mqChannel;
    @Autowired
    Configuration config;

    public static final AtomicLong eventMessageCounter = new AtomicLong();

    @PostConstruct
    public void init() {
        try {

            List<Address> list = new ArrayList<>();
            ConnectionFactory factory = new ConnectionFactory();

            if (config.get(Constants.SECONDARY_MQ_IP).contains(Constants.CSV_SPLIT_BY)) {
                String[] hosts = config.get(Constants.SECONDARY_MQ_IP).split(Constants.CSV_SPLIT_BY);
                for (int i = 0; i < hosts.length; i++) {
                    if (hosts[i].contains(Constants.COLON)) {
                        String[] hostports = hosts[i].split(Constants.COLON);
                        Address add = new Address(hostports[0], Integer.valueOf(hostports[1]));
                        list.add(add);
                    } else {
                        Address add = new Address(hosts[i], Integer.parseInt(config.get(Constants.SECONDARY_MQ_PORT)));
                        list.add(add);
                    }
                }

            } else {
                list.add(new Address(config.get(Constants.SECONDARY_MQ_IP)));
                factory.setPort(Integer.parseInt(config.get(Constants.SECONDARY_MQ_PORT)));
            }

            factory.setUsername(config.get(Constants.SECONDARY_MQ_USERID));
            factory.setPassword(config.get(Constants.SECONDARY_MQ_PASSWD));
            factory.setAutomaticRecoveryEnabled(true);
            factory.setConnectionTimeout(3000);
            Connection connection = factory.newConnection(list);
            mqChannel = connection.createChannel();
            mqChannel.exchangeDeclare(Constants.EXCHANGE_NAME_KEY, BuiltinExchangeType.FANOUT, false, false, null);

            logger.info(" MQ initialization done. channel:" + mqChannel + ", mq.ip:"
                    + config.get(Constants.SECONDARY_MQ_IP) + ", exchange.name:"
                    + config.get(Constants.EXCHANGE_NAME_KEY) + "  , routing key :"
                    + config.get(Constants.EXCHANGE_NAME_KEY) + ", host:" + factory.getHost() + ", port:"
                    + factory.getPort() + ", username:" + factory.getUsername() + ", passwd:" + factory.getPassword());
        } catch (Exception e) {
            logger.error("Exception while initializing  mq.", e);
        }
    }

    public <T extends Event> void publishToSecondaryMQ(T eventObject) {

        try {
            GsonTemplate gsonTemplate = new GsonTemplate();
            gsonTemplate.setAck(Integer.toString(0));
            gsonTemplate.setDest("eds");
            gsonTemplate.setId(Long.toString(eventMessageCounter.incrementAndGet()));
            gsonTemplate.setSource("psp-be");
            gsonTemplate.setSystime(Long.toString(System.currentTimeMillis()));
            gsonTemplate.setType(eventObject.getEventType());
            gsonTemplate.setValue(SatelliteSettlementHelper.formatJson(eventObject));

            String gsonString = SatelliteSettlementHelper.formatJson(gsonTemplate);
            logger.info("Publishing Event to EDS  exchange {} , message {} :", config.get(Constants.EXCHANGE_NAME_KEY),
                    gsonString);
            mqChannel.basicPublish(config.get(Constants.EXCHANGE_NAME_KEY), config.get(Constants.EXCHANGE_NAME_KEY),
                    null, gsonString.getBytes());
        } catch (Exception e) {
            logger.error("Exception while publishing event ", e);
        }
    }
}
