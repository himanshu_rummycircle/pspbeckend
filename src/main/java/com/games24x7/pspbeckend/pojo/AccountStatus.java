package com.games24x7.pspbeckend.pojo;

import java.util.List;

public class AccountStatus {
    private List<Long> sucessfulUserIds;
    private List<Long> failedUserIds;

    public List<Long> getSucessfulUserIds() {
        return sucessfulUserIds;
    }

    public void setSucessfulUserIds(List<Long> sucessfulUserIds) {
        this.sucessfulUserIds = sucessfulUserIds;
    }

    public List<Long> getFailedUserIds() {
        return failedUserIds;
    }

    public void setFailedUserIds(List<Long> failedUserIds) {
        this.failedUserIds = failedUserIds;
    }

    @Override
    public String toString() {
        return "AccountStatus [sucessfulUserIds=" + sucessfulUserIds + ", failedUserIds=" + failedUserIds + "]";
    }

}
