package com.games24x7.pspbeckend.pojo;

import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Alert {
    private static final Logger logger = LoggerFactory.getLogger(Alert.class);

    private Long alert_id;
    private Long user_id;
    private Integer event_type;
    private Integer status;
    private Long owner_id;
    private Long last_updatedby;
    private Integer reference;
    private Integer resolution;
    private Timestamp start_date;

    private Timestamp close_date;
    private Timestamp followup_date;
    private Timestamp creation_date;
    private Long created_by;
    private Timestamp last_update_date;
    private String comment;
    private Long withdraw_id;
    private String additional_info;

    /**
     * @return the alert_id
     */
    public Long getAlert_id() {
        return alert_id;
    }

    /**
     * @param alert_id
     *            the alert_id to set
     */
    public void setAlert_id(Long alert_id) {
        this.alert_id = alert_id;
    }

    /**
     * @return the user_id
     */
    public Long getUser_id() {
        return user_id;
    }

    /**
     * @param user_id
     *            the user_id to set
     */
    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the event_type
     */
    public Integer getEvent_type() {
        return event_type;
    }

    /**
     * @param event_type
     *            the event_type to set
     */
    public void setEvent_type(Integer event_type) {
        this.event_type = event_type;
    }

    /**
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return the owner_id
     */
    public Long getOwner_id() {
        return owner_id;
    }

    /**
     * @param owner_id
     *            the owner_id to set
     */
    public void setOwner_id(Long owner_id) {
        this.owner_id = owner_id;
    }

    /**
     * @return the reference
     */
    public Integer getReference() {
        return reference;
    }

    /**
     * @param reference
     *            the reference to set
     */
    public void setReference(Integer reference) {
        this.reference = reference;
    }

    /**
     * @return the resolution
     */
    public Integer getResolution() {
        return resolution;
    }

    /**
     * @param resolution
     *            the resolution to set
     */
    public void setResolution(Integer resolution) {
        this.resolution = resolution;
    }

    /**
     * @return the start_date
     */
    public Timestamp getStart_date() {
        return start_date;
    }

    /**
     * @param start_date
     *            the start_date to set
     */
    public void setStart_date(Timestamp start_date) {
        this.start_date = start_date;
    }

    /**
     * @return the close_date
     */
    public Timestamp getClose_date() {
        return close_date;
    }

    /**
     * @param close_date
     *            the close_date to set
     */
    public void setClose_date(Timestamp close_date) {
        this.close_date = close_date;
    }

    /**
     * @return the followup_date
     */
    public Timestamp getFollowup_date() {
        return followup_date;
    }

    /**
     * @param followup_date
     *            the followup_date to set
     */
    public void setFollowup_date(Timestamp followup_date) {
        this.followup_date = followup_date;
    }

    /**
     * @return the creation_date
     */
    public Timestamp getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date
     *            the creation_date to set
     */
    public void setCreation_date(Timestamp creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * @return the created_by
     */
    public Long getCreated_by() {
        return created_by;
    }

    /**
     * @param created_by
     *            the created_by to set
     */
    public void setCreated_by(Long created_by) {
        this.created_by = created_by;
    }

    /**
     * @return the last_update_date
     */
    public Timestamp getLast_update_date() {
        return last_update_date;
    }

    /**
     * @param last_update_date
     *            the last_update_date to set
     */
    public void setLast_update_date(Timestamp last_update_date) {
        this.last_update_date = last_update_date;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment
     *            the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return the withdraw_id
     */
    public Long getWithdraw_id() {
        return withdraw_id;
    }

    /**
     * @param withdraw_id
     *            the withdraw_id to set
     */
    public void setWithdraw_id(Long withdraw_id) {
        this.withdraw_id = withdraw_id;
    }

    /**
     * @return the additional_info
     */
    public String getAdditional_info() {
        return additional_info;
    }

    /**
     * @param additional_info
     *            the additional_info to set
     */
    public void setAdditional_info(String additional_info) {
        this.additional_info = additional_info;
    }

    @Override
    public String toString() {
        return "Alert [alert_id=" + alert_id + ", user_id=" + user_id + ", event_type=" + event_type + ", status="
                + status + ", owner_id=" + owner_id + ", last_updatedby=" + last_updatedby + ", reference=" + reference
                + ", resolution=" + resolution + ", start_date=" + start_date + ", close_date=" + close_date
                + ", followup_date=" + followup_date + ", creation_date=" + creation_date + ", created_by=" + created_by
                + ", last_update_date=" + last_update_date + ", comment=" + comment + ", withdraw_id=" + withdraw_id
                + ", additional_info=" + additional_info + "]";
    }

    /**
     * @return the last_updatedby
     */
    public Long getLast_updatedby() {
        return last_updatedby;
    }

    /**
     * @param last_updatedby
     *            the last_updatedby to set
     */
    public void setLast_updatedby(Long last_updatedby) {
        this.last_updatedby = last_updatedby;
    }

}
