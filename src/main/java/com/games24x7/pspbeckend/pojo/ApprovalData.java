package com.games24x7.pspbeckend.pojo;

public class ApprovalData {
    private long fromUserId;
    private long toUserId;
    private double deposit;
    private double withdrawable;
    private long approvalId;

    public long getFromUserId() {
        return fromUserId;
    }

    public long getToUserId() {
        return toUserId;
    }

    public ApprovalData(long fromUserId, long toUserId, double deposit, double withdrawable) {
        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
        this.deposit = deposit;
        this.withdrawable = withdrawable;
    }


    public double getDeposit() {
        return deposit;
    }

    public double getWithdrawable() {
        return withdrawable;
    }

    @Override
    public String toString() {
        return "ApprovalData [fromUserId=" + fromUserId + ", toUserId=" + toUserId + ", deposit=" + deposit
                + ", withdrawable=" + withdrawable + ", approvalId=" + approvalId + "]";
    }

    public long getApprovalId() {
        return approvalId;
    }

    public void setApprovalId(long approvalId) {
        this.approvalId = approvalId;
    }

}
