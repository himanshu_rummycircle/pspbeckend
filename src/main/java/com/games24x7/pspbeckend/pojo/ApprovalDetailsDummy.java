
package com.games24x7.pspbeckend.pojo;

import java.sql.Timestamp;

public class ApprovalDetailsDummy {

    private Long id;
    private Integer clevel = 1;
    private Integer mlevel = 1;
    private Integer status = 1;
    private Timestamp updateDate;
    private String requestor;
    private Timestamp generateDate;
    private Integer actionId;
    private Long playerId;
    private String loginId;
    private String requestedData;
    private String requestorComment;
    private RequestedDetails requestedDetails;
    private Long approvdetailsid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getClevel() {
        return clevel;
    }

    public Integer getMlevel() {
        return mlevel;
    }

    public Integer getStatus() {
        return status;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public String getRequestor() {
        return requestor;
    }

    public void setRequestor(String requestor) {
        this.requestor = requestor;
    }

    public Timestamp getGenerateDate() {
        return generateDate;
    }

    public Integer getActionId() {
        return actionId;
    }

    public void setActionId(Integer actionId) {
        this.actionId = actionId;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getRequestedData() {
        return requestedData;
    }

    public void setRequestedData(String requestedData) {
        this.requestedData = requestedData;
    }

    public String getRequestorComment() {
        return requestorComment;
    }

    public void setRequestorComment(String requestorComment) {
        this.requestorComment = requestorComment;
    }

    public RequestedDetails getRequestedDetails() {
        return requestedDetails;
    }

    public void setRequestedDetails(RequestedDetails requestedDetails) {
        this.requestedDetails = requestedDetails;
    }

    public Long getApprovdetailsid() {
        return approvdetailsid;
    }

    public void setApprovdetailsid(Long approvdetailsid) {
        this.approvdetailsid = approvdetailsid;
    }

}
