package com.games24x7.pspbeckend.pojo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BlockAccountStatusData {
    private static final Logger logger = LoggerFactory.getLogger(BlockAccountStatusData.class);

    private long userId;
    private int status = 5;
    private String comment = "Account is accessed through blacklisted device id hence blocked.";
    private String header = "Others";
    private String messagetouser = "Kindly contact RummyCircle support team for further information.";
    private int blockheaderid = 9;
    private PSPComments pspdata;
    private long agentId;

    /**
     * @return the userId
     */
    public long getUserId() {
        return userId;
    }

    /**
     * @param userId
     *            the userId to set
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment
     *            the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return the header
     */
    public String getHeader() {
        return header;
    }

    /**
     * @param header
     *            the header to set
     */
    public void setHeader(String header) {
        this.header = header;
    }

    /**
     * @return the messagetouser
     */
    public String getMessagetouser() {
        return messagetouser;
    }

    /**
     * @param messagetouser
     *            the messagetouser to set
     */
    public void setMessagetouser(String messagetouser) {
        this.messagetouser = messagetouser;
    }

    /**
     * @return the pspdata
     */
    public PSPComments getPspdata() {
        return pspdata;
    }

    /**
     * @param pspdata
     *            the pspdata to set
     */
    public void setPspdata(PSPComments pspdata) {
        this.pspdata = pspdata;
    }

    /**
     * @return the agentId
     */
    public long getAgentId() {
        return agentId;
    }

    /**
     * @param agentId
     *            the agentId to set
     */
    public void setAgentId(long agentId) {
        this.agentId = agentId;
    }

    @Override
    public String toString() {
        return "ChangeAccountStatusData [userId=" + userId + ", status=" + status + ", comment=" + comment + ", header="
                + header + ", messagetouser=" + messagetouser + ", blockheaderid=" + blockheaderid + ", pspdata="
                + pspdata + ", agentId=" + agentId + "]";
    }

    /**
     * @return the blockheaderid
     */
    public int getBlockheaderid() {
        return blockheaderid;
    }

    /**
     * @param blockheaderid
     *            the blockheaderid to set
     */
    public void setBlockheaderid(int blockheaderid) {
        this.blockheaderid = blockheaderid;
    }
}
