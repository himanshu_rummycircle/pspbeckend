package com.games24x7.pspbeckend.pojo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;

public class BulkStatusChangeData {
    public static final Logger logger = LoggerFactory.getLogger(BulkStatusChangeData.class);
    private long agentId;
    private String agent;
    private String filename;
    private List<UserData> userData;
    private Integer accountStatus;

    public List<Long> getUserIdList(List<UserData> list) {
        List<Long> userList = new ArrayList<>();
        list.forEach(ref -> userList.add(ref.getUserId()));
        return userList;
    }

    public void validateAccountStatus(List<UserData> list, int accStatus) {
        List<Long> userList = list.stream().filter(data -> data.getAccountStatus() >= accStatus)
                .map(UserData::getUserId).collect(Collectors.toList());
        logger.debug("validateAccountStatus list {}, userList {} , userList size {} ,", list, userList,
                userList.size());
        if (!userList.isEmpty()) {
            throw new ServiceException(Constants.USER_STATUS_ERROR_MESSAGE + userList,
                    Status.BAD_REQUEST.getStatusCode(), Constants.ERROR_IN_PROCESSING_REQUEST_CODE);
        }
    }

    public long getAgentId() {
        return agentId;
    }

    public void setAgentId(long agentId) {
        this.agentId = agentId;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Override
    public String toString() {
        return "BulkStatusChangeData [agentId=" + agentId + ", agent=" + agent + ", filename=" + filename
                + ", userDataList=" + userData + ", accountStatus=" + accountStatus + "]";
    }

    /**
     * @return the accountStatus
     */
    public Integer getAccountStatus() {
        return accountStatus;
    }

    /**
     * @param accountStatus
     *            the accountStatus to set
     */
    public void setAccountStatus(Integer accountStatus) {
        this.accountStatus = accountStatus;
    }

    /**
     * @param userIds
     * @return
     */
    public void validateDuplicateUserIds(List<Long> userIds) {

        Set<Long> duplicateUserIds = userIds.stream().filter(i -> Collections.frequency(userIds, i) > 1)
                .collect(Collectors.toSet());
        logger.debug("duplicateUserIds :" + duplicateUserIds);
        if (!duplicateUserIds.isEmpty()) {
            throw new ServiceException(Constants.DUPLICATE_USERS_IN_FILE + duplicateUserIds, Status.BAD_REQUEST,
                    Constants.DUPLICATE_USERS_IN_FILE_ERROR_CODE);
        }

    }

    /**
     * @return the userData
     */
    public List<UserData> getUserData() {
        return userData;
    }

    /**
     * @param userData
     *            the userData to set
     */
    public void setUserData(List<UserData> userData) {
        this.userData = userData;
    }

}
