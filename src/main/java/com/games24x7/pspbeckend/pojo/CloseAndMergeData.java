package com.games24x7.pspbeckend.pojo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CloseAndMergeData {

    private List<MergeAccount> mergeAccData;
    private List<ApprovalData> approvalData;
    private Map<Long, Long> userMap;

    private String agent;
    private long agentId;
    private String fileName;
    private List<Long> failedAccStatusChanged;
    private Map<Long, MergeAccount> fromUserMergeAccMap;
    private Map<Long, MergeAccount> toUserMergeAccMap;

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public List<MergeAccount> getMergeAccData() {
        return mergeAccData;
    }

    public void setMergeAccData(List<MergeAccount> mergeAccData) {
        this.mergeAccData = mergeAccData;
    }

    public List<ApprovalData> getApprovalData() {
        return approvalData;
    }

    public void setApprovalData(List<ApprovalData> approvalData) {
        this.approvalData = approvalData;
    }

    public Map<Long, Long> getUserMap() {
        return userMap;
    }

    public void setUserMap(Map<Long, Long> userMap) {
        this.userMap = userMap;
    }

    public List<Long> getToAccountList() {
        return new ArrayList<>(userMap.values());
    }

    public List<Long> getFromAccountList() {
        return new ArrayList<>(userMap.keySet());
    }

    public long getAgentId() {
        return agentId;
    }

    public void setAgentId(long agentId) {
        this.agentId = agentId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public List<Long> getFailedAccStatusChanged() {
        return failedAccStatusChanged;
    }

    public void setFailedAccStatusChanged(List<Long> failedAccStatusChanged) {
        this.failedAccStatusChanged = failedAccStatusChanged;
    }

    public Map<Long, MergeAccount> getFromUserMergeAccMap() {
        return fromUserMergeAccMap;
    }

    public void setFromUserMergeAccMap(Map<Long, MergeAccount> fromUserMergeAccMap) {
        this.fromUserMergeAccMap = fromUserMergeAccMap;
    }

    public Map<Long, MergeAccount> getToUserMergeAccMap() {
        return toUserMergeAccMap;
    }

    public void setToUserMergeAccMap(Map<Long, MergeAccount> toUserMergeAccMap) {
        this.toUserMergeAccMap = toUserMergeAccMap;
    }



    public List<MergeAccount> getFailedMergeAcc() {
        List<MergeAccount> list = new ArrayList<>();
        getMergeAccData().forEach(acc -> {
            if (!acc.isSucess()) {
                list.add(acc);
            }
        });
        return list;
    }

    public List<MergeAccount> getSucessfulMergeAcc() {
        List<MergeAccount> list = new ArrayList<>();
        getMergeAccData().forEach(acc -> {
            if (acc.isSucess()) {
                list.add(acc);
            }
        });
        return list;
    }

    public List<Long> getMergeUserList(List<MergeAccount> list, boolean fromUser) {

        List<Long> userIdList = new ArrayList<>();
        list.forEach(acc -> {
            if (fromUser)
                userIdList.add(acc.getFromUserId());
            else
                userIdList.add(acc.getToUserId());
        });

        return userIdList;
    }

    public List<MergeAccount> getMergeAccForAccStatus(List<MergeAccount> list, int accStatus) {
        List<MergeAccount> userAccList = new ArrayList<>();
        list.forEach(acc -> {
            if (acc.getAccountStatus() == accStatus) {
                userAccList.add(acc);
            }
        });
        return userAccList;
    }


    public List<MergeAccount> getMergeAccAfterRemoval(CloseAndMergeData data, List<Long> failedUserList) {
        List<Long> list = new ArrayList(data.getFromUserMergeAccMap().keySet());
        list.removeAll(failedUserList);
        return data.getMergeAcc(list, data);

    }

    public List<MergeAccount> getMergeAcc(List<Long> userList, CloseAndMergeData data) {
        List<MergeAccount> mergelist = new ArrayList<>();
        userList.forEach(userId -> {
            mergelist.add(data.getFromUserMergeAccMap().get(userId));
        });

        return mergelist;
    }

}
