package com.games24x7.pspbeckend.pojo;

import java.util.ArrayList;
import java.util.List;

public class CloseAndRefundData {
    private List<Refund> refundList;
    private long agentId;
    private String agent;
    private String filename;

    public List<Refund> getRefundList() {
        return refundList;
    }

    public void setRefundList(List<Refund> refundList) {
        this.refundList = refundList;
    }

    public List<Long> getRefundUserIdList(List<Refund> list) {
        List<Long> userList = new ArrayList<>();
        list.forEach(ref -> userList.add(ref.getUserId()));
        return userList;
    }

    public long getAgentId() {
        return agentId;
    }

    public void setAgentId(long agentId) {
        this.agentId = agentId;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

}
