package com.games24x7.pspbeckend.pojo;

import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DepositLimitData {
    private static final Logger logger = LoggerFactory.getLogger(DepositLimitData.class);
    private double currentLimit;
    private double maxLimit;
    private Timestamp updateDate;
    private double dailyLimit;
    private double monthlyDeposit;
    private double totalDeposited;
    private int noOfDeposited;
    private double prevMaxLimit;

    /**
     * @return the currentLimit
     */
    public double getCurrentLimit() {
        return currentLimit;
    }

    /**
     * @param currentLimit
     *            the currentLimit to set
     */
    public void setCurrentLimit(double currentLimit) {
        this.currentLimit = currentLimit;
    }

    /**
     * @return the maxLimit
     */
    public double getMaxLimit() {
        return maxLimit;
    }

    /**
     * @param maxLimit
     *            the maxLimit to set
     */
    public void setMaxLimit(double maxLimit) {
        this.maxLimit = maxLimit;
    }

    /**
     * @return the updateDate
     */
    public Timestamp getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate
     *            the updateDate to set
     */
    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the dailyLimit
     */
    public double getDailyLimit() {
        return dailyLimit;
    }

    /**
     * @param dailyLimit
     *            the dailyLimit to set
     */
    public void setDailyLimit(double dailyLimit) {
        this.dailyLimit = dailyLimit;
    }

    /**
     * @return the monthlyDeposit
     */
    public double getMonthlyDeposit() {
        return monthlyDeposit;
    }

    /**
     * @param monthlyDeposit
     *            the monthlyDeposit to set
     */
    public void setMonthlyDeposit(double monthlyDeposit) {
        this.monthlyDeposit = monthlyDeposit;
    }

    /**
     * @return the totalDeposited
     */
    public double getTotalDeposited() {
        return totalDeposited;
    }

    /**
     * @param totalDeposited
     *            the totalDeposited to set
     */
    public void setTotalDeposited(double totalDeposited) {
        this.totalDeposited = totalDeposited;
    }

    /**
     * @return the noOfDeposited
     */
    public int getNoOfDeposited() {
        return noOfDeposited;
    }

    /**
     * @param noOfDeposited
     *            the noOfDeposited to set
     */
    public void setNoOfDeposited(int noOfDeposited) {
        this.noOfDeposited = noOfDeposited;
    }

    @Override
    public String toString() {
        return "DepositLimitData [currentLimit=" + currentLimit + ", maxLimit=" + maxLimit + ", updateDate="
                + updateDate + ", dailyLimit=" + dailyLimit + ", monthlyDeposit=" + monthlyDeposit + ", totalDeposited="
                + totalDeposited + ", noOfDeposited=" + noOfDeposited + ", prevMaxLimit=" + prevMaxLimit + "]";
    }

    /**
     * @return the prevMaxLimit
     */
    public double getPrevMaxLimit() {
        return prevMaxLimit;
    }

    /**
     * @param prevMaxLimit the prevMaxLimit to set
     */
    public void setPrevMaxLimit(double prevMaxLimit) {
        this.prevMaxLimit = prevMaxLimit;
    }
}
