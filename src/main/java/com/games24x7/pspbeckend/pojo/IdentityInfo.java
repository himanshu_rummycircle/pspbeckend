package com.games24x7.pspbeckend.pojo;

import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IdentityInfo {
    private static final Logger logger = LoggerFactory.getLogger(IdentityInfo.class);
    private long id;
    private long user_id;
    private short id_type;
    private String id_no;
    private long player_update_id;
    private long updated_by;
    private Timestamp update_date;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the user_id
     */
    public long getUser_id() {
        return user_id;
    }

    /**
     * @param user_id
     *            the user_id to set
     */
    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the id_type
     */
    public short getId_type() {
        return id_type;
    }

    /**
     * @param id_type
     *            the id_type to set
     */
    public void setId_type(short id_type) {
        this.id_type = id_type;
    }

    /**
     * @return the id_no
     */
    public String getId_no() {
        return id_no;
    }

    /**
     * @param id_no
     *            the id_no to set
     */
    public void setId_no(String id_no) {
        this.id_no = id_no;
    }

    /**
     * @return the player_update_id
     */
    public long getPlayer_update_id() {
        return player_update_id;
    }

    /**
     * @param player_update_id
     *            the player_update_id to set
     */
    public void setPlayer_update_id(long player_update_id) {
        this.player_update_id = player_update_id;
    }

    /**
     * @return the updated_by
     */
    public long getUpdated_by() {
        return updated_by;
    }

    /**
     * @param updated_by
     *            the updated_by to set
     */
    public void setUpdated_by(long updated_by) {
        this.updated_by = updated_by;
    }

    /**
     * @return the update_date
     */
    public Timestamp getUpdate_date() {
        return update_date;
    }

    /**
     * @param update_date
     *            the update_date to set
     */
    public void setUpdate_date(Timestamp update_date) {
        this.update_date = update_date;
    }

    @Override
    public String toString() {
        return "IdentityInfo [id=" + id + ", user_id=" + user_id + ", id_type=" + id_type + ", id_no=" + id_no
                + ", player_update_id=" + player_update_id + ", updated_by=" + updated_by + ", update_date="
                + update_date + "]";
    }
}
