package com.games24x7.pspbeckend.pojo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IdfyData {
    private static final Logger logger = LoggerFactory.getLogger(IdfyData.class);
    private String documentId;
    private String yob;
    private String dob;
    private String state;
    private String pincode;
    private String address;
    private String fullname;

    /**
     * @return the documentId
     */
    public String getDocumentId() {
        return documentId;
    }

    /**
     * @param documentId
     *            the documentId to set
     */
    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    /**
     * @return the yob
     */
    public String getYob() {
        return yob;
    }

    /**
     * @param yob
     *            the yob to set
     */
    public void setYob(String yob) {
        this.yob = yob;
    }

    /**
     * @return the dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * @param dob
     *            the dob to set
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state
     *            the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the pincode
     */
    public String getPincode() {
        return pincode;
    }

    /**
     * @param pincode
     *            the pincode to set
     */
    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address
     *            the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the fullname
     */
    public String getFullname() {
        return fullname;
    }

    /**
     * @param fullname
     *            the fullname to set
     */
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    @Override
    public String toString() {
        return "IdfyData [documentId=" + documentId + ", yob=" + yob + ", dob=" + dob + ", state=" + state
                + ", pincode=" + pincode + ", address=" + address + ", fullname=" + fullname + "]";
    }
}
