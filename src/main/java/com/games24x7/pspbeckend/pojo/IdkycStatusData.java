package com.games24x7.pspbeckend.pojo;

import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IdkycStatusData {
    private static final Logger logger = LoggerFactory.getLogger(IdkycStatusData.class);
    private long user_id;
    private int id_verification_status;
    private int kyc_verification_status;
    private Timestamp update_time;
    private int id_verified;
    private int kyc_verified;
    private String rejection_reason;

    /**
     * @return the user_id
     */
    public long getUser_id() {
        return user_id;
    }

    /**
     * @param user_id
     *            the user_id to set
     */
    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the id_verification_status
     */
    public int getId_verification_status() {
        return id_verification_status;
    }

    /**
     * @param id_verification_status
     *            the id_verification_status to set
     */
    public void setId_verification_status(int id_verification_status) {
        this.id_verification_status = id_verification_status;
    }

    /**
     * @return the kyc_verification_status
     */
    public int getKyc_verification_status() {
        return kyc_verification_status;
    }

    /**
     * @param kyc_verification_status
     *            the kyc_verification_status to set
     */
    public void setKyc_verification_status(int kyc_verification_status) {
        this.kyc_verification_status = kyc_verification_status;
    }

    /**
     * @return the update_time
     */
    public Timestamp getUpdate_time() {
        return update_time;
    }

    /**
     * @param update_time
     *            the update_time to set
     */
    public void setUpdate_time(Timestamp update_time) {
        this.update_time = update_time;
    }

    /**
     * @return the id_verified
     */
    public int getId_verified() {
        return id_verified;
    }

    /**
     * @param id_verified
     *            the id_verified to set
     */
    public void setId_verified(int id_verified) {
        this.id_verified = id_verified;
    }

    /**
     * @return the kyc_verified
     */
    public int getKyc_verified() {
        return kyc_verified;
    }

    /**
     * @param kyc_verified
     *            the kyc_verified to set
     */
    public void setKyc_verified(int kyc_verified) {
        this.kyc_verified = kyc_verified;
    }

    @Override
    public String toString() {
        return "IdkycStatusData [user_id=" + user_id + ", id_verification_status=" + id_verification_status
                + ", kyc_verification_status=" + kyc_verification_status + ", update_time=" + update_time
                + ", id_verified=" + id_verified + ", kyc_verified=" + kyc_verified + ", rejection_reason="
                + rejection_reason + "]";
    }

    /**
     * @return the rejection_reason
     */
    public String getRejection_reason() {
        return rejection_reason;
    }

    /**
     * @param rejection_reason
     *            the rejection_reason to set
     */
    public void setRejection_reason(String rejection_reason) {
        this.rejection_reason = rejection_reason;
    }
}
