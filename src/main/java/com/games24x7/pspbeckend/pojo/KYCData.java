package com.games24x7.pspbeckend.pojo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KYCData {
    private static final Logger logger = LoggerFactory.getLogger(KYCData.class);
    private Boolean type;
    private Boolean isKycReVerified;
    private Long userId;
    private Boolean block;
    private String statusComment;
    private IdfyData data;
    private PSPComments pspdata;
    private Integer docType;
    private Long agentId;

    /**
     * @return the type
     */
    public Boolean getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(Boolean type) {
        this.type = type;
    }

    /**
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId
     *            the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return the block
     */
    public Boolean getBlock() {
        return block;
    }

    /**
     * @param block
     *            the block to set
     */
    public void setBlock(Boolean block) {
        this.block = block;
    }

    /**
     * @return the statusComment
     */
    public String getStatusComment() {
        return statusComment;
    }

    /**
     * @param statusComment
     *            the statusComment to set
     */
    public void setStatusComment(String statusComment) {
        this.statusComment = statusComment;
    }

    /**
     * @return the data
     */
    public IdfyData getData() {
        return data;
    }

    /**
     * @param data
     *            the data to set
     */
    public void setData(IdfyData data) {
        this.data = data;
    }

    /**
     * @return the pspdata
     */
    public PSPComments getPspdata() {
        return pspdata;
    }

    /**
     * @param pspdata
     *            the pspdata to set
     */
    public void setPspdata(PSPComments pspdata) {
        this.pspdata = pspdata;
    }

    /**
     * @return the docType
     */
    public Integer getDocType() {
        return docType;
    }

    /**
     * @param docType
     *            the docType to set
     */
    public void setDocType(Integer docType) {
        this.docType = docType;
    }

    /**
     * @return the agentId
     */
    public Long getAgentId() {
        return agentId;
    }

    /**
     * @param agentId
     *            the agentId to set
     */
    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    @Override
    public String toString() {
        return "KYCData [type=" + type + ", isKycReVerified=" + isKycReVerified + ", userId=" + userId + ", block="
                + block + ", statusComment=" + statusComment + ", data=" + data + ", pspdata=" + pspdata + ", docType="
                + docType + ", agentId=" + agentId + "]";
    }

    /**
     * @return the isKycReVerified
     */
    public Boolean getIsKycReVerified() {
        return isKycReVerified;
    }

    /**
     * @param isKycReVerified
     *            the isKycReVerified to set
     */
    public void setIsKycReVerified(Boolean isKycReVerified) {
        this.isKycReVerified = isKycReVerified;
    }

}
