package com.games24x7.pspbeckend.pojo;

public class KnownDevice {
    String action;
    String agentName;
    Long agentId;
    Long userId;
    String deviceId;
    int channelId;
    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }
    public String getAgentName() {
        return agentName;
    }
    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }
    public Long getAgentId() {
        return agentId;
    }
    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }
    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public String getDeviceId() {
        return deviceId;
    }
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
    public int getChannelId() {
        return channelId;
    }
    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }
    @Override
    public String toString() {
        return "KnownDevice [action=" + action + ", agentName=" + agentName + ", agentId=" + agentId + ", userId="
                + userId + ", deviceId=" + deviceId + ", channelId=" + channelId + "]";
    }
    
}
