package com.games24x7.pspbeckend.pojo;

public class MergeAccount {
    private long fromUserId;
    private long toUserId;
    private String comment;
    private double deposit;
    private double withdrawable;
    private int accountStatus;
    private boolean sucess = true;

    public double getDeposit() {
        return deposit;
    }

    public void setDeposit(double deposit) {
        this.deposit = deposit;
    }

    public double getWithdrawable() {
        return withdrawable;
    }

    public void setWithdrawable(double withdrawable) {
        this.withdrawable = withdrawable;
    }

    public MergeAccount(long fromUserId, long toUserId, String comment) {
        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "MergeAccount [fromUserId=" + fromUserId + ", toUserId=" + toUserId + ", comment=" + comment
                + ", deposit=" + deposit + ", withdrawable=" + withdrawable + ", accountStatus=" + accountStatus
                + ", sucess=" + sucess + "]";
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public MergeAccount(int fromUserId, int toUserId, String comment, double deposit, double withdrawable) {
        super();
        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
        this.comment = comment;
        this.deposit = deposit;
        this.withdrawable = withdrawable;
    }

    public int getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(int accountStatus) {
        this.accountStatus = accountStatus;
    }

    public long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public long getToUserId() {
        return toUserId;
    }

    public void setToUserId(long toUserId) {
        this.toUserId = toUserId;
    }

    public boolean isSucess() {
        return sucess;
    }

    public void setSucess(boolean sucess) {
        this.sucess = sucess;
    }

}
