/**
 * 
 */
package com.games24x7.pspbeckend.pojo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 */
public class NFSUserDetails {
    private static final Logger logger = LoggerFactory.getLogger(NFSUserDetails.class);
    private long userId;
    private int clubType;

    /**
     * @return the userId
     */
    public long getUserId() {
        return userId;
    }

    /**
     * @param userId
     *            the userId to set
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * @return the clubType
     */
    public int getClubType() {
        return clubType;
    }

    /**
     * @param clubType
     *            the clubType to set
     */
    public void setClubType(int clubType) {
        this.clubType = clubType;
    }

    @Override
    public String toString() {
        return "NFSUserDetails [userId=" + userId + ", clubType=" + clubType + "]";
    }

}
