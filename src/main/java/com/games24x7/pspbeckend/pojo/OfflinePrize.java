package com.games24x7.pspbeckend.pojo;

public class OfflinePrize {

	private Long userId;
	private Integer rank;
	private Double prizeAmount;
	
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public Double getPrizeAmount() {
		return prizeAmount;
	}
	public void setPrizeAmount(Double prizeAmount) {
		this.prizeAmount = prizeAmount;
	}
	@Override
	public String toString() {
		return "OfflinePrize [userId=" + userId + ", rank=" + rank
				+ ", prizeAmount=" + prizeAmount + "]";
	}
	public OfflinePrize(long userId, int rank, Double prizeAmount) {
		this.userId = userId;
		this.rank = rank;
		this.prizeAmount = prizeAmount;
	}
	public OfflinePrize() {
	}
	
}
