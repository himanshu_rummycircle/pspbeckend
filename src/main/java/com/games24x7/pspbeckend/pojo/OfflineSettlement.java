package com.games24x7.pspbeckend.pojo;

public class OfflineSettlement {

    private long userId;
    private int rank;
    private double prizeAmount;
    private long tournamentId;
    private long tableId;
    private double tdsLimit;
    private double tdsValue;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public double getPrizeAmount() {
        return prizeAmount;
    }

    public void setPrizeAmount(double prizeAmount) {
        this.prizeAmount = prizeAmount;
    }

    public long getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(long tournamentId) {
        this.tournamentId = tournamentId;
    }

    public long getTableId() {
        return tableId;
    }

    public void setTableId(long tableId) {
        this.tableId = tableId;
    }

    public double getTdsLimit() {
        return tdsLimit;
    }

    public void setTdsLimit(double tdsLimit) {
        this.tdsLimit = tdsLimit;
    }

    public double getTdsValue() {
        return tdsValue;
    }

    public void setTdsValue(double tdsValue) {
        this.tdsValue = tdsValue;
    }

    @Override
    public String toString() {
        return "OfflineSettlement [userId=" + userId + ", rank=" + rank + ", prizeAmount=" + prizeAmount
                + ", tournamentId=" + tournamentId + ", tableId=" + tableId + ", " + "tdsLimit=" + tdsLimit
                + ", tdsValue=" + tdsValue + "]";
    }

    public OfflineSettlement(long userId, int rank, double prizeAmount, long tournamentId, long tableId,
            double tdsLimit, double tdsValue) {
        this.userId = userId;
        this.rank = rank;
        this.prizeAmount = prizeAmount;
        this.tournamentId = tournamentId;
        this.tableId = tableId;
        this.tdsLimit = tdsLimit;
        this.tdsValue = tdsValue;
    }

    public OfflineSettlement() {
    }

}
