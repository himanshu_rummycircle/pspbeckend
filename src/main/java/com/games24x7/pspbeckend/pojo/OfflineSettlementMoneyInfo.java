package com.games24x7.pspbeckend.pojo;

public class OfflineSettlementMoneyInfo {

    private double totalMoneyToDistribute;
    private double totalPrizePool;
    private double totalQualiferContribution;
    
    public double getTotalMoneyToDistribute() {
        return totalMoneyToDistribute;
    }
    public void setTotalMoneyToDistribute(double totalMoneyToDistribute) {
        this.totalMoneyToDistribute = totalMoneyToDistribute;
    }
    public double getTotalPrizePool() {
        return totalPrizePool;
    }
    public void setTotalPrizePool(double totalPrizePool) {
        this.totalPrizePool = totalPrizePool;
    }
    public double getTotalQualifierContribution() {
        return totalQualiferContribution;
    }
    public void setTotalQualifierContribution(double totalMoneyCollected) {
        this.totalQualiferContribution = totalMoneyCollected;
    }
    public OfflineSettlementMoneyInfo() {
    }
    public OfflineSettlementMoneyInfo(double totalMoneyToDistribute, double totalPrizePool, double totalMoneyCollected) {
        this.totalMoneyToDistribute = totalMoneyToDistribute;
        this.totalPrizePool = totalPrizePool;
        this.totalQualiferContribution = totalMoneyCollected;
    }
    @Override
    public String toString() {
        return "OfflineSettlementMoneyInfo [totalMoneyToDistribute=" + totalMoneyToDistribute + ", totalPrizePool="
                + totalPrizePool + ", totalMoneyCollected=" + totalQualiferContribution + "]";
    }
}
