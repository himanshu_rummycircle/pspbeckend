package com.games24x7.pspbeckend.pojo;

import java.util.List;

public class OfflineSettlementPayload {

    private Long tournamentId;
    private String agentId;
    private String agentGroup;
    private String comment;
    private double expense;
    private List<OfflinePrize> prizelist;

    public Long getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(Long tournamentId) {
        this.tournamentId = tournamentId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getAgentGroup() {
        return agentGroup;
    }

    public void setAgentGroup(String agentGroup) {
        this.agentGroup = agentGroup;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<OfflinePrize> getPrizelist() {
        return prizelist;
    }

    public void setPrizelist(List<OfflinePrize> prizelist) {
        this.prizelist = prizelist;
    }

    public OfflineSettlementPayload(long tournamentId, String agentId, String agentGroup, String comment,
            List<OfflinePrize> prizelist) {
        this.tournamentId = tournamentId;
        this.agentId = agentId;
        this.agentGroup = agentGroup;
        this.comment = comment;
        this.prizelist = prizelist;
    }

    public OfflineSettlementPayload() {
    }

    @Override
    public String toString() {
        return "OfflineSettlementPayload [tournamentId=" + tournamentId + ", agentId=" + agentId + ", agentGroup="
                + agentGroup + ", comment=" + comment + ", expense=" + expense + ", prizelist=" + prizelist + "]";
    }

    /**
     * @return the expense
     */
    public double getExpense() {
        return expense;
    }

    /**
     * @param expense
     *            the expense to set
     */
    public void setExpense(double expense) {
        this.expense = expense;
    }

}
