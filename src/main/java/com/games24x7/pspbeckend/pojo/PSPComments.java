package com.games24x7.pspbeckend.pojo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PSPComments {
    private static final Logger logger = LoggerFactory.getLogger(PSPComments.class);
    private long id;
    private long p_id;
    private String department;
    private String issue_catagory;
    private String contact_type;
    private String cmr_interaction_id;
    private String contact_direction;
    private String gameType;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the p_id
     */
    public long getP_id() {
        return p_id;
    }

    /**
     * @param p_id
     *            the p_id to set
     */
    public void setP_id(long p_id) {
        this.p_id = p_id;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department
     *            the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return the issue_catagory
     */
    public String getIssue_catagory() {
        return issue_catagory;
    }

    /**
     * @param issue_catagory
     *            the issue_catagory to set
     */
    public void setIssue_catagory(String issue_catagory) {
        this.issue_catagory = issue_catagory;
    }

    /**
     * @return the contact_type
     */
    public String getContact_type() {
        return contact_type;
    }

    /**
     * @param contact_type
     *            the contact_type to set
     */
    public void setContact_type(String contact_type) {
        this.contact_type = contact_type;
    }

    /**
     * @return the cmr_interaction_id
     */
    public String getCmr_interaction_id() {
        return cmr_interaction_id;
    }

    /**
     * @param cmr_interaction_id
     *            the cmr_interaction_id to set
     */
    public void setCmr_interaction_id(String cmr_interaction_id) {
        this.cmr_interaction_id = cmr_interaction_id;
    }

    /**
     * @return the contact_direction
     */
    public String getContact_direction() {
        return contact_direction;
    }

    /**
     * @param contact_direction
     *            the contact_direction to set
     */
    public void setContact_direction(String contact_direction) {
        this.contact_direction = contact_direction;
    }

    /**
     * @return the gameType
     */
    public String getGameType() {
        return gameType;
    }

    /**
     * @param gameType
     *            the gameType to set
     */
    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    @Override
    public String toString() {
        return "PSPComments [id=" + id + ", p_id=" + p_id + ", department=" + department + ", issue_catagory="
                + issue_catagory + ", contact_type=" + contact_type + ", cmr_interaction_id=" + cmr_interaction_id
                + ", contact_direction=" + contact_direction + ", gameType=" + gameType + "]";
    }
}
