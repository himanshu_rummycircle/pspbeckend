package com.games24x7.pspbeckend.pojo;

import java.sql.Timestamp;

public class PlayerUpdates {

    private long id;
    private long playerUserId;
    private long agentId;
    private int fieldUpdated;
    private String previousData;
    private String comment;
    private Timestamp updateDate;
    private String additionalInfo = "N/A";
    private long txnId;

    public long getTxnId() {
        return txnId;
    }

    public PlayerUpdates(long playerUserId, long agentId, int fieldUpdated, String previousData, String comment,
            Timestamp updateDate, String additionalInfo, long txnId) {

        this.playerUserId = playerUserId;
        this.agentId = agentId;
        this.fieldUpdated = fieldUpdated;
        this.previousData = previousData;
        this.comment = comment;
        this.updateDate = updateDate;
        this.additionalInfo = additionalInfo;
        this.txnId = txnId;
    }

    public void setTxnId(long txnId) {
        this.txnId = txnId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPlayerUserId() {
        return playerUserId;
    }

    public void setPlayerUserId(long playerUserId) {
        this.playerUserId = playerUserId;
    }

    public long getAgentId() {
        return agentId;
    }

    public void setAgentId(long agentId) {
        this.agentId = agentId;
    }

    public int getFieldUpdated() {
        return fieldUpdated;
    }

    public void setFieldUpdated(int fieldUpdated) {
        this.fieldUpdated = fieldUpdated;
    }

    public String getPreviousData() {
        return previousData;
    }

    public void setPreviousData(String previousData) {
        this.previousData = previousData;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    @Override
    public String toString() {
        return "PlayerUpdates [id=" + id + ", playerUserId=" + playerUserId + ", agentId=" + agentId + ", fieldUpdated="
                + fieldUpdated + ", previousData=" + previousData + ", comment=" + comment + ", updateDate="
                + updateDate + ", additionalInfo=" + additionalInfo + ", txnId=" + txnId + "]";
    }

}
