package com.games24x7.pspbeckend.pojo;

public class PlayersRiskCertificate {
    
    private long userId;
    private String riskCertificateNames;
    private String contactType;
    private String contactDirection;
    private String interactionId;
    private String comment;
    private String riskStatus;
    private String riskStatusComment;
    private String riskStatusId;
    private String agentId;
    private String reasonForDowngrade;
    
    
    public String getRiskCertificateNames() {
        return riskCertificateNames;
    }
    public void setRiskCertificateNames(String riskCertificateName) {
        this.riskCertificateNames = riskCertificateName;
    }
    public String getContactType() {
        return contactType;
    }
    public void setContactType(String contactType) {
        this.contactType = contactType;
    }
    public String getContactDirection() {
        return contactDirection;
    }
    public void setContactDirection(String contactDirection) {
        this.contactDirection = contactDirection;
    }
    public String getInteractionId() {
        return interactionId;
    }
    public void setInteractionId(String interactionId) {
        this.interactionId = interactionId;
    }
    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }
    public String getRiskStatus() {
        return riskStatus;
    }
    public void setRiskStatus(String riskStatus) {
        this.riskStatus = riskStatus;
    }
    
    
    public String getRiskStatusComment() {
        return riskStatusComment;
    }
    public void setRiskStatusComment(String riskStatusComment) {
        this.riskStatusComment = riskStatusComment;
    }
    public String getRiskStatusId() {
        return riskStatusId;
    }
    public void setRiskStatusId(String riskStatusId) {
        this.riskStatusId = riskStatusId;
    }
    public String getAgentId() {
        return agentId;
    }
    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }
    
    public long getUserId() {
        return userId;
    }
    public void setUserId(long userId) {
        this.userId = userId;
    }
    
    public String getReasonForDowngrade() {
        return reasonForDowngrade;
    }
    public void setReasonForDowngrade(String reasonForDowngrade) {
        this.reasonForDowngrade = reasonForDowngrade;
    }
    @Override
    public String toString() {
        return "PlayersRiskCertificate [userId=" + userId + ", riskCertificateNames=" + riskCertificateNames
                + ", contactType=" + contactType + ", contactDirection=" + contactDirection + ", interactionId="
                + interactionId + ", comment=" + comment + ", riskStatus=" + riskStatus + ", riskStatusComment="
                + riskStatusComment + ", riskStatusId=" + riskStatusId + ", agentId=" + agentId
                + ", reasonForDowngrade=" + reasonForDowngrade + "]";
    }
}
