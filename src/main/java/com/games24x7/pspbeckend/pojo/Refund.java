package com.games24x7.pspbeckend.pojo;

public class Refund {
    private long userId;
    private String comments;
    private int accountStatus;

    public Refund(long userId, String comments) {
        super();
        this.userId = userId;
        this.comments = comments;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "Refund [userId=" + userId + ", comments=" + comments + ", accountStatus=" + accountStatus + "]";
    }

    public int getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(int accountStatus) {
        this.accountStatus = accountStatus;
    }

}
