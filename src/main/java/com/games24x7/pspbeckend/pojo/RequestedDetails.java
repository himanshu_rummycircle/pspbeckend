
package com.games24x7.pspbeckend.pojo;

public class RequestedDetails {
    private Integer id;
    private Double amount;
    private String loginId;
    private Long approvdetailsid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public Long getApprovdetailsid() {
        return approvdetailsid;
    }

    public void setApprovdetailsid(Long approvdetailsid) {
        this.approvdetailsid = approvdetailsid;
    }

}