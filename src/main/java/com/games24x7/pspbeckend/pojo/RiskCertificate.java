package com.games24x7.pspbeckend.pojo;

public class RiskCertificate {

    private long id;
    private String riskCertificateName;
    private String riskStatus;
    private String agentId;
    private String agentGroup;
    private int riskStatusType;
    private boolean isActive ;
    
   
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getRiskCertificateName() {
        return riskCertificateName;
    }
    public void setRiskCertificateName(String riskCertificateName) {
        this.riskCertificateName = riskCertificateName;
    }
    public String getRiskStatus() {
        return riskStatus;
    }
    public void setRiskStatus(String riskStatus) {
        this.riskStatus = riskStatus;
    }
    
    public String getAgentId() {
        return agentId;
    }
    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }
    public String getAgentGroup() {
        return agentGroup;
    }
    public void setAgentGroup(String agentGroup) {
        this.agentGroup = agentGroup;
    }
    
    public int getRiskStatusType() {
		return riskStatusType;
	}
	public void setRiskStatusType(int riskType) {
		this.riskStatusType = riskType;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	 
    @Override
    public String toString() {
        return "RiskCertificate [id=" + id + ", riskCertificateName=" + riskCertificateName + ", riskStatus="
                + riskStatus + ", agentId=" + agentId + ", agentGroup=" + agentGroup + ", riskStatusType="
                + riskStatusType + ", isActive=" + isActive + "]";
    }
    public RiskCertificate() {
    }
    public RiskCertificate(String riskCertificateName, String riskStatus) {
        this.riskCertificateName = riskCertificateName;
        this.riskStatus = riskStatus;
    }
    public RiskCertificate(long id, String riskCertificateName, String riskStatus,  boolean isActive) {
        this.id = id;
        this.riskCertificateName = riskCertificateName;
        this.riskStatus = riskStatus;
        this.isActive = isActive;
    }
    public RiskCertificate(long id, boolean isActive) {
        this.id = id;
        this.isActive = isActive;
    }
}
