package com.games24x7.pspbeckend.pojo;

public class RiskType {

    private int riskId;
    private String riskName;

    public int getRiskId() {
        return riskId;
    }

    public void setRiskId(int riskId) {
        this.riskId = riskId;
    }

    public String getRiskName() {
        return riskName;
    }

    public void setRiskName(String riskName) {
        this.riskName = riskName;
    }

    @Override
    public String toString() {
        return "RiskType [riskId=" + riskId + ", riskName=" + riskName + "]";
    }

}
