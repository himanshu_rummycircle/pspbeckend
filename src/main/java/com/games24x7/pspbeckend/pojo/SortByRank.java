package com.games24x7.pspbeckend.pojo;

import java.util.Comparator;

public class SortByRank implements Comparator<OfflinePrize>{

    @Override
    public int compare(OfflinePrize offlineP1, OfflinePrize offlineP2) {
        return offlineP1.getRank()-offlineP2.getRank();
    }

}
