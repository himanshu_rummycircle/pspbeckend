package com.games24x7.pspbeckend.pojo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UnblockUserData {
    private static final Logger logger = LoggerFactory.getLogger(UnblockUserData.class);

    private long userId;
    private int status;
    private int prevStatus;
    private String comment = "User unblocked .";
    private PSPComments pspdata;
    private long agentId;

    /**
     * @return the userId
     */
    public long getUserId() {
        return userId;
    }

    /**
     * @param userId
     *            the userId to set
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the prevStatus
     */
    public int getPrevStatus() {
        return prevStatus;
    }

    /**
     * @param prevStatus
     *            the prevStatus to set
     */
    public void setPrevStatus(int prevStatus) {
        this.prevStatus = prevStatus;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment
     *            the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return the pspdata
     */
    public PSPComments getPspdata() {
        return pspdata;
    }

    /**
     * @param pspdata
     *            the pspdata to set
     */
    public void setPspdata(PSPComments pspdata) {
        this.pspdata = pspdata;
    }

    /**
     * @return the agentId
     */
    public long getAgentId() {
        return agentId;
    }

    /**
     * @param agentId
     *            the agentId to set
     */
    public void setAgentId(long agentId) {
        this.agentId = agentId;
    }

    @Override
    public String toString() {
        return "UnblockUserData [userId=" + userId + ", status=" + status + ", prevStatus=" + prevStatus + ", comment="
                + comment + ", pspdata=" + pspdata + ", agentId=" + agentId + "]";
    }

}
