package com.games24x7.pspbeckend.pojo;

public class UpdateRiskCertificate {
    
    private String riskCertificateIds;
    private String agentId;
    private boolean activate ;
    
    
    public String getRiskCertificateIds() {
        return riskCertificateIds;
    }
    public void setRiskCertificateIds(String riskCertificateIds) {
        this.riskCertificateIds = riskCertificateIds;
    }
    public String getAgentId() {
        return agentId;
    }
    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }
     
    
    public boolean getActivate() {
        return activate;
    }
    public void setActivate(boolean activate) {
        this.activate = activate;
    }
    @Override
    public String toString() {
        return "UpdateRiskCertificate [riskCertificateIds=" + riskCertificateIds + ", agentId=" + agentId
                + ", activate=" + activate + "]";
    }
     
}
