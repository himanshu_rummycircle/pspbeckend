/**
 * 
 */
package com.games24x7.pspbeckend.pojo;

import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 */
public class UserDetails {
    private static final Logger logger = LoggerFactory.getLogger(UserDetails.class);
    private String loginid;
    private int mobile_confirmation;
    private long user_id;
    private Timestamp last_login;
    private int club_type;
    private int account_status;
    private long mobile;
    private int active;

    /**
     * @return the active
     */
    public int getActive() {
        return active;
    }

    /**
     * @param active
     *            the active to set
     */
    public void setActive(int active) {
        this.active = active;
    }

    /**
     * @return the loginid
     */
    public String getLoginid() {
        return loginid;
    }

    /**
     * @param loginid
     *            the loginid to set
     */
    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    /**
     * @return the mobile_confirmation
     */
    public int getMobile_confirmation() {
        return mobile_confirmation;
    }

    /**
     * @param mobile_confirmation
     *            the mobile_confirmation to set
     */
    public void setMobile_confirmation(int mobile_confirmation) {
        this.mobile_confirmation = mobile_confirmation;
    }

    /**
     * @return the user_id
     */
    public long getUser_id() {
        return user_id;
    }

    /**
     * @param user_id
     *            the user_id to set
     */
    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the last_login
     */
    public Timestamp getLast_login() {
        return last_login;
    }

    /**
     * @param last_login
     *            the last_login to set
     */
    public void setLast_login(Timestamp last_login) {
        this.last_login = last_login;
    }

    /**
     * @return the club_type
     */
    public int getClub_type() {
        return club_type;
    }

    /**
     * @param club_type
     *            the club_type to set
     */
    public void setClub_type(int club_type) {
        this.club_type = club_type;
    }

    /**
     * @return the account_status
     */
    public int getAccount_status() {
        return account_status;
    }

    /**
     * @param account_status
     *            the account_status to set
     */
    public void setAccount_status(int account_status) {
        this.account_status = account_status;
    }

    /**
     * @return the mobile
     */
    public long getMobile() {
        return mobile;
    }

    /**
     * @param mobile
     *            the mobile to set
     */
    public void setMobile(long mobile) {
        this.mobile = mobile;
    }

    @Override
    public String toString() {
        return "UserDetails [loginid=" + loginid + ", mobile_confirmation=" + mobile_confirmation + ", user_id="
                + user_id + ", last_login=" + last_login + ", club_type=" + club_type + ", account_status="
                + account_status + ", mobile=" + mobile + ", active=" + active + "]";
    }
}
