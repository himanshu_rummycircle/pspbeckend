package com.games24x7.pspbeckend.pojo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserKYCData {
    private static final Logger logger = LoggerFactory.getLogger(UserKYCData.class);
    private int kyc_status;
    private String firstname;
    private String loginid;
    private long mobile_no;
    private int account_status;
    private String email;
    private long user_id;

    /**
     * @return the kyc_status
     */
    public int getKyc_status() {
        return kyc_status;
    }

    /**
     * @param kyc_status
     *            the kyc_status to set
     */
    public void setKyc_status(int kyc_status) {
        this.kyc_status = kyc_status;
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname
     *            the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the mobile_no
     */
    public long getMobile_no() {
        return mobile_no;
    }

    /**
     * @param mobile_no
     *            the mobile_no to set
     */
    public void setMobile_no(long mobile_no) {
        this.mobile_no = mobile_no;
    }

    /**
     * @return the account_status
     */
    public int getAccount_status() {
        return account_status;
    }

    /**
     * @param account_status
     *            the account_status to set
     */
    public void setAccount_status(int account_status) {
        this.account_status = account_status;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the logger
     */
    public static Logger getLogger() {
        return logger;
    }

    @Override
    public String toString() {
        return "UserKYCData [kyc_status=" + kyc_status + ", firstname=" + firstname + ", loginid=" + loginid
                + ", mobile_no=" + mobile_no + ", account_status=" + account_status + ", email=" + email + ", user_id="
                + user_id + "]";
    }

    /**
     * @return the user_id
     */
    public long getUser_id() {
        return user_id;
    }

    /**
     * @param user_id
     *            the user_id to set
     */
    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the loginid
     */
    public String getLoginid() {
        return loginid;
    }

    /**
     * @param loginid
     *            the loginid to set
     */
    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }
}
