package com.games24x7.pspbeckend.pojo;

import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserUploadLog {
    private static final Logger logger = LoggerFactory.getLogger(UserUploadLog.class);
    private long id;
    private Long user_id;
    private int doc_type;
    private int verification_type;
    private String file_names;
    private Timestamp update_time;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the user_id
     */
    public Long getUser_id() {
        return user_id;
    }

    /**
     * @param user_id
     *            the user_id to set
     */
    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the doc_type
     */
    public int getDoc_type() {
        return doc_type;
    }

    /**
     * @param doc_type
     *            the doc_type to set
     */
    public void setDoc_type(int doc_type) {
        this.doc_type = doc_type;
    }

    /**
     * @return the verification_type
     */
    public int getVerification_type() {
        return verification_type;
    }

    /**
     * @param verification_type
     *            the verification_type to set
     */
    public void setVerification_type(int verification_type) {
        this.verification_type = verification_type;
    }

    /**
     * @return the file_names
     */
    public String getFile_names() {
        return file_names;
    }

    /**
     * @param file_names
     *            the file_names to set
     */
    public void setFile_names(String file_names) {
        this.file_names = file_names;
    }

    /**
     * @return the update_time
     */
    public Timestamp getUpdate_time() {
        return update_time;
    }

    /**
     * @param update_time
     *            the update_time to set
     */
    public void setUpdate_time(Timestamp update_time) {
        this.update_time = update_time;
    }

    @Override
    public String toString() {
        return "UserUploadLog [id=" + id + ", user_id=" + user_id + ", doc_type=" + doc_type + ", verification_type="
                + verification_type + ", file_names=" + file_names + ", update_time=" + update_time + "]";
    }
}
