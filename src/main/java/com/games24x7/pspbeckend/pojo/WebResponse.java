/**
 * 
 */
package com.games24x7.pspbeckend.pojo;

/**
 * 
 */
public class WebResponse {
    private int statusCode;
    private String payload;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "WebResponse [statusCode=" + statusCode + ", payload=" + payload + "]";
    }

}
