/**
 * 
 */
package com.games24x7.pspbeckend.responsehandler;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerResponse;
import com.sun.jersey.spi.container.ContainerResponseFilter;

/**
 * 
 */
@Provider
public class ResponseFilter implements ContainerResponseFilter {
    private static final Logger logger = LoggerFactory.getLogger(ResponseFilter.class);

    @Override
    public ContainerResponse filter(ContainerRequest request, ContainerResponse response) {
        logger.info("inside ResponseFilter---" + response.getHttpHeaders());

        final ResponseBuilder resp = Response.fromResponse(response.getResponse());
        resp.header("Access-Control-Allow-Origin", "*").header("Access-Control-Allow-Methods",
                "GET, POST, PUT, DELETE, OPTIONS");
        final String reqHead = request.getHeaderValue("Access-Control-Request-Headers");
        if (null != reqHead ) {
            resp.header("Access-Control-Allow-Headers", reqHead);
        }
        logger.info("Headers---" + response.getHttpHeaders());
        response.setResponse(resp.build());
        return response;

    }

}
