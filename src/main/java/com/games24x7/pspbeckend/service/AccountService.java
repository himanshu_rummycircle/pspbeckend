package com.games24x7.pspbeckend.service;

import java.util.List;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.AccountServiceUtil;
import com.games24x7.pspbeckend.Util.PlayerUpdateUtil;
import com.games24x7.pspbeckend.Util.WebServiceUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Globals;
import com.games24x7.pspbeckend.pojo.AccountStatus;
import com.games24x7.pspbeckend.pojo.BlockAccountStatusData;
import com.games24x7.pspbeckend.pojo.BulkStatusChangeData;
import com.games24x7.pspbeckend.pojo.CloseAccountStatusData;
import com.games24x7.pspbeckend.pojo.UnblockUserData;
import com.google.gson.Gson;

@Service
public class AccountService {
    private static final Logger logger = LoggerFactory.getLogger(AccountService.class);

    @Autowired
    AccountServiceUtil util;

    @Autowired
    PlayerUpdateUtil puUtil;

    public void processAccountClosure(Object data, int key) {

        util.getherUserData(data, key);

        util.closeAccount(data, key);
        if (key == Globals.CLOSE_AND_MERGE) {
            puUtil.insertIntoPlayerUpdates(data, Globals.ACCOUNT_CLOSED_PLAYER_UPDATES);
        } else {
            puUtil.insertIntoPlayerUpdates(data, key);
        }
    }

    /**
     * 
     * @param data
     * @param key
     */
    public void changeBulkAccountStatus(BulkStatusChangeData data, int key) {

        List<Long> userIdList = data.getUserIdList(data.getUserData());
        data.validateDuplicateUserIds(userIdList);
        util.getherUserData(data, key);
        data.validateAccountStatus(data.getUserData(), data.getAccountStatus());

        long agentId = data.getAgentId();
        String fileName = data.getFilename();
        String agentName = data.getAgent();
        AccountStatus status = util.updateAccountStatus(userIdList, data.getAccountStatus(), agentId);
        puUtil.insertIntoPlayerUpdates(data, key);
        util.sendAccountStatusCmMessage(status.getSucessfulUserIds(), status.getFailedUserIds(), fileName, agentId,
                agentName);
    }

    /**
     * 
     * @param value
     * @param context
     */

    public void blockUser(JSONObject value, JSONObject context) {
        try {
            if (!context.has(Constants.AGENT_ID) || context.isNull(Constants.AGENT_ID)) {
                context.put(Constants.AGENT_ID, 0);
            }
            if (!context.has(Constants.SOURCE) || context.isNull(Constants.SOURCE)) {
                context.put(Constants.SOURCE, Constants.PSP);
            }
            value.put(Constants.AGENT_ID, context.get(Constants.AGENT_ID));

            if (Constants.PSP.equalsIgnoreCase(context.getString(Constants.SOURCE)))
                validate(value);
            logger.debug("ChangeAccountStatusData Value BEFORE {} ", value.toString());
            BlockAccountStatusData obj = new Gson().fromJson(value.toString(), BlockAccountStatusData.class);
            logger.debug("ChangeAccountStatusData Value AFTER {} ", obj.toString());

            util.processBlockUser(obj, context);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(Constants.BLOCK_USER_PROCESS_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                    e, Constants.BLOCK_USER_PROCESS_ERROR_CODE);
        }

    }

    private void validate(JSONObject value) {
        if (!value.has(Constants.USERID) || value.isNull(Constants.USERID)) {
            throw new ServiceException(Constants.INVALID_USER, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_USER_ID_ERR_CODE);
        }
        if (!value.has(Constants.STATUS) || value.isNull(Constants.STATUS)
                || WebServiceUtil.isNullOrEmpty(value.get(Constants.STATUS).toString())) {
            throw new ServiceException(Constants.INVALID_STATUS, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_STATUS_ERR_CODE);
        }
        if (!value.has(Constants.COMMENT) || value.isNull(Constants.COMMENT)
                || WebServiceUtil.isNullOrEmpty(value.getString(Constants.COMMENT))) {
            throw new ServiceException(Constants.INVALID_COMMENT, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_COMMENT_ERR_CODE);
        }
        if (!value.has(Constants.BLOCK_HEADER_ID) || value.isNull(Constants.BLOCK_HEADER_ID)
                || WebServiceUtil.isNullOrEmpty(value.get(Constants.BLOCK_HEADER_ID).toString())) {
            throw new ServiceException(Constants.INVALID_BLOCK_HEADER_ID, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_BLOCK_HEADER_ID_ERR_CODE);
        }
        if (!value.has(Constants.HEADER) || value.isNull(Constants.HEADER)
                || WebServiceUtil.isNullOrEmpty(value.getString(Constants.HEADER))) {
            throw new ServiceException(Constants.INVALID_HEADER, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_HEADER_ERR_CODE);
        }
        if (!value.has(Constants.MESSAGE_TO_USER) || value.isNull(Constants.MESSAGE_TO_USER)
                || WebServiceUtil.isNullOrEmpty(value.getString(Constants.MESSAGE_TO_USER))) {
            throw new ServiceException(Constants.INVALID_MESSAGE_TO_USER, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_MESSAGE_TO_USER_ERR_CODE);
        }
    }

    public void closeUser(JSONObject value, JSONObject context) {
        try {
            if (!context.has(Constants.AGENT_ID) || context.isNull(Constants.AGENT_ID)) {
                context.put(Constants.AGENT_ID, 0);
            }
            if (!context.has(Constants.SOURCE) || context.isNull(Constants.SOURCE)) {
                context.put(Constants.SOURCE, Constants.PSP);
            }
            value.put(Constants.AGENT_ID, context.get(Constants.AGENT_ID));

            if (Constants.PSP.equalsIgnoreCase(context.getString(Constants.SOURCE)))
                validate(value);
            logger.debug("ChangeAccountStatusData Value BEFORE {} ", value.toString());
            CloseAccountStatusData obj = new Gson().fromJson(value.toString(), CloseAccountStatusData.class);
            logger.debug("ChangeAccountStatusData Value AFTER {} ", obj.toString());

            util.processCloseUser(obj, context);
        } catch (ServiceException e) {
            throw e;
        } catch (Exception e) {
            throw new ServiceException(Constants.CLOSE_USER_PROCESS_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                    e, Constants.CLOSE_USER_PROCESS_ERROR_CODE);
        }
    }

    public void unBlockUser(JSONObject value, JSONObject context) {
        if (!context.has(Constants.AGENT_ID) || context.isNull(Constants.AGENT_ID)) {
            context.put(Constants.AGENT_ID, 0);
        }
        value.put(Constants.AGENT_ID, context.get(Constants.AGENT_ID));

        if (!value.has(Constants.STATUS) || value.isNull(Constants.STATUS)
                || WebServiceUtil.isNullOrEmpty(value.get(Constants.STATUS).toString())
                || value.getInt(Constants.STATUS) <= 0) {
            throw new ServiceException(Constants.INVALID_STATUS, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_STATUS_ERR_CODE);
        }
        if (!value.has(Constants.PREV_STATUS) || value.isNull(Constants.PREV_STATUS)
                || WebServiceUtil.isNullOrEmpty(value.get(Constants.PREV_STATUS).toString())
                || value.getInt(Constants.PREV_STATUS) <= 0) {
            throw new ServiceException(Constants.INVALID_PREV_STATUS, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_PREV_STATUS_ERR_CODE);
        }
        if (!value.has(Constants.USERID) || value.isNull(Constants.USERID) || value.getInt(Constants.USERID) <= 0) {
            throw new ServiceException(Constants.INVALID_USER, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_USER_ID_ERR_CODE);
        }
        logger.debug("UnblockUserData Value BEFORE {} ", value.toString());
        UnblockUserData obj = new Gson().fromJson(value.toString(), UnblockUserData.class);
        logger.debug("UnblockUserData Value AFTER {} ", obj.toString());

        util.unBlockUser(obj, context);
    }

}
