package com.games24x7.pspbeckend.service;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;
import com.games24x7.pspbeckend.dao.AlertDao;
import com.games24x7.pspbeckend.pojo.Alert;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component
public class AlertService {
    private static final Logger logger = LoggerFactory.getLogger(AlertService.class);

    @Autowired
    AlertDao dao;

    public long createAlert(JSONObject jsonObject) {
        logger.debug("jsonObject before convertion {} :", jsonObject.toString());

        Alert alertObj = new GsonBuilder().setDateFormat("yyyy-MM-dd hh:mm:ss").create().fromJson(jsonObject.toString(),
                Alert.class);
        logger.debug("alertObj after pojo convertion {} :", alertObj.toString());
        validate(alertObj);
        return dao.createAlert(alertObj);

    }

    private void validate(Alert obj) {

        if (obj.getUser_id() == null) {
            throw new ServiceException(Constants.INVALID_USER_ID, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_USER_ID_ERR_CODE);
        }
        if (obj.getEvent_type() == null) {
            throw new ServiceException(Constants.INVALID_EVENT_TYPE, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_EVENT_TYPE_ERR_CODE);
        }
        if (obj.getStatus() == null) {
            throw new ServiceException(Constants.INVALID_STATUS, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_STATUS_ERR_CODE);
        }
        if (obj.getOwner_id() == null) {
            throw new ServiceException(Constants.INVALID_OWNER_ID, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_OWNER_ID_ERR_CODE);
        }
        if (obj.getReference() == null) {
            throw new ServiceException(Constants.INVALID_REFERENCE, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_REFERENCE_ERR_CODE);
        }
        if (obj.getCreation_date() == null) {
            throw new ServiceException(Constants.INVALID_CREATION_DATE, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_CREATION_DATE_ERR_CODE);
        }
        if (obj.getCreated_by() == null) {
            throw new ServiceException(Constants.INVALID_CREATED_BY, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_CREATED_BY_ERR_CODE);
        }
        if (obj.getWithdraw_id() == null) {
            throw new ServiceException(Constants.INVALID_WITHDRAW_ID, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_WITHDRAW_ID_ERR_CODE);
        }

    }

    public void updateAlert(long alertId, JSONObject value) {
        Alert alertObj = new GsonBuilder().setDateFormat("yyyy-MM-dd hh:mm:ss").create().fromJson(value.toString(),
                Alert.class);
        validate(alertObj);
        dao.updateAlert(alertId, alertObj);
    }

    public Alert getAlert(long alertId) {
        return dao.getAlertData(alertId);
    }

    public void updateAlertComment(long alertId, JSONObject value) {
        logger.debug("BEFORE MSG {} ", value.toString());
        Alert alertObj = new GsonBuilder().setDateFormat("yyyy-MM-dd hh:mm:ss").create().fromJson(value.toString(),
                Alert.class);
        logger.debug("AFTER MSG {} ", alertObj.toString());
        if (alertObj.getStatus() == null)
            alertObj.setStatus(1);
        validateComment(alertObj);
        dao.updateAlertComment(alertId, alertObj);
    }

    private void validateComment(Alert obj) {
        if (obj.getLast_updatedby() == null) {
            throw new ServiceException(Constants.INVALID_LAST_UPDATED_BY, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_LAST_UPDATED_BY_ERR_CODE);
        }
        if (obj.getComment() == null) {
            throw new ServiceException(Constants.INVALID_COMMENT, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_COMMENT_ERR_CODE);
        }
    }

}
