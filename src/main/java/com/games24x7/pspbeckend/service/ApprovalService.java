package com.games24x7.pspbeckend.service;

import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.games24x7.pspbeckend.Util.ApprovalDataUtil;
import com.games24x7.pspbeckend.Util.ThreadPoolExecutorUtil;
import com.games24x7.pspbeckend.mq.CMmqFactory;
import com.games24x7.pspbeckend.pojo.CloseAndMergeData;

@Service
public class ApprovalService {
    private static final Logger logger = LoggerFactory.getLogger(ApprovalService.class);
    @Autowired
    ApprovalDataUtil approval;

    @Autowired
    ThreadPoolExecutorUtil util;

    @Autowired
    CMmqFactory mq;

    public void processApproval(CloseAndMergeData data) {
        CompletableFuture.runAsync(() -> {
            approval.generateApproval(data);
            logger.info("Approval Process Completed for " + data.getFileName());
            mq.publishToCM(mq.getCmApprovalMQMessage(data));

        } , util.getApprovalThreadpoolexecutor()).exceptionally(e -> {
            logger.error("Exception while processing Approvals ", e);
            mq.publishToCM(mq.getFailedMessage(e, data.getFileName(), data.getAgent()));
            return null;
        });

    }

    /**
     * @param data
     * @return
     */
    public CloseAndMergeData generateApprovalAndMergeData(CloseAndMergeData data) {
        return approval.getApprovalData(data);
    }

}
