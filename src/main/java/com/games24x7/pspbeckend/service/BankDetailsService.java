package com.games24x7.pspbeckend.service;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.AuditUtil;
import com.games24x7.pspbeckend.Util.BankDetailsUtil;
import com.games24x7.pspbeckend.constants.Constants;

@Service
public class BankDetailsService {
	private static final Logger logger = LoggerFactory.getLogger(BankDetailsService.class);

	@Autowired
	BankDetailsUtil bankDetailsUtil;

	@Autowired
	AuditUtil auditUtil;

	public JSONObject getUserBankDetails(long userId) {
		if (userId <= 0) {
			throw new ServiceException(Constants.INVALID_USER_ID, Status.BAD_REQUEST,
					Constants.INVALID_USER_ID_ERR_CODE);
		}
		return bankDetailsUtil.getBankDetailForUser(userId);
	}

	public void updateUserBankDetails(long userId, JSONObject payload) {
		if (userId <= 0) {
			throw new ServiceException(Constants.INVALID_USER_ID, Status.BAD_REQUEST,
					Constants.INVALID_USER_ID_ERR_CODE);
		}
		validatePayloadForUpdateBankDetails(payload);
		bankDetailsUtil.updateBankDetailForUser(userId, payload.getJSONObject(Constants.VALUE));
		auditUtil.sendCustomAuditMessageToKafka(
				auditUtil.getAuditMessageForUpdateBank(userId, payload.getJSONObject(Constants.VALUE)));
	}

	private void validatePayloadForUpdateBankDetails(JSONObject payloadJson) {
		if (!payloadJson.has(Constants.VALUE)) {
			throw new ServiceException(Constants.VALUE_MISSING_FROM_PAYLOAD_MSG, Status.BAD_REQUEST,
					Constants.VALUE_MISSING_FROM_PAYLOAD);
		}
		JSONObject value = payloadJson.getJSONObject(Constants.VALUE);
		if (!value.has(Constants.IFSC_CODE) || !value.has(Constants.ACCOUNT_NUMBER) || !value.has(Constants.AGENT_NAME)) {
			throw new ServiceException(Constants.MISSING_IFSC_ACCOUNT_NUMBER, Status.BAD_REQUEST,
					Constants.MISSING_IFSC_ACCOUNT_NUMBER_ERR_CODE);
		}
	}

}
