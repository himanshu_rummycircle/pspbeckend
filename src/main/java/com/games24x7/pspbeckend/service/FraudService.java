package com.games24x7.pspbeckend.service;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.FraudServiceUtil;
import com.games24x7.pspbeckend.Util.WebServiceUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Globals;

@Service
public class FraudService {
    private static final Logger logger = LoggerFactory.getLogger(FraudService.class);

    @Autowired
    FraudServiceUtil util;

    public JSONObject getFraudDevicesInfo(int status, int limit, int offset, String deviceId) {

        validate(status, limit, offset, deviceId);
        if (deviceId != null) {
            return util.getFraudDetailForDevice(deviceId, status);
        } else {
            return util.getFraudDetailForStatus(status, limit, offset);
        }
    }

    private void validate(int status, int limit, int offset, String deviceId) {
        if (status <= 0) {
            throw new ServiceException(Constants.INVALID_STATUS_EXCEPTION, Status.BAD_REQUEST,
                    Constants.INVALID_STATUS_EXCEPTION_CODE);
        }
        if (limit < 0) {
            throw new ServiceException(Constants.INVALID_LIMIT_EXCEPTION, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_LIMIT_EXCEPTION_CODE);
        }
        if (offset < 0) {
            throw new ServiceException(Constants.INVALID_OFFSET_EXCEPTION, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_OFFSET_EXCEPTION_CODE);
        }
    }

    public void updateFraudDevice(int status, String agent, String deviceId) {

        if (status <= 0 || status > Globals.WHITELISTED_DEVICE_STATUS) {
            throw new ServiceException(Constants.INVALID_STATUS_EXCEPTION, Status.BAD_REQUEST,
                    Constants.INVALID_STATUS_EXCEPTION_CODE);
        }
        if (WebServiceUtil.isNullOrEmpty(agent)) {
            throw new ServiceException(Constants.INVALID_AGENT_EXCEPTION, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_AGENT_EXCEPTION_CODE);
        }
        if (WebServiceUtil.isNullOrEmpty(deviceId)) {
            throw new ServiceException(Constants.INVALID_DEVICE_ID_EXCEPTION, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_DEVICE_ID_EXCEPTION_CODE);
        }
        util.updateFraudDeviceStatus(status, agent, deviceId);
    }

}
