package com.games24x7.pspbeckend.service;

import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.games24x7.pspbeckend.Util.AccountServiceUtil;
import com.games24x7.pspbeckend.Util.AuditUtil;
import com.games24x7.pspbeckend.Util.FundTransferUtil;
import com.games24x7.pspbeckend.Util.PlayerUpdateUtil;
import com.games24x7.pspbeckend.Util.ThreadPoolExecutorUtil;
import com.games24x7.pspbeckend.constants.Globals;
import com.games24x7.pspbeckend.mq.CMmqFactory;
import com.games24x7.pspbeckend.pojo.CloseAndMergeData;

@Service
public class FundTransferService {
    private static final Logger logger = LoggerFactory.getLogger(FundTransferService.class);

    @Autowired
    FundTransferUtil fsUtil;
    @Autowired
    PlayerUpdateUtil puUtil;
    @Autowired
    AccountServiceUtil asutil;
    @Autowired
    ThreadPoolExecutorUtil util;
    @Autowired
    CMmqFactory mq;

    @Autowired
    AuditUtil auditUtil;

    public void processAccountMerging(CloseAndMergeData data) {
        CompletableFuture.runAsync(() -> {
            fsUtil.processPayload(data);
            logger.info("SucessfulMergeAcc size : " + data.getSucessfulMergeAcc().size() + " Failed :"
                    + data.getFailedMergeAcc().size());

            if (!data.getFailedMergeAcc().isEmpty()) {
                asutil.updateAcctStatusForFailedMergeAcc(data);
            }

            puUtil.insertIntoPlayerUpdates(data, Globals.ACCOUNT_MERGE_PLAYER_UPDATES);
            auditUtil.auditAccountMerging(data);
            mq.publishToCM(mq.getCmMergingMQMessage(data));
            logger.info("Account Merging Completed for : " + data.getFileName());

        } , util.getPlayerUpdateThreadpoolexecutor()).exceptionally(e -> {
            logger.error(e.getMessage(), e);
            mq.publishToCM(mq.getFailedMessage(e, data.getFileName(), data.getAgent()));
            return null;
        });

    }

}
