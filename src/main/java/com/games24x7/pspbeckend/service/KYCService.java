package com.games24x7.pspbeckend.service;

import javax.ws.rs.core.Response.Status;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.KYCUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.pojo.KYCData;
import com.google.gson.Gson;

@Service
public class KYCService {
    private static final Logger logger = LoggerFactory.getLogger(KYCService.class);

    @Autowired
    KYCUtil kycutil;

    public void process(JSONObject value, JSONObject context) {
        try {
            if (!context.has(Constants.AGENT_ID)) {
                context.put(Constants.AGENT_ID, 0);
            }
            if (!context.has(MTTSettlementConstant.CHANNELID)) {
                context.put(MTTSettlementConstant.CHANNELID, 1);
            }
            value.put(Constants.AGENT_ID, context.get(Constants.AGENT_ID));
            validateType(value);
            logger.debug("KYC Value BEFORE {} ", value.toString());
            KYCData obj = new Gson().fromJson(value.toString(), KYCData.class);
            logger.debug("KYC Value AFTER {} ", obj.toString());
            validate(obj, context);

            if (!kycutil.isCallFromAVS(context) && obj.getIsKycReVerified() != null && obj.getIsKycReVerified()) {
                kycutil.processKycReVerified(obj, context);
                return;
            }

            if (obj.getType()) {
                kycutil.processKycVerified(obj, context);
            } else {
                kycutil.processKycRejected(obj, context);
            }
        } catch (ServiceException e) {
            throw e;
        }

    }

    private void validateType(JSONObject value) {
        if (value.has(Constants.TYPE) && !value.isNull(Constants.TYPE)) {
            try {
                value.getBoolean(Constants.TYPE);
            } catch (JSONException e) {
                throw new ServiceException(Constants.TYPE_NOT_IN_BOOLEAN, Status.BAD_REQUEST.getStatusCode(),
                        Constants.TYPE_NOT_IN_BOOLEAN_ERR_CODE);
            }

        } else {
            throw new ServiceException(Constants.TYPE_MISSING_FROM_PAYLOAD_MSG, Status.BAD_REQUEST.getStatusCode(),
                    Constants.TYPE_MISSING_FROM_PAYLOAD);
        }
    }

    private void validate(KYCData obj, JSONObject context) {
        if (obj.getUserId() == null || obj.getUserId() <= 0) {
            throw new ServiceException(Constants.USER_ID_MISSING_FROM_PAYLOAD_MSG, Status.BAD_REQUEST.getStatusCode(),
                    Constants.KYC_PROCESS_FAILURE_CODE);
        }
        if (obj.getData() == null && kycutil.isCallFromAVS(context) && obj.getType()) {
            throw new ServiceException(Constants.DATA_MISSING_FROM_PAYLOAD_MSG, Status.BAD_REQUEST.getStatusCode(),
                    Constants.DATA_MISSING_FROM_PAYLOAD);
        }
        if (obj.getDocType() == null) {
            throw new ServiceException(Constants.DOC_TYPE_MISSING_FROM_PAYLOAD_MSG, Status.BAD_REQUEST.getStatusCode(),
                    Constants.DOC_TYPE_MISSING_FROM_PAYLOAD);
        }
        if (obj.getStatusComment() == null) {
            throw new ServiceException(Constants.COMMENT_MISSING_FROM_PAYLOAD_MSG, Status.BAD_REQUEST.getStatusCode(),
                    Constants.COMMENT_MISSING_FROM_PAYLOAD);
        }
    }

}
