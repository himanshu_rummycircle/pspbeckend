package com.games24x7.pspbeckend.service;

import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.KnownDevicesUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.pojo.KnownDevice;
import com.games24x7.pspbeckend.webclient.UserServiceClient;

@Component
public class KnownDevicesService {
    private static final Logger logger = LoggerFactory.getLogger(KnownDevicesService.class);
    
    @Autowired
    UserServiceClient userServiceClient;
    @Autowired
    Configuration config;
    @Autowired
    KnownDevicesUtil knownDevicesUtil;

    public String processKnowDevices(KnownDevice knownDevice) {
        switch(knownDevice.getAction())
        {
        case Constants.GET_LIST_ACTION: 
            try {
            JSONArray jArray = userServiceClient.getCall(knownDevicesUtil.getListURL(knownDevice.getUserId()));
            return jArray==null?null:jArray.toString();
            }catch (ServiceException e) {
                if(e.getStatus()==Status.NOT_FOUND)
                    return null;
                throw e;
            }
        case Constants.AUTHENTICATE_DEVICE_ACTION:
            userServiceClient.postCall(knownDevicesUtil.authenticateDeviceURL(knownDevice.getUserId()), knownDevicesUtil.getKnownDevicesRequestBody(knownDevice.getDeviceId(), knownDevice.getChannelId(), knownDevice.getAgentId()).toString(), "POST");
            return "success";
        case Constants.REMOVE_DEVICE_ACTION:
            userServiceClient.deleteCall(knownDevicesUtil.removeDevice(knownDevice.getUserId()), knownDevicesUtil.getKnownDevicesRequestBody(knownDevice.getDeviceId(), knownDevice.getChannelId(), knownDevice.getAgentId()).toString());
            return "success";
        default:
            logger.info("Invalid action given {}",knownDevice.getAction());
            return null;
        }
    }
}
