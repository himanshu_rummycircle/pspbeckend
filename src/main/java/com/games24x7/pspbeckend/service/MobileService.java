/**
 * 
 */
package com.games24x7.pspbeckend.service;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.pojo.NFSUserDetails;
import com.games24x7.pspbeckend.pojo.UserDetails;
import com.games24x7.pspbeckend.webclient.NFSClient;
import com.games24x7.pspbeckend.webclient.UserServiceClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * 
 */
@Component
public class MobileService {
    private static final Logger logger = LoggerFactory.getLogger(MobileService.class);

    @Autowired
    UserServiceClient usClient;
    @Autowired
    Configuration config;
    @Autowired
    NFSClient nfsClient;

    /**
     * @param mobileNo
     * @return
     */
    public long getUserData(long mobileNo) {
        long userId = 0;
        validateMobileno(mobileNo);
        String response = usClient.getStringResGetCall(config.get("ums.user.mobile.url") + mobileNo);
        if (response != null) {

            List<UserDetails> jsonObjList = getUserDetailsData(response);
            logger.debug("jsonObjList ::: {} ", jsonObjList);

            List<UserDetails> mobileConfirmed = getMobileConfirmedUserDetails(jsonObjList);

            if (!mobileConfirmed.isEmpty()) {

                userId = getMobileVerifiedAndActiveUser(mobileConfirmed);

                if (userId != 0) {
                    logger.info("getMobileVerifiedAndActiveUser fetched UserId {} for mobile {} ", userId, mobileNo);
                    return userId;
                }

                userId = getMobileVerifiedAndNonActiveUser(mobileConfirmed);

                if (userId != 0) {
                    logger.info("getMobileVerifiedAndNonActiveUser fetched UserId {} for mobile {} ", userId, mobileNo);
                    return userId;
                }
            }
            List<UserDetails> mobileNotConfirmed = getMobileNonConfirmedUserDetails(jsonObjList);

            if (!mobileNotConfirmed.isEmpty()) {

                userId = getMobileNonVerifiedAndActiveUser(mobileNotConfirmed);

                if (userId != 0) {
                    logger.info("getMobileNonVerifiedAndActiveUser fetched UserId {} for mobile {} ", userId, mobileNo);
                    return userId;
                }

                userId = getMobileNonVerifiedAndNonActiveUser(mobileNotConfirmed);

                if (userId != 0) {
                    logger.info("getMobileNonVerifiedAndNonActiveUser fetched UserId {} for mobile {} ", userId,
                            mobileNo);
                    return userId;
                }

            }

        } else {
            logger.info("response from ums for mobileNo {} was null ", mobileNo);
        }
        logger.info("getUserData fetched UserId {} for mobile {} ", userId, mobileNo);
        return userId;
    }

    /**
     * @param mobileNo
     */
    private void validateMobileno(long mobileNo) {
        if (mobileNo <= 0) {
            throw new ServiceException(Constants.INVALID_MOBILE, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_MOBILE_CODE);
        }
    }

    /**
     * @param mobileNotConfirmed
     * @return
     */
    private long getMobileNonVerifiedAndNonActiveUser(List<UserDetails> mobileNotConfirmed) {

        List<UserDetails> mobileNotConfirmedAndInActive = getInActiveUserDetails(mobileNotConfirmed);

        if (!mobileNotConfirmedAndInActive.isEmpty()) {
            mobileNotConfirmedAndInActive
                    .sort((UserDetails s1, UserDetails s2) -> s2.getLast_login().compareTo(s1.getLast_login()));
            logger.debug("mobileNotConfirmedAndInActive AFTER SORT BY Last_login {}", mobileNotConfirmedAndInActive);

            return mobileNotConfirmedAndInActive.get(0).getUser_id();
        }

        return 0;
    }

    /**
     * 
     * @param mobileNotConfirmed
     * @return
     */

    private long getMobileNonVerifiedAndActiveUser(List<UserDetails> mobileNotConfirmed) {

        List<UserDetails> mobileNotConfirmedAndActive = getActiveUserDetails(mobileNotConfirmed);
        if (!mobileNotConfirmedAndActive.isEmpty()) {
            logger.debug("mobileNotConfirmedAndActive before post call {}", mobileNotConfirmedAndActive);

            List<UserDetails> mobileNotConfirmedAndActiveWithClub = getClubTypeDetails(mobileNotConfirmedAndActive);

            mobileNotConfirmedAndActiveWithClub
                    .sort((UserDetails s1, UserDetails s2) -> s2.getClub_type() - s1.getClub_type());

            logger.debug("mobileNotConfirmedAndActive after SORTING  CLUBTYPE {}", mobileNotConfirmedAndActiveWithClub);

            int highestClub = mobileNotConfirmedAndActiveWithClub.get(0).getClub_type();
            List<UserDetails> mobileNotConfirmedAndActiveSameClubType = mobileNotConfirmedAndActiveWithClub.stream()
                    .filter(a -> a.getClub_type() == highestClub).collect(Collectors.toList());

            if (!mobileNotConfirmedAndActiveSameClubType.isEmpty()) {

                mobileNotConfirmedAndActiveSameClubType
                        .sort((UserDetails s1, UserDetails s2) -> s2.getLast_login().compareTo(s1.getLast_login()));
                logger.debug("mobileNotConfirmedAndActiveSameClubType AFTER SORT BY Last_login {}",
                        mobileNotConfirmedAndActiveSameClubType);
                return mobileNotConfirmedAndActiveSameClubType.get(0).getUser_id();

            } else {
                return mobileNotConfirmedAndActiveWithClub.get(0).getUser_id();
            }
        }
        return 0;

    }

    /**
     * @param mobileNotConfirmedAndActive
     * @return
     */
    private List<UserDetails> getClubTypeDetails(List<UserDetails> mobileNotConfirmedAndActive) {
        List<Long> userIdList = mobileNotConfirmedAndActive.stream().map(a -> a.getUser_id())
                .collect(Collectors.toList());
        logger.debug("mobileNotConfirmedAndActive userId List {}", userIdList);

        String result = nfsClient.postCall(config.get("nfs.club.type.url"), getNfsObj(userIdList), "POST");
        if (result != null) {
            Gson gJson = new Gson();
            Type typ = new TypeToken<List<NFSUserDetails>>() {
            }.getType();
            List<NFSUserDetails> nfsUserDetailsList = gJson.fromJson(result, typ);
            mobileNotConfirmedAndActive.forEach(a -> {
                int clubType = getClubType(nfsUserDetailsList, a.getUser_id());
                a.setClub_type(clubType);
            });
        }
        logger.debug("mobileNotConfirmedAndActive after post call {}", mobileNotConfirmedAndActive);
        return mobileNotConfirmedAndActive;
    }

    /**
     * @param list
     * @return
     */
    private List<UserDetails> getMobileNonConfirmedUserDetails(List<UserDetails> list) {
        return list.stream().filter(a -> a.getMobile_confirmation() == 0).collect(Collectors.toList());
    }

    /**
     * @param mobileConfirmed
     * @return
     */
    private long getMobileVerifiedAndNonActiveUser(List<UserDetails> mobileConfirmed) {
        List<UserDetails> mobileConfirmedAndInActive = getInActiveUserDetails(mobileConfirmed);
        if (!mobileConfirmedAndInActive.isEmpty()) {
            mobileConfirmedAndInActive
                    .sort((UserDetails s1, UserDetails s2) -> s2.getLast_login().compareTo(s1.getLast_login()));
            logger.debug("AFTER SORT BY Last_login mobileConfirmedAndInActive {}", mobileConfirmedAndInActive);
            return mobileConfirmedAndInActive.get(0).getUser_id();
        }
        return 0;
    }

    /**
     * @param mobileConfirmed
     * @return
     */
    private List<UserDetails> getInActiveUserDetails(List<UserDetails> mobileConfirmed) {
        return mobileConfirmed.stream().filter(a -> a.getActive() == 0).collect(Collectors.toList());
    }

    /**
     * @param mobileConfirmed
     * @return
     */
    private long getMobileVerifiedAndActiveUser(List<UserDetails> mobileConfirmed) {
        List<UserDetails> mobileConfirmedAndActive = getActiveUserDetails(mobileConfirmed);
        if (!mobileConfirmedAndActive.isEmpty()) {
            logger.warn("mobileConfirmedAndActive size {} ", mobileConfirmedAndActive.size());
            return mobileConfirmedAndActive.get(0).getUser_id();
        }
        return 0;
    }

    /**
     * @param list
     * @return
     */
    private List<UserDetails> getActiveUserDetails(List<UserDetails> list) {
        return list.stream().filter(a -> a.getActive() == 1).collect(Collectors.toList());
    }

    /**
     * @param list
     * @return
     */
    private List<UserDetails> getMobileConfirmedUserDetails(List<UserDetails> list) {
        return list.stream().filter(a -> a.getMobile_confirmation() == 1).collect(Collectors.toList());
    }

    /**
     * @param response
     * @return
     */
    private List<UserDetails> getUserDetailsData(String response) {
        Gson googleJson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
        Type type = new TypeToken<List<UserDetails>>() {
        }.getType();
        List<UserDetails> jsonObjList = googleJson.fromJson(response, type);
        jsonObjList.forEach(obj -> {
            logger.debug(obj.toString());
            obj.setActive((obj.getAccount_status() == 6 || obj.getAccount_status() == 5) ? 0 : 1);
        });
        return jsonObjList;
    }

    /**
     * @param nfsUserDetailsList
     * @param userId
     * @return
     */
    private int getClubType(List<NFSUserDetails> nfsUserDetailsList, long userId) {
        Optional<NFSUserDetails> option = nfsUserDetailsList.stream().filter(a -> a.getUserId() == userId).findFirst();
        if (option.isPresent()) {
            logger.debug("getNFSUserDataFor {}, clubType {}", userId, option.get().getClubType());
            return option.get().getClubType();
        } else {
            logger.debug("getNFSUserDataFor {}, clubType NA -0", userId);
            return 0;
        }
    }

    /**
     * @param userIdList
     * @return
     */
    private String getNfsObj(List<Long> userIdList) {
        JSONObject obj = new JSONObject();
        obj.put("userIds", userIdList);
        obj.put("projection", "userId,clubType");
        logger.info("getNfsObj {}", obj.toString());
        return obj.toString();
    }

}
