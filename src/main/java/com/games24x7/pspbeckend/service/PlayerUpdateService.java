package com.games24x7.pspbeckend.service;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.games24x7.frameworks.audit.entities.EventData;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.AuditUtil;
import com.games24x7.pspbeckend.Util.PlayerUpdateUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;

@Service
public class PlayerUpdateService {
    private static final Logger logger = LoggerFactory.getLogger(PlayerUpdateService.class);
    @Autowired
    AuditUtil util;

    public void processAuditData(JSONObject value, JSONObject context) {
        try {
            Long agentId = value.has(Constants.AGENT_ID) && !value.isNull(Constants.AGENT_ID)
                    ? value.getLong(Constants.AGENT_ID)
                    : 0;
            Long userId = value.has(Constants.USERID) && !value.isNull(Constants.USERID)
                    ? value.getLong(Constants.USERID)
                    : 0;
            Integer changedField = value.has(Constants.FIELD_UPDATED) && !value.isNull(Constants.FIELD_UPDATED)
                    ? value.getInt(Constants.FIELD_UPDATED)
                    : null;
            String comment = value.has(Constants.COMMENT) && !value.isNull(Constants.COMMENT)
                    ? value.getString(Constants.COMMENT)
                    : null;
            String prevData = value.has(Constants.PREV_DATA) && !value.isNull(Constants.PREV_DATA)
                    ? value.getString(Constants.PREV_DATA)
                    : null;
            String additionalInfo = value.has(Constants.ADDITIONAL_INFO) && !value.isNull(Constants.ADDITIONAL_INFO)
                    ? value.getString(Constants.ADDITIONAL_INFO)
                    : null;
            Integer docType = value.has(Constants.DOCTYPE) && !value.isNull(Constants.DOCTYPE)
                    ? value.getInt(Constants.DOCTYPE)
                    : null;
            String docId = value.has(Constants.DOCUMENT_ID) && !value.isNull(Constants.DOCUMENT_ID)
                    ? value.getString(Constants.DOCUMENT_ID)
                    : null;
            Boolean docVerified = value.has(Constants.DOC_VERIFIED) && !value.isNull(Constants.DOC_VERIFIED)
                    ? value.getBoolean(Constants.DOC_VERIFIED)
                    : null;

            String contactDirection = value.has(Constants.CONTACT_DIRECTION)
                    && !value.isNull(Constants.CONTACT_DIRECTION) ? value.getString(Constants.CONTACT_DIRECTION) : null;
            String contactType = value.has(Constants.CONTACT_TYPE) && !value.isNull(Constants.CONTACT_TYPE)
                    ? value.getString(Constants.CONTACT_TYPE)
                    : null;
            String cmrInteractionId = value.has(Constants.CMR_INTERACTION_ID)
                    && !value.isNull(Constants.CMR_INTERACTION_ID) ? value.getString(Constants.CMR_INTERACTION_ID)
                            : null;
            String gameType = value.has(Constants.GAME_TYPE) && !value.isNull(Constants.GAME_TYPE)
                    ? value.getString(Constants.GAME_TYPE)
                    : null;
            String issueCatagory = value.has(Constants.ISSUE_CATAGORY) && !value.isNull(Constants.ISSUE_CATAGORY)
                    ? value.getString(Constants.ISSUE_CATAGORY)
                    : null;

            String source = context.has(Constants.SOURCE) && !context.isNull(Constants.SOURCE)
                    ? context.getString(Constants.SOURCE)
                    : null;

            EventData event = util.getPspplayerupdates(agentId, userId, changedField, comment, prevData, additionalInfo,
                    docType, docId, docVerified, contactDirection, contactType, cmrInteractionId, gameType,
                    issueCatagory, source);
            util.sendCustomAuditMessageToKafka(event);
        } catch (Exception e) {
            throw new ServiceException(WebConstant.GENERIC_ERROR, Status.INTERNAL_SERVER_ERROR.getStatusCode(), e,
                    WebConstant.GENERIC_ERROR_CODE);
        }

    }
}
