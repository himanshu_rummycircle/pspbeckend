package com.games24x7.pspbeckend.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.RGPConstants;
import com.games24x7.pspbeckend.webclient.AuditServiceClient;
import com.games24x7.pspbeckend.webclient.RGPClient;

@Service
public class RGPService {

	private static final Logger logger = LoggerFactory.getLogger(RGPService.class);

	@Autowired
	RGPClient rgpClient;

	@Autowired
	Configuration config;

	@Autowired
	AuditServiceClient auditclient;

	public String getRGPStatus(long userId) {
		String url = config.get(RGPConstants.RGP_SERVICE_URL) + config.get(RGPConstants.GET_RGP_STATUS_API) + userId;
		return rgpClient.getCall(url);
	}

	@MonitorCalls
	public String getRGPStatusChangeLog(long userId) {
		String url = config.get(Constants.MTTSettlementConstant.AUDIT_SERVICE_URL)
				+ config.get(Constants.NEW_AUDIT_DATA_API);
		JSONObject requestBody = new JSONObject();
		requestBody.put(Constants.USERID, userId);
		requestBody.put(RGPConstants.DATAPOINT_KEY, RGPConstants.RGP_AUDIT_DATAPOINT);
		requestBody.put(RGPConstants.TYPE_KEY, RGPConstants.TYPE_COUNT);
		requestBody.put(RGPConstants.PARTITIONBY_KEY, Constants.USERID);
		requestBody.put(RGPConstants.SELECTORVAL_KEY, userId);
		requestBody.put(RGPConstants.COUNT_KEY, config.get(RGPConstants.MAX_RPG_STATUS_CHANGE_LOG_TO_SHOW));
		logger.debug(userId+": getRGPStatusChangeLog url "+url+" requestBody "+requestBody);
		return auditclient.postCall(url, requestBody.toString(), Constants.HTTP_POST);
	}

	public void updateRGPStatus(long userId, JSONObject payload) {
		validateNGetpayload(payload);
		String url = config.get(RGPConstants.RGP_SERVICE_URL) + config.get(RGPConstants.UPDATE_RGP_STATUS_API);
		rgpClient.postCall(url, payload);
	}

	private void validateNGetpayload(JSONObject json) {
		int rgpStatus = json.optInt(RGPConstants.RGP_STATUS_KEY);
		if (rgpStatus <= 0) {
			throw new ServiceException(RGPConstants.MISSING_INVALID_RGP_STATUS, Status.BAD_REQUEST.getStatusCode(),
					RGPConstants.MISSING_INVALID_RGP_STATUS_ERRCODE);
		}
		String contactType = json.optString(RGPConstants.CONTACT_TYPE_KEY, null);
		logger.debug("contactType " + contactType);
		if (contactType == null || contactType.trim().isEmpty()) {
			throw new ServiceException(RGPConstants.MISSING_CONTACT_TYPE, Status.BAD_REQUEST.getStatusCode(),
					RGPConstants.MISSING_CONTACT_TYPE_ERRCODE);
		}
		String contactDirection = json.optString(RGPConstants.CONTACT_DIRECTION_KEY, null);
		if (contactDirection == null || contactDirection.trim().isEmpty()) {
			throw new ServiceException(RGPConstants.MISSING_CONTACT_DIRECTION, Status.BAD_REQUEST.getStatusCode(),
					RGPConstants.MISSING_CONTACT_DIRECTION_ERRCODE);
		}
		validateComments(json.optString(RGPConstants.COMMENTS_KEY, null));
	}

	private void validateComments(String s) {
		if (s == null || s.trim().isEmpty() || s.trim().length() < 4) {
			throw new ServiceException(RGPConstants.MISSING_INVALID_COMMENT, Status.BAD_REQUEST.getStatusCode(),
					RGPConstants.MISSING_INVALID_COMMENT_ERR_CODE);
		}
		Pattern p = Pattern.compile("[^A-Za-z0-9 .,]");
		Matcher m = p.matcher(s);
		boolean b = m.find();
		if (b == true)
			throw new ServiceException(RGPConstants.SPECIAL_CHARACTER_IN_COMMENT, Status.BAD_REQUEST.getStatusCode(),
					RGPConstants.SPECIAL_CHARACTER_IN_COMMENT_ERR_CODE);
	}
}
