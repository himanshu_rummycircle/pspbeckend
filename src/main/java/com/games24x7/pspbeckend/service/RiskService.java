package com.games24x7.pspbeckend.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.frameworks.serviceutil.httpclient.RetryConfig;
import com.games24x7.pspbeckend.Util.AuditUtil;
import com.games24x7.pspbeckend.Util.RestCoreUtil;
import com.games24x7.pspbeckend.Util.RiskCertificateHelper;
import com.games24x7.pspbeckend.Util.RiskUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.constants.Constants.RiskConstant;
import com.games24x7.pspbeckend.dao.RiskCertificateDAO;
import com.games24x7.pspbeckend.pojo.PlayersRiskCertificate;
import com.games24x7.pspbeckend.pojo.RiskCertificate;
import com.games24x7.pspbeckend.pojo.UpdateRiskCertificate;
import com.games24x7.pspbeckend.pojo.WebResponse;
import com.games24x7.pspbeckend.webclient.AuditServiceClient;

@Component
public class RiskService {

    private static final Logger logger = LoggerFactory.getLogger(RiskService.class);

    @Autowired
    RiskCertificateHelper riskCerficateHelper;

    @Autowired
    RiskCertificateDAO certificateDAO;

    @Autowired
    Configuration config;

    @Autowired
    AuditUtil audit;

    @Autowired
    RiskUtil util;

    @Autowired
    RestCoreUtil restUtil;

    @Autowired
    AuditServiceClient auditServiceClient;

    HashMap<String, Integer> riskMapNameAsKey = null;
    HashMap<Integer, String> riskMapIntAsKey = null;
    Set<Integer> validRiskStatusSet = null;

    public void loadRiskCertifiatesInfo() {
        try {
            //riskMapNameAsKey = certificateDAO.getRiskType();
            String riskMapConfig = config.get(RiskConstant.RISK_MAP_VALUE,RiskConstant.RISK_MAP_VALUE_DEFAULT);
            List<String> riskMapList = Arrays.asList(riskMapConfig.split(","));
            riskMapNameAsKey = new HashMap<>();
            try {
                if (riskMapList != null && riskMapList.size() > 0) {
                    for (int i = 0; i < riskMapList.size(); i++) {
                        String[] risks = riskMapList.get(i).split(":");
                        riskMapNameAsKey.put(risks[0], Integer.parseInt( risks[1] ));
                    }
                }
            } catch (Exception e) {
                logger.error("Exception while executing updateStateBasedOnIFSCCode method : ", e);
            }
            riskMapIntAsKey = new HashMap<Integer, String>();
            for (String riskStatus : riskMapNameAsKey.keySet()) {
                riskMapIntAsKey.put(riskMapNameAsKey.get(riskStatus), riskStatus);
            }
        } catch (Exception e) {
            logger.error("Exception while getting getRiskCertificates , Error {}", e);
            throw new ServiceException(RiskConstant.DATABASE_OPERATION_ERROR, Status.BAD_REQUEST, e,
                    RiskConstant.DATABASE_OPERATION_ERROR_CODE);
        }

    }
    
    public void loadValidaRiskStatus(){
        String validRiskStatus = config.get(RiskConstant.VALID_RISK_STATUS, "0,1,2");
        List<String> validRiskStatusList = Arrays.asList(validRiskStatus.split(","));
        validRiskStatusSet= new HashSet<>();
        for (String riskStatusValue : validRiskStatusList) {
            validRiskStatusSet.add(Integer.parseInt(riskStatusValue));
        }
    }

    public String createRiskCertificate(JSONObject jsonPayload) {

        RiskCertificate riskPayload = riskCerficateHelper.jsonToRiskCertificatePayload(jsonPayload);
        riskCerficateHelper.validateCreateInputPayload(riskPayload);
        long certId = createRiskCertificateInDatabase(riskPayload);
        sendAuditMessage(riskPayload, certId);
        return new JSONObject().put(RiskConstant.STATUS, Constants.SUCCESS).toString();
    }

    void sendAuditMessage(RiskCertificate riskPayload, long certId) {
        try {
            audit.publishToAuditRiskCertificate(riskPayload, certId);
        } catch (Exception e) {
            logger.error("Exception is sending message to Audit Kafka ", e);
        }

    }
    public long createRiskCertificateInDatabase(RiskCertificate riskPayload) {
        try {
            loadRiskCertifiatesInfo();
            riskPayload.setRiskStatusType(riskMapNameAsKey.get(riskPayload.getRiskStatus()));
            certificateDAO.createRiskCertificate(riskPayload);
        } catch (DuplicateKeyException ex) {
            throw new ServiceException(RiskConstant.CERTIFICATE_NAME_EXIST, Status.BAD_REQUEST, ex,
                    RiskConstant.CERTIFICATE_NAME_EXIST_CODE);
        }catch (Exception e) {
            throw new ServiceException(RiskConstant.DATABASE_OPERATION_ERROR, Status.INTERNAL_SERVER_ERROR, e,
                    RiskConstant.DATABASE_OPERATION_ERROR_CODE);
        }
        return riskPayload.getId();
    }

    public JSONArray getRiskCertificate(int riskType, long offset) {
        loadValidaRiskStatus();
        loadRiskCertifiatesInfo();
        riskCerficateHelper.validateGetRiskCertificateInput(riskType,offset,validRiskStatusSet);
        return getRiskCertificateDB(riskType,offset);
    }

    public JSONArray getRiskCertificateDB(int riskType, long offset) {
        List<RiskCertificate> riskList = null;
        try {
            riskList = certificateDAO.getRiskCertificate(riskType, offset);
        } catch (Exception e) {
            logger.error("Exception while getting getRiskCertificates , Error {}", e);
            throw new ServiceException(RiskConstant.DATABASE_OPERATION_ERROR, Status.BAD_REQUEST, e,
                    RiskConstant.DATABASE_OPERATION_ERROR_CODE);
        }
        return riskCerficateHelper.getRiskListInJSONArray(riskList, riskMapIntAsKey);
    }

    public JSONArray updateRiskCertificate(JSONObject jsonObject) {

        UpdateRiskCertificate riskPayload = riskCerficateHelper.validateUpdateRiskCertificateInput(jsonObject);
        certificateDAO.updateRiskCertificateInDB(riskPayload);
        sendUpdateCertificateAuditMessage(riskPayload);
        if(riskPayload.getActivate()==true){
            return getRiskCertificatePostUpdate(0);
        }
        return  getRiskCertificatePostUpdate(1);
        
    }

    public void sendUpdateCertificateAuditMessage(UpdateRiskCertificate riskPayload) {
        String riskCertificateIds = riskPayload.getRiskCertificateIds();
        List<String> riskIdList = Arrays.asList(riskCertificateIds.split(","));;
        for(String riskId :  riskIdList){
            sendAuditMessage( new RiskCertificate(Long.parseLong( riskId ), riskPayload.getActivate()), Long.parseLong(riskId));
        }
    }

    public JSONArray getRiskCertificatePostUpdate(int allRiskType) {
        loadRiskCertifiatesInfo();
        try {
            return getRiskCertificateDB(allRiskType, 0);
        } catch (Exception e) {
            logger.info("Exception while fetching risk certificate post update  : {} ", e);
        }
        return new JSONArray();
    }


 
    

    /**
     * @param userId
     * @param riskStatus
     * @param riskCertificates
     * @param riskLogs
     * @return
     */
    public String fetchRiskStatusLogsAndCertificates(long userId, boolean riskStatus, boolean riskLogs,
            boolean riskCertificates) {
        JSONObject obj = new JSONObject();
        obj.put("currentStatus", riskStatus ? util.getRiskStatus(userId) : null);
        obj.put("logs", riskLogs ? util.getRiskLog(userId) : new JSONArray());
        obj.put("certificates", riskCertificates ? getRiskCertificateDB(RiskConstant.ACTIVE, 0) : new JSONArray());

        return obj.toString();
    }

    public String updatePlayersRiskStatus(JSONObject jsonObject) {
        PlayersRiskCertificate riskPayload = riskCerficateHelper.validatePlayersRiskCertificateInput(jsonObject);
        updatePlayersStatusAndPublishDataToAudit(riskPayload);
        return fetchRiskStatusLogsAndCertificates(riskPayload.getUserId(),true,true,true);
    }

    public void updatePlayersStatusAndPublishDataToAudit(PlayersRiskCertificate riskPayload) {

        // Call user service API
        updateRiskStatusCall(riskPayload);
        // Call Audit service
        callAuditService(riskPayload);
    }

    public void callAuditService(PlayersRiskCertificate riskPayload) {
        try {
            audit.publishToAuditPlayerRiskCertificate(riskPayload);
        } catch (Exception e) {
            logger.error("Exception is sending message to Audit Kafka ", e);
        }

    }

    public void updateRiskStatusCall(PlayersRiskCertificate riskPayload) {

        String url = config.get(MTTSettlementConstant.USER_SERVICE_URL)
                + MTTSettlementConstant.USER_SERVICE_IN_PREFIX_RISK + riskPayload.getUserId();
        RetryConfig retryConfig = RetryConfig.getStandardRetryConfigForPutPost();
        WebResponse webResponse = null;
        JSONObject payload = prepareUpdateRiskStatus(riskPayload);
        try {
            webResponse = restUtil.callPut(url, payload, retryConfig);
        } catch (Exception e) {
            logger.error("Excepton calling User Service :: " + e);
            throw new ServiceException(MTTSettlementConstant.US_CALL_FAILURE_MSG, Status.INTERNAL_SERVER_ERROR,
                    MTTSettlementConstant.US_CALL_FAILURE_CODE);
        }
        if (webResponse.getStatusCode() != Status.OK.getStatusCode()) {
            logger.error("Excepton calling User Service :: status {}  ", webResponse.getStatusCode());
            throw new ServiceException(MTTSettlementConstant.US_CALL_FAILURE_MSG, Status.INTERNAL_SERVER_ERROR,
                    MTTSettlementConstant.US_CALL_FAILURE_CODE);
        }
    }

    JSONObject prepareUpdateRiskStatus(PlayersRiskCertificate riskPayload) {
        loadRiskCertifiatesInfo();
        JSONObject payloadJSON = new JSONObject();
        JSONObject payload = new JSONObject();
        payload.put("risk_status", riskMapNameAsKey.get(riskPayload.getRiskStatus()));
        payloadJSON.put("value", payload);
        return payloadJSON;
    }

}
