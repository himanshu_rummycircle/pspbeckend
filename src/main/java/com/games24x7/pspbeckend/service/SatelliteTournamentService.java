package com.games24x7.pspbeckend.service;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.SatelliteSettlementHelper;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.dao.SatelliteSettlementDAO;
import com.games24x7.pspbeckend.pojo.OfflineSettlementMoneyInfo;
import com.games24x7.pspbeckend.pojo.OfflineSettlementPayload;
import com.games24x7.pspbeckend.pojo.WebResponse;
import com.games24x7.pspbeckend.webclient.FMSClient;
import com.games24x7.pspbeckend.webclient.GPCClient;

@Component
public class SatelliteTournamentService {
    private static final Logger logger = LoggerFactory.getLogger(SatelliteTournamentService.class);

    @Autowired
    SatelliteSettlementHelper offlineSettlementHelper;

    @Autowired
    FMSClient fmsClient;

    @Autowired
    SatelliteSettlementDAO settlementDAO;

    @Autowired
    Configuration config;

    @Autowired
    GPCClient gpcClient;

    /**
     * Code for processing the offline settlement.
     * 
     * @param jsonPayload
     * @return
     */
    public String processOfflineSatelliteSettlement(JSONObject jsonPayload) {
        OfflineSettlementPayload payload = offlineSettlementHelper.jsonToOfflineSettlementPayload(jsonPayload);
        OfflineSettlementMoneyInfo moneyInfo = offlineSettlementHelper.validateInputData(payload);
        callSettlementProcedure(payload, moneyInfo);
        offlineSettlementHelper.publishEdsMessageAndAuditLogs(payload);
        // call to GPC
        postDataToGPC(jsonPayload);
        return new JSONObject().put(Constants.SUCCESS, true).toString();
    }

    /**
     * @param jsonPayload
     */
    private void postDataToGPC(JSONObject jsonPayload) {
        try {
            String url = config.get(MTTSettlementConstant.GPC_URL,
                    "http://10.14.25.83:8080/gpc/api/lobby/updateOfflinePrizelist");
            gpcClient.postCall(url, jsonPayload.toString(), "POST");
        } catch (Exception e) {
            logger.error("EXCEPTION IN GPC post request ", e);
        }

    }

    /**
     * Calling stored procedure
     * 
     * @param payload
     * @param settlementMoneyInfo
     */
    void callSettlementProcedure(OfflineSettlementPayload payload, OfflineSettlementMoneyInfo settlementMoneyInfo) {

        try {
            settlementDAO.processOfflineSettlement(payload, settlementMoneyInfo.getTotalPrizePool());
        } catch (Exception e) {
            throw new ServiceException(MTTSettlementConstant.DATABASE_OPERTAION_FAILED_ERROR, Status.BAD_REQUEST,
                    MTTSettlementConstant.DATABASE_OPERTAION_FAILED_ERRPR_CODE);
        }
        if (settlementMoneyInfo.getTotalQualifierContribution() > settlementMoneyInfo.getTotalPrizePool()) {
            WebResponse fmsCallResponse = fmsClient.excessRevenueMovement(payload.getTournamentId(),payload.getExpense());
            if (fmsCallResponse.getStatusCode() != Status.ACCEPTED.getStatusCode()) {
                throw new ServiceException(MTTSettlementConstant.FMS_CALL_FAILURE_MSG, Status.BAD_REQUEST,
                        MTTSettlementConstant.FMS_CALL_FAILURE_MSG_CODE);
            }
            logger.info("Excess revenue post request done.Response ", fmsCallResponse);
        }
    }

}
