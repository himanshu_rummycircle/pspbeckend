package com.games24x7.pspbeckend.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.UpdateMobileUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.UMSConstants;
import com.games24x7.pspbeckend.pojo.WebResponse;

@Service
public class UpdateMobileService {

    @Autowired
    UpdateMobileUtil updateMobileUtil;

    public WebResponse updateMobile(long userId, JSONObject payload) {
        validateUpdateMobilePayload(userId, payload);
        WebResponse webResponse = updateMobileUtil.updateMobile(userId, payload);
        if (webResponse.getStatusCode() == Status.OK.getStatusCode()) {
            updateMobileUtil.deleteAllSessionsOfUser(userId);
        }
        return webResponse;
    }

    public void validateUpdateMobilePayload(long userId, JSONObject payload) {
        if (userId <= 0) {
            throw new ServiceException(Constants.INVALID_USER, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_USER_ID_ERR_CODE);
        }
        if (!payload.has(Constants.VALUE)) {
            throw new ServiceException(Constants.VALUE_MISSING_FROM_PAYLOAD_MSG, Status.BAD_REQUEST.getStatusCode(),
                    Constants.VALUE_MISSING_FROM_PAYLOAD);
        }
        JSONObject value = payload.getJSONObject(Constants.VALUE);
        if (!value.has(UMSConstants.MOBILE)) {
            throw new ServiceException(Constants.MOBILE_MISSING_MSG, Status.BAD_REQUEST.getStatusCode(),
                    Constants.MOBILE_MISSING_CODE);
        }
        if (value.get(UMSConstants.MOBILE) == null || !isMobileNumberValid(value.get(UMSConstants.MOBILE).toString())) {
            throw new ServiceException(Constants.INVALID_MOBILE_ERROR_MSG, Status.BAD_REQUEST.getStatusCode(),
                    Constants.INVALID_MOBILE_ERROR_CODE);
        }
        if (!payload.has(Constants.CONTEXT)) {
            throw new ServiceException(Constants.CONTEXT_MISSING_FROM_PAYLOAD_MSG, Status.BAD_REQUEST.getStatusCode(),
                    Constants.CONTEXT_MISSING_FROM_PAYLOAD);
        }
        JSONObject context = payload.getJSONObject(Constants.CONTEXT);
        if (!context.has(UMSConstants.AGENT_NAME) || context.get(UMSConstants.AGENT_NAME) == null
                || context.getString(UMSConstants.AGENT_NAME).trim().isEmpty()) {
            throw new ServiceException(Constants.AGENT_NAME_INVALID_MSG, Status.BAD_REQUEST.getStatusCode(),
                    Constants.AGENT_NAME_INVALID_CODE);
        }
        if (!context.has(UMSConstants.AGENT_GROUP) || context.get(UMSConstants.AGENT_GROUP) == null
                || context.getString(UMSConstants.AGENT_GROUP).trim().isEmpty()) {
            throw new ServiceException(Constants.AGENT_GROUP_INVALID_MSG, Status.BAD_REQUEST.getStatusCode(),
                    Constants.AGENT_GROUP_INVALID_CODE);
        }
        if (!context.has(UMSConstants.COMMENTS) || context.get(UMSConstants.COMMENTS) == null
                || context.getString(UMSConstants.COMMENTS).trim().isEmpty()) {
            throw new ServiceException(Constants.COMMENTS_INVALID_MSG, Status.BAD_REQUEST.getStatusCode(),
                    Constants.COMMENTS_INVALID_CODE);
        }
    }

    private boolean isMobileNumberValid(String mobileNumber) {
        Pattern pattern = Pattern.compile("[6-9][0-9]{9}");
        Matcher matcher = pattern.matcher(mobileNumber);
        return matcher.find() && matcher.group().equals(mobileNumber);
    }

}
