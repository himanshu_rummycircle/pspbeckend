package com.games24x7.pspbeckend.webclient;

import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.RestCoreUtil;
import com.games24x7.pspbeckend.constants.Constants;

@Component
public class AvsClient {
    private static final Logger logger = LoggerFactory.getLogger(AvsClient.class);
    @Autowired
    RestCoreUtil restUtil;

    /**
     * @param url
     * @param body
     * @param requestMethod
     * @return
     */
    @MonitorCalls
    public String postCall(String url, String body, String requestMethod) {
        String res = null;
        try {
            res = restUtil.postCall(url, body, requestMethod);
        } catch (ServiceException e) {
            logger.debug("Excepton error code :: " + e.getBusinessErrorCode() + " :: " + e.getErrorStatus());
            logger.error(e.getMessage(), e);

        }
        return res;

    }

    /**
     * @param url
     * @return
     */
    @MonitorCalls
    public JSONArray getCall(String url) {
        JSONArray arr = null;
        try {
            String res = restUtil.getCall(url);
            if (res != null)
                arr = new JSONArray(res);
            logger.debug("Output ** " + arr);
        } catch (ServiceException e) {
            logger.debug("Excepton error code :: " + e.getErrorStatus() + " :: " + Status.NOT_FOUND);
            if (e.getErrorStatus() == Status.NOT_FOUND)
                logger.error(e.getMessage(), e);
            else
                throw e;
        }

        return arr;
    }

    /**
     * 
     * @param url
     * @return
     */
    @MonitorCalls
    public String getStringResGetCall(String url) {
        String res = null;
        try {
            res = restUtil.getCall(url);
            logger.debug("Output ** " + res);
        } catch (ServiceException e) {
            logger.debug(Constants.ERROR_AT + e.getErrorStatus() + " :: " + Status.NOT_FOUND);
            if (e.getErrorStatus() == Status.NOT_FOUND)
                logger.error(e.getMessage(), e);
            else
                throw e;
        }

        return res;
    }
}
