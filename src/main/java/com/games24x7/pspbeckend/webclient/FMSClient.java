package com.games24x7.pspbeckend.webclient;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.frameworks.serviceutil.httpclient.RetryConfig;
import com.games24x7.pspbeckend.Util.RestCoreUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.pojo.WebResponse;

@Component
public class FMSClient {
    private static final Logger logger = LoggerFactory.getLogger(FMSClient.class);
    @Autowired
    RestCoreUtil restUtil;

    @Autowired
    Configuration config;

    /**
     * @param url
     * @param body
     * @param requestMethod
     * @return
     */
    @MonitorCalls
    public String postCall(String url, String body, String requestMethod) {
        try {
            return restUtil.postCall(url, body, requestMethod);
        } catch (ServiceException e) {
            logger.debug("Excepton error code :: " + e.getBusinessErrorCode() + " :: " + e.getErrorStatus());
            logger.error(e.getMessage(), e);

        }
        return null;

    }

    @MonitorCalls
    public WebResponse excessRevenueMovement(Long tournamentId, double expense) {

        WebResponse webResponse = null;
        RetryConfig retryConfig = RetryConfig.getStandardRetryConfigForPutPost();
        String url = config.get(MTTSettlementConstant.FMS_URL);
        JSONObject payload = prepareExcessRevenueBody(tournamentId, expense);
        try {
            webResponse = restUtil.callPost(url, payload, retryConfig);
        } catch (Exception e) {
            logger.error("Excepton calling FMS :: " + e);
            throw new ServiceException(MTTSettlementConstant.FMS_CALL_FAILURE_MSG, Status.BAD_REQUEST,
                    MTTSettlementConstant.FMS_CALL_FAILURE_MSG_CODE);
        }

        return webResponse;
    }

    private JSONObject prepareExcessRevenueBody(Long tournamentId, double expense) {
        JSONObject json = new JSONObject();
        JSONObject jsonValue = new JSONObject();
        jsonValue.put(MTTSettlementConstant.TOURNAMENT_ID, tournamentId);
        jsonValue.put(MTTSettlementConstant.EXPENSE, expense);
        JSONObject jsonContext = new JSONObject();
        jsonContext.put(MTTSettlementConstant.SOURCE, MTTSettlementConstant.PSPBE);
        json.put(Constants.VALUE, jsonValue);
        json.put(MTTSettlementConstant.CONTEXT, jsonContext);
        return json;
    }

}
