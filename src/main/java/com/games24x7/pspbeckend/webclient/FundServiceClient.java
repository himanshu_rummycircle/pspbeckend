/**
 * 
 */
package com.games24x7.pspbeckend.webclient;

import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.frameworks.serviceutil.httpclient.RetryConfig;
import com.games24x7.pspbeckend.Util.RestCoreUtil;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.pojo.WebResponse;

/**
 * 
 */
@Component
public class FundServiceClient {
    private static final Logger logger = LoggerFactory.getLogger(FundServiceClient.class);
    @Autowired
    RestCoreUtil restUtil;

    /**
     * @param url
     * @param body
     * @param requestMethod
     * @return
     */
    @MonitorCalls
    public String postCall(String url, String body, String requestMethod) {
        try {
            return restUtil.postCall(url, body, requestMethod);
        } catch (ServiceException e) {
            logger.debug("Excepton error code :: " + e.getBusinessErrorCode() + " :: " + e.getErrorStatus());
            logger.error(e.getMessage(), e);

        }
        return null;

    }

    /**
     * @param url
     * @return
     */
    @MonitorCalls
    public JSONArray getCall(String url) {
        JSONArray arr = null;
        try {
            String res = restUtil.getCall(url);
            if (res != null)
                arr = new JSONArray(res);
            logger.debug("Output ** " + arr);
        } catch (ServiceException e) {
            if (e.getErrorStatus() == Status.NOT_FOUND)
                logger.error(e.getMessage(), e);
            else
                throw e;
        }

        return arr;
    }

    @MonitorCalls
    public JSONObject getAccount(String url, RetryConfig retryConfig) {
        JSONObject accountJson = null;
        try {
            WebResponse webResponse = restUtil.callGet(url, retryConfig);
            if (webResponse != null && webResponse.getStatusCode() == Status.OK.getStatusCode()) {
                accountJson = new JSONObject(webResponse.getPayload());
            }
        } catch (Exception ex) {
            throw new ServiceException(MTTSettlementConstant.FS_CALL_FAILURE_MSG, Status.BAD_REQUEST,
                    MTTSettlementConstant.FS_CALL_FAILURE_MSG_CODE);
        }
        return accountJson;
    }
}
