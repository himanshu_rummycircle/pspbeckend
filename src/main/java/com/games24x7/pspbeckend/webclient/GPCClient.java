/**
 * 
 */
package com.games24x7.pspbeckend.webclient;

import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.frameworks.serviceutil.httpclient.RetryConfig;
import com.games24x7.pspbeckend.Util.RestCoreUtil;
import com.games24x7.pspbeckend.pojo.WebResponse;

/**
 * 
 */
@Component
public class GPCClient {
    private static final Logger logger = LoggerFactory.getLogger(GPCClient.class);
    @Autowired
    RestCoreUtil restUtil;

    /**
     * @param url
     * @return
     */
    @MonitorCalls
    public JSONArray getCall(String url) {
        JSONArray arr = null;
        try {
            String res = restUtil.getCall(url);
            if (res != null)
                arr = new JSONArray(res);
            logger.debug("Output ** " + arr);
        } catch (ServiceException e) {
            logger.debug("Excepton error code :: " + e.getErrorStatus() + " :: " + Status.NOT_FOUND);
            if (e.getErrorStatus() == Status.NOT_FOUND)
                logger.error(e.getMessage(), e);
            else
                throw e;
        }

        return arr;
    }

    /**
     * @param url
     * @param body
     * @param requestMethod
     * @return
     */
    @MonitorCalls
    public String postCall(String url, String body, String requestMethod) {
        try {
            return restUtil.postCall(url, body, requestMethod);
        } catch (ServiceException e) {
            logger.debug("Excepton error code :: " + e.getStatus() + " errorcodre  " + e.getErrorCode() + " :: "
                    + e.getBusinessErrorCode());
            logger.error("Exception in postCall for " + url + " : body :" + body, e);
        }
        return null;

    }

}
