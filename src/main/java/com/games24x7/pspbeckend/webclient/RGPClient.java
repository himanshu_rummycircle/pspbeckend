package com.games24x7.pspbeckend.webclient;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.frameworks.serviceutil.httpclient.RetryConfig;
import com.games24x7.pspbeckend.Util.RestCoreUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;
import com.games24x7.pspbeckend.pojo.WebResponse;

@Service
public class RGPClient {

    private static final Logger logger = LoggerFactory.getLogger(RGPClient.class);
    @Autowired
    RestCoreUtil restUtil;

    /**
     * @param url
     * @return
     */
    @MonitorCalls
    public String getCall(String url) {
        String res = null;
        try {
            logger.debug("RGPClient getCall url " + url);
            res = restUtil.getCall(url);
            logger.debug("RGPClient getCall Output: " + res);
        } catch (ServiceException e) {
            logger.debug("RGPClient ServiceException :: ", e);
            throw e;
        } catch (Exception e) {
            throw new ServiceException(Constants.MTTSettlementConstant.GENERIC_ERROR,
                    Status.INTERNAL_SERVER_ERROR.getStatusCode(), Constants.MTTSettlementConstant.GENERIC_ERROR_CODE);
        }
        return res;
    }

    /**
     * @param url
     * @param body
     * @param requestMethod
     * @return
     */
    @MonitorCalls
    public String postCall(String url, JSONObject body) {
        try {
            RetryConfig retryConfig = RetryConfig.getStandardRetryConfigForPutPost();
            retryConfig.setNoOfRetries(0);
            WebResponse wres = restUtil.callPost(url, body, retryConfig);
            if (wres.getStatusCode() == Status.OK.getStatusCode()) {
                logger.debug(url + ": Output: " + wres.getPayload());
                return wres.getPayload();
            } else {
                JSONObject err = new JSONObject(wres.getPayload());
                throw new ServiceException(
                        err.has("ErrorMessage") ? err.getString("ErrorMessage") : WebConstant.GENERIC_ERROR,
                        err.has("statusCode") ? err.getInt("statusCode") : Status.BAD_REQUEST.getStatusCode(),
                        err.has("ErrorCode") ? err.getInt("ErrorCode") : WebConstant.GENERIC_ERROR_CODE);
            }
        } catch (ServiceException se) {
            throw se;
        } catch (Exception e) {
            throw new ServiceException(Constants.RGP_CALL_FAILURE_MSG, Status.INTERNAL_SERVER_ERROR, e,
                    Constants.RGP_CALL_FAILURE_CODE);
        }
    }
}
