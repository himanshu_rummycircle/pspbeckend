package com.games24x7.pspbeckend.webclient;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.frameworks.serviceutil.httpclient.RetryConfig;
import com.games24x7.pspbeckend.Util.RestCoreUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.pojo.WebResponse;

@Component
public class SessionServiceClient {

    private static final Logger logger = LoggerFactory.getLogger(SessionServiceClient.class);

    @Autowired
    RestCoreUtil restUtil;

    @Autowired
    Configuration config;

    /**
     * 
     * @param url
     * @param payload
     * @param retryConfig
     * @return
     */
    @MonitorCalls
    public WebResponse postCall(String url, JSONObject payload, RetryConfig retryConfig) {
        try {
            return restUtil.callPost(url, payload, retryConfig);
        } catch (Exception e) {
            logger.error("Exception in postCall :: " + e);
            throw new ServiceException(Constants.SESSION_SERVICE_CALL_FAILED_MSG,
                    Status.INTERNAL_SERVER_ERROR.getStatusCode(), Constants.SESSION_SERVICE_CALL_FAILED_CODE);
        }
    }
}
