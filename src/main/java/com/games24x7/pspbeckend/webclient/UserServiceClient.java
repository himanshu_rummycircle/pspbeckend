/**
 * 
 */
package com.games24x7.pspbeckend.webclient;

import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.frameworks.serviceutil.httpclient.RetryConfig;
import com.games24x7.pspbeckend.Util.RestCoreUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.constants.Constants.RiskConstant;
import com.games24x7.pspbeckend.pojo.WebResponse;

/**
 * 
 */
@Component
public class UserServiceClient {
    private static final Logger logger = LoggerFactory.getLogger(UserServiceClient.class);

    @Autowired
    RestCoreUtil restUtil;

    @Autowired
    Configuration config;

    /**
     * @param url
     * @return
     */
    @MonitorCalls
    public JSONArray getCall(String url) {
        JSONArray arr = null;
        try {
            String res = restUtil.getCall(url);
            logger.debug("Response Recieved {}", res);
            if (res != null)
                arr = new JSONArray(res);
            else
                arr = new JSONArray();
            logger.debug("Output ** " + arr);
        } catch (ServiceException e) {
            logger.debug(Constants.ERROR_AT + e.getErrorStatus() + " :: " + Status.NOT_FOUND);
            if (e.getErrorStatus() == Status.NOT_FOUND)
                logger.error(e.getMessage(), e);
            else
                throw e;

        }
        return arr;
    }

    @MonitorCalls
    public String getStringResGetCall(String url) {
        String res = null;
        try {
            res = restUtil.getCall(url);
            logger.debug("Output ** " + res);
        } catch (ServiceException e) {
            logger.debug(Constants.ERROR_AT + e.getErrorStatus() + " :: " + Status.NOT_FOUND);
            if (e.getErrorStatus() == Status.NOT_FOUND)
                logger.error(e.getMessage(), e);
            else
                throw e;
        }

        return res;
    }

    /**
     * Method to call US API for getting users state.
     * 
     * @param url
     * @return
     */
    @MonitorCalls
    public WebResponse getCall(String url, RetryConfig retryConfig) {
        WebResponse webResponse = null;
        try {
            webResponse = restUtil.callGet(url, retryConfig);
            logger.info("webResponse " + webResponse.toString());
            if (webResponse != null && webResponse.getStatusCode() == Status.OK.getStatusCode()) {
                return webResponse;
            } else {
                throw new ServiceException(MTTSettlementConstant.US_CALL_FAILURE_MSG,
                        Status.INTERNAL_SERVER_ERROR.getStatusCode(), MTTSettlementConstant.US_CALL_FAILURE_CODE);
            }
        } catch (Exception ex) {
            throw new ServiceException(MTTSettlementConstant.US_CALL_FAILURE_MSG,
                    Status.INTERNAL_SERVER_ERROR.getStatusCode(), MTTSettlementConstant.US_CALL_FAILURE_CODE);
        }
    }

    /**
     * @param url
     * @param body
     * @param requestMethod
     * @return
     */
    @MonitorCalls
    public String postCall(String url, String body, String requestMethod) {
        try {
            return restUtil.postCall(url, body, requestMethod);
        } catch (ServiceException e) {
            logger.debug(Constants.ERROR_AT + e.getStatus() + " errorcodre  " + e.getErrorCode() + " :: "
                    + e.getBusinessErrorCode());
            logger.error("Exception in postCall for " + url + " : body :" + body, e);

        }
        return null;
    }

    /**
     * 
     * @param url
     * @param payload
     * @param retryConfig
     * @return
     */
    @MonitorCalls
    public WebResponse post(String url, JSONObject payload, RetryConfig retryConfig) {
        try {
            return restUtil.callPost(url, payload, retryConfig);
        } catch (Exception e) {
            logger.error("Exception in postCall :: " + e);
            throw new ServiceException(MTTSettlementConstant.US_CALL_FAILURE_MSG,
                    Status.INTERNAL_SERVER_ERROR.getStatusCode(), MTTSettlementConstant.US_CALL_FAILURE_CODE);
        }
    }

    /**
     * @param url
     * @param body
     * @param requestMethod
     * @return
     */
    public String deleteCall(String url, String body) {
        try {
            return restUtil.deleteCall(url, body);
        } catch (ServiceException e) {
            logger.debug("Excepton error code :: " + e.getStatus() + " errorcodre  " + e.getErrorCode() + " :: "
                    + e.getBusinessErrorCode());
            logger.error("Exception in postCall for " + url + " : body :" + body, e);
        }
        return null;
    }

    public int getValidUserListSize(String allUserIds) {
        try {
            logger.info("ALL USER IDS " + allUserIds);
            String url = config.get(MTTSettlementConstant.USER_SERVICE_URL)
                    + MTTSettlementConstant.USER_SERVICE_IN_PREFIX + allUserIds
                    + MTTSettlementConstant.USER_SERVICE_IN_SUFFIX;
            RetryConfig retryConfig = RetryConfig.getStandardRetryConfigForPutPost();
            logger.info("URL :: " + url);

            WebResponse webResponse = null;

            webResponse = restUtil.callGet(url, retryConfig);
            logger.info("webResponse ==> " + webResponse.toString());
            logger.info("webResponse.getStatusCode()" + webResponse.getStatusCode());
            logger.info("Status.OK.getStatusCode()" + Status.OK.getStatusCode());
            logger.info("Status.NOT_FOUND.getStatusCode()" + Status.NOT_FOUND.getStatusCode());
            if (webResponse != null && webResponse.getStatusCode() == Status.OK.getStatusCode()) {
                return new JSONArray(webResponse.getPayload()).length();
            } else if (webResponse != null && webResponse.getStatusCode() == Status.NOT_FOUND.getStatusCode()) {
                throw new ServiceException(RiskConstant.USER_NOT_FOUND, Status.BAD_REQUEST.getStatusCode(),
                        RiskConstant.USER_NOT_FOUND_ERROR_CODE);
            } else {
                throw new ServiceException(MTTSettlementConstant.US_CALL_FAILURE_MSG, Status.INTERNAL_SERVER_ERROR,
                        MTTSettlementConstant.US_CALL_FAILURE_CODE);
            }
        } catch (ServiceException se) {
            throw se;
        } catch (Exception ex) {
            logger.info("Error :: " + ex);
            throw new ServiceException(MTTSettlementConstant.US_CALL_FAILURE_MSG, Status.INTERNAL_SERVER_ERROR,
                    MTTSettlementConstant.US_CALL_FAILURE_CODE);
        }

    }

}
