package com.games24x7.pspbeckend.webclient;

import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.games24x7.aspect.MonitorCalls;
import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.frameworks.serviceutil.httpclient.RetryConfig;
import com.games24x7.pspbeckend.Util.RestCoreUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;
import com.games24x7.pspbeckend.pojo.WebResponse;

@Component
public class WithdrawServiceClient {
    private static final Logger logger = LoggerFactory.getLogger(WithdrawServiceClient.class);

    @Autowired
    RestCoreUtil restUtil;

    @Autowired
    Configuration config;

    /**
     * @param url
     * @return
     */
    @MonitorCalls
    public JSONArray getCall(String url) {
        JSONArray arr = null;
        try {
            String res = restUtil.getCall(url);
            logger.debug("Response Recieved {}", res);
            if (res != null)
                arr = new JSONArray(res);
            else
                arr = new JSONArray();
            logger.debug("Output ** " + arr);
        } catch (ServiceException e) {
            logger.debug(Constants.ERROR_AT + e.getErrorStatus());
            if (e.getErrorStatus() == Status.NOT_FOUND)
                logger.error(e.getMessage(), e);
            else
                throw e;

        }
        return arr;
    }

    @MonitorCalls
    public String getStringResGetCall(String url) {
        String res = null;
        try {
            res = restUtil.getCall(url);
            logger.debug(url + ": Output: " + res);
        } catch (ServiceException e) {
            logger.debug(Constants.ERROR_AT + e.getErrorStatus());
            throw e;
        }

        return res;
    }

    /**
     * @param url
     * @param body
     * @param requestMethod
     * @return
     */
    @MonitorCalls
    public String postCall(String url, JSONObject body) {
        try {
            RetryConfig retryConfig = RetryConfig.getStandardRetryConfigForPutPost();
            retryConfig.setNoOfRetries(0);
            WebResponse wres = restUtil.callPost(url, body, retryConfig);
            logger.debug(url + ": Output: " + wres);
            if (wres.getStatusCode() == Status.OK.getStatusCode()) {
                return wres.getPayload();
            } else {
                JSONObject err = new JSONObject(wres.getPayload());
                throw new ServiceException(
                        err.has("ErrorMessage") ? err.getString("ErrorMessage") : WebConstant.GENERIC_ERROR,
                        err.has("statusCode") ? Status.fromStatusCode(err.getInt("statusCode")) : Status.BAD_REQUEST,
                        err.has("ErrorCode") ? err.getInt("ErrorCode") : WebConstant.GENERIC_ERROR_CODE);
            }
        } catch (ServiceException se) {
            throw se;
        } catch (Exception e) {
            throw new ServiceException(Constants.WS_CALL_FAILURE_MSG, Status.INTERNAL_SERVER_ERROR, e,
                    Constants.WS_CALL_FAILURE_CODE);
        }
    }

}
