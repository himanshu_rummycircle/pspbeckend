package com.games24x7.com.pspbeckend.controller;

import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;
import com.games24x7.pspbeckend.controller.AccountStatusController;
import com.games24x7.pspbeckend.service.AccountService;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class AccountStatusControllerTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(AccountStatusControllerTest.class);
    @Autowired
    AccountService service;
    @InjectMocks
    AccountStatusController controller;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            controller.service = service;
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void closeUserTest() {

        try {
            controller.closeUser(null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), WebConstant.EMPTY_OR_NULL_PAYLOAD);
        }
        try {
            controller.closeUser("{}");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), Constants.VALUE_MISSING_FROM_PAYLOAD);
        }

        try {
            JSONObject obj = new JSONObject();
            obj.put(Constants.VALUE, "");
            controller.closeUser(obj.toString());
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), Constants.CONTEXT_MISSING_FROM_PAYLOAD);
        }
        try {
            JSONObject obj = new JSONObject();
            obj.put(Constants.VALUE, "");
            obj.put(Constants.CONTEXT, "");
            controller.closeUser(obj.toString());
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), WebConstant.INVALID_JSON_FORMAT);
        }

        try {
            JSONObject obj = new JSONObject();
            obj.put(Constants.VALUE, new JSONObject());
            obj.put(Constants.CONTEXT, new JSONObject());
            controller.closeUser(obj.toString());
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_USER_ID_ERR_CODE);
        }
        try {

            JSONObject obj = new JSONObject();
            JSONObject value = new JSONObject();
            value.put("status", 6);
            value.put("comment", "test");
            value.put("blockheaderid", 9);
            value.put("userId", 200);
            value.put("header", "header");
            value.put("messagetouser", "messagetouser");
            JSONObject context = new JSONObject();
            context.put("agentId", 200);
            context.put("source", "PSP");

            obj.put(Constants.VALUE, value);
            obj.put(Constants.CONTEXT, context);
            Response res = controller.blockUser(obj.toString());
            Assert.assertEquals(res.getStatus(), 200);
        } catch (Exception e) {
            logger.error("", e);
        }

        try {

            JSONObject obj = new JSONObject();
            JSONObject value = new JSONObject();
            value.put("userId", 200);
            JSONObject context = new JSONObject();
            context.put("agentId", 200);
            context.put("source", "UMS");

            obj.put(Constants.VALUE, value);
            obj.put(Constants.CONTEXT, context);
            Response res = controller.blockUser(obj.toString());
            Assert.assertEquals(res.getStatus(), 200);
        } catch (Exception e) {
            logger.error("", e);
        }

    }
    @Test
    public void blockUserTest() {

        try {
            controller.blockUser(null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), WebConstant.EMPTY_OR_NULL_PAYLOAD);
        }
        try {
            controller.blockUser("{}");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), Constants.VALUE_MISSING_FROM_PAYLOAD);
        }

        try {
            JSONObject obj = new JSONObject();
            obj.put(Constants.VALUE, "");
            controller.blockUser(obj.toString());
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), Constants.CONTEXT_MISSING_FROM_PAYLOAD);
        }
        try {
            JSONObject obj = new JSONObject();
            obj.put(Constants.VALUE, "");
            obj.put(Constants.CONTEXT, "");
            controller.blockUser(obj.toString());
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), WebConstant.INVALID_JSON_FORMAT);
        }

        try {
            JSONObject obj = new JSONObject();
            obj.put(Constants.VALUE, new JSONObject());
            obj.put(Constants.CONTEXT, new JSONObject());
            controller.blockUser(obj.toString());
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_USER_ID_ERR_CODE);
        }
        try {

            JSONObject obj = new JSONObject();
            JSONObject value = new JSONObject();
            value.put("status", 5);
            value.put("comment", "test");
            value.put("blockheaderid", 9);
            value.put("userId", 200);
            value.put("header", "header");
            value.put("messagetouser", "messagetouser");
            JSONObject context = new JSONObject();
            context.put("agentId", 200);
            context.put("source", "PSP");

            obj.put(Constants.VALUE, value);
            obj.put(Constants.CONTEXT, context);
            Response res = controller.blockUser(obj.toString());
            Assert.assertEquals(res.getStatus(), 200);
        } catch (Exception e) {
            logger.error("", e);
        }

        try {

            JSONObject obj = new JSONObject();
            JSONObject value = new JSONObject();
            value.put("userId", 200);
            JSONObject context = new JSONObject();
            context.put("agentId", 200);
            context.put("source", "UMS");

            obj.put(Constants.VALUE, value);
            obj.put(Constants.CONTEXT, context);
            Response res = controller.blockUser(obj.toString());
            Assert.assertEquals(res.getStatus(), 200);
        } catch (Exception e) {
            logger.error("", e);
        }

    }

    @Test
    public void unblockUserTest() {
        try {
            controller.unblockUser(null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), WebConstant.EMPTY_OR_NULL_PAYLOAD);
        }
        try {
            controller.unblockUser("{}");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), Constants.VALUE_MISSING_FROM_PAYLOAD);
        }

        try {
            JSONObject obj = new JSONObject();
            obj.put(Constants.VALUE, "");
            controller.unblockUser(obj.toString());
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), Constants.CONTEXT_MISSING_FROM_PAYLOAD);
        }
        try {
            JSONObject obj = new JSONObject();
            obj.put(Constants.VALUE, "");
            obj.put(Constants.CONTEXT, "");
            controller.unblockUser(obj.toString());
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), WebConstant.INVALID_JSON_FORMAT);
        }

        try {

            JSONObject obj = new JSONObject();
            JSONObject value = new JSONObject();
            value.put("status", 5);
            value.put("comment", "test");
            value.put("prevStatus", 1);
            value.put("userId", 200);
            JSONObject context = new JSONObject();
            context.put("agentId", 200);
            context.put("source", "PSP");

            obj.put(Constants.VALUE, value);
            obj.put(Constants.CONTEXT, context);
            Response res = controller.unblockUser(obj.toString());
            Assert.assertEquals(res.getStatus(), 200);
        } catch (Exception e) {
            logger.error("", e);
        }
    }

}
