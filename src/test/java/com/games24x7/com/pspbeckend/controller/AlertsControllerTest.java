package com.games24x7.com.pspbeckend.controller;

import static org.testng.Assert.assertEquals;

import java.sql.Timestamp;

import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.junit.Assert;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;
import com.games24x7.pspbeckend.controller.AlertsController;
import com.games24x7.pspbeckend.service.AlertService;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class AlertsControllerTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(AlertsControllerTest.class);
    @Autowired
    AlertService service;
    @InjectMocks
    AlertsController controller;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            controller.service = service;
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test(priority = 1)
    public long testCreateAlert() {

        JSONObject value = new JSONObject();
        JSONObject obj = new JSONObject();
        obj.put("user_id", 200);
        obj.put("event_type", 4);
        obj.put("status", 1);
        obj.put("owner_id", 200);
        obj.put("reference", 0);
        obj.put("created_by", 200);
        obj.put("withdraw_id", 0);
        obj.put("creation_date", new Timestamp(System.currentTimeMillis()));
        // obj.put("start_date", null);
        // obj.put("close_date", null);
        // obj.put("followup_date", null);
        // obj.put("additional_info", null);
        // obj.put("last_update_date", null);
        // obj.put("last_updatedby", null);
        // obj.put("comment", null);

        value.put("value", obj);

        Response res = controller.createAlert(value.toString());
        JSONObject alert = new JSONObject(res.getEntity().toString());
        logger.debug("resp ::{}", alert.getLong(Constants.ALERT_ID));
        long alertId = alert.getLong(Constants.ALERT_ID);
        Assert.assertNotNull(res);
        try {
            controller.createAlert("");
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), WebConstant.EMPTY_OR_NULL_PAYLOAD);
        }

        try {
            controller.createAlert(new JSONObject().toString());
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), WebConstant.INVALID_JSON_FORMAT);
        }

        try {
            value.remove("user_id");
            controller.createAlert(value.toString());
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_USER_ID_ERR_CODE);
        }
        return alertId;
    }

    @Test(priority = 0)
    public void testgetAlertData() {
        long alertId = testCreateAlert();
        Response res = controller.getAlertData(alertId);
        Assert.assertNotNull(res);

        try {
            controller.getAlertData(0);
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), WebConstant.INVALID_ALERT_ID_ERROR_CODE);
        }

        JSONObject value = new JSONObject();
        JSONObject obj = new JSONObject();
        obj.put("last_updatedby", 201);
        obj.put("comment", "updated comment");
        value.put("value", obj);
        res = controller.updateAlertComment(alertId, value.toString());
        Assert.assertNotNull(res);

        try {
            controller.updateAlertComment(0, value.toString());
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), WebConstant.INVALID_ALERT_ID_ERROR_CODE);
        }

        try {
            controller.updateAlertComment(alertId, null);
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), WebConstant.EMPTY_OR_NULL_PAYLOAD);
        }

        try {
            controller.updateAlertComment(alertId, new JSONObject().toString());
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), WebConstant.INVALID_JSON_FORMAT);
        }

        try {
            value.remove("comment");
            controller.updateAlertComment(alertId, value.toString());
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_COMMENT_ERR_CODE);
        }

        try {
            obj.put("comment", "updated comment");
            value.remove("last_updatedby");
            controller.updateAlertComment(alertId, value.toString());
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_LAST_UPDATED_BY_ERR_CODE);
        }
    }
}
