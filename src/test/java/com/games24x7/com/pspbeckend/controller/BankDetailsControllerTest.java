package com.games24x7.com.pspbeckend.controller;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;
import com.games24x7.pspbeckend.controller.BankDetailsController;
import com.games24x7.pspbeckend.service.BankDetailsService;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class BankDetailsControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(BankDetailsControllerTest.class);

	@Mock
	BankDetailsService bankDetailsService;

	@InjectMocks
	BankDetailsController bankDetailsController;

	@BeforeTest
	public void testSetup() {
		try {
			MockitoAnnotations.initMocks(this);
		} catch (Exception e) {
			logger.error("Exception in testSetup", e);
		}
	}

	@Test
	public void testgetUserBankDetails() {
		Mockito.when(bankDetailsService.getUserBankDetails(Mockito.anyLong())).thenReturn(null);
		try {
			Response res = bankDetailsController.getUserBankDetails(123l);
			Assert.fail();
		} catch (ServiceException se) {
			Assert.assertEquals(se.getStatus(), Status.NOT_FOUND);
		} catch (Exception e) {
			Assert.fail();
		}
		
		Mockito.when(bankDetailsService.getUserBankDetails(Mockito.anyLong())).thenReturn(new JSONObject());
		try {
			Response res = bankDetailsController.getUserBankDetails(123l);
			Assert.assertEquals(res.getStatus(), Status.OK.getStatusCode());
		} catch (ServiceException se) {
			Assert.fail();
		} catch (Exception e) {
			Assert.fail();
		}
		
		ServiceException ex = new ServiceException( "ServiceException ",Status.BAD_REQUEST );
		Mockito.doThrow( ex ).when( bankDetailsService ).getUserBankDetails( Mockito.anyLong() );
		
		try {
			 bankDetailsController.getUserBankDetails(123l);
			Assert.fail();
		} catch (ServiceException se) {
			Assert.assertEquals(se.getStatus(), Status.BAD_REQUEST);
		} catch (Exception e) {
			Assert.fail();
		}
		
		
		NullPointerException de = new NullPointerException( "NullPointerException" );
		Mockito.doThrow( de ).when( bankDetailsService ).getUserBankDetails( Mockito.anyLong() );
		
		try {
			 bankDetailsController.getUserBankDetails(123l);
			Assert.fail();
		} catch (ServiceException se) {
			Assert.assertEquals(se.getStatus(), Status.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			Assert.fail();
		}
	}
	
	@Test
	public void testupdateUserBankDetails() {
		String payload=null;
		try {
			Response res = bankDetailsController.updateUserBankDetails(123l,payload);
			Assert.fail();
		} catch (ServiceException se) {
			Assert.assertEquals(se.getStatus(), Status.BAD_REQUEST);
			Assert.assertEquals(se.getBusinessErrorCode().intValue(),WebConstant.EMPTY_OR_NULL_PAYLOAD);
		} catch (Exception e) {
			Assert.fail();
		}
		
		payload="";
		try {
			Response res = bankDetailsController.updateUserBankDetails(123l,payload);
			Assert.fail();
		} catch (ServiceException se) {
			Assert.assertEquals(se.getStatus(), Status.BAD_REQUEST);
			Assert.assertEquals(se.getBusinessErrorCode().intValue(),WebConstant.EMPTY_OR_NULL_PAYLOAD);
		} catch (Exception e) {
			Assert.fail();
		}
		
		payload="null";
		try {
			Response res = bankDetailsController.updateUserBankDetails(123l,payload);
			Assert.fail();
		} catch (ServiceException se) {
			Assert.assertEquals(se.getStatus(), Status.BAD_REQUEST);
			Assert.assertEquals(se.getBusinessErrorCode().intValue(),WebConstant.EMPTY_OR_NULL_PAYLOAD);
		} catch (Exception e) {
			Assert.fail();
		}
		
		payload="{";
		try {
			Response res = bankDetailsController.updateUserBankDetails(123l,payload);
			Assert.fail();
		} catch (ServiceException se) {
			Assert.assertEquals(se.getStatus(), Status.BAD_REQUEST);
			Assert.assertEquals(se.getBusinessErrorCode().intValue(),WebConstant.INVALID_JSON_FORMAT);
		} catch (Exception e) {
			Assert.fail();
		}
		

		payload="{}";
		try {
			Response res = bankDetailsController.updateUserBankDetails(123l,payload);
			Assert.assertEquals(res.getStatus(), Status.OK.getStatusCode());
		} catch (ServiceException se) {
			Assert.fail();
		} catch (Exception e) {
			Assert.fail();
		}
		
		ServiceException ex = new ServiceException( "ServiceException ",Status.BAD_REQUEST );
		Mockito.doThrow( ex ).when( bankDetailsService ).updateUserBankDetails(Mockito.anyLong(),Mockito.any() );
		
		try {
			 bankDetailsController.updateUserBankDetails(123l,payload);
			Assert.fail();
		} catch (ServiceException se) {
			Assert.assertEquals(se.getStatus(), Status.BAD_REQUEST);
		} catch (Exception e) {
			Assert.fail();
		}
		
		
		NullPointerException de = new NullPointerException( "NullPointerException" );
		Mockito.doThrow( de ).when( bankDetailsService ).updateUserBankDetails(Mockito.anyLong(),Mockito.any() );
		
		try {
			 bankDetailsController.updateUserBankDetails(123l,payload);
			Assert.fail();
		} catch (ServiceException se) {
			Assert.assertEquals(se.getStatus(), Status.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			Assert.fail();
		}
	}
}
