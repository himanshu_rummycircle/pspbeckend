/**
 * 
 */
package com.games24x7.com.pspbeckend.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.BulkStatusChangeUtil;
import com.games24x7.pspbeckend.constants.Globals;
import com.games24x7.pspbeckend.controller.BulkStatusChangeController;
import com.games24x7.pspbeckend.pojo.BulkStatusChangeData;
import com.games24x7.pspbeckend.pojo.MergeAccount;
import com.games24x7.pspbeckend.pojo.UserData;
import com.games24x7.pspbeckend.service.AccountService;
import com.games24x7.pspbeckend.service.RefundService;
import com.sun.jersey.core.header.FormDataContentDisposition;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class BulkStatusChangeControllerTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(BulkStatusChangeControllerTest.class);
    @Autowired
    Configuration config;

    @Mock
    BulkStatusChangeUtil util;

    @Mock
    RefundService refService;

    @Mock
    AccountService accService;

    @InjectMocks
    BulkStatusChangeController ctrl;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);

        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void changeAccountStatus() {

        Response res = null;
        try {
            Mockito.doNothing().when(util).basicValidations(Mockito.anyString(), Mockito.anyObject(),
                    Mockito.anyObject(), Mockito.anyLong());

            logger.debug("util " + util);
            Date date = new Date();
            FormDataContentDisposition contentDisposition = FormDataContentDisposition.name("testData")
                    .fileName("testBulkStatusChange.csv").creationDate(date).modificationDate(date).readDate(date)
                    .size(1222).build();

            BulkStatusChangeData data = Mockito.spy(new BulkStatusChangeData());
            data.setAgent("murali");
            data.setAgentId(1523);
            data.setAccountStatus(5);
            Mockito.when(data.getUserData()).thenReturn(new ArrayList<UserData>());

            Mockito.when(util.generateData(contentDisposition, "murali", 1523)).thenReturn(data);
            Mockito.doNothing().when(util).dataValidation(data);
            Mockito.doNothing().when(accService).changeBulkAccountStatus(data, Globals.BULK_ACCOUNT_STATUS_CHANGE);

            File initialFile = new File("/home/murali/Desktop/nonexist.csv");
            InputStream targetStream = null;
            try {
                targetStream = new FileInputStream(initialFile);
            } catch (FileNotFoundException e) {
                logger.error("", e);
            }
            res = ctrl.changeAccountStatus(targetStream, "murali", 1523l, Globals.BULK_ACCOUNT_STATUS_CHANGE,
                    contentDisposition);
            logger.debug("Response :: " + res);

            Assert.assertEquals(res.getStatus(), Status.OK.getStatusCode());

        } catch (ServiceException e) {
            logger.error("excep :" + e.getStatus());
        }

    }
}
