/**
 * 
 */
package com.games24x7.com.pspbeckend.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.MergeAndCloseUtil;
import com.games24x7.pspbeckend.constants.Globals;
import com.games24x7.pspbeckend.controller.CloseAndMergeAccountController;
import com.games24x7.pspbeckend.pojo.CloseAndMergeData;
import com.games24x7.pspbeckend.pojo.MergeAccount;
import com.games24x7.pspbeckend.service.AccountService;
import com.games24x7.pspbeckend.service.ApprovalService;
import com.games24x7.pspbeckend.service.FundTransferService;
import com.sun.jersey.core.header.FormDataContentDisposition;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class CloseAndMergeAccountControllerTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(CloseAndMergeAccountControllerTest.class);
    @Mock
    ApprovalService approvalService;
    @Autowired
    Configuration config;
    @Mock
    FundTransferService fService;
    @Mock
    AccountService accService;
    @Mock
    MergeAndCloseUtil util;
    /*@Mock
    ResponseUtil rutil;*/

    @InjectMocks
    CloseAndMergeAccountController controller;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void testCloseandMerge() {
        Response res = null;
        try {
            Mockito.doNothing().when(util).basicValidations(Mockito.anyString(), Mockito.anyObject(),
                    Mockito.anyObject(), Mockito.anyLong());

            logger.debug("util " + util);
            Date date = new Date();
            FormDataContentDisposition contentDisposition = FormDataContentDisposition.name("testData")
                    .fileName("test.csv").creationDate(date).modificationDate(date).readDate(date).size(1222).build();

            CloseAndMergeData data = Mockito.spy(new CloseAndMergeData());
            data.setAgent("murali");
            data.setAgentId(1523);
            Mockito.when(data.getMergeAccData()).thenReturn(new ArrayList<MergeAccount>());
            Mockito.when(data.getSucessfulMergeAcc()).thenReturn(new ArrayList<MergeAccount>());
            Mockito.when(util.mergeDataGeneration(contentDisposition, "murali", 1523)).thenReturn(data);
            Mockito.doNothing().when(util).mergeAccValidations(Mockito.anyObject());

            Mockito.when(approvalService.generateApprovalAndMergeData(data)).thenReturn(data);
            Mockito.doNothing().when(approvalService).processApproval(data);
            Mockito.doNothing().when(accService).processAccountClosure(data, Globals.CLOSE_AND_MERGE);
            Mockito.doNothing().when(fService).processAccountMerging(data);

           /* Mockito.when(rutil.finalResponse(Mockito.anyObject(), Mockito.anyObject()))
                    .thenReturn(Response.status(Status.OK).entity(contentDisposition.getFileName())
                            .header("Access-Control-Allow-Origin", "*").build());*/

            File initialFile = new File("/home/murali/Desktop/nonexist.csv");
            InputStream targetStream = null;
            try {
                targetStream = new FileInputStream(initialFile);
            } catch (FileNotFoundException e) {
                logger.error("", e);
            }
            res = controller.closeAndMerge(targetStream, "murali", 1523l, contentDisposition);
            logger.debug("Response :: " + res);

            Assert.assertEquals(res.getStatus(), Status.OK.getStatusCode());

        } catch (ServiceException e) {
            logger.error("excep :" + e.getStatus());
        }

    }

}
