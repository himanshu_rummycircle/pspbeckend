/**
 * 
 */
package com.games24x7.com.pspbeckend.controller;

import java.util.Date;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.pspbeckend.Util.CloseAndRefundUtil;
import com.games24x7.pspbeckend.controller.CloseAndRefundController;
import com.games24x7.pspbeckend.pojo.CloseAndRefundData;
import com.games24x7.pspbeckend.service.AccountService;
import com.games24x7.pspbeckend.service.RefundService;
import com.sun.jersey.core.header.FormDataContentDisposition;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class CloseAndRefundControllerTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(CloseAndRefundControllerTest.class);
    @Autowired
    Configuration config;

    @Mock
    CloseAndRefundUtil util;

    @Mock
    RefundService refService;

    @Mock
    AccountService accService;

   /* @Mock
    ResponseUtil rutil;*/

    @InjectMocks
    CloseAndRefundController clr;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void testCloseandMerge() {
        Response res = null;
        Mockito.doNothing().when(util).basicValidations(Mockito.anyString(), Mockito.anyObject(), Mockito.anyObject(),
                Mockito.anyLong());

        Date date = new Date();
        FormDataContentDisposition contentDisposition = FormDataContentDisposition.name("testData").fileName("test.csv")
                .creationDate(date).modificationDate(date).readDate(date).size(1222).build();

        Mockito.when(util.refundDataGeneration(contentDisposition, "agentName", 1000))
                .thenReturn(new CloseAndRefundData());

        Mockito.doNothing().when(util).refundDataValidation(Mockito.anyObject());
        Mockito.doNothing().when(accService).processAccountClosure(Mockito.anyObject(), Mockito.anyInt());
        Mockito.when(refService.processRefund(Mockito.anyObject())).thenReturn(null);

        /*Mockito.when(rutil.finalResponse(Mockito.anyObject(), Mockito.anyObject()))
                .thenReturn(Response.status(Status.OK).entity(contentDisposition.getFileName())
                        .header("Access-Control-Allow-Origin", "*").build());*/
        res = clr.closeAndRefund(null, "agent", 100l, contentDisposition);
        logger.debug("Res :: " + res);

        Assert.assertEquals(res.getStatus(), Status.OK.getStatusCode());

    }

}
