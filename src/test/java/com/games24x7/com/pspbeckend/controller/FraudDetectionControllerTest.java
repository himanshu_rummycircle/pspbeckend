package com.games24x7.com.pspbeckend.controller;

import static org.testng.Assert.assertEquals;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;
import com.games24x7.pspbeckend.controller.FraudDetectionController;
import com.games24x7.pspbeckend.service.FraudService;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class FraudDetectionControllerTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(FraudDetectionControllerTest.class);
    @Autowired
    FraudService service;
    @InjectMocks
    FraudDetectionController controller;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            controller.service = service;
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void getFraudDevicesTest() {
        try {
            controller.getFraudDevices(-11, 1, 0, null);
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_STATUS_EXCEPTION_CODE);
        }
        try {
            controller.getFraudDevices(1, -1, 1, null);
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_LIMIT_EXCEPTION_CODE);
        }
        try {
            controller.getFraudDevices(1, 1, -11, null);
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_OFFSET_EXCEPTION_CODE);
        }

        try {
            controller.getFraudDevices(1, 1, 1, "1");
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), 200);
        }
    }

}
