package com.games24x7.com.pspbeckend.controller;

import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;
import com.games24x7.pspbeckend.controller.KYCController;
import com.games24x7.pspbeckend.service.KYCService;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class KYCControllerTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(KYCControllerTest.class);
    @Autowired
    KYCService service;

    @InjectMocks
    KYCController controller;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            controller.service = service;
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void testProcess() {
        try {
            controller.processKYC(null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), WebConstant.EMPTY_OR_NULL_PAYLOAD);
        }
        try {
            controller.processKYC("{}");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), Constants.VALUE_MISSING_FROM_PAYLOAD);
        }

        try {
            JSONObject obj = new JSONObject();
            obj.put(Constants.VALUE, "");
            controller.processKYC(obj.toString());
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), Constants.CONTEXT_MISSING_FROM_PAYLOAD);
        }
        try {
            JSONObject obj = new JSONObject();
            obj.put(Constants.VALUE, "");
            obj.put(Constants.CONTEXT, "");
            controller.processKYC(obj.toString());
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), WebConstant.INVALID_JSON_FORMAT);
        }

        try {
            JSONObject obj = new JSONObject();
            obj.put(Constants.VALUE, new JSONObject());
            obj.put(Constants.CONTEXT, new JSONObject());
            controller.processKYC(obj.toString());
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), Constants.TYPE_MISSING_FROM_PAYLOAD);
        }
        try {

            JSONObject obj = new JSONObject();
            JSONObject value = new JSONObject();
            value.put("docType", 2);
            value.put("statusComment", "test");
            value.put(Constants.TYPE, false);
            value.put("userId", 200);
            JSONObject context = new JSONObject();
            context.put("agentId", 200);
            context.put("source", "PSP");

            obj.put(Constants.VALUE, value);
            obj.put(Constants.CONTEXT, context);
            Response res = controller.processKYC(obj.toString());
            Assert.assertEquals(res.getStatus(), 200);
        } catch (Exception e) {
        }
        try {

            JSONObject obj = new JSONObject();
            JSONObject value = new JSONObject();
            value.put("docType", 2);
            value.put("statusComment", "test");
            value.put(Constants.TYPE, false);
            value.put("userId", 200);
            value.put("isKycReVerified", true);
            JSONObject context = new JSONObject();
            context.put("agentId", 200);
            context.put("source", "PSP");

            obj.put(Constants.VALUE, value);
            obj.put(Constants.CONTEXT, context);
            Response res = controller.processKYC(obj.toString());
            Assert.assertEquals(res.getStatus(), 200);
        } catch (Exception e) {
        }
    }
}
