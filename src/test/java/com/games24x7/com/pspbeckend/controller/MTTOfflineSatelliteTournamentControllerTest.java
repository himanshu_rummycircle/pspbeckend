package com.games24x7.com.pspbeckend.controller;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.controller.MTTOfflineSatelliteTournamentController;
import com.games24x7.pspbeckend.service.SatelliteTournamentService;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class MTTOfflineSatelliteTournamentControllerTest extends AbstractTestNGSpringContextTests {

    @Mock
    SatelliteTournamentService satelliteService;
    
    @InjectMocks
    MTTOfflineSatelliteTournamentController mttController;
    
    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test(priority = 3, enabled = false)
    public void test1() throws Exception{
        Mockito.doThrow(new Exception()).when(satelliteService).processOfflineSatelliteSettlement(Mockito.any());
        try {
            mttController.mttOfflineSettlement("{}");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.INTERNAL_SERVER_ERROR.getStatusCode());
        }
    }
    @Test
    public void mttOfflineSettlementTest() throws Exception{
        
        Mockito.when(satelliteService.processOfflineSatelliteSettlement(Mockito.any())).thenReturn("string");
        try {
            mttController.mttOfflineSettlement(null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.EMPTY_OR_NULL_PAYLOAD);
        }
        
        try {
            mttController.mttOfflineSettlement("");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.EMPTY_OR_NULL_PAYLOAD);
        }
        
        try {
            mttController.mttOfflineSettlement("{\"abcd:11}");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.INVALID_JSON_FORMAT);
        }
        
        try {
            mttController.mttOfflineSettlement("{\"abcd:11}");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.INVALID_JSON_FORMAT);
        }
        Response resp = mttController.mttOfflineSettlement("{}");
        Assert.assertNotNull(resp);
        Assert.assertEquals(200, resp.getStatus());
        
        Mockito.doThrow(new ServiceException("AgentName is missing.", Status.BAD_REQUEST)).when(satelliteService)
        .processOfflineSatelliteSettlement(Mockito.any());
        try {
            mttController.mttOfflineSettlement("{}");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }
        
        
/*
        Mockito.doThrow(new NullPointerException()).when(satelliteService).processOfflineSatelliteSettlement();

        try {

            mttController.rbiCronAction();

        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(500, se.getErrorCode());

        }
        ServiceException excp  = new ServiceException("Errr",Status.BAD_REQUEST.getStatusCode() , 99);
        Mockito.doThrow(excp).when(satelliteService).processOfflineSatelliteSettlement();

        try {

            mttController.rbiCronAction();

        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(400, se.getErrorCode());

        }
*/
    }

}
