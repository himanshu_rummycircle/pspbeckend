package com.games24x7.com.pspbeckend.controller;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;
import com.games24x7.pspbeckend.controller.RiskController;
import com.games24x7.pspbeckend.service.RiskService;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class RiskControllerTest extends AbstractTestNGSpringContextTests {
    
    private static final Logger logger = LoggerFactory.getLogger(RiskControllerTest.class);
    
    @Mock
    RiskService riskService;

    @InjectMocks
    RiskController riskController;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }
    
    @Test
    public void createRiskCertificateTest() throws Exception{
        
        Mockito.when(riskService.createRiskCertificate(Mockito.any())).thenReturn("Risk");
        try {
            riskController.createRiskCertificate(null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.EMPTY_OR_NULL_PAYLOAD);
        }
        
        try {
            riskController.createRiskCertificate("avbc");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.INVALID_JSON_FORMAT);
        }
        
        Mockito.when(riskService.createRiskCertificate(Mockito.any())).thenReturn("Risk");
        Response rs = riskController.createRiskCertificate("{}");
        Assert.assertNotNull(rs);
        Assert.assertEquals(Status.OK.getStatusCode(), rs.getStatus());
        
        Mockito.doThrow(new NullPointerException()).when(riskService).createRiskCertificate(Mockito.any());
        
        try {
            riskController.createRiskCertificate("{}");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), WebConstant.GENERIC_ERROR_CODE);
        }
        
        Mockito.doThrow(new ServiceException("E",Status.BAD_REQUEST, 100)).when(riskService).createRiskCertificate(Mockito.any());
        
        try {
            riskController.createRiskCertificate("{}");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), 100);
        }
   
    }
    
    @Test
    public void getRiskCertificateTest() throws Exception{
        JSONArray jsonArray = new JSONArray();
        Mockito.when(riskService.getRiskCertificate(Mockito.anyInt(),Mockito.anyInt())).thenReturn(jsonArray);
        try {
            riskController.getRiskCertificate(0,0);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.EMPTY_OR_NULL_PAYLOAD);
        }
        
        Mockito.doThrow(new JSONException("JSONErr")).when(riskService).getRiskCertificate(Mockito.anyInt(),Mockito.anyInt());
        try {
            riskController.getRiskCertificate(0,0);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.INVALID_JSON_FORMAT);
        }
        
        Mockito.doThrow(new JSONException("JSONErr")).when(riskService).getRiskCertificate(Mockito.anyInt(),Mockito.anyInt());
        try {
            riskController.getRiskCertificate(0,0);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.INVALID_JSON_FORMAT);
        }
        
        Mockito.doThrow(new NullPointerException()).when(riskService).getRiskCertificate(Mockito.anyInt(),Mockito.anyInt());
        try {
            riskController.getRiskCertificate(0,0);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.GENERIC_ERROR_CODE);
        }
        
        Mockito.doThrow(new ServiceException("E",Status.BAD_REQUEST, 100)).when(riskService).getRiskCertificate(Mockito.anyInt(),Mockito.anyInt());
        
        try {
            riskController.getRiskCertificate(0,0);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), 100);
        }
   
    }
    
    @Test
    public void updateRiskCertificateTest() throws Exception{
        JSONArray jsonArray = new JSONArray();
        Mockito.when(riskService.updateRiskCertificate(Mockito.any())).thenReturn(jsonArray);
        try {
            riskController.updateRiskCertificate(null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.EMPTY_OR_NULL_PAYLOAD);
        }
        

        try {
            riskController.updateRiskCertificate("avbc");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.INVALID_JSON_FORMAT);
        }
        
        Mockito.when(riskService.updateRiskCertificate(Mockito.any())).thenReturn(jsonArray);
        Response rs = riskController.updateRiskCertificate("{}");
        Assert.assertNotNull(rs);
        Assert.assertEquals(Status.OK.getStatusCode(), rs.getStatus());
        
        
        Mockito.doThrow(new NullPointerException()).when(riskService).updateRiskCertificate(Mockito.any());
        try {
            riskController.updateRiskCertificate("{}");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.GENERIC_ERROR_CODE);
        }
        
        Mockito.doThrow(new ServiceException("E",Status.BAD_REQUEST, 100)).when(riskService).updateRiskCertificate(Mockito.any());
        
        try {
            riskController.updateRiskCertificate("{}");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), 100);
        }
   
    }
    
    @Test
    public void updatePlayersRiskStatusAndAutiLogCreationTest() throws Exception{
        Mockito.when(riskService.updatePlayersRiskStatus(Mockito.any())).thenReturn("str");
        try {
            riskController.updatePlayersRiskStatusAndAuditLogCreation(null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.EMPTY_OR_NULL_PAYLOAD);
        }
        

        try {
            riskController.updatePlayersRiskStatusAndAuditLogCreation("avbc");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.INVALID_JSON_FORMAT);
        }
        
        Mockito.when(riskService.updatePlayersRiskStatus(Mockito.any())).thenReturn("str");
        Response rs = riskController.updatePlayersRiskStatusAndAuditLogCreation("{}");
        Assert.assertNotNull(rs);
        Assert.assertEquals(Status.OK.getStatusCode(), rs.getStatus());
        
        
        Mockito.doThrow(new NullPointerException()).when(riskService).updatePlayersRiskStatus(Mockito.any());
        try {
            riskController.updatePlayersRiskStatusAndAuditLogCreation("{}");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.GENERIC_ERROR_CODE);
        }
        
        Mockito.doThrow(new ServiceException("E",Status.BAD_REQUEST, 100)).when(riskService).updatePlayersRiskStatus(Mockito.any());
        
        try {
            riskController.updatePlayersRiskStatusAndAuditLogCreation("{}");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), 100);
        }
   
    }
    
    @Test
    public void getRiskStatusLogsAndCertificatesTest() throws Exception{
        Mockito.when(riskService.fetchRiskStatusLogsAndCertificates(Mockito.anyLong(),Mockito.anyBoolean(),Mockito.anyBoolean(),Mockito.anyBoolean())).thenReturn("str");
        try {
            riskController.getRiskStatusLogsAndCertificates(null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), Constants.PAYLOAD_NULL_OR_EMPTY_CODE);
        }
        

        try {
            riskController.getRiskStatusLogsAndCertificates("avbc");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(),  Constants.GENERIC_IMPROPER_PAYLOAD_CODE);
        }
        JSONObject payload = new JSONObject();
        JSONObject payloadValue = new JSONObject();
        payload.put("value", payloadValue);
        Mockito.when(riskService.fetchRiskStatusLogsAndCertificates(Mockito.anyLong(),Mockito.anyBoolean(),Mockito.anyBoolean(),Mockito.anyBoolean())).thenReturn(payload.toString());
        
        try {
            riskController.getRiskStatusLogsAndCertificates(payload.toString());
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), Constants.USER_ID_MISSING_FROM_PAYLOAD);
        }
        payloadValue.put("userId", 111);
        payloadValue.put(Constants.RISK_STATUS, true);
        payloadValue.put(Constants.JSON_RISK_LOGS, true);
        payloadValue.put(Constants.JSON_RISK_CERTIFICATES, true);
        payload.put("value", payloadValue);
        
        Response rs = riskController.getRiskStatusLogsAndCertificates(payload.toString());
        Assert.assertNotNull(rs);
        Assert.assertEquals(Status.OK.getStatusCode(), rs.getStatus());
        
        
        payloadValue.put(Constants.RISK_STATUS, false);
        payloadValue.put(Constants.JSON_RISK_LOGS, false);
        payloadValue.put(Constants.JSON_RISK_CERTIFICATES, false);
        payload.put("value", payloadValue);
        
        rs = riskController.getRiskStatusLogsAndCertificates(payload.toString());
        Assert.assertNotNull(rs);
        Assert.assertEquals(Status.OK.getStatusCode(), rs.getStatus());
        
        Mockito.doThrow(new NullPointerException()).when(riskService).fetchRiskStatusLogsAndCertificates(Mockito.anyLong(),Mockito.anyBoolean(),Mockito.anyBoolean(),Mockito.anyBoolean());
        try {
            riskController.getRiskStatusLogsAndCertificates(payload.toString());
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), Constants.ERROR_IN_PROCESSING_REQUEST_CODE);
        }
        
        Mockito.doThrow(new ServiceException("E",Status.BAD_REQUEST, 100)).when(riskService).fetchRiskStatusLogsAndCertificates(Mockito.anyLong(),Mockito.anyBoolean(),Mockito.anyBoolean(),Mockito.anyBoolean());
        
        try {
            riskController.getRiskStatusLogsAndCertificates(payload.toString());
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), 100);
        }
   
 
    }

}
