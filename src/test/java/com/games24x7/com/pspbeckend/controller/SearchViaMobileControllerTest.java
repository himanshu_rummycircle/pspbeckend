/**
 * 
 */
package com.games24x7.com.pspbeckend.controller;

import org.json.JSONException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.controller.SearchViaMobileController;
import com.games24x7.pspbeckend.service.MobileService;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class SearchViaMobileControllerTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(SearchViaMobileControllerTest.class);

    @Mock
    MobileService service;
    @InjectMocks
    SearchViaMobileController controller;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);

        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void getUserDatailsTest() {
        try {

            Mockito.when(service.getUserData(5)).thenReturn(100l);
            controller.getUserDatails(5);

            try {
                Mockito.when(service.getUserData(6)).thenThrow(new ServiceException("Test....", 100));
                controller.getUserDatails(6);
            } catch (Exception e) {
                logger.info("case1.........");
            }

            try {
                
                Mockito.when(service.getUserData(10l)).thenThrow(new RuntimeException("Test...."));
                controller.getUserDatails(10l);
                
                
            } catch (Exception e) {
                logger.info("case3.........");
            }

        } catch (Exception e) {
            logger.info("case2.........");
        }

    }

}
