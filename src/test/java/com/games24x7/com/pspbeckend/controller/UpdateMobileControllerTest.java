package com.games24x7.com.pspbeckend.controller;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.controller.UpdateMobileController;
import com.games24x7.pspbeckend.pojo.WebResponse;
import com.games24x7.pspbeckend.service.UpdateMobileService;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class UpdateMobileControllerTest extends AbstractTestNGSpringContextTests {

    @InjectMocks
    UpdateMobileController updateMobileControllerMockInject;

    @Mock
    UpdateMobileService updateMobileServiceMock;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);

        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void testUpdateMobile() {
        try {
            updateMobileControllerMockInject.updateMobile(321857, null);
        } catch (ServiceException se) {
            Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(se.getMessage(), "Payload not available");
        }

        try {
            updateMobileControllerMockInject.updateMobile(321857, "");
        } catch (ServiceException se) {
            Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(se.getMessage(), "Payload not available");
        }

        try {
            updateMobileControllerMockInject.updateMobile(321857, "abc");
        } catch (ServiceException se) {
            Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(se.getMessage(), "Payload not in Json Format");
        }

        WebResponse webResponse = new WebResponse();
        webResponse.setStatusCode(200);
        webResponse.setPayload("");
        Mockito.when(updateMobileServiceMock.updateMobile(Mockito.anyLong(), Mockito.any())).thenReturn(webResponse)
                .thenThrow(new ServiceException("errormessage", 123)).thenThrow(new RuntimeException());

        JSONObject json = new JSONObject();

        Response response = updateMobileControllerMockInject.updateMobile(321857, json.toString());
        Assert.assertEquals(response.getStatus(), 200);

        try {
            updateMobileControllerMockInject.updateMobile(321857, json.toString());
        } catch (ServiceException se) {
            Assert.assertEquals(se.getErrorCode(), 123);
            Assert.assertEquals(se.getMessage(), "errormessage");
        }

        try {
            updateMobileControllerMockInject.updateMobile(321857, json.toString());
        } catch (ServiceException se) {
            Assert.assertEquals(se.getErrorCode(), 500);
            Assert.assertEquals(se.getMessage(), "Unable to process data");
        }
    }

}
