/**
 * 
 */
package com.games24x7.pspbeckend.Util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.constants.Globals;
import com.games24x7.pspbeckend.mq.CMmqFactory;
import com.games24x7.pspbeckend.pojo.ApprovalData;
import com.games24x7.pspbeckend.pojo.BlockAccountStatusData;
import com.games24x7.pspbeckend.pojo.BulkStatusChangeData;
import com.games24x7.pspbeckend.pojo.CloseAndMergeData;
import com.games24x7.pspbeckend.pojo.CloseAndRefundData;
import com.games24x7.pspbeckend.pojo.Refund;
import com.games24x7.pspbeckend.pojo.UserData;
import com.games24x7.pspbeckend.webclient.UserServiceClient;
import com.sun.jersey.core.header.FormDataContentDisposition;

/**
 * 
 */

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class AccountServiceUtilTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(AccountServiceUtilTest.class);
    @Mock
    Configuration config;
    @Mock
    CMmqFactory mq;
    @Mock
    UserServiceClient usClient;
    @InjectMocks
    AccountServiceUtil util;
    @InjectMocks
    MergeAndCloseUtil mutil;
    @Mock
    FundTransferUtil fsUtil;
    @InjectMocks
    ApprovalDataUtil appUtil;

    @Mock
    PlayerUpdateUtil puUtil;
    @Mock
    PSPCommentsUtil pspCommentsUtil;
    @Mock
    BlockedUsersMappingUtil blockedUsersMappingUtil;

    private CloseAndMergeData cmdata = null;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            this.cmdata = getCloseAndMergeData();
            logger.debug("cmdata :: " + cmdata.getMergeAccData());
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    private CloseAndMergeData getCloseAndMergeData() {
        Date date = new Date();
        FormDataContentDisposition contentDisposition = FormDataContentDisposition.name("testData").fileName("test.csv")
                .creationDate(date).modificationDate(date).readDate(date).size(1222).build();
        Mockito.when(config.get(Constants.FILE_LOCATION)).thenReturn("src/test/resources/");
        Mockito.when(config.getInt(Constants.ALLOWED_ENTRY)).thenReturn(1000);
        CloseAndMergeData data = mutil.mergeDataGeneration(contentDisposition, "murali", 1523l);
        Mockito.when(config.getInt(Constants.APPROVAL_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(config.getLong(Constants.APPROVAL_GENERATION_AMOUNT, 2000)).thenReturn(2000l);
        Mockito.when(fsUtil.getDepositAndWithdrawalAmt(Mockito.anyList())).thenReturn(getJsonArrForMerge());
        return appUtil.getApprovalData(data);
    }

    private JSONArray getJsonArrForMerge() {
        JSONArray value1 = new JSONArray();
        JSONObject obj1 = new JSONObject();
        obj1.put(Constants.USER_ID, 8000);
        obj1.put(Constants.DEPOSIT_AMOUNT, 300);
        obj1.put(Constants.WITHDRAWABLE_AMOUNT, 10);
        obj1.put(Constants.ACCOUNT_STATUS, 1);

        value1.put(obj1);

        JSONObject obj2 = new JSONObject();
        obj2.put(Constants.USER_ID, 178);
        obj2.put(Constants.DEPOSIT_AMOUNT, 300);
        obj2.put(Constants.WITHDRAWABLE_AMOUNT, 10);
        obj2.put(Constants.ACCOUNT_STATUS, 1);
        value1.put(obj2);

        JSONObject obj3 = new JSONObject();
        obj3.put(Constants.USER_ID, 522923);
        obj3.put(Constants.DEPOSIT_AMOUNT, 300);
        obj3.put(Constants.WITHDRAWABLE_AMOUNT, 10);
        obj3.put(Constants.ACCOUNT_STATUS, 1);
        value1.put(obj3);
        return value1;
    }

    private JSONArray getJsonArrForMergePart() {
        JSONArray value1 = new JSONArray();
        JSONObject obj1 = new JSONObject();
        obj1.put(Constants.USER_ID, 8000);
        obj1.put(Constants.DEPOSIT_AMOUNT, 300);
        obj1.put(Constants.WITHDRAWABLE_AMOUNT, 10);
        obj1.put(Constants.ACCOUNT_STATUS, 1);

        value1.put(obj1);

        JSONObject obj2 = new JSONObject();
        obj2.put(Constants.USER_ID, 178);
        obj2.put(Constants.DEPOSIT_AMOUNT, 300);
        obj2.put(Constants.WITHDRAWABLE_AMOUNT, 10);
        obj2.put(Constants.ACCOUNT_STATUS, 1);
        value1.put(obj2);
        return value1;

    }

    private JSONArray getSatusChangeResponseData() {
        JSONArray value1 = new JSONArray();
        JSONObject obj1 = new JSONObject();
        obj1.put(Constants.USER_ID, 8000);
        obj1.put(Constants.DEPOSIT_AMOUNT, 300);
        obj1.put(Constants.WITHDRAWABLE_AMOUNT, 10);
        obj1.put(Constants.ACCOUNT_STATUS, 1);

        value1.put(obj1);

        return value1;
    }

    @Test
    public void getherUserDataTest() {
        Mockito.when(config.getInt(Constants.ACC_CLOSURE_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(config.get(Constants.UMS_ACC_STATUS_URL)).thenReturn(
                "http://10.14.24.59:8080/user_service/api/usermanagement/users/?projection=user_id,loginid,email,account_status&_s=user_id=in=(USERIDS)");
        Mockito.when(usClient.getCall(Mockito.anyString())).thenReturn(getSatusChangeResponseData());

        UserData data = new UserData(1, "comments");
        data.setAccountStatus(2);
        data.toString();
        data.getAccountStatus();
        data.getComments();
        data.getUserId();

        List<UserData> list = new ArrayList<>();
        list.add(data);
        BulkStatusChangeData changeData = new BulkStatusChangeData();
        changeData.setAccountStatus(1);
        changeData.setAgent("agent");
        changeData.setAgentId(100);
        changeData.setFilename("FileName");
        changeData.setUserData(list);

        try {
            util.getherUserData(changeData, Globals.BULK_ACCOUNT_STATUS_CHANGE);
        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    @Test
    public void getherUserDataBukStatusChangeTest() {
        Mockito.when(config.getInt(Constants.ACC_CLOSURE_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(config.get(Constants.UMS_ACC_STATUS_URL)).thenReturn(
                "http://10.14.24.59:8080/user_service/api/usermanagement/users/?projection=user_id,loginid,email,account_status&_s=user_id=in=(USERIDS)");
        Mockito.when(usClient.getCall(Mockito.anyString())).thenReturn(getJsonArrForMerge());
        util.getherUserData(cmdata, Globals.CLOSE_AND_MERGE);
    }

    @Test
    public void getherUserDataTestRefund() {
        Mockito.when(config.getInt(Constants.ACC_CLOSURE_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(config.get(Constants.UMS_ACC_STATUS_URL)).thenReturn(
                "http://10.14.24.59:8080/user_service/api/usermanagement/users/?projection=user_id,loginid,email,account_status&_s=user_id=in=(USERIDS)");
        Mockito.when(usClient.getCall(Mockito.anyString())).thenReturn(getJsonArrForMerge());
        CloseAndRefundData data = Mockito.spy(new CloseAndRefundData());
        List<Refund> list = new ArrayList<>();
        Refund r1 = new Refund(1, "comments");
        list.add(r1);
        Mockito.when(data.getRefundList()).thenReturn(list);
        try {
            util.getherUserData(data, Globals.CLOSE_AND_REFUND);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getMessage(), "These Users Do not exist in DB [1]");
        }

    }

    @Test
    public void getherUserDataTestRefund1() {
        JSONArray value1 = new JSONArray();
        JSONObject obj1 = new JSONObject();
        obj1.put(Constants.USER_ID, 2001);
        obj1.put(Constants.DEPOSIT_AMOUNT, 300);
        obj1.put(Constants.WITHDRAWABLE_AMOUNT, 10);
        obj1.put(Constants.ACCOUNT_STATUS, 1);
        value1.put(obj1);
        Mockito.when(config.getInt(Constants.ACC_CLOSURE_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(config.get(Constants.UMS_ACC_STATUS_URL)).thenReturn(
                "http://10.14.24.59:8080/user_service/api/usermanagement/users/?projection=user_id,loginid,email,account_status&_s=user_id=in=(USERIDS)");
        Mockito.when(usClient.getCall(Mockito.anyString())).thenReturn(value1);
        CloseAndRefundData data = Mockito.spy(new CloseAndRefundData());
        List<Refund> list = new ArrayList<>();
        Refund r1 = new Refund(2001, "comments");
        list.add(r1);
        List<Long> l = new ArrayList<>();
        l.add(2001l);
        Mockito.when(data.getRefundList()).thenReturn(list);
        Mockito.when(data.getRefundUserIdList(list)).thenReturn(l);
        util.getherUserData(data, Globals.CLOSE_AND_REFUND);
    }

    @Test
    public void closeAccountTestRefund() {
        JSONArray value1 = new JSONArray();
        JSONObject obj1 = new JSONObject();
        obj1.put(Constants.USER_ID, 2001);
        obj1.put(Constants.DEPOSIT_AMOUNT, 300);
        obj1.put(Constants.WITHDRAWABLE_AMOUNT, 10);
        obj1.put(Constants.ACCOUNT_STATUS, 1);
        value1.put(obj1);
        Mockito.when(config.getInt(Constants.ACC_CLOSURE_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(usClient.getCall(Mockito.anyString())).thenReturn(value1);
        CloseAndRefundData data = Mockito.spy(new CloseAndRefundData());
        List<Refund> list = new ArrayList<>();
        Refund r1 = new Refund(2001, "comments");
        list.add(r1);
        List<Long> l = new ArrayList<>();
        l.add(2001l);
        Mockito.when(data.getRefundList()).thenReturn(list);
        Mockito.when(data.getRefundUserIdList(list)).thenReturn(l);
        Mockito.when(config.get(Constants.BULK_ACC_CLOSURE_URL))
                .thenReturn("http://10.14.24.136:8081/user_service/api/usermanagement/bulkstatusupdate/");
        Mockito.when(usClient.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn("");
        util.closeAccount(data, Globals.CLOSE_AND_REFUND);
    }

    @Test
    public void getherUserDataTestPartial() {
        Mockito.when(config.getInt(Constants.ACC_CLOSURE_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(config.get(Constants.UMS_ACC_STATUS_URL)).thenReturn(
                "http://10.14.24.59:8080/user_service/api/usermanagement/users/?projection=user_id,loginid,email,account_status&_s=user_id=in=(USERIDS)");
        Mockito.when(usClient.getCall(Mockito.anyString())).thenReturn(getJsonArrForMergePart());
        try {
            util.getherUserData(cmdata, Globals.CLOSE_AND_MERGE);
            Assert.fail();
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }
    }

    @Test
    public void getherUserDataTestFailure() {
        Mockito.when(config.getInt(Constants.ACC_CLOSURE_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(config.get(Constants.UMS_ACC_STATUS_URL)).thenReturn(
                "http://10.14.24.59:8080/user_service/api/usermanagement/users/?projection=user_id,loginid,email,account_status&_s=user_id=in=(USERIDS)");
        Mockito.when(usClient.getCall(Mockito.anyString())).thenReturn(null);
        try {
            util.getherUserData(cmdata, Globals.CLOSE_AND_MERGE);
            Assert.fail();
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

    }

    @Test
    public void closeAccountTestAllSuccess() {
        Mockito.when(config.getInt(Constants.ACC_CLOSURE_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(config.get(Constants.BULK_ACC_CLOSURE_URL))
                .thenReturn("http://10.14.24.136:8081/user_service/api/usermanagement/bulkstatusupdate/");
        Mockito.when(usClient.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn("");
        util.closeAccount(cmdata, Globals.CLOSE_AND_MERGE);
    }

    @Test
    public void closeAccountTestAllFailed() {
        Mockito.when(config.getInt(Constants.ACC_CLOSURE_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(config.get(Constants.BULK_ACC_CLOSURE_URL))
                .thenReturn("http://10.14.24.136:8081/user_service/api/usermanagement/bulkstatusupdate/");
        Mockito.when(usClient.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(null);
        util.closeAccount(cmdata, Globals.CLOSE_AND_MERGE);
    }

    @Test
    public void closeAccountTest() {
        Mockito.when(config.getInt(Constants.ACC_CLOSURE_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(config.get(Constants.BULK_ACC_CLOSURE_URL))
                .thenReturn("http://10.14.24.136:8081/user_service/api/usermanagement/bulkstatusupdate/");
        logger.debug("getJsonArrForMerge().toString() :: " + getJsonArrForMerge().toString());
        Mockito.when(usClient.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
                .thenReturn(new JSONObject().toString());
        util.closeAccount(cmdata, Globals.CLOSE_AND_MERGE);
    }

    @Test
    public void closeAccountTestpartialFailed() {
        Mockito.when(config.getInt(Constants.ACC_CLOSURE_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(config.get(Constants.BULK_ACC_CLOSURE_URL))
                .thenReturn("http://10.14.24.136:8081/user_service/api/usermanagement/bulkstatusupdate1/");
        JSONArray success = new JSONArray();
        success.put(1);
        JSONArray failed = new JSONArray();
        success.put(2);
        success.put(3);
        JSONObject obj1 = new JSONObject();
        obj1.put(Constants.FAILED_LIST, failed);
        obj1.put(Constants.SUCESS_LIST, success);

        Mockito.when(usClient.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
                .thenReturn(obj1.toString());
        util.closeAccount(cmdata, Globals.CLOSE_AND_MERGE);
    }

    @Test
    public void validateAccountStatusTest() {
        List<Long> list = new ArrayList<>();
        list.add(1l);
        list.add(2l);
        list.add(3l);
        Mockito.when(config.getInt(Constants.MERGE_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(config.get(Constants.UMS_URL)).thenReturn(
                "http://10.14.24.59:8080/user_service/api/usermanagement/users/?projection=user_id,loginid,email,account_status&_s=user_id=in=(USERIDS);account_status=in=(ACC_STATUS)");
        Mockito.when(usClient.getCall(Mockito.anyString())).thenReturn(null);
        JSONArray arr = util.validateAccountStatus(list, "5,6");
        Assert.assertNull(arr);
    }

    @Test
    public void validateAccountStatusTestSuccess() {
        List<Long> list = new ArrayList<>();
        list.add(1l);
        list.add(2l);
        list.add(3l);
        Mockito.when(config.getInt(Constants.MERGE_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(config.get(Constants.UMS_URL)).thenReturn(
                "http://10.14.24.59:8080/user_service/api/usermanagement/users/?projection=user_id,loginid,email,account_status&_s=user_id=in=(USERIDS);account_status=in=(ACC_STATUS)");
        Mockito.when(usClient.getCall(Mockito.anyString())).thenReturn(new JSONArray());
        JSONArray arr = util.validateAccountStatus(list, "5,6");
        Assert.assertNotNull(arr);
    }

    @Test
    public void validateUserExistanceTest() {
        List<Long> list = new ArrayList<>();
        list.add(1l);
        list.add(2l);
        list.add(3l);
        Mockito.when(config.getInt(Constants.MERGE_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(config.get(Constants.UMS_ACC_STATUS_URL)).thenReturn(
                "http://10.14.24.59:8080/user_service/api/usermanagement/users/?projection=user_id,loginid,email,account_status&_s=user_id=in=(USERIDS)");
        Mockito.when(usClient.getCall(Mockito.anyString())).thenReturn(getJsonArrForMerge());
        util.validateUserExistance(list);

    }

    @Test
    public void validateUserExistanceTestFail() {
        List<Long> list = new ArrayList<>();
        list.add(1l);
        list.add(2l);
        list.add(3l);
        Mockito.when(config.getInt(Constants.MERGE_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(config.get(Constants.UMS_ACC_STATUS_URL)).thenReturn(
                "http://10.14.24.59:8080/user_service/api/usermanagement/users/?projection=user_id,loginid,email,account_status&_s=user_id=in=(USERIDS)");
        Mockito.when(usClient.getCall(Mockito.anyString())).thenReturn(null);
        try {
            util.validateUserExistance(list);
            Assert.fail();
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

    }

    @Test
    public void updateAcctStatusForFailedMergeAcc() {
        Mockito.when(config.get(Constants.ALL_ACC_STATUS)).thenReturn("1,2,3,4,5,6,7");
        Mockito.when(config.getInt(Constants.ACC_CLOSURE_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(config.get(Constants.BULK_ACC_CLOSURE_URL))
                .thenReturn("http://10.14.24.136:8081/user_service/api/usermanagement/bulkstatusupdate/");
        Mockito.when(usClient.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn("");
        util.updateAcctStatusForFailedMergeAcc(cmdata);

    }

    @Test
    public void validateUserExistanceTestPartialFail() {
        List<Long> list = new ArrayList<>();
        list.add(1l);
        list.add(2l);
        list.add(3l);
        Mockito.when(config.getInt(Constants.MERGE_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(config.get(Constants.UMS_ACC_STATUS_URL)).thenReturn(
                "http://10.14.24.59:8080/user_service/api/usermanagement/users/?projection=user_id,loginid,email,account_status&_s=user_id=in=(USERIDS)");
        Mockito.when(usClient.getCall(Mockito.anyString())).thenReturn(getJsonArrForMergePart());

        try {
            util.validateUserExistance(list);
            Assert.fail();
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getMessage(), "These Users Do not exist in DB [1, 2, 3]");
        }

        CloseAndRefundData data = new CloseAndRefundData();
        data.setRefundList(null);
        data.setAgentId(2);
        data.setAgent("");
        data.setFilename("");

        ApprovalData dataa = new ApprovalData(100, 100, 100, 100);

    }

    @Test
    public void processBlockUserTest() {
        JSONObject context = new JSONObject();
        context.put("agentId", 200);
        context.put("source", "PSP");
        BlockAccountStatusData data = new BlockAccountStatusData();
        try {
            util.processBlockUser(data, context);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.US_CALL_FAILURE_CODE);
        }

        Mockito.when(config.get("ums.user.kyc.url")).thenReturn(
                "http://10.14.24.59:8080/user_service/api/usermanagement/users/USERID?projection=account_status,kyc_status,firstname,email,mobile_no,user_id,loginid");
        Mockito.when(usClient.getStringResGetCall(Mockito.anyString())).thenReturn(
                "{\"kyc_status\":3,\"firstname\":\"Harsh\",\"loginid\":\"qatest_200\",\"user_id\":200,\"mobile_no\":\"9833416628\",\"account_status\":5,\"email\":\"qatest_200@thisrandomdomian.der\"}");
        Mockito.when(puUtil.insertIntoPlayerUpddatesForBlockUserHeaderData(Mockito.anyLong(), Mockito.anyLong(),
                Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyObject(),Mockito.anyString())).thenReturn(100l);
        Mockito.when(puUtil.insertIntoPlayerUpddatesForBlockUser(Mockito.anyObject(), Mockito.anyString(),
                Mockito.anyObject(),Mockito.anyString())).thenReturn(101l);
        Mockito.doNothing().when(pspCommentsUtil).savePSPComments(Mockito.anyLong(), Mockito.anyObject());
        Mockito.doNothing().when(blockedUsersMappingUtil).saveBlockedUsers(Mockito.anyLong(), Mockito.anyInt(),
                Mockito.anyString(), Mockito.anyString());
        data.setUserId(200);
        try {
            context.put("source", "UMS");
            util.processBlockUser(data, context);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(),
                    Constants.BLOCK_USER_PROCESS_ERROR_CODE.intValue());
            logger.error("", e);
        }

        Mockito.when(config.get("ums.user.update.url")).thenReturn(
                "http://10.14.24.59:8080/user_service/api/usermanagement/users/USERID?projection=account_status,kyc_status,firstname,email,mobile_no,user_id,loginid");
        Mockito.when(usClient.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
                .thenReturn("success");
        context.put("source", "PSP");
        util.processBlockUser(data, context);

    }

}
