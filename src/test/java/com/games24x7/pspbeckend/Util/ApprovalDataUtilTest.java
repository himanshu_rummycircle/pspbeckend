/**
 * 
 */
package com.games24x7.pspbeckend.Util;

import java.util.Date;

import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.dao.ApprovalDAO;
import com.games24x7.pspbeckend.pojo.CloseAndMergeData;
import com.sun.jersey.core.header.FormDataContentDisposition;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class ApprovalDataUtilTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(ApprovalDataUtilTest.class);
    @Mock
    ApprovalDAO dao;

    @Mock
    Configuration config;

    @Mock
    ThreadPoolExecutorUtil util;

    @Mock
    FundTransferUtil fsUtil;
    @InjectMocks
    ApprovalDataUtil appUtil;
    @InjectMocks
    MergeAndCloseUtil mutil;
    private CloseAndMergeData cmdata = null;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            this.cmdata = getCloseAndMergeData();
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void getApprovalDataTest() {

        JSONArray value = new JSONArray();
        JSONObject obj = new JSONObject();
        obj.put(Constants.USER_ID, 8000);
        obj.put(Constants.DEPOSIT_AMOUNT, 3000);
        obj.put(Constants.WITHDRAWABLE_AMOUNT, 10);
        value.put(obj);
        Mockito.when(config.getInt(Constants.APPROVAL_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(fsUtil.getDepositAndWithdrawalAmt(Mockito.anyList())).thenReturn(value);

        try {
            appUtil.getApprovalData(cmdata);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals("These Users are not Present In DB [178, 522923]", e.getMessage());
        }

    }

    @Test
    public void getApprovalDataTestException() {
        logger.debug("4");
        Mockito.when(config.getInt(Constants.APPROVAL_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(fsUtil.getDepositAndWithdrawalAmt(Mockito.anyList())).thenReturn(null);

        try {
            appUtil.getApprovalData(cmdata);

        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals("These Users are not Present In DB [8000, 178, 522923]", e.getMessage());
        }
    }

    @Test
    public void getApprovalDataTestSucess() {
        Mockito.when(config.getInt(Constants.APPROVAL_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(config.getLong(Constants.APPROVAL_GENERATION_AMOUNT, 2000)).thenReturn(2000l);
        Mockito.when(fsUtil.getDepositAndWithdrawalAmt(Mockito.anyList())).thenReturn(getJsonArr());
        CloseAndMergeData data = appUtil.getApprovalData(cmdata);

        Assert.assertNotNull(data);
        Assert.assertEquals(data.getApprovalData().size(), 3);
        Assert.assertEquals(data.getMergeAccData().size(), 0);
    }

    @Test
    public void getMergeDataTestSucess() {
        Mockito.when(config.getInt(Constants.APPROVAL_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.when(config.getLong(Constants.APPROVAL_GENERATION_AMOUNT, 2000)).thenReturn(2000l);
        Mockito.when(fsUtil.getDepositAndWithdrawalAmt(Mockito.anyList())).thenReturn(getJsonArrForMerge());
        CloseAndMergeData data = appUtil.getApprovalData(cmdata);

        Assert.assertNotNull(data);
        Assert.assertEquals(data.getApprovalData().size(), 0);
        Assert.assertEquals(data.getMergeAccData().size(), 3);
    }

    @Test(dependsOnMethods = { "getApprovalDataTestSucess" }, alwaysRun = true)
    public void generateApprovalTest() {
        Mockito.when(config.getInt(Constants.APPROVAL_BATCH_SIZE, 100)).thenReturn(100);
        Mockito.doNothing().when(dao).generateApprovalId(Mockito.anyList());
        Mockito.doNothing().when(dao).generateDataShowLogAndAccMergingLog(Mockito.anyList());

        Mockito.when(config.getLong(Constants.APPROVAL_GENERATION_AMOUNT, 2000)).thenReturn(2000l);
        Mockito.when(fsUtil.getDepositAndWithdrawalAmt(Mockito.anyList())).thenReturn(getJsonArr());
        CloseAndMergeData data = appUtil.getApprovalData(cmdata);
        try {
            appUtil.generateApproval(data);
        } catch (Exception e) {
            logger.error("", e);
        }

    }

    /**
     * @return
     */
    private JSONArray getJsonArr() {
        JSONArray value1 = new JSONArray();
        JSONObject obj1 = new JSONObject();
        obj1.put(Constants.USER_ID, 8000);
        obj1.put(Constants.DEPOSIT_AMOUNT, 3000);
        obj1.put(Constants.WITHDRAWABLE_AMOUNT, 10);
        value1.put(obj1);

        JSONObject obj2 = new JSONObject();
        obj2.put(Constants.USER_ID, 178);
        obj2.put(Constants.DEPOSIT_AMOUNT, 3000);
        obj2.put(Constants.WITHDRAWABLE_AMOUNT, 10);
        value1.put(obj2);

        JSONObject obj3 = new JSONObject();
        obj3.put(Constants.USER_ID, 522923);
        obj3.put(Constants.DEPOSIT_AMOUNT, 3000);
        obj3.put(Constants.WITHDRAWABLE_AMOUNT, 10);
        value1.put(obj3);
        return value1;
    }

    private JSONArray getJsonArrForMerge() {
        JSONArray value1 = new JSONArray();
        JSONObject obj1 = new JSONObject();
        obj1.put(Constants.USER_ID, 8000);
        obj1.put(Constants.DEPOSIT_AMOUNT, 300);
        obj1.put(Constants.WITHDRAWABLE_AMOUNT, 10);
        value1.put(obj1);

        JSONObject obj2 = new JSONObject();
        obj2.put(Constants.USER_ID, 178);
        obj2.put(Constants.DEPOSIT_AMOUNT, 300);
        obj2.put(Constants.WITHDRAWABLE_AMOUNT, 10);
        value1.put(obj2);

        JSONObject obj3 = new JSONObject();
        obj3.put(Constants.USER_ID, 522923);
        obj3.put(Constants.DEPOSIT_AMOUNT, 300);
        obj3.put(Constants.WITHDRAWABLE_AMOUNT, 10);
        value1.put(obj3);
        return value1;
    }

    /**
     * @return
     */
    private CloseAndMergeData getCloseAndMergeData() {
        Date date = new Date();
        FormDataContentDisposition contentDisposition = FormDataContentDisposition.name("testData").fileName("test.csv")
                .creationDate(date).modificationDate(date).readDate(date).size(1222).build();
        Mockito.when(config.get(Constants.FILE_LOCATION)).thenReturn("src/test/resources/");
        Mockito.when(config.getInt(Constants.ALLOWED_ENTRY)).thenReturn(1000);
        return mutil.mergeDataGeneration(contentDisposition, "murali", 1523l);
    }

}
