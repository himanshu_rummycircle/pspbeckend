/**
 * 
 */
package com.games24x7.pspbeckend.Util;

import static org.mockito.Matchers.anyString;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.kafka.core.KafkaMessagePublisher;
import com.games24x7.pspbeckend.pojo.CloseAndMergeData;
import com.games24x7.pspbeckend.pojo.MergeAccount;
import com.games24x7.pspbeckend.pojo.PlayerUpdates;
import com.games24x7.pspbeckend.pojo.PlayersRiskCertificate;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class AuditUtilTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(AuditUtilTest.class);
    @Mock
    ThreadPoolExecutorUtil util;

    @Mock
    KafkaMessagePublisher messagePublisher;

    @InjectMocks
    AuditUtil autil;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void auditAccountMergingTest() {
        Mockito.when(util.getPlayerUpdateThreadpoolexecutor()).thenReturn(
                new ThreadPoolExecutor(3, 10, 2, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>()));
        try {
            Mockito.doNothing().when(messagePublisher).publishKafkaMessage(Mockito.anyString(), Mockito.anyString());
        } catch (Exception e) {
            logger.error("", e);
        }
        try {
            autil.auditAccountMerging(null);
        } catch (Exception e) {
        }

        List<MergeAccount> list = new ArrayList<>();
        MergeAccount a1 = new MergeAccount(1, 2, "comment", 1, 2);
        a1.setAccountStatus(6);
        list.add(a1);
        CloseAndMergeData data = Mockito.spy(new CloseAndMergeData());
        Mockito.when(data.getMergeAccData()).thenReturn(list);
        Mockito.when(data.getAgent()).thenReturn("murali");
        Mockito.when(data.getAgentId()).thenReturn(100l);
        autil.auditAccountMerging(data);

        try {
            Mockito.doThrow(new RuntimeException("Testing ....")).when(messagePublisher)
                    .publishKafkaMessage(Mockito.anyString(), Mockito.anyString());
        } catch (Exception e) {
            logger.error("", e);
        }
        CloseAndMergeData data1 = Mockito.spy(new CloseAndMergeData());
        Mockito.when(data1.getMergeAccData()).thenReturn(list);
        Mockito.when(data1.getAgent()).thenReturn("murali");
        Mockito.when(data1.getAgentId()).thenReturn(100l);
        autil.auditAccountMerging(data1);

    }

    @Test
    public void playerUpdateAuditLogTest() {
        Mockito.when(util.getPlayerUpdateThreadpoolexecutor()).thenReturn(
                new ThreadPoolExecutor(3, 10, 2, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>()));

        List<PlayerUpdates> list = new ArrayList<>();
        PlayerUpdates p = new PlayerUpdates(1l, 2l, 3, "NA", "comment", new Timestamp(System.currentTimeMillis()),
                "additionalInfo", 1l);
        list.add(p);
        autil.playerUpdateAuditLog(list);

        try {
            Mockito.doThrow(new RuntimeException("Testing ....")).when(messagePublisher)
                    .publishKafkaMessage(Mockito.anyString(), Mockito.anyString());
        } catch (Exception e) {
            logger.error("", e);
        }
        autil.playerUpdateAuditLog(list);
    }
    
    @Test
    public void publishToAuditPlayerRiskCertificateTest(){
        
        PlayersRiskCertificate riskPayload = new PlayersRiskCertificate();
        riskPayload.setUserId(100l);
        riskPayload.setAgentId("A");
        riskPayload.setComment("Comment");
        riskPayload.setRiskCertificateNames("Comment");
        riskPayload.setContactType("Ctype");
        riskPayload.setContactDirection("CDirection");
        riskPayload.setRiskStatus("Status");
        riskPayload.setRiskStatusComment("Status Comment");
        
        autil.publishToAuditPlayerRiskCertificate(riskPayload);
        
        riskPayload.setReasonForDowngrade("");
        riskPayload.setInteractionId("");
        
        autil.publishToAuditPlayerRiskCertificate(riskPayload);
        
        riskPayload.setReasonForDowngrade("setInteractionId");
        riskPayload.setInteractionId("Status Comment");
        
        autil.publishToAuditPlayerRiskCertificate(riskPayload);
    }
    
    @Test
    public void publishToAuditTest(){
        try {
            Mockito.doNothing().when(messagePublisher).publishKafkaMessage(Mockito.anyString(), Mockito.anyString());
        } catch (Exception e) {
            logger.error("", e);
        }
        
        autil.publishToAudit("riskPayload",new JSONObject());
        
        try {
            Mockito.doThrow(new NullPointerException()).when(messagePublisher).publishKafkaMessage(Mockito.anyString(), Mockito.anyString());
        } catch (Exception e) {
            logger.error("",e);
        }
        
        autil.publishToAudit("riskPayload",new JSONObject());
    }
}
