package com.games24x7.pspbeckend.Util;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.webclient.WithdrawServiceClient;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class BankDetailUtilTest {
	private static final Logger logger = LoggerFactory.getLogger(BankDetailUtilTest.class);

	@Mock
	WithdrawServiceClient wsClient;
	@Mock
	Configuration config;
	@InjectMocks
	BankDetailsUtil bankDetailsUtil;

	@BeforeTest
	public void testSetup() {
		try {
			MockitoAnnotations.initMocks(this);
			Mockito.when(config.get(Mockito.anyString())).thenReturn("abc");
		} catch (Exception e) {
			logger.error("Exception in testSetup", e);
		}
	}

	@Test
	public void testgetBankDetailForUser() {
		try {
			Mockito.when(wsClient.getStringResGetCall(Mockito.anyString())).thenReturn(null);
			JSONObject res = bankDetailsUtil.getBankDetailForUser(123l);
			Assert.assertEquals(res, null);
		} catch (ServiceException se) {
			Assert.fail();
		} catch (Exception e) {
			Assert.fail();
		}

		try {
			Mockito.when(wsClient.getStringResGetCall(Mockito.anyString())).thenReturn("{'abc':'pqr'}");
			JSONObject res = bankDetailsUtil.getBankDetailForUser(123l);
			Assert.assertEquals(res.get("abc"), "pqr");
		} catch (ServiceException se) {
			Assert.fail();
		} catch (Exception e) {
			Assert.fail();
		}

		try {
			Mockito.when(wsClient.getStringResGetCall(Mockito.anyString())).thenReturn("{");
			JSONObject res = bankDetailsUtil.getBankDetailForUser(123l);
			Assert.fail();
		} catch (ServiceException se) {
			Assert.assertEquals(se.getStatus(), Status.INTERNAL_SERVER_ERROR);
			Assert.assertEquals(se.getBusinessErrorCode().intValue(), Constants.WS_CALL_FAILURE_CODE);
		} catch (Exception e) {
			Assert.fail();
		}

		ServiceException ex = new ServiceException("ServiceException ", Status.BAD_REQUEST);
		Mockito.doThrow(ex).when(wsClient).getStringResGetCall(Mockito.anyString());

		try {
			JSONObject res = bankDetailsUtil.getBankDetailForUser(123l);
			Assert.fail();
		} catch (ServiceException se) {
			Assert.assertEquals(se.getStatus(), Status.BAD_REQUEST);
		} catch (Exception e) {
			Assert.fail();
		}

	}

	@Test
	public void testupdateBankDetailForUser() {
		JSONObject payload = new JSONObject();
		Mockito.when(wsClient.postCall(Mockito.anyString(), Mockito.any())).thenReturn(null);
		try {
			bankDetailsUtil.updateBankDetailForUser(123l, payload);
			Assert.assertTrue(true);
		} catch (ServiceException se) {
			Assert.fail();
		} catch (Exception e) {
			Assert.fail();
		}

		payload = new JSONObject();
		ServiceException ex = new ServiceException("ServiceException ", Status.BAD_REQUEST);
		Mockito.doThrow(ex).when(wsClient).postCall(Mockito.anyString(), Mockito.any());
		try {
			bankDetailsUtil.updateBankDetailForUser(123l, payload);
			Assert.fail();
		} catch (ServiceException se) {
			Assert.assertEquals(se.getStatus(), Status.BAD_REQUEST);
		} catch (Exception e) {
			Assert.fail();
		}

	}

}
