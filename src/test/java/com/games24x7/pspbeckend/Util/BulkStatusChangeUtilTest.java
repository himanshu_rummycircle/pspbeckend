/**
 * 
 */
package com.games24x7.pspbeckend.Util;

import static org.testng.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.core.Response.Status;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.pojo.BulkStatusChangeData;
import com.games24x7.pspbeckend.pojo.UserData;
import com.sun.jersey.core.header.FormDataContentDisposition;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class BulkStatusChangeUtilTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(BulkStatusChangeUtilTest.class);
    @Mock
    Configuration config;
    @Mock
    FileValidationsUtil fileValidation;

    @InjectMocks
    BulkStatusChangeUtil util;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            // util.config = config;
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void basicValidations() {
        try {
            Mockito.doThrow(new ServiceException("AgentName is missing.", Status.BAD_REQUEST)).when(fileValidation)
                    .agentInputValidation(Mockito.anyString(), Mockito.anyLong());
            util.basicValidations(null, null, null, null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

        try {
            Mockito.doThrow(new ServiceException("", Status.BAD_REQUEST)).when(fileValidation)
                    .fileValidation(Mockito.anyObject(), Mockito.anyObject());
            util.basicValidations(null, null, null, null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

        try {
            Mockito.doThrow(new ServiceException("", Status.BAD_REQUEST)).when(fileValidation)
                    .validateFileHeader(Mockito.anyObject(), Mockito.anyInt());
            util.basicValidations(null, null, null, null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

        Mockito.doNothing().when(fileValidation).agentInputValidation(Mockito.anyString(), Mockito.anyLong());
        Mockito.doNothing().when(fileValidation).fileValidation(Mockito.anyObject(), Mockito.anyObject());
        Mockito.doNothing().when(fileValidation).validateFileHeader(Mockito.anyObject(), Mockito.anyInt());
        util.basicValidations(null, null, null, null);

    }

    @Test
    public void testgenerateData() {
        Date date = new Date();
        FormDataContentDisposition contentDisposition = FormDataContentDisposition.name("testData")
                .fileName("testBulkStatusChange.csv").creationDate(date).modificationDate(date).readDate(date)
                .size(1222).build();
        Mockito.when(config.get(Constants.FILE_LOCATION)).thenReturn("src/test/resources/");
        Mockito.when(config.getInt(Constants.ALLOWED_ENTRY)).thenReturn(1000);
        BulkStatusChangeData data = util.generateData(contentDisposition, "murali", 1523l);
        Assert.assertNotNull(data);
    }

    @Test
    public void testdataValidation() {

        Mockito.when(config.getInt(Constants.ALLOWED_ENTRY)).thenReturn(1000);
        Mockito.when(config.get(Constants.VALID_ACCOUNT_STATUS)).thenReturn(Constants.ALLOWED_ACCOUNT_STATUS);

        BulkStatusChangeData bulkdata = Mockito.spy(new BulkStatusChangeData());
        Mockito.when(bulkdata.getUserData()).thenReturn(new ArrayList<UserData>());
        try {
            util.dataValidation(bulkdata);
        } catch (ServiceException e) {
            assertEquals(e.getStatus(), Status.BAD_REQUEST);
        }

        try {
            ArrayList<UserData> list = new ArrayList<>();
            UserData data = new UserData(100, "comments");
            list.add(data);
            Mockito.when(bulkdata.getUserData()).thenReturn(list);
            Mockito.when(config.getInt(Constants.ALLOWED_ENTRY)).thenReturn(0);
            util.dataValidation(bulkdata);
        } catch (ServiceException e) {
            assertEquals(e.getStatus(), Status.BAD_REQUEST);
        }

        try {
            ArrayList<UserData> list = new ArrayList<>();
            UserData data = new UserData(100, "comments");
            list.add(data);
            Mockito.when(bulkdata.getUserData()).thenReturn(list);
            Mockito.when(bulkdata.getAccountStatus()).thenReturn(null);
            Mockito.when(config.getInt(Constants.ALLOWED_ENTRY)).thenReturn(10);
            util.dataValidation(bulkdata);
        } catch (ServiceException e) {
            assertEquals(e.getStatus(), Status.BAD_REQUEST);
        }

        try {
            ArrayList<UserData> list = new ArrayList<>();
            UserData data = new UserData(100, "comments");
            list.add(data);
            Mockito.when(bulkdata.getUserData()).thenReturn(list);
            Mockito.when(bulkdata.getAccountStatus()).thenReturn(0);
            Mockito.when(config.getInt(Constants.ALLOWED_ENTRY)).thenReturn(10);
            util.dataValidation(bulkdata);
        } catch (ServiceException e) {
            assertEquals(e.getStatus(), Status.BAD_REQUEST);
        }

        try {
            ArrayList<UserData> list = new ArrayList<>();
            UserData data = new UserData(100, "comments");
            list.add(data);
            Mockito.when(bulkdata.getUserData()).thenReturn(list);
            Mockito.when(config.getInt(Constants.ALLOWED_ENTRY)).thenReturn(10);
            Mockito.when(config.get(Constants.VALID_ACCOUNT_STATUS)).thenReturn(Constants.ALLOWED_ACCOUNT_STATUS);
            bulkdata.setAccountStatus(100);
            util.dataValidation(bulkdata);
        } catch (ServiceException e) {
            assertEquals(e.getStatus(), Status.BAD_REQUEST);
        }
    }

    @Test
    public void test() {

        UserData data = new UserData(1, "comments");
        data.setAccountStatus(2);
        data.toString();
        data.getAccountStatus();
        data.getComments();
        data.getUserId();

        List<UserData> list = new ArrayList<>();
        list.add(data);
        BulkStatusChangeData changeData = new BulkStatusChangeData();
        changeData.setAccountStatus(1);
        changeData.setAgent("agent");
        changeData.setAgentId(100);
        changeData.setFilename("FileName");
        changeData.setUserData(list);

        Assert.assertNotNull(changeData.getAccountStatus());
        Assert.assertNotNull(changeData.getAgent());
        Assert.assertNotNull(changeData.getFilename());
        Assert.assertNotNull(changeData.getUserData());
        Assert.assertNotNull(changeData.getUserIdList(list));
        try {
            changeData.validateAccountStatus(list, 1);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
        }

        try {
            List<Long> l = new ArrayList<>();
            l.add(1l);
            l.add(1l);
            changeData.validateDuplicateUserIds(l);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus(), Status.BAD_REQUEST);
        }

    }
}
