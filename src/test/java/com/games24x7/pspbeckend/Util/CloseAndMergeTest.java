package com.games24x7.pspbeckend.Util;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.pspbeckend.pojo.CloseAndMergeData;
import com.games24x7.pspbeckend.pojo.MergeAccount;
import com.games24x7.pspbeckend.pojo.PlayerUpdates;
import com.jamonapi.aop.spring.SystemAopPointcutDefinitions;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class CloseAndMergeTest extends AbstractTestNGSpringContextTests{

    @InjectMocks
    CloseAndMergeData closeAndMergeData;
    
    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }
    
    @Test
    public void getMergeAccForAccStatusTest(){
        List<MergeAccount> list = new ArrayList<MergeAccount>();
        int accStatus =4;
        MergeAccount mergeAccount = new MergeAccount(11,11,"C");
        mergeAccount.setAccountStatus(4);
        list.add(mergeAccount);
        
        mergeAccount = new MergeAccount(11,11,"C");
        mergeAccount.setAccountStatus(5);
        list.add(mergeAccount);
        
        closeAndMergeData.getMergeAccForAccStatus(list, accStatus);
    }
    
    @Test
    public void getMergeAccAfterRemovalTest(){
        CloseAndMergeData data = new CloseAndMergeData();
        Map<Long, MergeAccount> mapList = new HashMap<Long,MergeAccount>();
        MergeAccount mergeAccount = new MergeAccount(11,11,"C");
        
        mapList.put(100l, mergeAccount);
        data.setFromUserMergeAccMap(mapList);
        List<Long> failedUserList = new ArrayList<Long>();
        failedUserList.add(100l);
        closeAndMergeData.getMergeAccAfterRemoval(data, failedUserList);
    }
    
    @Test
    public void getMergeAccTest(){
        CloseAndMergeData data = new CloseAndMergeData();
        Map<Long, MergeAccount> fromUserMergeAccMap = new HashMap<Long,MergeAccount>();
        MergeAccount mAccount = new MergeAccount(10, 11, "C");
        fromUserMergeAccMap.put(100l, mAccount);
        data.setFromUserMergeAccMap(fromUserMergeAccMap);
        List<Long> userList = new ArrayList<Long>();
        userList.add(100l);
        closeAndMergeData.getMergeAcc(userList, data);
        
        PlayerUpdates pUpdate = new PlayerUpdates(100l, 100l,12, "", "", new Timestamp(10000000000l), "", 111l);
        pUpdate.setAdditionalInfo("");
        pUpdate.setAgentId(200l);
        pUpdate.setComment("");
        pUpdate.setFieldUpdated(2);
        pUpdate.setId(122l);
        pUpdate.setPlayerUserId(99l);
        pUpdate.setTxnId(100l);
        pUpdate.setUpdateDate(new Timestamp(10000000000l));
        pUpdate.setPreviousData("");
    }

}
