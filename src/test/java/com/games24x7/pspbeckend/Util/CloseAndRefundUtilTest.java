/**
 * 
 */
package com.games24x7.pspbeckend.Util;

import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.core.Response.Status;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.pojo.CloseAndRefundData;
import com.games24x7.pspbeckend.pojo.Refund;
import com.sun.jersey.core.header.FormDataContentDisposition;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class CloseAndRefundUtilTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(CloseAndRefundUtilTest.class);

    @Mock
    Configuration config;
    @Mock
    FileValidationsUtil fileValidation;

    @InjectMocks
    CloseAndRefundUtil util;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void basicValidations() {
        Mockito.when(config.get(Constants.FILE_LOCATION)).thenReturn("src/test/resources/");
        Mockito.when(config.getInt(Constants.ALLOWED_ENTRY)).thenReturn(1000);
        try {
            Mockito.doThrow(new ServiceException("AgentName is missing.", Status.BAD_REQUEST)).when(fileValidation)
                    .agentInputValidation(Mockito.anyString(), Mockito.anyLong());
            util.basicValidations(null, null, null, null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

        try {
            Mockito.doThrow(new ServiceException("", Status.BAD_REQUEST)).when(fileValidation)
                    .fileValidation(Mockito.anyObject(), Mockito.anyObject());
            util.basicValidations(null, null, null, null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

        try {
            Mockito.doThrow(new ServiceException("", Status.BAD_REQUEST)).when(fileValidation)
                    .validateFileHeader(Mockito.anyObject(), Mockito.anyInt());
            util.basicValidations(null, null, null, null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

        Mockito.doNothing().when(fileValidation).agentInputValidation(Mockito.anyString(), Mockito.anyLong());
        Mockito.doNothing().when(fileValidation).fileValidation(Mockito.anyObject(), Mockito.anyObject());
        Mockito.doNothing().when(fileValidation).validateFileHeader(Mockito.anyObject(), Mockito.anyInt());
        util.basicValidations(null, null, null, null);

    }

    @Test
    public void refundDataValidationTest() {
        Mockito.when(config.get(Constants.FILE_LOCATION)).thenReturn("src/test/resources/");
        Mockito.when(config.getInt(Constants.ALLOWED_ENTRY)).thenReturn(1000);
        CloseAndRefundData data = Mockito.spy(new CloseAndRefundData());
        Mockito.when(data.getRefundList()).thenReturn(new ArrayList<Refund>());
        try {
            util.refundDataValidation(data);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

        ArrayList<Refund> l = Mockito.spy(new ArrayList<Refund>());
        Mockito.when(l.isEmpty()).thenReturn(false);
        Mockito.when(l.size()).thenReturn(2000);
        Mockito.when(data.getRefundList()).thenReturn(l);
        try {
            util.refundDataValidation(data);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

        Mockito.when(l.isEmpty()).thenReturn(false);
        Mockito.when(l.size()).thenReturn(300);
        Mockito.when(data.getRefundList()).thenReturn(l);
        try {
            util.refundDataValidation(data);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

    }

    @Test
    public void refundDataGenerationTest() {
        Date date = new Date();
        FormDataContentDisposition contentDisposition = FormDataContentDisposition.name("testData").fileName("test.csv")
                .creationDate(date).modificationDate(date).readDate(date).size(1222).build();

        Mockito.when(config.get(Constants.FILE_LOCATION)).thenReturn("src/test/resources/");
        Mockito.when(config.getInt(Constants.ALLOWED_ENTRY)).thenReturn(1000);
        CloseAndRefundData data = util.refundDataGeneration(contentDisposition, "murali", 1523l);
        Assert.assertNotNull(data);

    }

}
