/**
 * 
 */
package com.games24x7.pspbeckend.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;

import javax.ws.rs.core.Response.Status;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Globals;
import com.sun.jersey.core.header.FormDataContentDisposition;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class FileValidationsUtilTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(FileValidationsUtilTest.class);
    @Mock
    Configuration config;

    @InjectMocks
    FileValidationsUtil util;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void fileValidationTest() {
        Mockito.when(config.get(Constants.FILE_LOCATION)).thenReturn("src/test/resources/");
        Mockito.when(config.get("allowedFileExtention", Constants.CSV)).thenReturn("csv");
        try {
            util.fileValidation(null, null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

        Date date = new Date();
        FormDataContentDisposition contentDisposition = FormDataContentDisposition.name("testData").fileName("test.csv")
                .creationDate(date).modificationDate(date).readDate(date).size(1222).build();
        InputStream uploadedInputStream = null;
        try {
            uploadedInputStream = new FileInputStream("src/test/resources/test.csv");
        } catch (FileNotFoundException e) {
            logger.error("", e);
        }
        try {
            util.fileValidation(contentDisposition, uploadedInputStream);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

        try {
            util.fileValidation(contentDisposition, uploadedInputStream);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

        FormDataContentDisposition contentDisposition1 = FormDataContentDisposition.name("testData")
                .fileName("test.txt").creationDate(date).modificationDate(date).readDate(date).size(1222).build();
        try {
            util.fileValidation(contentDisposition1, uploadedInputStream);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

        Mockito.when(config.get(Constants.FILE_LOCATION)).thenReturn("src/test/resources/test.csv");
        try {
            util.fileValidation(contentDisposition, uploadedInputStream);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.INTERNAL_SERVER_ERROR.getStatusCode());
        }

        Mockito.when(config.get(Constants.FILE_LOCATION)).thenReturn("src/test/resources/");
        contentDisposition = FormDataContentDisposition.name("testData").fileName("test1.csv").creationDate(date)
                .modificationDate(date).readDate(date).size(1222).build();

        util.fileValidation(contentDisposition, uploadedInputStream);
        File file = new File("src/test/resources/test1.csv");
        file.delete();

    }

    @Test
    public void validateFileHeaderTest() {

        Mockito.when(config.get(Constants.FILE_LOCATION)).thenReturn("src/test/resources/");
        Date date = new Date();
        FormDataContentDisposition contentDisposition = FormDataContentDisposition.name("testData").fileName("test.csv")
                .creationDate(date).modificationDate(date).readDate(date).size(1222).build();

        util.validateFileHeader(contentDisposition, Globals.CLOSE_AND_MERGE);

        try {
            util.validateFileHeader(contentDisposition, Globals.CLOSE_AND_REFUND);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

        try {
            util.validateFileHeader(contentDisposition, 3);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

        contentDisposition = FormDataContentDisposition.name("testData").fileName("test1.csv").creationDate(date)
                .modificationDate(date).readDate(date).size(1222).build();
        try {
            util.validateFileHeader(contentDisposition, Globals.CLOSE_AND_MERGE);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

    }

    @Test
    public void agentInputValidationTest() {
        try {
            util.agentInputValidation(null, null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

        try {
            util.agentInputValidation("", null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

        try {
            util.agentInputValidation("test", 0l);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }
        util.agentInputValidation("test", 100l);

    }

}
