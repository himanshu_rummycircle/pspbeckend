package com.games24x7.pspbeckend.Util;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.pspbeckend.webclient.UserServiceClient;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class FraudServiceUtilTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(FraudServiceUtilTest.class);
    @Autowired
    UserServiceClient usClient;
    @Autowired
    Configuration config;
    @InjectMocks
    FraudServiceUtil util;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            util.usClient = usClient;
            util.config = config;
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void testFraud() {
        try {
            util.getFraudDetailForDevice("1", 1);
        } catch (Exception e) {
        }
        try {
            util.getFraudDetailForStatus(1, 1, 0);
        } catch (Exception e) {
        }

    }
}
