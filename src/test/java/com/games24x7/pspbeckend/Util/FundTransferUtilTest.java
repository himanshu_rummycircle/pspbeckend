/**
 * 
 */
package com.games24x7.pspbeckend.Util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.pojo.CloseAndMergeData;
import com.games24x7.pspbeckend.pojo.MergeAccount;
import com.games24x7.pspbeckend.webclient.FundServiceClient;
import com.sun.jersey.core.header.FormDataContentDisposition;

/**
 * 
 */

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class FundTransferUtilTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(FundTransferUtilTest.class);
    private CloseAndMergeData cmdata = null;
    @Mock
    FundServiceClient fsClient;

    @Mock
    Configuration config;
    @Mock
    ThreadPoolExecutorUtil util;
    @InjectMocks
    MergeAndCloseUtil mutil;
    @InjectMocks
    FundTransferUtil fsUtil;
    @InjectMocks
    ApprovalDataUtil appUtil;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            this.cmdata = getCloseAndMergeData();
            logger.debug("cmdata :: " + cmdata.getMergeAccData());
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    private CloseAndMergeData getCloseAndMergeData() {

        Date date = new Date();
        FormDataContentDisposition contentDisposition = FormDataContentDisposition.name("testData").fileName("test.csv")
                .creationDate(date).modificationDate(date).readDate(date).size(1222).build();
        Mockito.when(config.get(Constants.FILE_LOCATION)).thenReturn("src/test/resources/");
        Mockito.when(config.getInt(Constants.ALLOWED_ENTRY)).thenReturn(1000);
        return mutil.mergeDataGeneration(contentDisposition, "murali", 1523l);

    }

    private JSONArray getJsonArrForMerge() {
        JSONArray value1 = new JSONArray();
        JSONObject obj1 = new JSONObject();
        obj1.put(Constants.USER_ID, 8000);
        obj1.put(Constants.DEPOSIT_AMOUNT, 300);
        obj1.put(Constants.WITHDRAWABLE_AMOUNT, 10);
        obj1.put(Constants.ACCOUNT_STATUS, 1);

        value1.put(obj1);

        JSONObject obj2 = new JSONObject();
        obj2.put(Constants.USER_ID, 178);
        obj2.put(Constants.DEPOSIT_AMOUNT, 300);
        obj2.put(Constants.WITHDRAWABLE_AMOUNT, 10);
        obj2.put(Constants.ACCOUNT_STATUS, 1);
        value1.put(obj2);

        JSONObject obj3 = new JSONObject();
        obj3.put(Constants.USER_ID, 522923);
        obj3.put(Constants.DEPOSIT_AMOUNT, 300);
        obj3.put(Constants.WITHDRAWABLE_AMOUNT, 10);
        obj3.put(Constants.ACCOUNT_STATUS, 1);
        value1.put(obj3);
        return value1;
    }

    @Test
    public void getDepositAndWithdrawalAmtTest() {
        Mockito.when(config.get(Constants.FS_URL)).thenReturn(
                "http://10.14.24.105:8080/fund_service/api/account/1/player_account?projection=user_id,withdrawable,deposit&_s=user_id=in=(USERIDS)");
        Mockito.when(fsClient.getCall(Mockito.anyString())).thenReturn(getJsonArrForMerge());
        JSONArray arr = fsUtil.getDepositAndWithdrawalAmt(cmdata.getFromAccountList());
        Assert.assertNotNull(arr);
        
        List<MergeAccount> list = new ArrayList<MergeAccount>();
        MergeAccount account = new MergeAccount(8000, 2, "test",300,10);
        MergeAccount account1 = new MergeAccount(178, 2, "test",300,10);
        MergeAccount account2 = new MergeAccount(522923, 2, "test",300,10);
        list.add(account);
        list.add(account1);
        list.add(account2);
        cmdata.setMergeAccData(list);
        Mockito.when(config.getInt(Constants.MERGE_BATCH_SIZE, 50)).thenReturn(100 );
        Mockito.when(config.get(Constants.ACC_MERGE_URL)).thenReturn(
                "http://10.14.24.105:8080/fund_service/api/transfer/1?responseRequirement=2");
        
        Mockito.when(fsClient.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(new JSONObject().toString());
        fsUtil.processPayload(cmdata);
        
        
        Mockito.when(fsClient.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(null);
        fsUtil.processPayload(cmdata);
        
        JSONObject obj = new JSONObject();
        obj.put(Constants.PARTIAL_FAILURE, true);
        JSONArray transArr  = new JSONArray();
        JSONObject obj1 = new JSONObject();
        obj1.put(Constants.STATUS_CODE, 0);
        transArr.put(obj1);
        obj.put(Constants.TRANSACTIONS, transArr);
        Mockito.when(fsClient.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(obj.toString());
        fsUtil.processPayload(cmdata);
        
        
        JSONObject objfail = new JSONObject();
        obj.put(Constants.PARTIAL_FAILURE, true);
        JSONArray transArr1  = new JSONArray();
        JSONObject objinter = new JSONObject();
        objinter.put(Constants.STATUS_CODE, 9);
        transArr1.put(objinter);
        objfail.put(Constants.TRANSACTIONS, transArr1);
        Mockito.when(fsClient.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(obj.toString());
        fsUtil.processPayload(cmdata);
        
        
    }

}
