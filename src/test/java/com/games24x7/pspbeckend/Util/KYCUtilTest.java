package com.games24x7.pspbeckend.Util;

import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.dao.BlockUsersMappingDao;
import com.games24x7.pspbeckend.dao.IdentityInfoDao;
import com.games24x7.pspbeckend.mq.CMmqFactory;
import com.games24x7.pspbeckend.mq.NotifierMQFactory;
import com.games24x7.pspbeckend.pojo.IdfyData;
import com.games24x7.pspbeckend.pojo.KYCData;
import com.games24x7.pspbeckend.pojo.PSPComments;
import com.games24x7.pspbeckend.webclient.AvsClient;
import com.games24x7.pspbeckend.webclient.LimitServiceClient;
import com.games24x7.pspbeckend.webclient.UserServiceClient;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class KYCUtilTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(KYCUtilTest.class);
    @Autowired
    PlayerUpdateUtil puUtil;
    @Autowired
    IdentityInfoDao identityInfoDao;

    @Autowired
    UserPreferedLanguageUtil userPreferedLanguageUtil;
    @Autowired
    PSPCommentsUtil pspCommentsUtil;
    @Autowired
    BlockUsersMappingDao blockUsersMappingDao;
    @Autowired
    LimitServiceClient limitClient;
    @Autowired
    UserServiceClient usClient;
    @Autowired
    AvsClient avsClient;
    @Autowired
    Configuration config;

    @Autowired
    AccountServiceUtil accServiceUtil;
    @Autowired
    CMmqFactory cm;
    @Autowired
    NotifierMQFactory notifier;

    @InjectMocks
    KYCUtil util;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            util.puUtil = puUtil;
            util.identityInfoDao = identityInfoDao;
            util.pspCommentsUtil = pspCommentsUtil;
            util.blockUsersMappingDao = blockUsersMappingDao;
            util.accServiceUtil = accServiceUtil;
            util.userPreferedLanguageUtil = userPreferedLanguageUtil;
            util.limitClient = limitClient;
            util.usClient = usClient;
            util.avsClient = avsClient;
            util.config = config;
            util.cm = cm;
            util.notifier = notifier;
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void testprocessKycVerified() {
        JSONObject context = new JSONObject();
        context.put("agentId", 200);
        context.put("source", "PSP");
        context.put(MTTSettlementConstant.CHANNELID, 1);
        KYCData data = new KYCData();
        data.setAgentId(200l);
        data.setDocType(2);
        data.setStatusComment("statusComment");
        data.setType(true);
        data.setUserId(200l);
        IdfyData idfy = new IdfyData();

        idfy.setAddress("address");
        idfy.setDob("dob");
        idfy.setDocumentId("documentId");
        idfy.setFullname("fullname");
        idfy.setPincode("pincode");
        idfy.setState("state");
        idfy.setYob("yob");

        data.setData(idfy);

        PSPComments psp = new PSPComments();
        psp.setCmr_interaction_id("1");
        psp.setContact_direction("In");
        psp.setDepartment("Risk");
        data.setPspdata(psp);
        util.processKycVerified(data, context);

        context.put("source", "AVS");
        util.processKycVerified(data, context);

        data.setUserId(1523l);
        data.setDocType(5);
        util.processKycVerified(data, context);

        data.setUserId(1523l);
        data.setDocType(6);
        context.put("source", "PSP");
        util.processKycVerified(data, context);

    }

    @Test
    public void testprocessKycRejected() {
        JSONObject context = new JSONObject();
        context.put("agentId", 200);
        context.put("source", "PSP");
        context.put(MTTSettlementConstant.CHANNELID, 1);
        KYCData data = new KYCData();
        data.setAgentId(200l);
        data.setDocType(2);
        data.setStatusComment("statusComment");
        data.setType(false);
        data.setUserId(200l);
        IdfyData idfy = new IdfyData();

        idfy.setAddress("address");
        idfy.setDob("dob");
        idfy.setDocumentId("documentId");
        idfy.setFullname("fullname");
        idfy.setPincode("pincode");
        idfy.setState("state");
        idfy.setYob("yob");

        data.setData(idfy);

        PSPComments psp = new PSPComments();
        psp.setCmr_interaction_id("1");
        psp.setContact_direction("In");
        psp.setDepartment("Risk");
        data.setPspdata(psp);
        util.processKycRejected(data, context);

        context.put("source", "AVS");
        util.processKycRejected(data, context);
    }
}
