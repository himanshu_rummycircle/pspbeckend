/**
 * 
 */
package com.games24x7.pspbeckend.Util;

import java.util.Date;

import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.pojo.CloseAndMergeData;
import com.sun.jersey.core.header.FormDataContentDisposition;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class MergeAndCloseUtilTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(MergeAndCloseUtilTest.class);
    @Mock
    Configuration config;
    @Mock
    FileValidationsUtil fileValidation;

    @Mock
    AccountServiceUtil accServiceUtil;
    @InjectMocks
    MergeAndCloseUtil util;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void basicValidations() {
        try {
            Mockito.doThrow(new ServiceException("AgentName is missing.", Status.BAD_REQUEST)).when(fileValidation)
                    .agentInputValidation(Mockito.anyString(), Mockito.anyLong());
            util.basicValidations(null, null, null, null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

        try {
            Mockito.doThrow(new ServiceException("", Status.BAD_REQUEST)).when(fileValidation)
                    .fileValidation(Mockito.anyObject(), Mockito.anyObject());
            util.basicValidations(null, null, null, null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

        try {
            Mockito.doThrow(new ServiceException("", Status.BAD_REQUEST)).when(fileValidation)
                    .validateFileHeader(Mockito.anyObject(), Mockito.anyInt());
            util.basicValidations(null, null, null, null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

        Mockito.doNothing().when(fileValidation).agentInputValidation(Mockito.anyString(), Mockito.anyLong());
        Mockito.doNothing().when(fileValidation).fileValidation(Mockito.anyObject(), Mockito.anyObject());
        Mockito.doNothing().when(fileValidation).validateFileHeader(Mockito.anyObject(), Mockito.anyInt());
        util.basicValidations(null, null, null, null);

    }

    @Test
    public CloseAndMergeData mergeDataGenerationTest() {
        Date date = new Date();
        FormDataContentDisposition contentDisposition = FormDataContentDisposition.name("testData").fileName("test.csv")
                .creationDate(date).modificationDate(date).readDate(date).size(1222).build();
        Mockito.when(config.get(Constants.FILE_LOCATION)).thenReturn("src/test/resources/");
        Mockito.when(config.getInt(Constants.ALLOWED_ENTRY)).thenReturn(1000);
        CloseAndMergeData data = util.mergeDataGeneration(contentDisposition, "murali", 1523l);
        Assert.assertNotNull(data);
        return data;
    }
    @Test
    public void mergeAccValidationsTest() {
        


        CloseAndMergeData data = mergeDataGenerationTest();
        util.mergeAccValidations(data);
        
        
        
        Mockito.doNothing().when(accServiceUtil).validateUserExistance(data.getToAccountList());
        Mockito.doNothing().when(accServiceUtil).validateUserExistance(data.getFromAccountList());
        Mockito.when(config.get(Constants.ACC_STATUS)).thenReturn("5,6");
        JSONArray arr = new JSONArray();
        arr.put(new JSONObject().put(Constants.USER_ID, 10l));
        Mockito.when(accServiceUtil.validateAccountStatus(Mockito.anyObject(), Mockito.anyString())).thenReturn(arr);
        try {
            util.mergeAccValidations(data);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals("These users account are already closed or blocked . [10]", e.getMessage());
        }
        

    }

}
