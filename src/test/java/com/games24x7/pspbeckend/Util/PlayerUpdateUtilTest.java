/**
 * 
 */
package com.games24x7.pspbeckend.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.pspbeckend.constants.Globals;
import com.games24x7.pspbeckend.dao.PlayerUpdatesDAO;
import com.games24x7.pspbeckend.pojo.BulkStatusChangeData;
import com.games24x7.pspbeckend.pojo.CloseAndMergeData;
import com.games24x7.pspbeckend.pojo.CloseAndRefundData;
import com.games24x7.pspbeckend.pojo.MergeAccount;
import com.games24x7.pspbeckend.pojo.Refund;
import com.games24x7.pspbeckend.pojo.UserData;

import junit.framework.Assert;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class PlayerUpdateUtilTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(PlayerUpdateUtilTest.class);

    @Autowired
    Configuration config;
    @Mock
    ThreadPoolExecutorUtil util;

    @Mock
    PlayerUpdatesDAO dao;

    @Mock
    AuditUtil auditUtil;
    @InjectMocks
    PlayerUpdateUtil putil;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            putil.config = config;
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void insertIntoPlayerUpdatesTest() {
        Mockito.doNothing().when(auditUtil).playerUpdateAuditLog(Mockito.anyList());
        Mockito.doNothing().when(dao).insertIntoPlayerUpdatesDB(Mockito.anyList());
        Mockito.when(util.getPlayerUpdateThreadpoolexecutor()).thenReturn(
                new ThreadPoolExecutor(3, 10, 2, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>()));
        putil.insertIntoPlayerUpdates(null, 1);
        try {
            putil.insertIntoPlayerUpdates(null, Globals.ACCOUNT_CLOSED_PLAYER_UPDATES);
            Assert.fail();
        } catch (Exception e) {
        }

        try {
            putil.insertIntoPlayerUpdates(null, Globals.ACCOUNT_MERGE_PLAYER_UPDATES);
            Assert.fail();
        } catch (Exception e) {
        }

        try {
            putil.insertIntoPlayerUpdates(null, Globals.CLOSE_AND_REFUND);
            Assert.fail();
        } catch (Exception e) {
        }

        List<MergeAccount> list = new ArrayList<>();
        MergeAccount a1 = new MergeAccount(1, 2, "comment", 1, 2);
        a1.setAccountStatus(6);
        list.add(a1);
        CloseAndMergeData data = Mockito.spy(new CloseAndMergeData());
        Mockito.when(data.getMergeAccData()).thenReturn(list);
        Mockito.when(data.getAgent()).thenReturn("murali");
        putil.insertIntoPlayerUpdates(data, Globals.ACCOUNT_CLOSED_PLAYER_UPDATES);
        putil.insertIntoPlayerUpdates(data, Globals.ACCOUNT_MERGE_PLAYER_UPDATES);

        Mockito.doThrow(new RuntimeException("testing ...")).when(dao).insertIntoPlayerUpdatesDB(Mockito.anyList());
        putil.insertIntoPlayerUpdates(data, Globals.ACCOUNT_MERGE_PLAYER_UPDATES);

        List<Refund> rlist = new ArrayList<>();
        Refund r = new Refund(1, "comments");
        r.setAccountStatus(6);
        rlist.add(r);
        CloseAndRefundData cld = Mockito.spy(new CloseAndRefundData());
        Mockito.when(cld.getRefundList()).thenReturn(rlist);
        putil.insertIntoPlayerUpdates(cld, Globals.CLOSE_AND_REFUND);
        
        
        UserData statusChangedata = new UserData(1, "comments");
        statusChangedata.setAccountStatus(2);
        statusChangedata.toString();
        statusChangedata.getAccountStatus();
        statusChangedata.getComments();
        statusChangedata.getUserId();

        List<UserData> list1 = new ArrayList<>();
        list1.add(statusChangedata);
        BulkStatusChangeData changeData = new BulkStatusChangeData();
        changeData.setAccountStatus(1);
        changeData.setAgent("agent");
        changeData.setAgentId(100);
        changeData.setFilename("FileName");
        changeData.setUserData(list1);
        putil.insertIntoPlayerUpdates(changeData, Globals.BULK_ACCOUNT_STATUS_CHANGE);
    }

}
