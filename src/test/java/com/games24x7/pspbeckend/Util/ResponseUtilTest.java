/**
 * 
 *//*
package com.games24x7.pspbeckend.Util;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

*//**
 * 
 *//*
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class ResponseUtilTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(ResponseUtilTest.class);

    @InjectMocks
    ResponseUtil util;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void finalResponseTest() {
        Response res = util.finalResponse(Status.OK, "test");
        Assert.assertNotNull(res);
        Assert.assertEquals(res.getStatus(), Status.OK.getStatusCode());

    }

}
*/