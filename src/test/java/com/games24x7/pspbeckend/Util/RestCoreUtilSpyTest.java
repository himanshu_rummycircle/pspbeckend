package com.games24x7.pspbeckend.Util;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import org.apache.http.client.methods.HttpUriRequest;
import org.json.JSONObject;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.pspbeckend.pojo.WebResponse;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class RestCoreUtilSpyTest extends AbstractTestNGSpringContextTests{

    @Autowired
    Configuration config;

    @Spy
    RestCoreUtil util;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            util.config = config;
            
            WebResponse webResponse = new WebResponse();
            webResponse.setStatusCode(1);
            webResponse.setPayload("success");
            when(util.executeHttpRequest(any(HttpUriRequest.class), null)).thenReturn(webResponse);
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }
    
    
    @Test(priority = 1)
    public void testCallGet() {
        try {
            WebResponse response = util.callGet("", null);
            Assert.assertNotNull(response);
        } catch (Exception ex) {
            logger.error("Exception in testCallGet", ex);
        }
    }

    @Test(priority = 2)
    public void testCallPut() {
        try {
            WebResponse response = util.callPut("", new JSONObject(), null);
            Assert.assertNotNull(response);
        } catch (Exception ex) {
            logger.error("Exception in testCallGet", ex);
        }
    }

    @Test(priority = 3)
    public void testCallPost() {
        try {
            WebResponse response = util.callPost("", new JSONObject(), null);
            Assert.assertNotNull(response);
        } catch (Exception ex) {
            logger.error("Exception in testCallGet", ex);
        }
    }
}
