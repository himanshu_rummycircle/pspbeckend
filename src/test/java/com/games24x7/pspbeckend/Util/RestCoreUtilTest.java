/**
 * 
 */
package com.games24x7.pspbeckend.Util;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import javax.ws.rs.core.Response.Status;

import org.apache.http.client.methods.HttpUriRequest;
import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.pojo.WebResponse;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class RestCoreUtilTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(RestCoreUtilTest.class);
    @Autowired
    Configuration config;

    @InjectMocks
    RestCoreUtil util;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            util.config = config;
             
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void getCallException() {
        try {
            util.getCall("");
            Assert.fail();
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.INTERNAL_SERVER_ERROR.getStatusCode());
        }
        try {
            util.getCall(null);
            Assert.fail();
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.INTERNAL_SERVER_ERROR.getStatusCode());
        }

    }

    @Test
    public void getCallSuccess() {

        String res = util.getCall(config.get("fsUrl").replace("USERIDS", "200,201"));
        Assert.assertNotNull(res);
    }

    @Test
    public void getCallnotfound() {
        String res = null;
        try {
            res = util.getCall(config.get("fsUrl").replace("USERIDS", "00,2"));
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.NOT_FOUND.getStatusCode());
        }

    }

    @Test
    public void getCallFail() {
        String res = null;
        try {
            res = util.getCall("http://test.test.com/test");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.INTERNAL_SERVER_ERROR.getStatusCode());
        }

    }

    @Test
    public void postCallTest() {
        try {
            String res = util.postCall(null, null, null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.INTERNAL_SERVER_ERROR.getStatusCode());
        }

    }

    @Test
    public void postCallTestSuccess() {
        String body = "{\"value\":{\"allInOneDBTxn\":false,\"transactions\":[{\"globalTransaction\":{\"txnType\":38},\"debitAccounts\":[{\"subaccountAmounts\":{\"deposit\":0.0,\"withdrawable\":0.0,\"non_withdrawable\":0.0},\"accountType\":\"player_account\",\"key\":{\"user_id\":522923}}],\"creditAccounts\":[{\"subaccountAmounts\":{\"deposit\":0.0,\"withdrawable\":0.0,\"non_withdrawable\":0.0},\"createAccountIfAbsent\":false,\"accountType\":\"player_account\",\"key\":{\"user_id\":100016301}}]},{\"globalTransaction\":{\"txnType\":38},\"debitAccounts\":[{\"subaccountAmounts\":{\"deposit\":1000.0,\"withdrawable\":1000.0,\"non_withdrawable\":0.0},\"accountType\":\"player_account\",\"key\":{\"user_id\":100016050}}],\"creditAccounts\":[{\"subaccountAmounts\":{\"deposit\":1000.0,\"withdrawable\":1000.0,\"non_withdrawable\":0.0},\"createAccountIfAbsent\":false,\"accountType\":\"player_account\",\"key\":{\"user_id\":522907}}]},{\"globalTransaction\":{\"txnType\":38},\"debitAccounts\":[{\"subaccountAmounts\":{\"deposit\":1999.0,\"withdrawable\":0.0,\"non_withdrawable\":0.0},\"accountType\":\"player_account\",\"key\":{\"user_id\":100016053}}],\"creditAccounts\":[{\"subaccountAmounts\":{\"deposit\":1999.0,\"withdrawable\":0.0,\"non_withdrawable\":0.0},\"createAccountIfAbsent\":false,\"accountType\":\"player_account\",\"key\":{\"user_id\":522905}}]},{\"globalTransaction\":{\"txnType\":38},\"debitAccounts\":[{\"subaccountAmounts\":{\"deposit\":0.0,\"withdrawable\":1999.0,\"non_withdrawable\":0.0},\"accountType\":\"player_account\",\"key\":{\"user_id\":100016056}}],\"creditAccounts\":[{\"subaccountAmounts\":{\"deposit\":0.0,\"withdrawable\":1999.0,\"non_withdrawable\":0.0},\"createAccountIfAbsent\":false,\"accountType\":\"player_account\",\"key\":{\"user_id\":522908}}]},{\"globalTransaction\":{\"txnType\":38},\"debitAccounts\":[{\"subaccountAmounts\":{\"deposit\":100.5,\"withdrawable\":100.5,\"non_withdrawable\":0.0},\"accountType\":\"player_account\",\"key\":{\"user_id\":100016058}}],\"creditAccounts\":[{\"subaccountAmounts\":{\"deposit\":100.5,\"withdrawable\":100.5,\"non_withdrawable\":0.0},\"createAccountIfAbsent\":false,\"accountType\":\"player_account\",\"key\":{\"user_id\":522906}}]}]},\"context\":{\"userId\":900116,\"source\":\"psp_beckend\",\"userAction\":\"bulkAccountMerging\"}}";
        String res = util.postCall(config.get("acc.merge.url"), body, "POST");
        Assert.assertNotNull(res);
    }

    @Test
    public void postCallTestException() {
        try {
            String body = "{\"value\":{\"allInOneDBTxn\":false,\"transactions\":[{\"globalTransaction\":{\"txnType\":38},\"debitAccounts\":[{\"subaccountAmounts\":{\"deposit\":0.0,\"withdrawable\":0.0,\"non_withdrawable\":0.0},\"accountType\":\"player_account\",\"key\":{\"user_id\":522923}}],\"creditAccounts\":[{\"subaccountAmounts\":{\"deposit\":0.0,\"withdrawable\":0.0,\"non_withdrawable\":0.0},\"createAccountIfAbsent\":false,\"accountType\":\"player_account\",\"key\":{\"user_id\":100016301}}]},{\"globalTransaction\":{\"txnType\":38},\"debitAccounts\":[{\"subaccountAmounts\":{\"deposit\":1000.0,\"withdrawable\":1000.0,\"non_withdrawable\":0.0},\"accountType\":\"player_account\",\"key\":{\"user_id\":100016050}}],\"creditAccounts\":[{\"subaccountAmounts\":{\"deposit\":1000.0,\"withdrawable\":1000.0,\"non_withdrawable\":0.0},\"createAccountIfAbsent\":false,\"accountType\":\"player_account\",\"key\":{\"user_id\":522907}}]},{\"globalTransaction\":{\"txnType\":38},\"debitAccounts\":[{\"subaccountAmounts\":{\"deposit\":1999.0,\"withdrawable\":0.0,\"non_withdrawable\":0.0},\"accountType\":\"player_account\",\"key\":{\"user_id\":100016053}}],\"creditAccounts\":[{\"subaccountAmounts\":{\"deposit\":1999.0,\"withdrawable\":0.0,\"non_withdrawable\":0.0},\"createAccountIfAbsent\":false,\"accountType\":\"player_account\",\"key\":{\"user_id\":522905}}]},{\"globalTransaction\":{\"txnType\":38},\"debitAccounts\":[{\"subaccountAmounts\":{\"deposit\":0.0,\"withdrawable\":1999.0,\"non_withdrawable\":0.0},\"accountType\":\"player_account\",\"key\":{\"user_id\":100016056}}],\"creditAccounts\":[{\"subaccountAmounts\":{\"deposit\":0.0,\"withdrawable\":1999.0,\"non_withdrawable\":0.0},\"createAccountIfAbsent\":false,\"accountType\":\"player_account\",\"key\":{\"user_id\":522908}}]},{\"globalTransaction\":{\"txnType\":38},\"debitAccounts\":[{\"subaccountAmounts\":{\"deposit\":100.5,\"withdrawable\":100.5,\"non_withdrawable\":0.0},\"accountType\":\"player_account\",\"key\":{\"user_id\":100016058}}],\"creditAccounts\":[{\"subaccountAmounts\":{\"deposit\":100.5,\"withdrawable\":100.5,\"non_withdrawable\":0.0},\"createAccountIfAbsent\":false,\"accountType\":\"player_account\",\"key\":{\"user_id\":522906}}]}]},\"context\":{\"userId\":900116,\"source\":\"psp_beckend\",\"userAction\":\"bulkAccountMerging\"}}";
            String res = util.postCall("http://test.test/q", body, "PUT");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.INTERNAL_SERVER_ERROR.getStatusCode());
        }

    }

    @Test
    public void postCallTest404() {
        try {
            String body = "{\"value\":{\"allInOneDBTxn\":false,\"transactions\":[{\"globalTransaction\":{\"txnType\":38},\"debitAccounts\":[{\"subaccountAmounts\":{\"deposit\":0.0,\"withdrawable\":0.0,\"non_withdrawable\":0.0},\"accountType\":\"player_account\",\"key\":{\"user_id\":522923}}],\"creditAccounts\":[{\"subaccountAmounts\":{\"deposit\":0.0,\"withdrawable\":0.0,\"non_withdrawable\":0.0},\"createAccountIfAbsent\":false,\"accountType\":\"player_account\",\"key\":{\"user_id\":100016301}}]},{\"globalTransaction\":{\"txnType\":38},\"debitAccounts\":[{\"subaccountAmounts\":{\"deposit\":1000.0,\"withdrawable\":1000.0,\"non_withdrawable\":0.0},\"accountType\":\"player_account\",\"key\":{\"user_id\":100016050}}],\"creditAccounts\":[{\"subaccountAmounts\":{\"deposit\":1000.0,\"withdrawable\":1000.0,\"non_withdrawable\":0.0},\"createAccountIfAbsent\":false,\"accountType\":\"player_account\",\"key\":{\"user_id\":522907}}]},{\"globalTransaction\":{\"txnType\":38},\"debitAccounts\":[{\"subaccountAmounts\":{\"deposit\":1999.0,\"withdrawable\":0.0,\"non_withdrawable\":0.0},\"accountType\":\"player_account\",\"key\":{\"user_id\":100016053}}],\"creditAccounts\":[{\"subaccountAmounts\":{\"deposit\":1999.0,\"withdrawable\":0.0,\"non_withdrawable\":0.0},\"createAccountIfAbsent\":false,\"accountType\":\"player_account\",\"key\":{\"user_id\":522905}}]},{\"globalTransaction\":{\"txnType\":38},\"debitAccounts\":[{\"subaccountAmounts\":{\"deposit\":0.0,\"withdrawable\":1999.0,\"non_withdrawable\":0.0},\"accountType\":\"player_account\",\"key\":{\"user_id\":100016056}}],\"creditAccounts\":[{\"subaccountAmounts\":{\"deposit\":0.0,\"withdrawable\":1999.0,\"non_withdrawable\":0.0},\"createAccountIfAbsent\":false,\"accountType\":\"player_account\",\"key\":{\"user_id\":522908}}]},{\"globalTransaction\":{\"txnType\":38},\"debitAccounts\":[{\"subaccountAmounts\":{\"deposit\":100.5,\"withdrawable\":100.5,\"non_withdrawable\":0.0},\"accountType\":\"player_account\",\"key\":{\"user_id\":100016058}}],\"creditAccounts\":[{\"subaccountAmounts\":{\"deposit\":100.5,\"withdrawable\":100.5,\"non_withdrawable\":0.0},\"createAccountIfAbsent\":false,\"accountType\":\"player_account\",\"key\":{\"user_id\":522906}}]}]},\"context\":{\"userId\":900116,\"source\":\"psp_beckend\",\"userAction\":\"bulkAccountMerging\"}}";
            String res = util.postCall(config.get("acc.merge.url") + "/test", body, "PUT");
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.INTERNAL_SERVER_ERROR.getStatusCode());
        }

    }
   

}
