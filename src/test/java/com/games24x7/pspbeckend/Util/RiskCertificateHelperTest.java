package com.games24x7.pspbeckend.Util;

import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.core.Response.Status;

import junit.framework.Assert;

import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants.RiskConstant;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;
import com.games24x7.pspbeckend.dao.RiskCertificateDAO;
import com.games24x7.pspbeckend.pojo.PlayersRiskCertificate;
import com.games24x7.pspbeckend.pojo.RiskCertificate;
import com.games24x7.pspbeckend.pojo.RiskType;
import com.games24x7.pspbeckend.pojo.UpdateRiskCertificate;
import com.games24x7.pspbeckend.webclient.UserServiceClient;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class RiskCertificateHelperTest extends AbstractTestNGSpringContextTests {

    @Autowired
    Configuration config;

    @Mock
    UserServiceClient userServiceClient;
    
    @Mock
    RiskCertificateDAO ceriticateDAO;
    
    @InjectMocks
    RiskCertificateHelper riskCertificateHelper;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            riskCertificateHelper.config = config;
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void jsonToPlayersRiskCertificatePayloadTest() {

        JSONObject jsonObject = new JSONObject();
        try {
            riskCertificateHelper.jsonToPlayersRiskCertificatePayload(jsonObject);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
        }
        jsonObject.put("userId", "ui1200");
        try {

            riskCertificateHelper.jsonToPlayersRiskCertificatePayload(jsonObject);
            Assert.fail();
        } catch (ServiceException se) {
            logger.info("SE " + se.getMessage());
            Assert.assertNotNull(se);
        }

        jsonObject.put("userId", 1100);
        PlayersRiskCertificate rsC = riskCertificateHelper.jsonToPlayersRiskCertificatePayload(jsonObject);
        Assert.assertNotNull(rsC);
        Assert.assertEquals(1100, rsC.getUserId());

    }

    @Test
    public void validateContactTypeTest() {

        PlayersRiskCertificate playerRiskCert = new PlayersRiskCertificate();
        try {
            riskCertificateHelper.validateContactType(playerRiskCert);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.MISSING_CONTACT_TYPE_ERRCODE, se.getBusinessErrorCode().intValue());
        }
        playerRiskCert.setContactType("1");
        try {
            riskCertificateHelper.validateContactType(playerRiskCert);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.INVALID_CONTACT_TYPE_ERRCODE, se.getBusinessErrorCode().intValue());
        }
        playerRiskCert.setContactType("PHONE");
        riskCertificateHelper.validateContactType(playerRiskCert);

    }
    
    @Test
    public void validateOffsetTest() {

        try {
            riskCertificateHelper.validateOffset(-1);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.INVALID_OFFSET_ERRCODE, se.getBusinessErrorCode().intValue());
        }
        riskCertificateHelper.validateOffset(1);

    }
    
    @Test
    public void validateRiskTypeTest() {
        Set<Integer> validRiskStatusSet = new HashSet<>();
       
        Assert.assertTrue(riskCertificateHelper.validateRiskType(-1, validRiskStatusSet));
        validRiskStatusSet.add(0);
        validRiskStatusSet.add(1);
        Assert.assertTrue(riskCertificateHelper.validateRiskType(-1, validRiskStatusSet));
        Assert.assertFalse(riskCertificateHelper.validateRiskType(1, validRiskStatusSet));
    }
    
    @Test
    public void validateGetRiskCertificateInputTest() {
        Set<Integer> validRiskStatusSet = new HashSet<>();
        try {
            riskCertificateHelper.validateGetRiskCertificateInput(-1,0,validRiskStatusSet);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(WebConstant.INVALID_RISK_TYPE_CODE, se.getBusinessErrorCode().intValue());
        }
        validRiskStatusSet.add(0);
        validRiskStatusSet.add(1);
        try {
            riskCertificateHelper.validateGetRiskCertificateInput(-1,0,validRiskStatusSet);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(WebConstant.INVALID_RISK_TYPE_CODE, se.getBusinessErrorCode().intValue());
        }
        riskCertificateHelper.validateGetRiskCertificateInput(1,0,validRiskStatusSet);
    }
    
    @Test
    public void validateContactDirectionTest() {
        PlayersRiskCertificate playerRiskCert = new PlayersRiskCertificate();
        try {
            riskCertificateHelper.validateContactDirection(playerRiskCert);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.MISSING_CONTACT_DIRECTION_ERRCODE, se.getBusinessErrorCode().intValue());
        }
        playerRiskCert.setContactDirection("");
        try {
            riskCertificateHelper.validateContactDirection(playerRiskCert);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.MISSING_CONTACT_DIRECTION_ERRCODE, se.getBusinessErrorCode().intValue());
        }
        playerRiskCert.setContactDirection("1234");
        try {
            riskCertificateHelper.validateContactDirection(playerRiskCert);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.INVALID_CONTACT_DIRECTION_ERRCODE, se.getBusinessErrorCode().intValue());
        }
        playerRiskCert.setContactDirection("IN");
        riskCertificateHelper.validateContactDirection(playerRiskCert);
    }
    
    @Test
    public void validateCommentTest() {
        PlayersRiskCertificate playerRiskCert = new PlayersRiskCertificate();
        try {
            riskCertificateHelper.validateComment(playerRiskCert);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.MISSING_COMMENT_ERRCODE, se.getBusinessErrorCode().intValue());
        }
        
        playerRiskCert.setComment("");
        try {
            riskCertificateHelper.validateComment(playerRiskCert);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.MISSING_COMMENT_ERRCODE, se.getBusinessErrorCode().intValue());
        }
        playerRiskCert.setComment("1234");
        riskCertificateHelper.validateComment(playerRiskCert);
    }
    
//    @Test
    /*
     * public void validateUserIdDBTest(){ long userId=0l;
     * Mockito.when(userServiceClient.getValidUserListSize(Mockito.anyString()))
     * .thenReturn(0);
     * Assert.assertFalse(riskCertificateHelper.validateUserIdDB(userId));
     * Mockito.when(userServiceClient.getValidUserListSize(Mockito.anyString()))
     * .thenReturn(1);
     * Assert.assertTrue(riskCertificateHelper.validateUserIdDB(userId)); }
     */
    
    @Test
    public void validateUserIdTest(){
        PlayersRiskCertificate playerRiskCert = new PlayersRiskCertificate();
        playerRiskCert.setUserId(-1l);
        Mockito.when(userServiceClient.getValidUserListSize(Mockito.anyString())).thenReturn(0);
        try {
            riskCertificateHelper.validateUserId(playerRiskCert);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.INVALID_USERID_ERRCODE, se.getBusinessErrorCode().intValue());
        }
        playerRiskCert.setUserId(0l);
        try {
            riskCertificateHelper.validateUserId(playerRiskCert);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.INVALID_USERID_ERRCODE, se.getBusinessErrorCode().intValue());
        }
        playerRiskCert.setUserId(200l);
        Mockito.when(userServiceClient.getValidUserListSize(Mockito.anyString())).thenReturn(1);
        riskCertificateHelper.validateUserId(playerRiskCert);
    }
    
    @Test(priority=5)
    public void validateRiskNameValueTest(){
        String certificateName=null;
        List<Long> list = new ArrayList<Long>();
        list.add(100l);
        list.add(101l);
        try {
            riskCertificateHelper.validateRiskNameValue(certificateName);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.RISK_NAME_EMPTY_ERROR_CODE, se.getBusinessErrorCode().intValue());
        }
        certificateName="";
        try {
            riskCertificateHelper.validateRiskNameValue(certificateName);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.RISK_NAME_EMPTY_ERROR_CODE, se.getBusinessErrorCode().intValue());
        }
        Mockito.when(ceriticateDAO.getRiskCertificateName(Mockito.anyString())).thenReturn(list);
        certificateName=",";
        try {
            riskCertificateHelper.validateRiskNameValue(",");
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.LIST_DISTINCT_RISKNAME_NOT_MATCHING_ERR_CODE, se.getBusinessErrorCode().intValue());
        }
        list = new ArrayList<Long>(); 
        Mockito.when(ceriticateDAO.getRiskCertificateName(Mockito.anyString())).thenReturn(list);
        certificateName="a,aa";
        try {
            riskCertificateHelper.validateRiskNameValue(certificateName);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.LIST_DISTINCT_RISKNAME_NOT_MATCHING_ERR_CODE, se.getBusinessErrorCode().intValue());
        }
        list = new ArrayList<Long>();
        list.add(100l);
        list.add(101l);
        Mockito.when(ceriticateDAO.getRiskCertificateName(Mockito.anyString())).thenReturn(list);
        riskCertificateHelper.validateRiskNameValue(certificateName);
    }
    
    @Test(priority=10)
    public void ifValidRiskStatusValueTest(){
        String riskName="";
        Mockito.when(ceriticateDAO.checkIfValidRiskStatus(Mockito.anyString())).thenReturn(false);
        Assert.assertFalse(  riskCertificateHelper.ifValidRiskStatusValue(riskName) );
        Mockito.when(ceriticateDAO.checkIfValidRiskStatus(Mockito.anyString())).thenReturn(true);
        Assert.assertTrue(  riskCertificateHelper.ifValidRiskStatusValue(riskName) );
        
        Mockito.doThrow(new NullPointerException()).when(ceriticateDAO).checkIfValidRiskStatus(anyString());
        try {
            riskCertificateHelper.ifValidRiskStatusValue(riskName); 
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.DATABASE_OPERATION_ERROR_CODE, se.getBusinessErrorCode().intValue());
        }
    }
    
    @Test
    public void validateRiskStatusTest(){
        Mockito.when(ceriticateDAO.checkIfValidRiskStatus(Mockito.anyString())).thenReturn(false);
        String riskS =null;
        try {
            riskCertificateHelper.validateRiskStatus(riskS);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.EMPTY_RISK_STATUS_VALUE_CODE, se.getBusinessErrorCode().intValue());
        }
        riskS ="";
        try {
            riskCertificateHelper.validateRiskStatus(riskS);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.EMPTY_RISK_STATUS_VALUE_CODE, se.getBusinessErrorCode().intValue());
        }
        riskS ="1";
        try {
            riskCertificateHelper.validateRiskStatus(riskS);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.INVALID_RISK_STATUS_VALUE_CODE, se.getBusinessErrorCode().intValue());
        }
        Mockito.when(ceriticateDAO.checkIfValidRiskStatus(Mockito.anyString())).thenReturn(true);
        riskCertificateHelper.validateRiskStatus(riskS);
    }
    @Test
    public void validatePlayersRiskCertificateInputTest(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("riskCertificateNames", "N1,N2");
        jsonObject.put("contactType", "EMAIL");
        jsonObject.put("contactDirection", "IN");
        jsonObject.put("interactionId", "IN");
        jsonObject.put("comment", "IN");
        jsonObject.put("riskStatus", "Not Risky");
        jsonObject.put("riskStatusComment", "Not Risky");
        jsonObject.put("userId", 1200);
        jsonObject.put("agentId", "Not Risky");
        jsonObject.put("reasonForDowngrade", "None");
        List<Long> list = new ArrayList<Long>();
        Mockito.when(ceriticateDAO.checkIfValidRiskStatus(Mockito.anyString())).thenReturn(true);
        Mockito.when(userServiceClient.getValidUserListSize(Mockito.anyString())).thenReturn(1);
        list.add(100l);
        list.add(101l);
        Mockito.when(ceriticateDAO.getRiskCertificateName(Mockito.anyString())).thenReturn(list);
        riskCertificateHelper.validatePlayersRiskCertificateInput(jsonObject);
    }
    @Test
    public void checkRiskIdValuesTest(){
        
        String riskCertIds=null;
        try {
            riskCertificateHelper.checkRiskIdValues(riskCertIds);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.RISKIDS_EMPTY_ERROR_CODE, se.getBusinessErrorCode().intValue());
        }
        riskCertIds="";
        try {
            riskCertificateHelper.checkRiskIdValues(riskCertIds);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.RISKIDS_EMPTY_ERROR_CODE, se.getBusinessErrorCode().intValue());
        }
        
        riskCertIds="-1,-2";
        try {
            riskCertificateHelper.checkRiskIdValues(riskCertIds);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.INVALID_RISK_ID_CODE, se.getBusinessErrorCode().intValue());
        }
        

        riskCertIds="1312333333333333333333333333333333333333333333333333333333333";
        try {
            riskCertificateHelper.checkRiskIdValues(riskCertIds);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.INVALID_RISK_ID_CODE, se.getBusinessErrorCode().intValue());
        }
        riskCertIds="12,11";
        Assert.assertEquals(2,riskCertificateHelper.checkRiskIdValues(riskCertIds).size());
    }
    
    @Test
    public void validateRiskIdsTest(){
        String riskCertIds="11,56";
        Mockito.when(ceriticateDAO.distinctRiskId(Mockito.anyString())).thenReturn(0);
        try {
            riskCertificateHelper.validateRiskIds(riskCertIds);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.LIST_DISTINCT_RISKID_NOT_MATCHING_ERR_CODE, se.getBusinessErrorCode().intValue());
        }
        Mockito.when(ceriticateDAO.distinctRiskId(Mockito.anyString())).thenReturn(2);
        riskCertificateHelper.validateRiskIds(riskCertIds);
    }
    
    @Test
    public void validateActivateStatusTest(){
        
        JSONObject jsonObject = new JSONObject();
        
        try {
            riskCertificateHelper.validateActivateStatus(jsonObject);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.ACTIVATE_DATA_NOTFOUND_CODE, se.getBusinessErrorCode().intValue());
        }
        jsonObject.put(RiskConstant.ACTIVATE, "R");
        try {
            riskCertificateHelper.validateActivateStatus(jsonObject);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.INVALID_ACTIVATE_VALUE_FOUND_ERR_CODE, se.getBusinessErrorCode().intValue());
        }
        jsonObject.put(RiskConstant.ACTIVATE, "true");
        riskCertificateHelper.validateActivateStatus(jsonObject);
        jsonObject.put(RiskConstant.ACTIVATE, "false");
        riskCertificateHelper.validateActivateStatus(jsonObject);
        
    }
    
    @Test
    public void validateUpdateRiskCertificateInputTest(){
        
        JSONObject jsonObject = new JSONObject();
        Mockito.when(ceriticateDAO.distinctRiskId(Mockito.anyString())).thenReturn(2);
        jsonObject.put(RiskConstant.ACTIVATE, "false");
        jsonObject.put("riskCertificateIds", "1,2");
        jsonObject.put("agentId", "1333");
        Assert.assertNotNull(riskCertificateHelper.validateUpdateRiskCertificateInput(jsonObject));
        
    }
    
    @Test
    public void jsonToUpdateRiskCertificatePayloadTest(){
        
        JSONObject jsonObject = new JSONObject();
        try {
            riskCertificateHelper.jsonToUpdateRiskCertificatePayload(jsonObject);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
        }
        jsonObject.put("activate", "ui1200");
        try {

            riskCertificateHelper.jsonToUpdateRiskCertificatePayload(jsonObject);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(WebConstant.IMPROPER_INPUT_PAYLOAD_CODE, se.getBusinessErrorCode().intValue());
        }

        jsonObject.put("riskCertificateIds", "1,2");
        jsonObject.put("activate", true);
        UpdateRiskCertificate rsC = riskCertificateHelper.jsonToUpdateRiskCertificatePayload(jsonObject);
        Assert.assertNotNull(rsC);
        Assert.assertEquals("1,2", rsC.getRiskCertificateIds());
    }
    
    @Test
    public void getRiskListInJSONArrayTest(){
        
        List<RiskCertificate> riskList=null;
        HashMap<Integer,String> riskMap =null;
        Assert.assertEquals(0,riskCertificateHelper.getRiskListInJSONArray(riskList,riskMap).length());
        riskList = new ArrayList<RiskCertificate>();
        riskMap = new HashMap<Integer,String>();
        riskMap.put(1, "One");
        
        riskList.add(new RiskCertificate(1, "Name", "1", true));
        Assert.assertEquals(1,riskCertificateHelper.getRiskListInJSONArray(riskList,riskMap).length());
        
    }
    
    
    @Test
    public void validateAgentGroupTest() {
        RiskCertificate payload = new RiskCertificate();
        payload.setAgentGroup(null);
        try {
            riskCertificateHelper.validateAgentGroup(payload);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.MISSING_AGENT_GROUP_CODE, se.getBusinessErrorCode().intValue());
        }
        
        payload.setAgentGroup("");
        try {
            riskCertificateHelper.validateAgentGroup(payload);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.MISSING_AGENT_GROUP_CODE, se.getBusinessErrorCode().intValue());
        }
        payload.setAgentGroup("1234");
        riskCertificateHelper.validateAgentGroup(payload);
    }
   
    
    @Test
    public void validateAgentTest() {
        String agentId=null;
        try {
            riskCertificateHelper.validateAgent(agentId);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.MISSING_AGENT_CODE, se.getBusinessErrorCode().intValue());
        }
        
        agentId="";
        try {
            riskCertificateHelper.validateAgent(agentId);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.MISSING_AGENT_CODE, se.getBusinessErrorCode().intValue());
        }
        agentId = "1234";
        riskCertificateHelper.validateAgent(agentId);
    }
    /*
    @Test
    public void certificateNameExistInDBTest() {
        List<Long> list = new ArrayList<Long>();
        Mockito.when(ceriticateDAO.getCountOfCertificate(Mockito.anyString())).thenReturn(0);
        String riskName=null;
        Assert.assertFalse( riskCertificateHelper.certificateNameExistInDB(riskName) );
        
        list.add(111l);
        Mockito.when(ceriticateDAO.getCountOfCertificate(Mockito.anyString())).thenReturn(1);
        Assert.assertTrue( riskCertificateHelper.certificateNameExistInDB(riskName) );
        
    }
    
    @Test(priority=10)
    public void certificateNameExistErrorTest(){
        String riskName="name";
        
        Mockito.doThrow(new NullPointerException()).when(ceriticateDAO).getCountOfCertificate(anyString());
        try {
            riskCertificateHelper.certificateNameExistInDB(riskName);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.DATABASE_OPERATION_ERROR_CODE, se.getBusinessErrorCode().intValue());
        }
    }*/
    @Test
    public void validateRiskCertificateNameTest(){
        
        RiskCertificate payload = new RiskCertificate();
        
        try {
            riskCertificateHelper.validateRiskCertificateName(payload);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.EMPTY_CERTIFICATE_NAME_CODE, se.getBusinessErrorCode().intValue());
        }
        
        payload.setRiskCertificateName("");
        try {
            riskCertificateHelper.validateRiskCertificateName(payload);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.EMPTY_CERTIFICATE_NAME_CODE, se.getBusinessErrorCode().intValue());
        }
        
        payload.setRiskCertificateName("10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
        try {
            riskCertificateHelper.validateRiskCertificateName(payload);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.MAX_CHAR_LIMIT_EXCEEDED_CODE, se.getBusinessErrorCode().intValue());
        }
        payload.setRiskCertificateName("100");
        riskCertificateHelper.validateRiskCertificateName(payload);
    }
    
    
    @Test
    public void validateRiskStatusCommentTest() {
        String riskStatusComment=null;
        try {
            riskCertificateHelper.validateRiskStatusComment(riskStatusComment);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.EMPTY_RISK_STATUS_COMMENT_VALUE_CODE, se.getBusinessErrorCode().intValue());
        }
        
        riskStatusComment="";
        try {
            riskCertificateHelper.validateRiskStatusComment(riskStatusComment);
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.EMPTY_RISK_STATUS_COMMENT_VALUE_CODE, se.getBusinessErrorCode().intValue());
        }
        riskStatusComment="1234";
        riskCertificateHelper.validateRiskStatusComment(riskStatusComment);
    }
    
    @Test
    public void validateCreateInputPayloadTest(){
        RiskCertificate riskPayload = new RiskCertificate();
        Mockito.when(ceriticateDAO.checkIfValidRiskStatus(Mockito.anyString())).thenReturn(true);
        riskPayload.setRiskCertificateName("ana");
        riskPayload.setRiskStatus("111");
        riskPayload.setAgentId("Agent1");
        riskCertificateHelper.validateCreateInputPayload(riskPayload);
    }
    
    @Test
    public void jsonToRiskCertificatePayloadTest(){
        
        JSONObject jsonObject = new JSONObject();
        try {
            riskCertificateHelper.jsonToRiskCertificatePayload(jsonObject);
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
        }
        jsonObject.put("riskStatusType", "abcd");
        try {

            riskCertificateHelper.jsonToRiskCertificatePayload(jsonObject);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(WebConstant.IMPROPER_INPUT_PAYLOAD_CODE, se.getBusinessErrorCode().intValue());
        }

        jsonObject.put("riskStatusType", "1");
        jsonObject.put("isActive", true);
        RiskCertificate rsC = riskCertificateHelper.jsonToRiskCertificatePayload(jsonObject);
        rsC.setId(22l);
        rsC.setActive(true);
        Assert.assertNotNull(rsC);
        Assert.assertEquals(1, rsC.getRiskStatusType());
        
        rsC = new RiskCertificate("1", "2");
        
        PlayersRiskCertificate prs = new PlayersRiskCertificate();
        prs.setRiskStatusId("2");
        prs.getRiskStatusId();
        
        RiskType riskType= new RiskType();
        riskType.setRiskId(11);
        riskType.setRiskName("R1");
        
        riskType.getRiskId();
        riskType.getRiskName();
        
        riskType.toString();
        
        UpdateRiskCertificate urc = new  UpdateRiskCertificate();
        urc.getActivate();
    }
    
}
