package com.games24x7.pspbeckend.Util;

import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.webclient.AuditServiceClient;
import com.games24x7.pspbeckend.webclient.UserServiceClient;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class RiskUtilTest extends AbstractTestNGSpringContextTests {

    private static final Logger logger = LoggerFactory.getLogger(RiskUtilTest.class);
    
    @Autowired
    Configuration config;
    
    @InjectMocks
    RiskUtil riskUtil;
    
    @Mock
    AuditServiceClient auditclient;
    @Mock
    UserServiceClient usClient;
    
    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            riskUtil.config = config;
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }
    
    @Test
    public void getRiskStatusTest(){
        Mockito.when(usClient.getStringResGetCall(Mockito.anyString())).thenReturn(null);
        long userId=0;
        try {
            riskUtil.getRiskStatus(userId) ;
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.INTERNAL_SERVER_ERROR.getStatusCode());
        }
        
        
        JSONObject json = new JSONObject();
        json.put("risk_status", 100);
        Mockito.when(usClient.getStringResGetCall(Mockito.anyString())).thenReturn(json.toString());
        Assert.assertEquals("100",  riskUtil.getRiskStatus(userId) );
    }
    
    @Test
    public void getRiskLogTest(){
        
        JSONArray jsonArray = new JSONArray();
        Mockito.when(auditclient.getCall(Mockito.anyString())).thenReturn(jsonArray);
         
        JSONObject updateTimeJSON = new JSONObject();
        updateTimeJSON.put("$numberLong","2313231220");
        
        JSONObject jsonObj  = new JSONObject();
        jsonObj.put(Constants.UPDATE_TIME, updateTimeJSON);
        jsonObj.put(Constants.RISK_CERTIFICATE_NAMES, "n1,n001");
        jsonObj.put(Constants.CONTACT_TYPE, "n1,n001");
        jsonObj.put(Constants.CONTACT_DIRECTION, "n1,n001");
        jsonObj.put(Constants.INTERACTION_ID, "n1,n001");
        jsonObj.put(Constants.COMMENT, "n1,n001");
        jsonObj.put(Constants.RISK_STATUS, "n1,n001");
        jsonObj.put(Constants.RISK_STATUS_COMMENT, "n1,n001");
        jsonObj.put(Constants.REASON_FOR_DOWNGRADE, "n1,n001");
        jsonObj.put(Constants.AGENT_ID, "n1,n001");
        jsonArray.put(jsonObj);
        Mockito.when(auditclient.getCall(Mockito.anyString())).thenReturn(jsonArray);
        Assert.assertEquals(1,  riskUtil.getRiskLog(100l).length() );
    }
}
