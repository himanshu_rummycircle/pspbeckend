package com.games24x7.pspbeckend.Util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.eds.events.CurrentWithdrawable;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.dao.SatelliteSettlementDAO;
import com.games24x7.pspbeckend.mq.SecondaryMQFactory;
import com.games24x7.pspbeckend.pojo.OfflinePrize;
import com.games24x7.pspbeckend.pojo.OfflineSettlement;
import com.games24x7.pspbeckend.pojo.OfflineSettlementMoneyInfo;
import com.games24x7.pspbeckend.pojo.OfflineSettlementPayload;
import com.games24x7.pspbeckend.pojo.WebResponse;
import com.games24x7.pspbeckend.webclient.FundServiceClient;
import com.games24x7.pspbeckend.webclient.UserServiceClient;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class SatelliteSettlementHelperTest extends AbstractTestNGSpringContextTests {

    private static final Logger logger = LoggerFactory.getLogger(RestCoreUtilTest.class);
    @Autowired
    Configuration config;

    @Mock
    SecondaryMQFactory secondaryMQ;

    @Mock
    AuditUtil audit;
    @Mock
    FundServiceClient fundServiceClient;
    @Mock
    UserServiceClient userServiceClient;
    @InjectMocks
    SatelliteSettlementHelper satelliteHelper;

    @Mock
    SatelliteSettlementDAO settlementDAO;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            satelliteHelper.config = config;
            Mockito.when(fundServiceClient.getAccount(Mockito.anyString(), Mockito.any())).thenReturn(new JSONObject());
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void formatJsonTest() {

        CurrentWithdrawable currentWithdrawable = new CurrentWithdrawable();
        Assert.assertNotNull(satelliteHelper.formatJson(currentWithdrawable));
    }

    @Test
    public void getPlayerAccountJsonTest() {
        Mockito.when(fundServiceClient.getAccount(Mockito.anyString(), Mockito.any())).thenReturn(new JSONObject());
        long userId = 220;
        Assert.assertNotNull(satelliteHelper.getPlayerAccountJson(userId));

        Mockito.when(fundServiceClient.getAccount(Mockito.anyString(), Mockito.any())).thenReturn(new JSONObject());
        JSONObject jsonCurrentWD = new JSONObject();
        jsonCurrentWD.put("deposit", 100D);
        jsonCurrentWD.put("withdrawable", 0D);
        jsonCurrentWD.put("non_withdrawable", 0D);
        Mockito.when(fundServiceClient.getAccount(Mockito.anyString(), Mockito.any())).thenReturn(jsonCurrentWD);
        userId = 220;
        CurrentWithdrawable currentEEvent = (CurrentWithdrawable) satelliteHelper.prepareEDSMsg(userId);
        Assert.assertNotNull(currentEEvent);
        Assert.assertEquals(100D, currentEEvent.getDepositAmt());

        try {
            Mockito.doThrow(new ServiceException("AgentName is missing.", Status.BAD_REQUEST)).when(fundServiceClient)
                    .getAccount(Mockito.anyString(), Mockito.any());
            satelliteHelper.getPlayerAccountJson(userId);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }
        // Mockito.when(fundServiceClient.getAccount(Mockito.anyString(),
        // Mockito.any())).thenReturn(jsonCurrentWD);

        Mockito.doThrow(new ServiceException("AgentName is missing.", Status.BAD_REQUEST)).when(fundServiceClient)
                .getAccount(Mockito.anyString(), Mockito.any());
        try {
            satelliteHelper.getPlayerAccountJson(userId);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }
    }

    @Test
    public void beanVerification() {
        OfflineSettlement settlement = new OfflineSettlement();
        settlement.setPrizeAmount(21D);
        settlement.setRank(1);
        settlement.setTournamentId(2222l);
        settlement.setUserId(110);
        settlement.setTableId(0);
        settlement.setTdsLimit(122D);
        settlement.setTdsValue(10000D);

        settlement.getPrizeAmount();
        settlement.getUserId();
        settlement.getRank();
        settlement.getTournamentId();
        settlement.getTableId();
        settlement.getTdsLimit();
        settlement.getTdsValue();

        settlement = new OfflineSettlement(12312l, 2, 100D, 222l, 0, 2999D, 22D);

        OfflineSettlementPayload payload = new OfflineSettlementPayload(1111l, "abcd", "warr", "comments",
                new ArrayList<OfflinePrize>());
    }

    @Test
    public void preparedAuditDataTest() {
        OfflinePrize prize = new OfflinePrize();
        prize.setUserId(99l);
        String auditString = satelliteHelper.preparedAuditData(prize, 121l);
        Assert.assertNotNull(auditString);
    }

    @Test
    public void publishEdsMessageAnddAuditLogsTest() {
        Mockito.doNothing().when(secondaryMQ).publishToSecondaryMQ(Mockito.any());
        Mockito.doNothing().when(audit).publishToAudit(Mockito.any(), Mockito.any());

        OfflineSettlementPayload payload = new OfflineSettlementPayload();
        payload.setTournamentId(99l);
        OfflinePrize prize = new OfflinePrize();
        prize.setPrizeAmount(100D);
        prize.setRank(1);
        prize.setUserId(101l);
        ArrayList<OfflinePrize> prizelist = new ArrayList<OfflinePrize>();
        prizelist.add(prize);
        payload.setPrizelist(prizelist);

        satelliteHelper.publishEdsMessageAndAuditLogs(payload);
        Mockito.doThrow(new ServiceException("AgentName is missing.", Status.BAD_REQUEST)).when(audit)
                .publishToAudit(Mockito.any(), Mockito.any());
        satelliteHelper.publishEdsMessageAndAuditLogs(payload);
    }

    @Test
    public void jsonToOfflineSettlementPayloadTest() {
        JSONObject jsonPayload = new JSONObject();
        try {
            satelliteHelper.jsonToOfflineSettlementPayload(jsonPayload);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

        jsonPayload.put("tournamentId", "boolean");
        try {
            satelliteHelper.jsonToOfflineSettlementPayload(jsonPayload);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.IMPROPER_INPUT_PAYLOAD_CODE);
        }
    }

    @Test
    public void validateTournamentStateTest() {
        int tStatus = 6;
        try {
            satelliteHelper.validateTournamentState(tStatus);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(),
                    MTTSettlementConstant.TOURNAMENT_COMPLETED_STATE_ERROR_CODE);

        }
        tStatus = 1;
        try {
            satelliteHelper.validateTournamentState(tStatus);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(),
                    MTTSettlementConstant.TOURNAMENT_NOTIN_CANCELLED_STATE_ERROR_CODE);
        }
        tStatus = 5;
        satelliteHelper.validateTournamentState(tStatus);
    }

    @Test
    public void checkForAllowedTListZKTest() {
        long tournamentId = 1100;
        // Mockito.when(config.get(Mockito.anyString())).thenReturn("1200");

        try {
            satelliteHelper.checkForAllowedTListZK(tournamentId);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(),
                    MTTSettlementConstant.OFFLINE_SETTLEMENT_NOT_PERMITTED_CODE);

        }

        tournamentId = 1200;
        satelliteHelper.checkForAllowedTListZK(tournamentId);

    }

    @Test
    public void validateTournamentPropertiesTest() {
        OfflineSettlementPayload payload = new OfflineSettlementPayload();
        payload.setTournamentId(null);
        try {
            satelliteHelper.validateTournamentProperties(payload, null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(),
                    MTTSettlementConstant.TOURNAMENT_NUMBER_ERROR_CODE);

        }

        payload.setTournamentId(-1l);
        try {
            satelliteHelper.validateTournamentProperties(payload, null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(),
                    MTTSettlementConstant.TOURNAMENT_NUMBER_ERROR_CODE);

        }

        payload.setTournamentId(0l);
        try {
            satelliteHelper.validateTournamentProperties(payload, null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(),
                    MTTSettlementConstant.TOURNAMENT_NUMBER_ERROR_CODE);

        }

        payload.setTournamentId(1111111111111111111l);
        try {
            satelliteHelper.validateTournamentProperties(payload, null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(),
                    MTTSettlementConstant.TOURNAMENT_NUMBER_ERROR_CODE);

        }
        payload.setTournamentId(1111l);
        try {
            satelliteHelper.validateTournamentProperties(payload, null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(),
                    MTTSettlementConstant.NO_DATA_FOR_THIS_TOURNAMENTID_CODE);

        }
        Map<String, Object> torunamentInfoMap = new HashMap<String, Object>();
        torunamentInfoMap.put(MTTSettlementConstant.TOURNAMENT_TYPE, 2);
        try {
            satelliteHelper.validateTournamentProperties(payload, torunamentInfoMap);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(),
                    MTTSettlementConstant.TOURNAMENT_INVALID_TYPE_ERROR_CODE);

        }
        torunamentInfoMap.put(MTTSettlementConstant.TOURNAMENT_TYPE, 1);
        torunamentInfoMap.put(MTTSettlementConstant.TOURNAMENT_STATUS, 5);
        try {
            satelliteHelper.validateTournamentProperties(payload, torunamentInfoMap);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(),
                    MTTSettlementConstant.TOURNAMENT_INVALID_TYPE_ERROR_CODE);

        }
    }

    @Test
    public void validateAgentGroupTest() {
        OfflineSettlementPayload payload = new OfflineSettlementPayload();
        payload.setAgentGroup(null);
        try {
            satelliteHelper.validateAgentGroup(payload);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.MISSING_AGENT_GROUP_CODE);

        }
        payload.setAgentGroup("");
        try {
            satelliteHelper.validateAgentGroup(payload);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.MISSING_AGENT_GROUP_CODE);

        }
        payload.setAgentGroup("G1");
        satelliteHelper.validateAgentGroup(payload);
    }

    @Test
    public void validateAgentTest() {
        OfflineSettlementPayload payload = new OfflineSettlementPayload();
        payload.setAgentId(null);
        try {
            satelliteHelper.validateAgent(payload);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.MISSING_AGENT_CODE);

        }
        payload.setAgentId("");
        try {
            satelliteHelper.validateAgent(payload);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.MISSING_AGENT_CODE);

        }
        payload.setAgentId("G1");
        satelliteHelper.validateAgent(payload);
    }

    @Test
    public void validateCommentTest() {
        OfflineSettlementPayload payload = new OfflineSettlementPayload();
        payload.setComment(null);
        try {
            satelliteHelper.validateComment(payload);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.MISSING_COMMENT_CODE);

        }
        payload.setComment("");
        try {
            satelliteHelper.validateComment(payload);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.MISSING_COMMENT_CODE);

        }
        payload.setComment("Gs     asd   ");
        try {
            satelliteHelper.validateComment(payload);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.MISSING_COMMENT_CODE);

        }
        payload.setComment("Gs sdasdasdsad   asd   ");
        satelliteHelper.validateComment(payload);
    }

    @Test
    public void getValidUserListSizeTest() {
        String allUserIds = "";
        WebResponse webR = new WebResponse();
        webR.setPayload("[]");
        Mockito.when(userServiceClient.getCall(Mockito.anyString(), Mockito.any())).thenReturn(webR);

        try {
            satelliteHelper.getValidUserListSize(allUserIds);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.MISSING_COMMENT_CODE);

        }
    }

    @Test
    public void validateUserIdTest() {
        long userId = -1;
        try {
            satelliteHelper.validateUserId(userId);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.INVALID_USER_ID_CODE);

        }
        userId = 0;
        try {
            satelliteHelper.validateUserId(userId);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.INVALID_USER_ID_CODE);

        }
        userId = 21;
        satelliteHelper.validateUserId(userId);
    }

    @Test
    public void validateRanksTest() {
        int rank = -1;
        int totalPrize = 2;
        try {
            satelliteHelper.validateRanks(rank, totalPrize);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.INVALID_RANK_VALUE_CODE);

        }
        rank = 0;
        try {
            satelliteHelper.validateRanks(rank, totalPrize);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.INVALID_RANK_VALUE_CODE);

        }
        rank = 11;
        try {
            satelliteHelper.validateRanks(rank, totalPrize);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.INVALID_RANK_VALUE_CODE);

        }
        rank = 1;
        satelliteHelper.validateRanks(rank, totalPrize);

    }

    @Test
    public void validatePrizeAmountTest() {
        double prizeAmount = -10D;
        try {
            satelliteHelper.validatePrizeAmount(prizeAmount);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(),
                    MTTSettlementConstant.INVALID_PRIZE_AMOUNT_VALUE_CODE);

        }
        prizeAmount = 0;
        satelliteHelper.validatePrizeAmount(prizeAmount);
    }

    @Test
    public void validatingAmountAsPerTheRankTest() {
        double currentAmount = 100;
        double previousAmount = 120;
        try {
            satelliteHelper.validatingAmountAsPerTheRank(currentAmount, previousAmount);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(),
                    MTTSettlementConstant.HIGHER_POSITION_LOWER_AMOUNT_CODE);

        }
        previousAmount = 100;
        satelliteHelper.validatingAmountAsPerTheRank(currentAmount, previousAmount);
    }

    // @Test
    @Test(priority = 3, enabled = false)
    public void validateAndReturnQualifierContributionTest() {
        long tid = 100;
        Mockito.when(settlementDAO.getTournamentQualifierContribution(Mockito.anyLong())).thenReturn(0D);
        try {
            satelliteHelper.validateAndReturnQualifierContribution(tid);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.ZERO_PRIZE_ALLOCATED_CODE);

        }

        Mockito.when(settlementDAO.getTournamentQualifierContribution(Mockito.anyLong())).thenReturn(-11D);
        try {
            satelliteHelper.validateAndReturnQualifierContribution(tid);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.ZERO_PRIZE_ALLOCATED_CODE);

        }
        Mockito.when(settlementDAO.getTournamentQualifierContribution(Mockito.anyLong())).thenReturn(11D);
        Assert.assertEquals(11D, satelliteHelper.validateAndReturnQualifierContribution(tid));

        Mockito.doThrow(new ServiceException("AgentName is missing.", Status.BAD_REQUEST)).when(settlementDAO)
                .getTournamentQualifierContribution(Mockito.anyLong());

        try {
            satelliteHelper.validateAndReturnQualifierContribution(tid);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.ZERO_PRIZE_ALLOCATED_CODE);
        }

    }

    @Test
    public void checkForThreeDecimalTest() {
        List<OfflinePrize> prizelist = new ArrayList<OfflinePrize>();

        try {
            satelliteHelper.checkForThreeDecimal(prizelist);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.AMOUNT_VALUE_NOT_FOUND_CODE);

        }

        prizelist.add(new OfflinePrize(100, 1, 100.3456));
        satelliteHelper.checkForThreeDecimal(prizelist);
    }

    @Test
    public void validatePrizeListPropertiesTest() {
        List<OfflinePrize> prizelist = new ArrayList<OfflinePrize>();
        prizelist.add(new OfflinePrize(100, 1, 100.3456));
        prizelist.add(new OfflinePrize(100, 2, 100.3456));
        OfflineSettlementPayload payload = new OfflineSettlementPayload();
        payload.setTournamentId(99l);
        payload.setPrizelist(prizelist);
        double totalPrizePool = 100D;
        try {
            satelliteHelper.validatePrizeListProperties(payload, totalPrizePool);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(),
                    MTTSettlementConstant.UNIQUE_USERID_COUNT_FAILED_CODE);

        }
        prizelist = new ArrayList<OfflinePrize>();
        prizelist.add(new OfflinePrize(100, 1, 100D));
        prizelist.add(new OfflinePrize(101, 2, 100D));
        payload.setPrizelist(prizelist);
        WebResponse webR = new WebResponse();
        webR.setPayload("[{},{},{}]");
        Mockito.when(userServiceClient.getCall(Mockito.anyString(), Mockito.any())).thenReturn(webR);
        try {
            satelliteHelper.validatePrizeListProperties(payload, totalPrizePool);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.INVALID_USERIDS_FOUND_CODE);

        }
        webR = new WebResponse();
        webR.setPayload("[{},{}]");
        Mockito.when(userServiceClient.getCall(Mockito.anyString(), Mockito.any())).thenReturn(webR);
        totalPrizePool = 0;
        try {
            satelliteHelper.validatePrizeListProperties(payload, totalPrizePool);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.GUARANTEED_AMOUNT_ZERO_CODE);

        }

        totalPrizePool = 100;
        try {
            satelliteHelper.validatePrizeListProperties(payload, totalPrizePool);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.PRIZE_AMOUNT_NOT_EQUAL_CODE);

        }

        /*
         * Mockito.when(settlementDAO.getTournamentQualifierContribution(Mockito
         * .anyLong())).thenReturn(110D); totalPrizePool=200;
         * OfflineSettlementMoneyInfo monyeInfo =
         * satelliteHelper.validatePrizeListProperties(payload,totalPrizePool);
         * Assert.assertNotNull(monyeInfo);
         * Assert.assertEquals(monyeInfo.getTotalQualifierContribution(), 110D);
         * try { satelliteHelper.validateInputData(null); } catch
         * (ServiceException e) {
         * Assert.assertEquals(e.getStatus().getStatusCode(),
         * Status.BAD_REQUEST.getStatusCode());
         * Assert.assertEquals(e.getBusinessErrorCode().intValue(),
         * MTTSettlementConstant.EMPTY_OR_NULL_PAYLOAD); } payload = new
         * OfflineSettlementPayload(); long tournamentId = 1200;
         * payload.setTournamentId(tournamentId);
         * 
         * Map<String, Object> torunamentInfoMap = new HashMap<String,
         * Object>();
         * torunamentInfoMap.put(MTTSettlementConstant.TOURNAMENT_TYPE, 1);
         * torunamentInfoMap.put(MTTSettlementConstant.TOURNAMENT_STATUS, 12);
         * torunamentInfoMap.put(MTTSettlementConstant.PRIZE_POOL, new
         * BigDecimal(200) );
         * Mockito.when(settlementDAO.getTournamentInfo(Mockito.anyLong())).
         * thenReturn(torunamentInfoMap);
         * 
         * prizelist = new ArrayList<OfflinePrize>(); prizelist.add(new
         * OfflinePrize(100, 1, 100D)); prizelist.add(new OfflinePrize(101, 2,
         * 100D)); payload.setPrizelist(prizelist);
         * 
         * webR = new WebResponse(); webR.setPayload("[{},{}]");
         * Mockito.when(userServiceClient.getUserStatus(Mockito.anyString(),
         * Mockito.any())).thenReturn(webR); payload.setAgentGroup("aa");
         * payload.setAgentId("isisd"); payload.setComment("commemememmeme");
         * Mockito.when(settlementDAO.getTournamentQualifierContribution(Mockito
         * .anyLong())).thenReturn(110D);
         * satelliteHelper.validateInputData(payload);
         */
    }

    @Test
    public void validatePrizeListUsersAmountAndReturnMoneyInfoTest() {
        Mockito.when(settlementDAO.getTournamentQualifierContribution(Mockito.anyLong())).thenReturn(110D);
        double totalPrizePool = 200;

        List<OfflinePrize> prizelist = new ArrayList<OfflinePrize>();

        OfflineSettlementPayload payload = new OfflineSettlementPayload();
        payload.setTournamentId(99l);
        payload.setPrizelist(prizelist);
        WebResponse webR = new WebResponse();
        webR.setPayload("[{},{}]");
        Mockito.when(userServiceClient.getCall(Mockito.anyString(), Mockito.any())).thenReturn(webR);
        prizelist = null;
        payload.setPrizelist(prizelist);
        try {
            satelliteHelper.validatePrizeListUsersAmountAndReturnMoneyInfo(payload, totalPrizePool);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.MISSING_PRIZE_LIST_CODE);
        }
        prizelist = new ArrayList<OfflinePrize>();
        payload.setPrizelist(prizelist);
        try {
            satelliteHelper.validatePrizeListUsersAmountAndReturnMoneyInfo(payload, totalPrizePool);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.MISSING_PRIZE_LIST_CODE);
        }
        prizelist.add(new OfflinePrize(100, 1, 100D));
        prizelist.add(new OfflinePrize(101, 2, 100D));
        payload.setPrizelist(prizelist);
        OfflineSettlementMoneyInfo monyeInfo = satelliteHelper.validatePrizeListUsersAmountAndReturnMoneyInfo(payload,
                totalPrizePool);
        Assert.assertNotNull(monyeInfo);
        Assert.assertEquals(monyeInfo.getTotalQualifierContribution(), 110D);

        Mockito.when(settlementDAO.getTournamentQualifierContribution(Mockito.anyLong())).thenReturn(110D);
        totalPrizePool = 200;
        monyeInfo = satelliteHelper.validatePrizeListProperties(payload, totalPrizePool);
        Assert.assertNotNull(monyeInfo);
        Assert.assertEquals(monyeInfo.getTotalQualifierContribution(), 110D);
        try {
            satelliteHelper.validateInputData(null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.EMPTY_OR_NULL_PAYLOAD);
        }
        payload = new OfflineSettlementPayload();
        long tournamentId = 1200;
        payload.setTournamentId(tournamentId);

        Map<String, Object> torunamentInfoMap = new HashMap<String, Object>();
        torunamentInfoMap.put(MTTSettlementConstant.TOURNAMENT_TYPE, 1);
        torunamentInfoMap.put(MTTSettlementConstant.TOURNAMENT_STATUS, 5);
        torunamentInfoMap.put(MTTSettlementConstant.PRIZE_POOL, new BigDecimal(200));
        Mockito.when(settlementDAO.getTournamentInfo(Mockito.anyLong())).thenReturn(torunamentInfoMap);

        prizelist = new ArrayList<OfflinePrize>();
        prizelist.add(new OfflinePrize(100, 1, 100D));
        prizelist.add(new OfflinePrize(101, 2, 100D));
        payload.setPrizelist(prizelist);

        webR = new WebResponse();
        webR.setPayload("[{},{}]");
        Mockito.when(userServiceClient.getCall(Mockito.anyString(), Mockito.any())).thenReturn(webR);
        payload.setAgentGroup("aa");
        payload.setAgentId("isisd");
        payload.setComment("commemememmeme");
        Mockito.when(settlementDAO.getTournamentQualifierContribution(Mockito.anyLong())).thenReturn(110D);
        satelliteHelper.validateInputData(payload);

        //
        Mockito.when(settlementDAO.getTournamentQualifierContribution(Mockito.anyLong())).thenReturn(110D);
        totalPrizePool = 200;
        monyeInfo = satelliteHelper.validatePrizeListProperties(payload, totalPrizePool);
        Assert.assertNotNull(monyeInfo);
        Assert.assertEquals(monyeInfo.getTotalQualifierContribution(), 110D);
        try {
            satelliteHelper.validateInputData(null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.EMPTY_OR_NULL_PAYLOAD);
        }
        payload = new OfflineSettlementPayload();
        tournamentId = 1200;
        payload.setTournamentId(tournamentId);

        torunamentInfoMap = new HashMap<String, Object>();
        torunamentInfoMap.put(MTTSettlementConstant.TOURNAMENT_TYPE, 1);
        torunamentInfoMap.put(MTTSettlementConstant.TOURNAMENT_STATUS, 5);
        torunamentInfoMap.put(MTTSettlementConstant.PRIZE_POOL, new BigDecimal(200));
        Mockito.when(settlementDAO.getTournamentInfo(Mockito.anyLong())).thenReturn(torunamentInfoMap);

        prizelist = new ArrayList<OfflinePrize>();
        prizelist.add(new OfflinePrize(100, 1, 100D));
        prizelist.add(new OfflinePrize(101, 2, 100D));
        payload.setPrizelist(prizelist);

        webR = new WebResponse();
        webR.setPayload("[{},{}]");
        Mockito.when(userServiceClient.getCall(Mockito.anyString(), Mockito.any())).thenReturn(webR);
        payload.setAgentGroup("aa");
        payload.setAgentId("isisd");
        payload.setComment("commemememmeme");
        Mockito.when(settlementDAO.getTournamentQualifierContribution(Mockito.anyLong())).thenReturn(110D);
        satelliteHelper.validateInputData(payload);

    }

    // @Test
    public void validateInputDataTest() {
        try {
            satelliteHelper.validateInputData(null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.EMPTY_OR_NULL_PAYLOAD);
        }
        OfflineSettlementPayload payload = new OfflineSettlementPayload();
        long tournamentId = 1200;
        payload.setTournamentId(tournamentId);

        Map<String, Object> torunamentInfoMap = new HashMap<String, Object>();
        torunamentInfoMap.put(MTTSettlementConstant.TOURNAMENT_TYPE, 1);
        torunamentInfoMap.put(MTTSettlementConstant.TOURNAMENT_STATUS, 5);
        torunamentInfoMap.put(MTTSettlementConstant.PRIZE_POOL, new BigDecimal(200));
        Mockito.when(settlementDAO.getTournamentInfo(Mockito.anyLong())).thenReturn(torunamentInfoMap);

        List<OfflinePrize> prizelist = new ArrayList<OfflinePrize>();
        prizelist.add(new OfflinePrize(100, 1, 100D));
        prizelist.add(new OfflinePrize(101, 2, 100D));
        payload.setPrizelist(prizelist);

        WebResponse webR = new WebResponse();
        webR.setPayload("[{},{}]");
        Mockito.when(userServiceClient.getCall(Mockito.anyString(), Mockito.any())).thenReturn(webR);
        payload.setAgentGroup("aa");
        payload.setAgentId("isisd");
        payload.setComment("commemememmeme");
        Mockito.when(settlementDAO.getTournamentQualifierContribution(Mockito.anyLong())).thenReturn(110D);
        satelliteHelper.validateInputData(payload);
    }
}
