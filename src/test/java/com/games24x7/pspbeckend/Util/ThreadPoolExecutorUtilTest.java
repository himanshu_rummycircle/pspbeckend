/**
 * 
 */
package com.games24x7.pspbeckend.Util;

import java.util.concurrent.ExecutorService;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class ThreadPoolExecutorUtilTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(ThreadPoolExecutorUtilTest.class);
    @Autowired
    Configuration config;
    @InjectMocks
    ThreadPoolExecutorUtil util;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void getApprovalThreadpoolexecutorTest() {
        ExecutorService ser = util.getApprovalThreadpoolexecutor();
        logger.debug("down :: " + ser);
    }

    @Test
    public void getPlayerUpdateThreadpoolexecutorTest() {
        ExecutorService ser = util.getPlayerUpdateThreadpoolexecutor();
        logger.debug("down :: " + ser);
    }

}
