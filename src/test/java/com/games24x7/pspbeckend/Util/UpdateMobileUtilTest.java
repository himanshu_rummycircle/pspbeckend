package com.games24x7.pspbeckend.Util;

import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.pojo.WebResponse;
import com.games24x7.pspbeckend.webclient.SessionServiceClient;
import com.games24x7.pspbeckend.webclient.UserServiceClient;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class UpdateMobileUtilTest extends AbstractTestNGSpringContextTests {

    @InjectMocks
    UpdateMobileUtil updateMobileUtilMockInjected;

    @Mock
    UserServiceClient userServiceClientMock;

    @Mock
    SessionServiceClient sessionServiceClientMock;

    @Mock
    Configuration configMock;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);

        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void testUpdateMobile() {
        WebResponse webResponse = new WebResponse();
        webResponse.setStatusCode(200);
        Mockito.when(userServiceClientMock.post(Mockito.anyString(), Mockito.any(), Mockito.any())).thenReturn(null)
                .thenReturn(webResponse);
        JSONObject payload = new JSONObject();
        try {
            updateMobileUtilMockInjected.updateMobile(321857, payload);
        } catch (ServiceException se) {
            Assert.assertEquals(se.getErrorCode(), 500);
            Assert.assertEquals(se.getMessage(), "ERROR while updating mobile number");
        }

        WebResponse response = updateMobileUtilMockInjected.updateMobile(321857, payload);
        Assert.assertEquals(response.getStatusCode(), 200);
    }

    @Test
    public void testDeleteAllSessionsOfUser() {
        Mockito.when(sessionServiceClientMock.postCall(Mockito.anyString(), Mockito.any(), Mockito.any()))
                .thenReturn(new WebResponse());
        updateMobileUtilMockInjected.deleteAllSessionsOfUser(321857);
    }

}
