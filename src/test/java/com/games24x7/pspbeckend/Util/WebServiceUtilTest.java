package com.games24x7.pspbeckend.Util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class WebServiceUtilTest  extends AbstractTestNGSpringContextTests {

private static final Logger logger = LoggerFactory.getLogger(WebServiceUtilTest.class);
    
    
    
    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }
    
    @Test
    public void isNullOrEmptyTest(){
        
       Assert.assertTrue(WebServiceUtil.isNullOrEmpty(null));
       Assert.assertTrue(WebServiceUtil.isNullOrEmpty(""));
       Assert.assertTrue(WebServiceUtil.isNullOrEmpty("null"));
       Assert.assertFalse(WebServiceUtil.isNullOrEmpty("notnull"));
    }
}
