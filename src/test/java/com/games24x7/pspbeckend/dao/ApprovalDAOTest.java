/**
 * 
 */
package com.games24x7.pspbeckend.dao;

import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.pspbeckend.constants.Globals;
import com.games24x7.pspbeckend.pojo.ApprovalDetailsDummy;
import com.games24x7.pspbeckend.pojo.RequestedDetails;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class ApprovalDAOTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(ApprovalDAOTest.class);

    @InjectMocks
    ApprovalDAO dao;
    @Autowired
    SqlSessionTemplate sqlSessionMasterBatch;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            dao.sqlSessionMasterBatch = sqlSessionMasterBatch;
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void generateApprovalIdTest() {
        List<ApprovalDetailsDummy> list = new ArrayList<>();
        ApprovalDetailsDummy dummy = new ApprovalDetailsDummy();
        dummy.setActionId(Globals.MERGE_ACCOUNTS);
        dummy.setRequestor("test");
        dummy.setPlayerId(1l);
        dummy.setRequestorComment("Account Merge");
        String data = "from userId :" + 1 + ",to userId:" + 2 + ",trnsfrdamt:" + 3 + ", transfrWithdral: " + 4;
        dummy.setRequestedData(data);
        dummy.setLoginId("2");
        RequestedDetails details = new RequestedDetails();
        details.setAmount(3D);
        details.setLoginId("3");

        dummy.setRequestedDetails(details);
        list.add(dummy);
        dao.generateApprovalId(list);
    }

    @Test
    public void generateDataShowLogAndAccMergingLogTest() {
        List<ApprovalDetailsDummy> list = new ArrayList<>();
        ApprovalDetailsDummy dummy = new ApprovalDetailsDummy();
        dummy.setActionId(Globals.MERGE_ACCOUNTS);
        dummy.setRequestor("test");
        dummy.setPlayerId(1l);
        dummy.setRequestorComment("Account Merge");
        String data = "from userId :" + 1 + ",to userId:" + 2 + ",trnsfrdamt:" + 3 + ", transfrWithdral: " + 4;
        dummy.setRequestedData(data);
        dummy.setLoginId("2");
        RequestedDetails details = new RequestedDetails();
        details.setAmount(3D);
        details.setLoginId("3");

        dummy.setRequestedDetails(details);
        list.add(dummy);
        dao.generateDataShowLogAndAccMergingLog(list);
    }
}
