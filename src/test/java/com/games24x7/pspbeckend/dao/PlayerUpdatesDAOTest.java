/**
 * 
 */
package com.games24x7.pspbeckend.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.pspbeckend.pojo.PlayerUpdates;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class PlayerUpdatesDAOTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(PlayerUpdatesDAOTest.class);
    @Autowired
    SqlSessionTemplate sqlSessionMaster;
    @InjectMocks
    PlayerUpdatesDAO dao;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            dao.sqlSessionMaster = sqlSessionMaster;
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void insertIntoPlayerUpdatesDBTest() {
        List<PlayerUpdates> list = new ArrayList<>();
        PlayerUpdates p = new PlayerUpdates(1l, 2l, 1, "previousData", "comment",
                new Timestamp(System.currentTimeMillis()), "additionalInfo", 1l);
        list.add(p);
        dao.insertIntoPlayerUpdatesDB(list);
    }
}
