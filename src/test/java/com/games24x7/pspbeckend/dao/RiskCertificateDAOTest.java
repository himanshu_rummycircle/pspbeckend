package com.games24x7.pspbeckend.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.core.Response.Status;

import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants.RiskConstant;
import com.games24x7.pspbeckend.pojo.RiskCertificate;
import com.games24x7.pspbeckend.pojo.RiskType;
import com.games24x7.pspbeckend.pojo.UpdateRiskCertificate;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class RiskCertificateDAOTest extends AbstractTestNGSpringContextTests {

    private static final Logger logger = LoggerFactory.getLogger(ApprovalDAO.class);

    @InjectMocks
    RiskCertificateDAO riskCertificateDAO;
    @Autowired
    SqlSessionTemplate sqlSessionMaster;

    @Autowired
    SqlSessionTemplate sqlSessionMasterBatch;

    @Autowired
    Configuration config;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            riskCertificateDAO.config = config;
            riskCertificateDAO.sqlSessionMasterBatch = sqlSessionMasterBatch;
            riskCertificateDAO.sqlSessionMaster = sqlSessionMaster;
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void checkIfValidRiskStatusTest() {

        Assert.assertFalse(riskCertificateDAO.checkIfValidRiskStatus("lol"));

    }

    @Test
    public void createRiskCertificateTest() {

        try {
            riskCertificateDAO.createRiskCertificate(new RiskCertificate()); 
        } catch (Exception e) {
            // TODO: handle exception
        }
        

    }
    
    @Test
    public void updateRiskCertificateInDBTest(){
        UpdateRiskCertificate riskPayload = new UpdateRiskCertificate();
        StringBuilder strB=  new StringBuilder();
        for(int i =0 ; i < 1205 ; i++){
            if(strB.length()>0){
                strB.append(",roja"+i);
            } else{
                strB.append("roja"+i);
            }
        }
        riskPayload.setRiskCertificateIds(strB.toString());
        //Mockito.doNothing().when(riskCertificateDAO).updateRiskCertificate(Mockito.any(),Mockito.anyBoolean());
       /* try {
            riskCertificateDAO.updateRiskCertificateInDB(riskPayload); 
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.BAD_REQUEST, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.DATABASE_OPERATION_ERROR_CODE, se.getBusinessErrorCode().intValue());
        }*/
        
        strB=  new StringBuilder();
        for(int i =0 ; i < 1205 ; i++){
            if(strB.length()>0){
                strB.append(",100"+i);
            } else{
                strB.append("100"+i);
            }
        }
        riskPayload.setRiskCertificateIds(strB.toString());
        riskCertificateDAO.updateRiskCertificateInDB(riskPayload); 
    }
    


    @Test
    public void getRiskCertificateTest(){

        riskCertificateDAO.getRiskCertificate(0, 0);
        riskCertificateDAO.getRiskCertificate(2, 0);
    }
    
    @Test
    public void distinctRiskIdTest(){

        riskCertificateDAO.distinctRiskId("'abcd'");
    }
    @Test
    public void updateRiskCertificateTest(){
        List<Long> riskIdList = new ArrayList<Long>();
        riskIdList.add(100l);
        riskIdList.add(101l);
        
        riskCertificateDAO.updateRiskCertificate(riskIdList,true);
        riskCertificateDAO.updateRiskCertificate(riskIdList,false);
    }
    
    @Test
    public void getRiskCertificateNameTest(){
        riskCertificateDAO.getRiskCertificateName("'ask'");
    }
}

