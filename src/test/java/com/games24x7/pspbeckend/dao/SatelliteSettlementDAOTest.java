package com.games24x7.pspbeckend.dao;

import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.pspbeckend.pojo.OfflinePrize;
import com.games24x7.pspbeckend.pojo.OfflineSettlement;
import com.games24x7.pspbeckend.pojo.OfflineSettlementPayload;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class SatelliteSettlementDAOTest extends AbstractTestNGSpringContextTests {

    private static final Logger logger = LoggerFactory.getLogger(ApprovalDAO.class);

    @InjectMocks
    SatelliteSettlementDAO satelliteSettlementDAO;
    @Autowired
    SqlSessionTemplate sqlSessionMaster;

    @Autowired
    SqlSessionTemplate sqlSessionMasterBatch;

    @Autowired
    Configuration config;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            satelliteSettlementDAO.config = config;
            satelliteSettlementDAO.sqlSessionMasterBatch = sqlSessionMasterBatch;
            satelliteSettlementDAO.sqlSessionMaster = sqlSessionMaster;
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }
    
    @Test
    public void getTournamentInfoTest(){
     
        satelliteSettlementDAO.getTournamentInfo(100l);
    }
    
   // @Test
    public void getTournamentQualifierContributionTest(){
     
      //  satelliteSettlementDAO.getTournamentQualifierContribution(100l);
    }
    
    @Test
    public void insertTempTableIfNotThereTest(){
     
        satelliteSettlementDAO.insertTempTableIfNotThere();
    }
    
    @Test
    public void insertPlayersSettlementDetailsTempAndInPlayAccountTest(){
        satelliteSettlementDAO.insertTempTableIfNotThere();
        List<OfflineSettlement> tempPrizelist = new ArrayList<OfflineSettlement>();
        OfflineSettlement offLine = new OfflineSettlement();
        tempPrizelist.add(offLine);
        satelliteSettlementDAO.insertPlayersSettlementDetailsTempAndInPlayAccount(tempPrizelist);
    }
    
    @Test
    public void insertDataInTempTableTest(){
        satelliteSettlementDAO.insertTempTableIfNotThere();
        OfflineSettlementPayload payload = new OfflineSettlementPayload();
        payload.setTournamentId(10001l);
        List<OfflinePrize> prizeList = new ArrayList<OfflinePrize>();
        OfflinePrize offlinePrize = new OfflinePrize(110,1,100.0D);
        prizeList.add(offlinePrize);
        payload.setPrizelist(prizeList);
        satelliteSettlementDAO.insertDataInTempTable(payload);
    }
    
    @Test
    public void processOfflineSettlementTest(){
        
        satelliteSettlementDAO.insertTempTableIfNotThere();
        OfflineSettlementPayload payload = new OfflineSettlementPayload();
        payload.setTournamentId(10001l);
        List<OfflinePrize> prizeList = new ArrayList<OfflinePrize>();
        OfflinePrize offlinePrize = new OfflinePrize(110,1,100.0D);
        prizeList.add(offlinePrize);
        payload.setPrizelist(prizeList);
        
        satelliteSettlementDAO.processOfflineSettlement(payload, 10000);
    }
}
