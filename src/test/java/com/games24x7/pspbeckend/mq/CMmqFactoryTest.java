/**
 * 
 */
package com.games24x7.pspbeckend.mq;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.pspbeckend.pojo.ApprovalData;
import com.games24x7.pspbeckend.pojo.CloseAndMergeData;
import com.games24x7.pspbeckend.pojo.MergeAccount;

import junit.framework.Assert;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class CMmqFactoryTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(CMmqFactoryTest.class);
    @InjectMocks
    CMmqFactory f;
    @Autowired
    Configuration config;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            f.config = config;
            f.init();
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void publishToCMTest() {
        f.publishToCM("test");

    }

    @Test
    public void getCmMergingMQMessageTest() {

        MergeAccount a1 = new MergeAccount(1, 2, "comment");
        MergeAccount a2 = new MergeAccount(3, 4, "comment");
        ArrayList<MergeAccount> list = new ArrayList<MergeAccount>();
        list.add(a1);
        list.add(a2);
        CloseAndMergeData testClass = Mockito.spy(new CloseAndMergeData());
        Mockito.when(testClass.getFileName()).thenReturn("TestSpy.csv");
        Mockito.when(testClass.getMergeAccData()).thenReturn(list);
        Mockito.when(testClass.getSucessfulMergeAcc()).thenReturn(list);
        Mockito.when(testClass.getFailedMergeAcc()).thenReturn(list);
        Mockito.when(testClass.getAgent()).thenReturn("Test");
        Mockito.when(testClass.getUserMap()).thenReturn(new HashMap<>());
        String res = f.getCmMergingMQMessage(testClass);
        Assert.assertNotNull(res);

        Mockito.when(testClass.getFileName()).thenReturn("TestSpy.csv");
        Mockito.when(testClass.getMergeAccData()).thenReturn(list);
        Mockito.when(testClass.getSucessfulMergeAcc()).thenReturn(null);
        Mockito.when(testClass.getFailedMergeAcc()).thenReturn(list);
        Mockito.when(testClass.getAgent()).thenReturn("Test");
        Mockito.when(testClass.getUserMap()).thenReturn(new HashMap<>());
        try {
            res = f.getCmMergingMQMessage(testClass);
        } catch (Exception e) {
        }

        Mockito.when(testClass.getFileName()).thenReturn("TestSpy.csv");
        Mockito.when(testClass.getMergeAccData()).thenReturn(list);
        Mockito.when(testClass.getSucessfulMergeAcc()).thenReturn(new ArrayList<>());
        Mockito.when(testClass.getFailedMergeAcc()).thenReturn(list);
        Mockito.when(testClass.getAgent()).thenReturn("Test");
        Mockito.when(testClass.getUserMap()).thenReturn(new HashMap<>());
        res = f.getCmMergingMQMessage(testClass);
        Assert.assertNotNull(res);

        Mockito.when(testClass.getFileName()).thenReturn("TestSpy.csv");
        Mockito.when(testClass.getMergeAccData()).thenReturn(list);
        Mockito.when(testClass.getSucessfulMergeAcc()).thenReturn(list);
        Mockito.when(testClass.getFailedMergeAcc()).thenReturn(new ArrayList<>());
        Mockito.when(testClass.getAgent()).thenReturn("Test");
        Mockito.when(testClass.getUserMap()).thenReturn(new HashMap<>());
        res = f.getCmMergingMQMessage(testClass);
        Assert.assertNotNull(res);

    }

    @Test
    public void getCmApprovalMQMessageTest() {

        MergeAccount a1 = new MergeAccount(1, 2, "comment");
        MergeAccount a2 = new MergeAccount(3, 4, "comment");
        ArrayList<MergeAccount> list = new ArrayList<MergeAccount>();
        list.add(a1);
        list.add(a2);

        ApprovalData b1 = new ApprovalData(1, 2, 3, 4);
        ApprovalData b2 = new ApprovalData(1, 2, 3, 4);
        List<ApprovalData> list1 = new ArrayList<>();
        list1.add(b1);
        list1.add(b2);
        CloseAndMergeData testClass = Mockito.spy(new CloseAndMergeData());
        Mockito.when(testClass.getFileName()).thenReturn("TestSpy.csv");
        Mockito.when(testClass.getMergeAccData()).thenReturn(list);
        Mockito.when(testClass.getSucessfulMergeAcc()).thenReturn(list);
        Mockito.when(testClass.getFailedMergeAcc()).thenReturn(list);
        Mockito.when(testClass.getAgent()).thenReturn("Test");
        Mockito.when(testClass.getUserMap()).thenReturn(new HashMap<>());
        Mockito.when(testClass.getApprovalData()).thenReturn(list1);
        String res = f.getCmApprovalMQMessage(testClass);
        logger.debug("res " + res);
        Assert.assertNotNull(res);

        Mockito.when(testClass.getFileName()).thenReturn("TestSpy.csv");
        Mockito.when(testClass.getMergeAccData()).thenReturn(list);
        Mockito.when(testClass.getSucessfulMergeAcc()).thenReturn(list);
        Mockito.when(testClass.getFailedMergeAcc()).thenReturn(list);
        Mockito.when(testClass.getAgent()).thenReturn("Test");
        Mockito.when(testClass.getUserMap()).thenReturn(new HashMap<>());
        Mockito.when(testClass.getApprovalData()).thenReturn(new ArrayList<>());
        res = f.getCmApprovalMQMessage(testClass);
        logger.debug("res " + res);
        Assert.assertNotNull(res);

        Mockito.when(testClass.getFileName()).thenReturn("TestSpy.csv");
        Mockito.when(testClass.getMergeAccData()).thenReturn(list);
        Mockito.when(testClass.getSucessfulMergeAcc()).thenReturn(list);
        Mockito.when(testClass.getFailedMergeAcc()).thenReturn(list);
        Mockito.when(testClass.getAgent()).thenReturn("Test");
        Mockito.when(testClass.getUserMap()).thenReturn(new HashMap<>());
        Mockito.when(testClass.getApprovalData()).thenReturn(null);
        try {
            res = f.getCmApprovalMQMessage(testClass);
        } catch (Exception e) {
        }

    }

    @Test
    public void getFailedMessageTest() {

        String res = f.getFailedMessage(new RuntimeException("Tisting...."), "TestSpy.csv", "agent");
        logger.debug("res " + res);
        Assert.assertNotNull(res);
    }

    @Test
    public void getAccStatusChangeMessageTest() {
        List l = new ArrayList<>();
        l.add(100);
        l.add(101);

        String res = f.getAccStatusChangeMessage(l, l, "fileName", 100, "agentName");
        logger.debug("res " + res);
        Assert.assertNotNull(res);

        res = f.getAccStatusChangeMessage(new ArrayList<>(), l, "fileName", 100, "agentName");
        logger.debug("res " + res);
        Assert.assertNotNull(res);

        res = f.getAccStatusChangeMessage(l, new ArrayList<>(), "fileName", 100, "agentName");
        logger.debug("res " + res);
        Assert.assertNotNull(res);
    }

}
