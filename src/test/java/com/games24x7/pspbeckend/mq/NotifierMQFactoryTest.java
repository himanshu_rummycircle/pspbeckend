package com.games24x7.pspbeckend.mq;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class NotifierMQFactoryTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(NotifierMQFactoryTest.class);
    @Autowired
    Configuration config;
    @InjectMocks
    NotifierMQFactory mq;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            mq.config = config;
            mq.init();
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void testpublishToNotifier() {
        mq.publishToNotifier("test");
    }

    @Test
    public void testgetNotifierMQMessage() {
        String res = mq.getNotifierMQMessage("hi", 200l);
        Assert.assertNotNull(res);
    }
}
