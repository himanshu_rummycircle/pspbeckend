package com.games24x7.pspbeckend.mq;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.eds.events.CurrentWithdrawable;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class SecondaryMQFactoryTest  extends AbstractTestNGSpringContextTests {

    @InjectMocks
    SecondaryMQFactory secondaryMQ;
    @Autowired
    Configuration config;
    
    @BeforeTest
    public void testSetup() {
        
        try {
            logger.debug("setup start");
            secondaryMQ.config=config;
            secondaryMQ.init();
        } catch (Exception e) {
            logger.error("Exception in setup ", e);
        }
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }
    
    @Test
    public void publishToSecondaryMQTest() {
        secondaryMQ.publishToSecondaryMQ( new CurrentWithdrawable());

    }
    
     
}
