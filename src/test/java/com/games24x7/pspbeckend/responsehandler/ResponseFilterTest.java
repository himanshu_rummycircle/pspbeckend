package com.games24x7.pspbeckend.responsehandler;

import static org.mockito.Mockito.RETURNS_MOCKS;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;

import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.sun.jersey.core.header.InBoundHeaders;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerResponse;
import com.sun.jersey.spi.container.ContainerResponseWriter;
import com.sun.jersey.spi.container.WebApplication;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class ResponseFilterTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(ResponseFilterTest.class);
    @InjectMocks
    ResponseFilter filter;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void toResponseTest() {
        WebApplication webApp = Mockito.mock(WebApplication.class, RETURNS_MOCKS);
        ContainerRequest request = new ContainerRequest(webApp, "POST", URI.create("http://www.example.com/"),
                URI.create("http://www.example.com/"), new InBoundHeaders(), new ByteArrayInputStream(new byte[0]));
        ContainerResponseWriter responseWriter = new ContainerResponseWriter() {

            @Override
            public OutputStream writeStatusAndHeaders(long contentLength, ContainerResponse response)
                    throws IOException {
                return null;
            }

            @Override
            public void finish() throws IOException {
            }
        };
        ContainerResponse res = new ContainerResponse(webApp, request, responseWriter);
        ContainerResponse result = filter.filter(request, res);
        logger.debug("Res :::::::::" + request);
        Assert.assertNotNull(result);
    }

}
