/**
 * 
 */
package com.games24x7.pspbeckend.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.AccountServiceUtil;
import com.games24x7.pspbeckend.Util.PlayerUpdateUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Globals;
import com.games24x7.pspbeckend.pojo.AccountStatus;
import com.games24x7.pspbeckend.pojo.BulkStatusChangeData;
import com.games24x7.pspbeckend.pojo.UserData;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class AccountServiceTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(AccountServiceTest.class);

    @Mock
    AccountServiceUtil util;

    @Mock
    PlayerUpdateUtil puUtil;
    @InjectMocks
    AccountService aUtil;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void processAccountClosureTest() {
        Mockito.doNothing().when(util).getherUserData(Mockito.anyObject(), Mockito.anyInt());
        Mockito.doNothing().when(util).closeAccount(Mockito.anyObject(), Mockito.anyInt());
        Mockito.doNothing().when(puUtil).insertIntoPlayerUpdates(Mockito.anyObject(), Mockito.anyInt());
        aUtil.processAccountClosure(Mockito.anyObject(), Mockito.anyInt());
        aUtil.processAccountClosure(null, Globals.CLOSE_AND_MERGE);
    }

    @Test
    public void changeBulkAccountStatusTest() {
        try {
            UserData data = new UserData(1, "comments");
            data.setAccountStatus(2);
            data.toString();
            data.getAccountStatus();
            data.getComments();
            data.getUserId();

            List<UserData> list = new ArrayList<>();
            list.add(data);
            BulkStatusChangeData changeData = new BulkStatusChangeData();
            changeData.setAccountStatus(5);
            changeData.setAgent("agent");
            changeData.setAgentId(100);
            changeData.setFilename("FileName");
            changeData.setUserData(list);

            Mockito.doNothing().when(util).getherUserData(Mockito.anyObject(), Mockito.anyInt());

            Mockito.when(util.updateAccountStatus(Mockito.anyListOf(Long.class), Mockito.anyInt(), Mockito.anyLong()))
                    .thenReturn(new AccountStatus());
            Mockito.doNothing().when(util).sendAccountStatusCmMessage(Mockito.anyListOf(Long.class),
                    Mockito.anyListOf(Long.class), Mockito.anyString(), Mockito.anyLong(), Mockito.anyString());
            Mockito.doNothing().when(puUtil).insertIntoPlayerUpdates(Mockito.anyObject(), Mockito.anyInt());

            aUtil.changeBulkAccountStatus(changeData, Globals.BULK_ACCOUNT_STATUS_CHANGE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void blockUserTest() {
        Mockito.doNothing().when(util).processBlockUser(Mockito.anyObject(), Mockito.anyObject());
        try {

            JSONObject obj = new JSONObject();
            JSONObject value = new JSONObject();
            value.put("userId", 200);
            JSONObject context = new JSONObject();
            context.put("agentId", 200);
            context.put("source", "UMS");

            obj.put(Constants.VALUE, value);
            obj.put(Constants.CONTEXT, context);

            aUtil.blockUser(value, context);
        } catch (Exception e) {
            logger.error("", e);
        }

        try {

            JSONObject obj = new JSONObject();
            JSONObject value = new JSONObject();
            value.put("status", 5);
            value.put("comment", "test");
            value.put("blockheaderid", 9);
            value.put("userId", 200);
            value.put("header", "header");
            value.put("messagetouser", "messagetouser");
            JSONObject context = new JSONObject();
            context.put("agentId", 200);
            context.put("source", "PSP");

            obj.put(Constants.VALUE, value);
            obj.put(Constants.CONTEXT, context);
            aUtil.blockUser(value, context);
        } catch (Exception e) {
            logger.error("", e);
        }

        try {

            JSONObject obj = new JSONObject();
            JSONObject value = new JSONObject();
            value.put("status", 5);
            value.put("comment", "test");
            value.put("blockheaderid", 9);
            value.put("userId", 200);
            value.put("header", "header");
            value.put("messagetouser", "messagetouser");
            JSONObject context = new JSONObject();

            obj.put(Constants.VALUE, value);
            obj.put(Constants.CONTEXT, context);
            aUtil.blockUser(value, context);

            try {
                value.remove("userId");
                aUtil.blockUser(value, context);
            } catch (ServiceException e) {
                Assert.assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_USER_ID_ERR_CODE);
            }

            try {
                value.put("userId", 200);
                value.remove("status");
                aUtil.blockUser(value, context);
            } catch (ServiceException e) {
                Assert.assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_STATUS_ERR_CODE);
            }

            try {
                value.put("status", 5);
                value.remove("comment");
                aUtil.blockUser(value, context);
            } catch (ServiceException e) {
                Assert.assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_COMMENT_ERR_CODE);
            }

            try {
                value.put("comment", "test");
                value.remove("blockheaderid");
                aUtil.blockUser(value, context);
            } catch (ServiceException e) {
                Assert.assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_BLOCK_HEADER_ID_ERR_CODE);
            }
            try {
                value.put("blockheaderid", 9);
                value.remove("header");
                aUtil.blockUser(value, context);
            } catch (ServiceException e) {
                Assert.assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_HEADER_ERR_CODE);
            }
            try {
                value.put("header", "header");
                value.remove("messagetouser");
                aUtil.blockUser(value, context);
            } catch (ServiceException e) {
                Assert.assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_MESSAGE_TO_USER_ERR_CODE);
            }

        } catch (Exception e) {
            logger.error("", e);
        }
    }
}
