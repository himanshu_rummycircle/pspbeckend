package com.games24x7.pspbeckend.service;

import static org.testng.Assert.assertEquals;

import java.sql.Timestamp;

import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.dao.AlertDao;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class AlertServiceTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(AlertServiceTest.class);

    @Autowired
    AlertDao dao;

    @InjectMocks
    AlertService service;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            service.dao = dao;
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void testcreateAlert() {

        JSONObject obj = new JSONObject();
        obj.put("user_id", 200);
        obj.put("event_type", 4);
        obj.put("status", 1);
        obj.put("owner_id", 200);
        obj.put("reference", 0);
        obj.put("created_by", 200);
        obj.put("withdraw_id", 0);
        obj.put("creation_date", new Timestamp(System.currentTimeMillis()));

        long alertId = service.createAlert(obj);
        assertEquals(alertId > 0, true);

        try {
            obj.remove("user_id");
            service.createAlert(obj);
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_USER_ID_ERR_CODE);
        }

        try {
            obj.put("user_id", 200);
            obj.remove("event_type");
            service.createAlert(obj);
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_EVENT_TYPE_ERR_CODE);
        }

        try {
            obj.put("event_type", 4);
            obj.remove("status");
            service.createAlert(obj);
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_STATUS_ERR_CODE);
        }

        try {
            obj.put("status", 1);
            obj.remove("owner_id");
            service.createAlert(obj);
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_OWNER_ID_ERR_CODE);
        }

        try {
            obj.put("owner_id", 200);
            obj.remove("creation_date");
            service.createAlert(obj);
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_CREATION_DATE_ERR_CODE);
        }

        try {
            obj.put("creation_date", new Timestamp(System.currentTimeMillis()));
            obj.remove("created_by");
            service.createAlert(obj);
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_CREATED_BY_ERR_CODE);
        }

        try {
            obj.put("created_by", 200);
            obj.remove("withdraw_id");
            service.createAlert(obj);
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_WITHDRAW_ID_ERR_CODE);
        }
        try {
            obj.put("withdraw_id", 0);
            obj.remove("reference");
            service.createAlert(obj);
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), Constants.INVALID_REFERENCE_ERR_CODE);
        }
    }
}
