/**
 * 
 */
package com.games24x7.pspbeckend.service;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.pspbeckend.Util.ApprovalDataUtil;
import com.games24x7.pspbeckend.Util.ThreadPoolExecutorUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.mq.CMmqFactory;
import com.games24x7.pspbeckend.pojo.CloseAndMergeData;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class ApprovalServiceTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(ApprovalServiceTest.class);
    @Mock
    ApprovalDataUtil approval;

    @Mock
    ThreadPoolExecutorUtil util;

    @Mock
    CMmqFactory mq;
    @InjectMocks
    ApprovalService autil;

    @Autowired
    Configuration config;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void processApprovalTest() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(Constants.SUCCESS, new JSONArray());
        jsonObject.put(Constants.TYPE, Constants.APPROVAL);
        jsonObject.put(Constants.FILE_NAME, "test.csv");
        jsonObject.put(Constants.AGENT_ID, 1523);
        jsonObject.put(Constants.AGENT_NAME, "murali");
        jsonObject.put(Constants.FILE_SIZE, 2);

        JSONObject obj = new JSONObject();
        obj.put(Constants.EXCEPTION, new Throwable());
        obj.put(Constants.FILE_NAME, "test.csv");
        obj.put(Constants.TYPE, Constants.ERROR);
        obj.put(Constants.AGENT_NAME, "murali");

        Mockito.doNothing().when(approval).generateApproval(Mockito.anyObject());
        Mockito.when(mq.getCmApprovalMQMessage(Mockito.anyObject())).thenReturn(jsonObject.toString());
        Mockito.doNothing().when(mq).publishToCM(Mockito.anyString());
        Mockito.when(mq.getFailedMessage(Mockito.anyObject(), Mockito.anyString(), Mockito.anyString()))
                .thenReturn(obj.toString());
        Mockito.when(util.getApprovalThreadpoolexecutor()).thenReturn(
                new ThreadPoolExecutor(3, 10, 2, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>()));

        try {
            autil.processApproval(null);
        } catch (Exception e) {
        }

        CloseAndMergeData testClass = Mockito.spy(new CloseAndMergeData());
        Mockito.when(testClass.getFileName()).thenReturn("TestSpy.csv");

        autil.processApproval(testClass);
        
        
        CloseAndMergeData data = Mockito.spy(new CloseAndMergeData());
        Mockito.when(data.getFileName()).thenReturn("TestSpy1.csv");
        Mockito.when(data.getAgent()).thenReturn("murli");
        Mockito.doThrow(new RuntimeException("Testing...")).when(approval).generateApproval(data);
        Mockito.when(util.getApprovalThreadpoolexecutor()).thenReturn(
                new ThreadPoolExecutor(3, 10, 2, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>()));
        autil.processApproval(data);

        autil.generateApprovalAndMergeData(Mockito.anyObject());

    }
}
