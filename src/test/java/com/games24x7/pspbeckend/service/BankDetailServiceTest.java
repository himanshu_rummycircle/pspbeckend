package com.games24x7.pspbeckend.service;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.audit.entities.EventData;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.AuditUtil;
import com.games24x7.pspbeckend.Util.BankDetailsUtil;
import com.games24x7.pspbeckend.constants.Constants;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class BankDetailServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(BankDetailServiceTest.class);

	@Mock
	BankDetailsUtil bankDetailsUtil;

	@InjectMocks
	BankDetailsService bankDetailsService;

	@Mock
	AuditUtil auditUtil;
	
	@BeforeTest
	public void testSetup() {
		try {
			MockitoAnnotations.initMocks(this);
			Mockito.when(auditUtil.getAuditMessageForUpdateBank(Mockito.anyLong(), Mockito.any())).thenReturn(new EventData());
		} catch (Exception e) {
			logger.error("Exception in testSetup", e);
		}
	}

	@Test
	public void testgetUserBankDetails() {
		long userId = -1;
		try {
			bankDetailsService.getUserBankDetails(userId);
			Assert.fail();
		} catch (ServiceException se) {
			Assert.assertEquals(se.getStatus(), Status.BAD_REQUEST);
			Assert.assertEquals(se.getBusinessErrorCode().intValue(), Constants.INVALID_USER_ID_ERR_CODE);
		} catch (Exception e) {
			Assert.fail();
		}
		userId = 0;
		try {
			bankDetailsService.getUserBankDetails(userId);
			Assert.fail();
		} catch (ServiceException se) {
			Assert.assertEquals(se.getStatus(), Status.BAD_REQUEST);
			Assert.assertEquals(se.getBusinessErrorCode().intValue(), Constants.INVALID_USER_ID_ERR_CODE);
		} catch (Exception e) {
			Assert.fail();
		}

		userId = 123l;
		try {
			bankDetailsService.getUserBankDetails(userId);
			Assert.assertTrue(true);
		} catch (ServiceException se) {
			Assert.fail();
		} catch (Exception e) {
			Assert.fail();
		}
	}
	
	@Test
	public void testupdateUserBankDetails() {
		JSONObject payload=null;
		long userId = -1;
		try {
			bankDetailsService.updateUserBankDetails(userId,payload);
			Assert.fail();
		} catch (ServiceException se) {
			Assert.assertEquals(se.getStatus(), Status.BAD_REQUEST);
			Assert.assertEquals(se.getBusinessErrorCode().intValue(), Constants.INVALID_USER_ID_ERR_CODE);
		} catch (Exception e) {
			Assert.fail();
		}
		userId = 0;
		try {
			bankDetailsService.updateUserBankDetails(userId,payload);
			Assert.fail();
		} catch (ServiceException se) {
			Assert.assertEquals(se.getStatus(), Status.BAD_REQUEST);
			Assert.assertEquals(se.getBusinessErrorCode().intValue(), Constants.INVALID_USER_ID_ERR_CODE);
		} catch (Exception e) {
			Assert.fail();
		}

		userId = 123l;
		payload=new JSONObject();
		try {
			bankDetailsService.updateUserBankDetails(userId,payload);
			Assert.fail();
		} catch (ServiceException se) {
			Assert.assertEquals(se.getStatus(), Status.BAD_REQUEST);
			Assert.assertEquals(se.getBusinessErrorCode().intValue(), Constants.VALUE_MISSING_FROM_PAYLOAD);
		} catch (Exception e) {
			Assert.fail();
		}
		JSONObject value=new JSONObject();
		payload.put("value", value);
		try {
			bankDetailsService.updateUserBankDetails(userId,payload);
			Assert.fail();
		} catch (ServiceException se) {
			Assert.assertEquals(se.getStatus(), Status.BAD_REQUEST);
			Assert.assertEquals(se.getBusinessErrorCode().intValue(), Constants.MISSING_IFSC_ACCOUNT_NUMBER_ERR_CODE);
		} catch (Exception e) {
			Assert.fail();
		}
		value.put(Constants.IFSC_CODE, "123");
		payload.put("value", value);
		try {
			bankDetailsService.updateUserBankDetails(userId,payload);
			Assert.fail();
		} catch (ServiceException se) {
			Assert.assertEquals(se.getStatus(), Status.BAD_REQUEST);
			Assert.assertEquals(se.getBusinessErrorCode().intValue(), Constants.MISSING_IFSC_ACCOUNT_NUMBER_ERR_CODE);
		} catch (Exception e) {
			Assert.fail();
		}
		
		value=new JSONObject();
		value.put(Constants.ACCOUNT_NUMBER, "123");
		payload.put("value", value);
		try {
			bankDetailsService.updateUserBankDetails(userId,payload);
			Assert.fail();
		} catch (ServiceException se) {
			Assert.assertEquals(se.getStatus(), Status.BAD_REQUEST);
			Assert.assertEquals(se.getBusinessErrorCode().intValue(), Constants.MISSING_IFSC_ACCOUNT_NUMBER_ERR_CODE);
		} catch (Exception e) {
			Assert.fail();
		}
		
		value=new JSONObject();
		value.put(Constants.AGENT_NAME, "abc");
		payload.put("value", value);
		try {
			bankDetailsService.updateUserBankDetails(userId,payload);
			Assert.fail();
		} catch (ServiceException se) {
			Assert.assertEquals(se.getStatus(), Status.BAD_REQUEST);
			Assert.assertEquals(se.getBusinessErrorCode().intValue(), Constants.MISSING_IFSC_ACCOUNT_NUMBER_ERR_CODE);
		} catch (Exception e) {
			Assert.fail();
		}
		
		value.put(Constants.IFSC_CODE, "123");
		value.put(Constants.ACCOUNT_NUMBER, "abc");
		payload.put("value", value);
		try {
			bankDetailsService.updateUserBankDetails(userId,payload);
			Assert.assertTrue(true);
		} catch (ServiceException se) {
			Assert.fail();
		} catch (Exception e) {
			Assert.fail();
		}
	}
}
