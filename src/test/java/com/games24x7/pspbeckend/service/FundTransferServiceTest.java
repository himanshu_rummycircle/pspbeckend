/**
 * 
 */
package com.games24x7.pspbeckend.service;

import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.pspbeckend.Util.AccountServiceUtil;
import com.games24x7.pspbeckend.Util.AuditUtil;
import com.games24x7.pspbeckend.Util.FundTransferUtil;
import com.games24x7.pspbeckend.Util.PlayerUpdateUtil;
import com.games24x7.pspbeckend.Util.ThreadPoolExecutorUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.mq.CMmqFactory;
import com.games24x7.pspbeckend.pojo.CloseAndMergeData;
import com.games24x7.pspbeckend.pojo.MergeAccount;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class FundTransferServiceTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(FundTransferServiceTest.class);

    @Mock
    FundTransferUtil fsUtil;
    @Mock
    PlayerUpdateUtil puUtil;
    @Mock
    AccountServiceUtil asutil;
    @Mock
    ThreadPoolExecutorUtil util;
    @Mock
    CMmqFactory mq;

    @Mock
    AuditUtil auditUtil;

    @InjectMocks
    FundTransferService fs;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void processAccountMergingTest() {

        JSONObject jsonObject = new JSONObject();

        jsonObject.put(Constants.SUCCESS, new JSONArray());
        jsonObject.put(Constants.FAILURE, new JSONArray());
        jsonObject.put(Constants.TYPE, Constants.ACC_MERGING);
        jsonObject.put(Constants.FILE_NAME, "test.csv");
        jsonObject.put(Constants.AGENT_ID, 1000);
        jsonObject.put(Constants.AGENT_NAME, "murali");
        jsonObject.put(Constants.FILE_SIZE, 10);

        Mockito.doNothing().when(fsUtil).processPayload(Mockito.anyObject());
        Mockito.doNothing().when(asutil).updateAcctStatusForFailedMergeAcc(Mockito.anyObject());
        Mockito.doNothing().when(puUtil).insertIntoPlayerUpdates(Mockito.anyObject(), Mockito.anyInt());
        Mockito.doNothing().when(auditUtil).auditAccountMerging(Mockito.anyObject());
        Mockito.doNothing().when(mq).publishToCM(Mockito.anyString());
        Mockito.when(mq.getCmMergingMQMessage(Mockito.anyObject())).thenReturn(jsonObject.toString());
        Mockito.when(util.getPlayerUpdateThreadpoolexecutor()).thenReturn(
                new ThreadPoolExecutor(3, 10, 2, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>()));

        fs.processAccountMerging(null);
        
        
        
        MergeAccount a1 = new MergeAccount(1, 2, "comment");
        MergeAccount a2 = new MergeAccount(3, 4, "comment");
        ArrayList<MergeAccount> list= new ArrayList<MergeAccount>();
        list.add(a1);
        list.add(a2);
        CloseAndMergeData testClass = Mockito.spy(new CloseAndMergeData());
        Mockito.when(testClass.getFileName()).thenReturn("TestSpy.csv");
        Mockito.when(testClass.getMergeAccData()).thenReturn(list);
        Mockito.when(testClass.getSucessfulMergeAcc()).thenReturn(list);
        Mockito.when(testClass.getFailedMergeAcc()).thenReturn(list);
        Mockito.when(testClass.getAgent()).thenReturn("Test");

        fs.processAccountMerging(testClass);
        
        
        
       
        CloseAndMergeData data = Mockito.spy(new CloseAndMergeData());
        Mockito.when(data.getFileName()).thenReturn("TestSpy1.csv");
        Mockito.when(data.getMergeAccData()).thenReturn(list);
        Mockito.when(data.getSucessfulMergeAcc()).thenReturn(list);
        Mockito.when(data.getFailedMergeAcc()).thenReturn(new ArrayList<>());
        Mockito.when(data.getAgent()).thenReturn("Test1");

        fs.processAccountMerging(data);
        
        
        
        CloseAndMergeData data1 = Mockito.spy(new CloseAndMergeData());
        Mockito.when(data1.getFileName()).thenReturn("TestSpy1.csv");
        Mockito.when(data1.getMergeAccData()).thenReturn(list);
        Mockito.when(data1.getSucessfulMergeAcc()).thenReturn(list);
        Mockito.when(data1.getFailedMergeAcc()).thenReturn(new ArrayList<>());
        Mockito.when(data1.getAgent()).thenReturn("Test1");
        Mockito.doThrow(new RuntimeException("Testing ....")).when(fsUtil).processPayload(data1);

        fs.processAccountMerging(data1);
    }

}
