package com.games24x7.pspbeckend.service;

import static org.testng.Assert.assertEquals;

import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.KYCUtil;
import com.games24x7.pspbeckend.constants.Constants;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class KYCServiceTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(KYCServiceTest.class);

    @Autowired
    KYCUtil kycutil;

    @InjectMocks
    KYCService service;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            service.kycutil = kycutil;
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void testProcess() {
        JSONObject value = new JSONObject();
        value.put("docType", 2);
        value.put("statusComment", "test");
        value.put(Constants.TYPE, false);
        value.put("userId", 200);
        JSONObject context = new JSONObject();
        context.put("agentId", 200);
        context.put("source", "PSP");

        value.remove(Constants.TYPE);
        try {
            service.process(value, context);
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), Constants.TYPE_MISSING_FROM_PAYLOAD);
        }

        try {
            value.put(Constants.TYPE, "Hi");
            service.process(value, context);
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), Constants.TYPE_NOT_IN_BOOLEAN_ERR_CODE);
        }
        try {
            value.remove(Constants.TYPE);
            value.put(Constants.TYPE, "false");
            value.remove(Constants.USERID);
            service.process(value, context);
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), Constants.KYC_PROCESS_FAILURE_CODE);
        }

        try {
            value.put(Constants.TYPE, "false");
            value.put(Constants.USERID, 0);
            service.process(value, context);
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), Constants.KYC_PROCESS_FAILURE_CODE);
        }

        try {
            value.remove("docType");
            value.put(Constants.USERID, 200);
            service.process(value, context);
        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), Constants.DOC_TYPE_MISSING_FROM_PAYLOAD);
        }
        try {
            value.put("docType", 2);
            value.remove("statusComment");
            service.process(value, context);

        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), Constants.COMMENT_MISSING_FROM_PAYLOAD);
        }

        try {
            value.put("statusComment", "test");
            value.put(Constants.TYPE, true);
            context.put("source", "AVS");
            service.process(value, context);

        } catch (ServiceException e) {
            assertEquals(e.getBusinessErrorCode().intValue(), Constants.DATA_MISSING_FROM_PAYLOAD);
        }
    }
}
