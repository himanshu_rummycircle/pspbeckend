/**
 * 
 */
package com.games24x7.pspbeckend.service;

import static org.junit.Assert.assertNull;
import static org.testng.Assert.assertEquals;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.pspbeckend.Util.KnownDevicesUtil;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.pojo.KnownDevice;
import com.games24x7.pspbeckend.webclient.UserServiceClient;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class KnownDevicesServiceTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(KnownDevicesServiceTest.class);

    @Mock
    KnownDevicesUtil util;

    @Mock
    UserServiceClient userServiceClient;

    @InjectMocks
    KnownDevicesService  knownDevicesService;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
    }

    @Test
    public void processKnowDevicesTest() {
        KnownDevice knownDevice = new KnownDevice();
        knownDevice.setAction(Constants.GET_LIST_ACTION);
        knownDevice.setAgentId(1l);
        knownDevice.setAgentName("abc");
        knownDevice.setChannelId(1);
        knownDevice.setDeviceId("abc123");
        knownDevice.setUserId(123l);
        JSONArray jArr = new JSONArray();
        JSONObject jObj = new JSONObject();
        jObj.put("get", "list");
        jArr.put(jObj);
        Mockito.when(util.getListURL(knownDevice.getUserId())).thenReturn("http://stage2-ums.stage-sin.games24x7.com");
        Mockito.when(userServiceClient.getCall(util.getListURL(knownDevice.getUserId()))).thenReturn(jArr);
        assertEquals(knownDevicesService.processKnowDevices(knownDevice),"[{\"get\":\"list\"}]");
        JSONArray jArr1 = new JSONArray();
        JSONObject jObj1 = new JSONObject();
        jObj1.put("authenticate", "device");
        jArr1.put(jObj1);
        Mockito.when(userServiceClient.getCall(util.getListURL(knownDevice.getUserId()))).thenReturn(jArr1);
        assertEquals(knownDevicesService.processKnowDevices(knownDevice),"[{\"authenticate\":\"device\"}]");
        JSONArray jArr2 = new JSONArray();
        JSONObject jObj2 = new JSONObject();
        jObj2.put("remove", "device");
        jArr2.put(jObj2);
        Mockito.when(userServiceClient.getCall(util.getListURL(knownDevice.getUserId()))).thenReturn(jArr2);
        assertEquals(knownDevicesService.processKnowDevices(knownDevice),"[{\"remove\":\"device\"}]");
        knownDevice.setAction("someaction");
        assertNull(knownDevicesService.processKnowDevices(knownDevice));
    }
}
