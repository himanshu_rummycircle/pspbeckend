/**
 * 
 */
package com.games24x7.pspbeckend.service;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.webclient.NFSClient;
import com.games24x7.pspbeckend.webclient.UserServiceClient;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class MobileServiceTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(MobileServiceTest.class);

    @Mock
    UserServiceClient usClient;
    @Autowired
    Configuration config;
    @Mock
    NFSClient nfsClient;
    @InjectMocks
    MobileService service;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            service.config = config;

        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void getUserDataTest() {
        try {
            service.getUserData(0);
        } catch (ServiceException e) {
        }

        String res = "[{\"loginid\":\"harsh976\",\"mobile_confirmation\":0,\"user_id\":1523,\"last_login\":\"2018-09-03 16:21:06.0\",\"club_type\":7,\"mobile\":7878723332,\"account_status\":2}]";
        Mockito.when(usClient.getStringResGetCall(Mockito.anyString())).thenReturn(res);
        service.getUserData(7878723332l);

        String res4 = "[{\"loginid\":\"harsh976\",\"mobile_confirmation\":0,\"user_id\":1523,\"last_login\":\"2018-09-03 16:21:06.0\",\"club_type\":7,\"mobile\":7878723332,\"account_status\":6}]";
        Mockito.when(usClient.getStringResGetCall(Mockito.anyString())).thenReturn(res4);
        service.getUserData(7878723332l);

        String res1 = "[{\"loginid\":\"harsh976\",\"mobile_confirmation\":1,\"user_id\":1523,\"last_login\":\"2018-09-03 16:21:06.0\",\"club_type\":7,\"mobile\":7878723332,\"account_status\":2}]";
        Mockito.when(usClient.getStringResGetCall(Mockito.anyString())).thenReturn(res1);
        service.getUserData(7878723332l);

        String res2 = "[{\"loginid\":\"harsh976\",\"mobile_confirmation\":1,\"user_id\":1523,\"last_login\":\"2018-09-03 16:21:06.0\",\"club_type\":7,\"mobile\":7878723332,\"account_status\":6}]";
        Mockito.when(usClient.getStringResGetCall(Mockito.anyString())).thenReturn(res2);
        service.getUserData(7878723332l);

        String res3 = "[{\"loginid\":\"ZhN4GcT91234\",\"mobile_confirmation\":0,\"user_id\":100020679,\"last_login\":\"2018-04-19 10:17:20.0\",\"club_type\":1,\"mobile\":7895226666,\"account_status\":2},{\"loginid\":\"UrtALp391234\",\"mobile_confirmation\":0,\"user_id\":100020915,\"last_login\":\"2018-04-26 10:27:37.0\",\"club_type\":1,\"mobile\":7895226666,\"account_status\":2},{\"loginid\":\"IjyxFlRA1234\",\"mobile_confirmation\":0,\"user_id\":100020958,\"last_login\":\"2018-04-27 10:18:00.0\",\"club_type\":1,\"mobile\":7895226666,\"account_status\":2},{\"loginid\":\"6hpwt1pI1234\",\"mobile_confirmation\":0,\"user_id\":100020977,\"last_login\":\"2018-04-27 10:20:46.0\",\"club_type\":4,\"mobile\":7895226666,\"account_status\":2},{\"loginid\":\"rt7Wdmvz1234\",\"mobile_confirmation\":0,\"user_id\":100021000,\"last_login\":\"2018-04-30 10:17:51.0\",\"club_type\":1,\"mobile\":7895226666,\"account_status\":2}]";
        Mockito.when(usClient.getStringResGetCall(Mockito.anyString())).thenReturn(res3);
        String resp = "[{\"clubType\": 6,\"userId\": 100020679},{\"clubType\": 6,\"userId\": 100020915},{\"clubType\": 1,\"userId\": 100020958}]";
        Mockito.when(nfsClient.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
                .thenReturn(resp);
        service.getUserData(7895226666l);
    }
}
