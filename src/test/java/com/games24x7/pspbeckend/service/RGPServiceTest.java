package com.games24x7.pspbeckend.service;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.constants.Constants;
import com.games24x7.pspbeckend.constants.Constants.RGPConstants;
import com.games24x7.pspbeckend.webclient.AuditServiceClient;
import com.games24x7.pspbeckend.webclient.RGPClient;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class RGPServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(RGPServiceTest.class);
	@Mock
	RGPClient rgpClient;

	@Mock
	Configuration config;

	@Mock
	AuditServiceClient auditclient;

	@InjectMocks
	RGPService rgpService;

	@BeforeTest
	public void testSetup() {
		try {
			MockitoAnnotations.initMocks(this);
			Mockito.when(config.get(Mockito.anyString())).thenReturn("abc");
		} catch (Exception e) {
			logger.error("Exception in testSetup", e);
		}
	}
	
	@Test
	public void testgetRGPStatus() {
		try {
			rgpService.getRGPStatus(123l);
			Assert.assertTrue(true);
		}catch (Exception se) {
			Assert.fail();
		}
	}

	@Test
	public void testgetRGPStatusChangeLog() {
		try {
			rgpService.getRGPStatusChangeLog(123l);
			Assert.assertTrue(true);
		}catch (Exception se) {
			Assert.fail();
		}
	}
	
	@Test
	public void testupdateRGPStatus() {
		long userId = 123;
		JSONObject payload = new JSONObject();
		
		try {
			rgpService.updateRGPStatus(userId, payload);
		} catch (ServiceException se) {
			Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
			Assert.assertEquals(se.getBusinessErrorCode().intValue(), RGPConstants.MISSING_INVALID_RGP_STATUS_ERRCODE);
		} catch (Exception se) {
			Assert.fail();
		}

		payload.put(RGPConstants.RGP_STATUS_KEY, 1);
		try {
			rgpService.updateRGPStatus(userId, payload);
		} catch (ServiceException se) {
			Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
			Assert.assertEquals(se.getBusinessErrorCode().intValue(), RGPConstants.MISSING_CONTACT_TYPE_ERRCODE);
		} catch (Exception se) {
			Assert.fail();
		}
		
		payload.put(RGPConstants.CONTACT_TYPE_KEY, "");
		try {
			rgpService.updateRGPStatus(userId, payload);
		} catch (ServiceException se) {
			Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
			Assert.assertEquals(se.getBusinessErrorCode().intValue(), RGPConstants.MISSING_CONTACT_TYPE_ERRCODE);
		} catch (Exception se) {
			Assert.fail();
		}
		
		payload.put(RGPConstants.CONTACT_TYPE_KEY, " ");
		try {
			rgpService.updateRGPStatus(userId, payload);
		} catch (ServiceException se) {
			Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
			Assert.assertEquals(se.getBusinessErrorCode().intValue(), RGPConstants.MISSING_CONTACT_TYPE_ERRCODE);
		} catch (Exception se) {
			Assert.fail();
		}

		payload.put(RGPConstants.CONTACT_TYPE_KEY, "abc");
		try {
			rgpService.updateRGPStatus(userId, payload);
		} catch (ServiceException se) {
			Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
			Assert.assertEquals(se.getBusinessErrorCode().intValue(), RGPConstants.MISSING_CONTACT_DIRECTION_ERRCODE);
		} catch (Exception se) {
			Assert.fail();
		}
		
		payload.put(RGPConstants.CONTACT_DIRECTION_KEY, " ");
		try {
			rgpService.updateRGPStatus(userId, payload);
		} catch (ServiceException se) {
			Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
			Assert.assertEquals(se.getBusinessErrorCode().intValue(), RGPConstants.MISSING_CONTACT_DIRECTION_ERRCODE);
		} catch (Exception se) {
			Assert.fail();
		}

		payload.put(RGPConstants.CONTACT_DIRECTION_KEY, "abc");
		try {
			rgpService.updateRGPStatus(userId, payload);
		} catch (ServiceException se) {
			Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
			Assert.assertEquals(se.getBusinessErrorCode().intValue(), RGPConstants.MISSING_INVALID_COMMENT_ERR_CODE);
		} catch (Exception se) {
			Assert.fail();
		}

		payload.put(RGPConstants.COMMENTS_KEY, "");
		try {
			rgpService.updateRGPStatus(userId, payload);
		} catch (ServiceException se) {
			Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
			Assert.assertEquals(se.getBusinessErrorCode().intValue(), RGPConstants.MISSING_INVALID_COMMENT_ERR_CODE);
		} catch (Exception se) {
			Assert.fail();
		}

		payload.put(RGPConstants.COMMENTS_KEY, " ere");
		try {
			rgpService.updateRGPStatus(userId, payload);
		} catch (ServiceException se) {
			Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
			Assert.assertEquals(se.getBusinessErrorCode().intValue(), RGPConstants.MISSING_INVALID_COMMENT_ERR_CODE);
		} catch (Exception se) {
			Assert.fail();
		}

		payload.put(RGPConstants.COMMENTS_KEY, " er;e");
		try {
			rgpService.updateRGPStatus(userId, payload);
		} catch (ServiceException se) {
			Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
			Assert.assertEquals(se.getBusinessErrorCode().intValue(),
					RGPConstants.SPECIAL_CHARACTER_IN_COMMENT_ERR_CODE);
		} catch (Exception se) {
			Assert.fail();
		}

		payload.put(RGPConstants.COMMENTS_KEY, "ere, kk.");
		try {
			rgpService.updateRGPStatus(userId, payload);
			Assert.assertTrue(true);
		} catch (Exception se) {
			Assert.fail();
		}
	}
}
