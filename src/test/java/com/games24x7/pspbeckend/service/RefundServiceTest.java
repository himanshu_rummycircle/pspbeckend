/**
 * 
 */
package com.games24x7.pspbeckend.service;

import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.pspbeckend.Util.AccountServiceUtil;
import com.games24x7.pspbeckend.Util.PlayerUpdateUtil;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class RefundServiceTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(RefundServiceTest.class);

    @Mock
    PlayerUpdateUtil puUtil;
    @Mock
    AccountServiceUtil util;

    @InjectMocks
    RefundService rs;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void processRefundTest() {
        List<Long> ls = rs.processRefund(null);
        Assert.assertNull(ls);
    }

}
