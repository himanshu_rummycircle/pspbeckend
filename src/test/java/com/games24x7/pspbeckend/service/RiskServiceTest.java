package com.games24x7.pspbeckend.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.AuditUtil;
import com.games24x7.pspbeckend.Util.RestCoreUtil;
import com.games24x7.pspbeckend.Util.RiskCertificateHelper;
import com.games24x7.pspbeckend.Util.RiskUtil;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.constants.Constants.RiskConstant;
import com.games24x7.pspbeckend.dao.RiskCertificateDAO;
import com.games24x7.pspbeckend.pojo.PlayersRiskCertificate;
import com.games24x7.pspbeckend.pojo.RiskCertificate;
import com.games24x7.pspbeckend.pojo.UpdateRiskCertificate;
import com.games24x7.pspbeckend.pojo.WebResponse;
import com.games24x7.pspbeckend.webclient.AuditServiceClient;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class RiskServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    Configuration config;

    @Mock
    RiskCertificateHelper riskCerficateHelper;

    @Mock
    RiskCertificateDAO certificateDAO;

    @Mock
    AuditUtil audit;

    @Mock
    RiskUtil util;

    @Mock
    RestCoreUtil restUtil;

    @Mock
    AuditServiceClient auditServiceClient;

    @InjectMocks
    RiskService riskService;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            riskService.config = config;
            HashMap<String, Integer> riskMap = new HashMap<String, Integer>();
            riskMap.put("Not Risky", 0);
            riskMap.put("Risky", 1);
            riskService.loadRiskCertifiatesInfo();

        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void prepareUpdateRiskStatusTest() {
        PlayersRiskCertificate riskPayload = new PlayersRiskCertificate();
        riskPayload.setRiskStatus("Risky");
        Assert.assertNotNull(riskService.prepareUpdateRiskStatus(riskPayload));
    }

    @Test(priority=2)
    public void updateRiskStatusCallTest() throws Exception {
        PlayersRiskCertificate riskPayload = new PlayersRiskCertificate();
        riskPayload.setRiskStatus("Risky");
        WebResponse webResponse = new WebResponse();
        webResponse.setStatusCode(202);
        webResponse.setPayload("Payload");
        Mockito.when(restUtil.callPut(Mockito.anyString(), Mockito.any(), Mockito.any())).thenReturn(webResponse);

        try {
            riskService.updateRiskStatusCall(riskPayload);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.INTERNAL_SERVER_ERROR, se.getErrorStatus());
            Assert.assertEquals(MTTSettlementConstant.US_CALL_FAILURE_CODE, se.getBusinessErrorCode().intValue());
        }

        Mockito.doThrow(new NullPointerException()).when(restUtil)
                .callPut(Mockito.anyString(), Mockito.any(), Mockito.any());
        try {
            riskService.updateRiskStatusCall(riskPayload);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.INTERNAL_SERVER_ERROR, se.getErrorStatus());
            Assert.assertEquals(MTTSettlementConstant.US_CALL_FAILURE_CODE, se.getBusinessErrorCode().intValue());
        }
    }

    @Test(priority=2)
    public void callAuditServiceTest() {
        Mockito.doNothing().when(audit).publishToAuditPlayerRiskCertificate(Mockito.any());
        PlayersRiskCertificate riskPayload = new PlayersRiskCertificate();
        riskPayload.setRiskStatus("Risky");
        riskService.callAuditService(riskPayload);
        
        Mockito.doThrow(new NullPointerException()).when(audit)
                .publishToAuditPlayerRiskCertificate(Mockito.any());
        riskService.callAuditService(riskPayload); 
    }

    @Test
    public void updatePlayersStatusAndPublishDataToAuditTest(){
        Mockito.doNothing().when(audit).publishToAuditPlayerRiskCertificate(Mockito.any());
        PlayersRiskCertificate riskPayload = new PlayersRiskCertificate();
        riskPayload.setRiskStatus("Risky");
        WebResponse webResponse = new WebResponse();
        webResponse.setStatusCode(200);
        webResponse.setPayload("Payload");
        try {
            Mockito.when(restUtil.callPut(Mockito.anyString(), Mockito.any(), Mockito.any())).thenReturn(webResponse);
        } catch (Exception e) {
            logger.error("",e);
        }
        riskService.updatePlayersStatusAndPublishDataToAudit(riskPayload);
    }
    
    @Test
    public void fetchRiskStatusLogsAndCertificatesTest(){
        Mockito.when(util.getRiskStatus(Mockito.anyLong())).thenReturn("Risky");
        Mockito.when(util.getRiskLog(Mockito.anyLong())).thenReturn(new JSONArray());
        Mockito.when(util.getRiskStatus(Mockito.anyLong())).thenReturn("Risky");
        
        List<RiskCertificate> riskCertificateList = new ArrayList<RiskCertificate>();
        Mockito.doNothing().when(riskCerficateHelper).validateGetRiskCertificateInput(Mockito.anyInt(),Mockito.anyLong(),Mockito.anySet());
        Mockito.when(certificateDAO.getRiskCertificate(Mockito.anyInt(),Mockito.anyLong())).thenReturn(riskCertificateList);
        Mockito.when(riskCerficateHelper.getRiskListInJSONArray(Mockito.any(),Mockito.any())).thenReturn(new JSONArray());
        
        
        Assert.assertNotNull( riskService.fetchRiskStatusLogsAndCertificates(100,true, true, true) );
        Assert.assertNotNull( riskService.fetchRiskStatusLogsAndCertificates(100,false, false, false) );
    }
    
    @Test
    public void getRiskCertificateTest(){
        List<RiskCertificate> riskCertificateList = new ArrayList<RiskCertificate>();
        Mockito.doNothing().when(riskCerficateHelper).validateGetRiskCertificateInput(Mockito.anyInt(),Mockito.anyLong(),Mockito.anySet());
        Mockito.when(certificateDAO.getRiskCertificate(Mockito.anyInt(),Mockito.anyLong())).thenReturn(riskCertificateList);
        Mockito.when(riskCerficateHelper.getRiskListInJSONArray(Mockito.any(),Mockito.any())).thenReturn(new JSONArray());
        Assert.assertNotNull( riskService.getRiskCertificate(1, 4) );
    }
    
    @Test
    public void updatePlayersRiskStatusTest(){
        Mockito.when(util.getRiskStatus(Mockito.anyLong())).thenReturn("Risky");
        Mockito.when(util.getRiskLog(Mockito.anyLong())).thenReturn(new JSONArray());
        Mockito.when(util.getRiskStatus(Mockito.anyLong())).thenReturn("Risky");
        
        List<RiskCertificate> riskCertificateList = new ArrayList<RiskCertificate>();
        Mockito.doNothing().when(riskCerficateHelper).validateGetRiskCertificateInput(Mockito.anyInt(),Mockito.anyLong(),Mockito.anySet());
        Mockito.when(certificateDAO.getRiskCertificate(Mockito.anyInt(),Mockito.anyLong())).thenReturn(riskCertificateList);
        Mockito.when(riskCerficateHelper.getRiskListInJSONArray(Mockito.any(),Mockito.any())).thenReturn(new JSONArray());
        WebResponse webResponse = new WebResponse();
        webResponse.setStatusCode(200);
        webResponse.setPayload("Payload");
        try {
            Mockito.when(restUtil.callPut(Mockito.anyString(), Mockito.any(), Mockito.any())).thenReturn(webResponse);
        } catch (Exception e) {
            logger.error("",e);
        }
        PlayersRiskCertificate playerRiskCert = new PlayersRiskCertificate();
        playerRiskCert.setUserId(121);
        Mockito.when(riskCerficateHelper.validatePlayersRiskCertificateInput(Mockito.any())).thenReturn(playerRiskCert);
        
        Assert.assertNotNull( riskService.updatePlayersRiskStatus(null) );
    }
    
  
    @Test(priority=3)
    public void getRiskCertificatePostUpdateTest(){
        
        List<RiskCertificate> riskCertificateList = new ArrayList<RiskCertificate>();
        Mockito.doNothing().when(riskCerficateHelper).validateGetRiskCertificateInput(Mockito.anyInt(),Mockito.anyLong(),Mockito.anySet());
        Mockito.when(certificateDAO.getRiskCertificate(Mockito.anyInt(),Mockito.anyLong())).thenReturn(riskCertificateList);
        Mockito.when(riskCerficateHelper.getRiskListInJSONArray(Mockito.any(),Mockito.any())).thenReturn(new JSONArray());
        Assert.assertNotNull( riskService.getRiskCertificatePostUpdate(1));
        
        Mockito.doThrow(new NullPointerException()).when(certificateDAO).getRiskCertificate(Mockito.anyInt(),Mockito.anyLong());
        riskService.getRiskCertificatePostUpdate(1); 
    }
    
    @Test
    public void updateRiskCertificateTest(){
        UpdateRiskCertificate riskPayload = new UpdateRiskCertificate();
        StringBuilder strB=  new StringBuilder();
        for(int i =0 ; i < 1205 ; i++){
            if(strB.length()>0){
                strB.append(",100"+i);
            } else{
                strB.append("100"+i);
            }
        }
        riskPayload.setRiskCertificateIds(strB.toString());
        Mockito.when(riskCerficateHelper.validateUpdateRiskCertificateInput(Mockito.any())).thenReturn(riskPayload);
        Mockito.doNothing().when(certificateDAO).updateRiskCertificate(Mockito.any(),Mockito.anyBoolean());
        List<RiskCertificate> riskCertificateList = new ArrayList<RiskCertificate>();
        Mockito.doNothing().when(riskCerficateHelper).validateGetRiskCertificateInput(Mockito.anyInt(),Mockito.anyLong(),Mockito.anySet());
        Mockito.when(certificateDAO.getRiskCertificate(Mockito.anyInt(),Mockito.anyLong())).thenReturn(riskCertificateList);
        Mockito.when(riskCerficateHelper.getRiskListInJSONArray(Mockito.any(),Mockito.any())).thenReturn(new JSONArray());
        riskService.updateRiskCertificate(new JSONObject()); 
    }
 
    @Test
    public void createRiskCertificateInDatabaseTest(){
        RiskCertificate riskPayload = new RiskCertificate();
        riskPayload.setRiskStatus("Risky");
        riskPayload.setId(1);
        Mockito.doNothing().when(certificateDAO).createRiskCertificate(Mockito.any());
        Assert.assertEquals(1, riskService.createRiskCertificateInDatabase(riskPayload));
        
        Mockito.doThrow(new NullPointerException()).when(certificateDAO).createRiskCertificate(Mockito.any());
        try {
            riskService.createRiskCertificateInDatabase(riskPayload);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.INTERNAL_SERVER_ERROR, se.getErrorStatus());
            Assert.assertEquals(RiskConstant.DATABASE_OPERATION_ERROR_CODE, se.getBusinessErrorCode().intValue());
        }
    }
    @Test
    public void createRiskCertificateTest(){
        
        RiskCertificate riskPayload = new RiskCertificate();
        riskPayload.setRiskStatus("Risky");
        riskPayload.setId(1);
        Mockito.doNothing().when(certificateDAO).createRiskCertificate(Mockito.any());
        
        Mockito.doNothing().when(riskCerficateHelper).validateCreateInputPayload(Mockito.any());
        Mockito.when(riskCerficateHelper.jsonToRiskCertificatePayload(Mockito.any())).thenReturn(riskPayload);
        
        
        
        riskService.createRiskCertificate(new JSONObject());
    }
    
}
