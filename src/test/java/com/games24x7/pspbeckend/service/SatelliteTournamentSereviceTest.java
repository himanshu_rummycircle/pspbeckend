package com.games24x7.pspbeckend.service;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.SatelliteSettlementHelper;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.dao.SatelliteSettlementDAO;
import com.games24x7.pspbeckend.pojo.OfflineSettlementMoneyInfo;
import com.games24x7.pspbeckend.pojo.OfflineSettlementPayload;
import com.games24x7.pspbeckend.pojo.WebResponse;
import com.games24x7.pspbeckend.webclient.FMSClient;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class SatelliteTournamentSereviceTest extends AbstractTestNGSpringContextTests {

    @Mock
    SatelliteSettlementHelper offlineSettlementHelper;

    @Mock
    FMSClient fmsClient;

    @Mock
    SatelliteSettlementDAO settlementDAO;

    @Autowired
    Configuration config;

    @InjectMocks
    SatelliteTournamentService satelliteService;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            satelliteService.config = config;
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void processOfflineSatelliteSettlementTest() {
        JSONObject payloadJSON = new JSONObject();
        OfflineSettlementPayload payload = new OfflineSettlementPayload();
        payload.setTournamentId(99l);
        Mockito.when(offlineSettlementHelper.jsonToOfflineSettlementPayload(Mockito.any())).thenReturn(payload);
        Mockito.when(offlineSettlementHelper.validateInputData(Mockito.any()))
                .thenReturn(new OfflineSettlementMoneyInfo());
        Mockito.doNothing().when(offlineSettlementHelper).publishEdsMessageAndAuditLogs(Mockito.any());
        WebResponse webR = new WebResponse();
        webR.setPayload("[{},{}]");
        webR.setStatusCode(202);
        Mockito.when(fmsClient.excessRevenueMovement(Mockito.anyLong(), Mockito.anyDouble())).thenReturn(webR);
        OfflineSettlementMoneyInfo settlementMoneyInfo = new OfflineSettlementMoneyInfo();
        settlementMoneyInfo.setTotalQualifierContribution(100D);
        settlementMoneyInfo.setTotalPrizePool(20D);

        satelliteService.processOfflineSatelliteSettlement(payloadJSON);
    }

    @Test
    public void callSettlementProcedureTest() {
        OfflineSettlementPayload payload = new OfflineSettlementPayload();
        OfflineSettlementMoneyInfo settlementMoneyInfo = new OfflineSettlementMoneyInfo();
        try {
            Mockito.doThrow(new ServiceException("AgentName is missing.", Status.BAD_REQUEST)).when(settlementDAO)
                    .processOfflineSettlement(Mockito.any(), Mockito.anyDouble());
            satelliteService.callSettlementProcedure(payload, settlementMoneyInfo);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(),
                    MTTSettlementConstant.DATABASE_OPERTAION_FAILED_ERRPR_CODE);
        }
        Mockito.doNothing().when(settlementDAO).processOfflineSettlement(Mockito.any(), Mockito.anyDouble());
        settlementMoneyInfo.setTotalQualifierContribution(100D);
        settlementMoneyInfo.setTotalPrizePool(20D);

        WebResponse webR = new WebResponse();
        webR.setPayload("[{},{}]");
        webR.setStatusCode(200);
        Mockito.when(fmsClient.excessRevenueMovement(Mockito.anyLong(), Mockito.anyDouble())).thenReturn(webR);
        try {
            satelliteService.callSettlementProcedure(payload, settlementMoneyInfo);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(e.getBusinessErrorCode().intValue(), MTTSettlementConstant.FMS_CALL_FAILURE_MSG_CODE);
        }
        webR.setStatusCode(202);
        Mockito.when(fmsClient.excessRevenueMovement(Mockito.anyLong(), Mockito.anyDouble())).thenReturn(webR);
        payload.setTournamentId(99l);
        satelliteService.callSettlementProcedure(payload, settlementMoneyInfo);
    }
}
