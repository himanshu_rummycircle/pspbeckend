package com.games24x7.pspbeckend.service;

import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.UpdateMobileUtil;
import com.games24x7.pspbeckend.pojo.WebResponse;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class UpdateMobileServiceTest extends AbstractTestNGSpringContextTests {

    @InjectMocks
    @Spy
    UpdateMobileService updateMobileServiceMockInjected;

    @Mock
    UpdateMobileUtil updateMobileUtilMock;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);

        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void testValidateUpdateMobilePayload() {
        try {
            updateMobileServiceMockInjected.validateUpdateMobilePayload(0, null);
        } catch (ServiceException se) {
            Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(se.getMessage(), "Invalid UserId ");
        }

        try {
            updateMobileServiceMockInjected.validateUpdateMobilePayload(-1, null);
        } catch (ServiceException se) {
            Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(se.getMessage(), "Invalid UserId ");
        }

        JSONObject inputJson = new JSONObject();
        try {
            updateMobileServiceMockInjected.validateUpdateMobilePayload(321857, inputJson);
        } catch (ServiceException se) {
            Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(se.getMessage(), "value Missing from Payload");
        }

        JSONObject valueJson = new JSONObject();
        inputJson.put("value", valueJson);
        try {
            updateMobileServiceMockInjected.validateUpdateMobilePayload(321857, inputJson);
        } catch (ServiceException se) {
            Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(se.getMessage(), "mobile is missing");
        }

        valueJson.put("mobile", 0);
        inputJson.put("value", valueJson);
        try {
            updateMobileServiceMockInjected.validateUpdateMobilePayload(321857, inputJson);
        } catch (ServiceException se) {
            Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(se.getMessage(), "Invalid mobile");
        }

        valueJson.put("mobile", "8888888888");
        inputJson.put("value", valueJson);
        try {
            updateMobileServiceMockInjected.validateUpdateMobilePayload(321857, inputJson);
        } catch (ServiceException se) {
            Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(se.getMessage(), "context Missing from Payload");
        }

        JSONObject contextJson = new JSONObject();
        inputJson.put("context", contextJson);
        try {
            updateMobileServiceMockInjected.validateUpdateMobilePayload(321857, inputJson);
        } catch (ServiceException se) {
            Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(se.getMessage(), "agentName is missing/invalid");
        }

        contextJson.put("agentName", "");
        inputJson.put("context", contextJson);
        try {
            updateMobileServiceMockInjected.validateUpdateMobilePayload(321857, inputJson);
        } catch (ServiceException se) {
            Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(se.getMessage(), "agentName is missing/invalid");
        }

        contextJson.put("agentName", "tester");
        inputJson.put("context", contextJson);
        try {
            updateMobileServiceMockInjected.validateUpdateMobilePayload(321857, inputJson);
        } catch (ServiceException se) {
            Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(se.getMessage(), "agentGroup is missing/invalid");
        }

        contextJson.put("agentGroup", "");
        inputJson.put("context", contextJson);
        try {
            updateMobileServiceMockInjected.validateUpdateMobilePayload(321857, inputJson);
        } catch (ServiceException se) {
            Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(se.getMessage(), "agentGroup is missing/invalid");
        }

        contextJson.put("agentGroup", "test");
        inputJson.put("context", contextJson);
        try {
            updateMobileServiceMockInjected.validateUpdateMobilePayload(321857, inputJson);
        } catch (ServiceException se) {
            Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(se.getMessage(), "comments is missing/invalid");
        }

        contextJson.put("comments", "");
        inputJson.put("context", contextJson);
        try {
            updateMobileServiceMockInjected.validateUpdateMobilePayload(321857, inputJson);
        } catch (ServiceException se) {
            Assert.assertEquals(se.getErrorCode(), Status.BAD_REQUEST.getStatusCode());
            Assert.assertEquals(se.getMessage(), "comments is missing/invalid");
        }

        contextJson.put("comments", "testing");
        inputJson.put("context", contextJson);
        updateMobileServiceMockInjected.validateUpdateMobilePayload(321857, inputJson);
    }

    @Test
    public void testUpdateMobile() {
        Mockito.doNothing().when(updateMobileServiceMockInjected).validateUpdateMobilePayload(Mockito.anyLong(),
                Mockito.any());
        WebResponse webResponse = new WebResponse();
        webResponse.setStatusCode(200);
        Mockito.doReturn(webResponse).when(updateMobileUtilMock).updateMobile(Mockito.anyLong(), Mockito.any());
        Mockito.doNothing().when(updateMobileUtilMock).deleteAllSessionsOfUser(Mockito.anyLong());

        WebResponse response = updateMobileServiceMockInjected.updateMobile(321857, new JSONObject());
        Assert.assertEquals(response.getStatusCode(), 200);

        webResponse.setStatusCode(400);
        Mockito.doReturn(webResponse).when(updateMobileUtilMock).updateMobile(Mockito.anyLong(), Mockito.any());
        response = updateMobileServiceMockInjected.updateMobile(321857, new JSONObject());
        Assert.assertEquals(response.getStatusCode(), 400);
    }

}
