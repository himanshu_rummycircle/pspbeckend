package com.games24x7.pspbeckend.webclient;

import javax.ws.rs.core.Response.Status;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.RestCoreUtil;
import com.games24x7.pspbeckend.constants.Constants.WebConstant;
import com.games24x7.pspbeckend.pojo.WebResponse;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class AuditServiceClientTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(FundServiceClientTest.class);

    @Mock
    RestCoreUtil restUtil;
    @InjectMocks
    AuditServiceClient auditServiceClient;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void getPlayerRiskCertificateAuditDataTest() throws Exception {

        Mockito.when(restUtil.callGet(Mockito.anyString(), Mockito.any())).thenReturn(new WebResponse());
        auditServiceClient.getPlayerRiskCertificateAuditData(null);

    }

    @Test(priority = 9)
    public void getPlayerRiskCertificateAuditDataErrTest() throws Exception {

        Mockito.when(restUtil.callGet(Mockito.anyString(), Mockito.any())).thenThrow(
                new ServiceException("Testing....", 400));
        Assert.assertNull(auditServiceClient.getPlayerRiskCertificateAuditData(null));

    }

    @Test
    public void postCallTest() throws Exception {

        Mockito.when(restUtil.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn("1");
        Assert.assertNotNull(auditServiceClient.postCall(null, null, null));

        Mockito.when(restUtil.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenThrow(
                new ServiceException("Testing....", 400));
        Assert.assertNull(auditServiceClient.postCall(null, null, null));

    }

    @Test
    public void getCallTest() throws Exception {

        Mockito.when(restUtil.getCall(Mockito.anyString())).thenReturn("[]");
        Assert.assertNotNull(auditServiceClient.getCall(null));

        Mockito.when(restUtil.getCall(Mockito.anyString())).thenThrow(
                new ServiceException("Testing....", Status.NOT_FOUND));
        Assert.assertNull(auditServiceClient.getCall(null));

    }

    //@Test(priority=7)
    public void getCallErrTest() {
        Mockito.when(restUtil.getCall(Mockito.anyString())).thenThrow(
                new ServiceException("Testing....", Status.NO_CONTENT));

        try {
            auditServiceClient.getCall(null);
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.NO_CONTENT, se.getStatus());
        }
    }
}
