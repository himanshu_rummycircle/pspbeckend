package com.games24x7.pspbeckend.webclient;

import javax.ws.rs.core.Response.Status;

import junit.framework.Assert;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.RestCoreUtil;
import com.games24x7.pspbeckend.pojo.WebResponse;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class FMSClientTest extends AbstractTestNGSpringContextTests {

    private static final Logger logger = LoggerFactory.getLogger(FMSClientTest.class);

    @Mock
    RestCoreUtil restUtil;
    @InjectMocks
    FMSClient fmsClient;
    @Autowired
    Configuration config;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            fmsClient.config = config;
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void postCallTest() {
        Mockito.when(restUtil.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn("");
        String res = fmsClient.postCall(null, null, null);
        Assert.assertNotNull(res);

        Mockito.when(restUtil.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
                .thenThrow(new ServiceException("Testing....", 400));
        try {
            res = fmsClient.postCall(null, null, null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getErrorCode(), 400);
        }

    }

    @Test
    public void excessRevenueMovementTest() throws Exception {
        Mockito.when(restUtil.callPost(Mockito.anyString(), Mockito.any(), Mockito.any())).thenReturn(null);
        Assert.assertNull(fmsClient.excessRevenueMovement(null, 0D));

        try {
            Mockito.doThrow(new ServiceException("AgentName is missing.", Status.BAD_REQUEST)).when(restUtil)
                    .callPost(Mockito.anyString(), Mockito.any(), Mockito.any());
            fmsClient.excessRevenueMovement(1000l, 10D);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getStatus().getStatusCode(), Status.BAD_REQUEST.getStatusCode());
        }

    }

}
