/**
 * 
 */
package com.games24x7.pspbeckend.webclient;

import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.RestCoreUtil;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.pojo.WebResponse;

import junit.framework.Assert;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class FundServiceClientTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(FundServiceClientTest.class);

    @Mock
    RestCoreUtil restUtil;
    @InjectMocks
    FundServiceClient fs;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void postCallTest() {
        Mockito.when(restUtil.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn("");
        String res = fs.postCall(null, null, null);
        Assert.assertNotNull(res);

        Mockito.when(restUtil.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
                .thenThrow(new ServiceException("Testing....", 400));
        try {
            res = fs.postCall(null, null, null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getErrorCode(), 400);
        }

    }

    @Test
    public void getCallTest() {
        Mockito.when(restUtil.getCall(Mockito.anyString())).thenReturn(new JSONArray().toString());
        JSONArray arr = fs.getCall(null);
        Assert.assertNotNull(arr);

        Mockito.when(restUtil.getCall(Mockito.anyString()))
                .thenThrow(new ServiceException("Testing...", Status.NOT_FOUND));
        try {
            arr = fs.getCall(null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getErrorStatus(), Status.NOT_FOUND);
        }

        
        try {
            Mockito.when(restUtil.getCall(Mockito.anyString())).thenThrow(new RuntimeException("Testing..."));
            arr = fs.getCall(null);
        } catch (Exception e) {
        }

    }
    
    @Test
    public void getAccountTest() {
        try {
            Mockito.when(restUtil.callGet(Mockito.any(),Mockito.any())).thenReturn(new WebResponse());
        } catch (Exception e1) {
            logger.error("",e1);
        }
        
        try {
            fs.getAccount(null,null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(),
                    MTTSettlementConstant.FS_CALL_FAILURE_MSG_CODE);
        }
        WebResponse rs = new WebResponse();
        rs.setStatusCode(200);
        rs.setPayload("");
        try {
            Mockito.when(restUtil.callGet(Mockito.any(),Mockito.any())).thenReturn(rs);
        } catch (Exception e1) {
            logger.error("",e1);
        }
        
        try {
            fs.getAccount(null,null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(),
                    MTTSettlementConstant.FS_CALL_FAILURE_MSG_CODE);
        }

    }

}
