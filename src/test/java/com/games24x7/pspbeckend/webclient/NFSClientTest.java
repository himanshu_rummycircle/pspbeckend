/**
 * 
 */
package com.games24x7.pspbeckend.webclient;

import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.RestCoreUtil;

import junit.framework.Assert;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class NFSClientTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(NFSClientTest.class);

    @Mock
    RestCoreUtil restUtil;

    @InjectMocks
    NFSClient nfs;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void postCallTest() {
        Mockito.when(restUtil.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn("");
        String res = nfs.postCall(null, null, null);
        Assert.assertNotNull(res);

        Mockito.when(restUtil.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
                .thenThrow(new ServiceException("Testing....", 400));
        try {
            res = nfs.postCall(null, null, null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getErrorCode(), 400);
        }

    }

    @Test
    public void getCallTest() {
        Mockito.when(restUtil.getCall(Mockito.anyString())).thenReturn(new JSONArray().toString());
        JSONArray arr = nfs.getCall(null);
        Assert.assertNotNull(arr);

        Mockito.when(restUtil.getCall(Mockito.anyString()))
                .thenThrow(new ServiceException("Testing...", Status.NOT_FOUND));
        try {
            arr = nfs.getCall(null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getErrorStatus(), Status.NOT_FOUND);
        }

        try {
            Mockito.when(restUtil.getCall(Mockito.anyString())).thenThrow(new RuntimeException("Testing..."));
            arr = nfs.getCall(null);
        } catch (Exception e) {
        }

    }
}
