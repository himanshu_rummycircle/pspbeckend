/**
 * 
 */
package com.games24x7.pspbeckend.webclient;

import static org.mockito.Matchers.anyString;

import javax.ws.rs.core.Response.Status;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.RestCoreUtil;
import com.games24x7.pspbeckend.constants.Constants.MTTSettlementConstant;
import com.games24x7.pspbeckend.constants.Constants.RiskConstant;
import com.games24x7.pspbeckend.pojo.WebResponse;

/**
 * 
 */
@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class UserServiceClientTest extends AbstractTestNGSpringContextTests {
    private static final Logger logger = LoggerFactory.getLogger(UserServiceClientTest.class);
    @Mock
    RestCoreUtil restUtil;
    @InjectMocks
    UserServiceClient userServiceClient;
    @Autowired
    Configuration config;

    @BeforeTest
    public void testSetup() {
        try {
            springTestContextPrepareTestInstance();
            MockitoAnnotations.initMocks(this);
            userServiceClient.config = config;
        } catch (Exception e) {
            logger.error("Exception in testSetup", e);
        }
    }

    @Test
    public void postCallTest() {
        Mockito.when(restUtil.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn("");
        String res = userServiceClient.postCall(null, null, null);
        Assert.assertNotNull(res);

        Mockito.when(restUtil.postCall(Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
                .thenThrow(new ServiceException("Testing....", 400));
        try {
            res = userServiceClient.postCall(null, null, null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getErrorCode(), 400);
        }

    }

    @Test
    public void getCallTest() {
        Mockito.when(restUtil.getCall(Mockito.anyString())).thenReturn(new JSONArray().toString());
        JSONArray arr = userServiceClient.getCall(null);
        Assert.assertNotNull(arr);

        Mockito.when(restUtil.getCall(Mockito.anyString()))
                .thenThrow(new ServiceException("Testing...", Status.NOT_FOUND));
        try {
            arr = userServiceClient.getCall(null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getErrorStatus(), Status.NOT_FOUND);
        }

        try {
            Mockito.when(restUtil.getCall(Mockito.anyString())).thenThrow(new RuntimeException("Testing..."));
            arr = userServiceClient.getCall(null);
        } catch (Exception e) {
        }

    }
    
    @Test
    public void getUserStatusTest() {
        try {
            Mockito.when(restUtil.callGet(Mockito.any(),Mockito.any())).thenReturn(new WebResponse());
        } catch (Exception e1) {
            logger.error("",e1);
        }
        
        try {
            userServiceClient.getCall(null,null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(),
                    MTTSettlementConstant.US_CALL_FAILURE_CODE);
        }
        WebResponse rs = new WebResponse();
        rs.setStatusCode(200);
        rs.setPayload("");
        try {
            Mockito.when(restUtil.callGet(Mockito.any(),Mockito.any())).thenReturn(new WebResponse());
        } catch (Exception e1) {
            logger.error("",e1);
        }
        
        try {
            userServiceClient.getCall(null,null);
        } catch (ServiceException e) {
            Assert.assertEquals(e.getBusinessErrorCode().intValue(),
                    MTTSettlementConstant.US_CALL_FAILURE_CODE);
        }

    }
    
    @Test(priority=10)
    public void getStringResGetCallErrTest(){
        
        //Mockito.when(restUtil.getCall(Mockito.anyString())).thenReturn("2");
        Assert.assertEquals(null, userServiceClient.getStringResGetCall("1"));
        
        ServiceException se = new ServiceException("s",Status.NOT_FOUND);
        Mockito.doThrow(se).when(restUtil).getCall(anyString());
        userServiceClient.getStringResGetCall("1");
        
        se = new ServiceException("s",Status.OK);
        Mockito.doThrow(se).when(restUtil).getCall(anyString());
        try {
            userServiceClient.getStringResGetCall("1");
            Assert.fail();
        } catch (ServiceException ee) {
            Assert.assertNotNull(ee);
        }
    }
    
    @Test
    public void getValidUserListSizeTest() throws Exception{
        WebResponse resp = new WebResponse();
        
        Mockito.when(restUtil.callGet(Mockito.anyString(),Mockito.any())).thenReturn(resp);
        try {
            userServiceClient.getValidUserListSize("1");
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.INTERNAL_SERVER_ERROR, se.getErrorStatus());
            Assert.assertEquals(MTTSettlementConstant.US_CALL_FAILURE_CODE, se.getBusinessErrorCode().intValue());
        }
        resp.setStatusCode(200);
        Mockito.when(restUtil.callGet(Mockito.anyString(),Mockito.any())).thenReturn(resp);
        try {
            userServiceClient.getValidUserListSize("1");
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.INTERNAL_SERVER_ERROR, se.getErrorStatus());
            Assert.assertEquals(MTTSettlementConstant.US_CALL_FAILURE_CODE, se.getBusinessErrorCode().intValue());
        }
        JSONArray jArray = new JSONArray();
        JSONObject jjson = new JSONObject();
        jjson.put("user_id", 77);
        jArray.put(jjson);
        resp.setPayload(jArray.toString());
        Mockito.when(restUtil.callGet(Mockito.anyString(),Mockito.any())).thenReturn(resp);
        Assert.assertEquals(1, userServiceClient.getValidUserListSize("1"));
        
        resp.setStatusCode(400);
        Mockito.when(restUtil.callGet(Mockito.anyString(),Mockito.any())).thenReturn(resp);
        try {
            userServiceClient.getValidUserListSize("1");
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(Status.INTERNAL_SERVER_ERROR, se.getErrorStatus());
            Assert.assertEquals(MTTSettlementConstant.US_CALL_FAILURE_CODE, se.getBusinessErrorCode().intValue());
        }
        
        resp.setStatusCode(404);
        Mockito.when(restUtil.callGet(Mockito.anyString(),Mockito.any())).thenReturn(resp);
        try {
            userServiceClient.getValidUserListSize("1");
            Assert.fail();
        } catch (ServiceException se) {
            Assert.assertNotNull(se);
            Assert.assertEquals(RiskConstant.USER_NOT_FOUND_ERROR_CODE, se.getBusinessErrorCode().intValue());
        }
    }

}
