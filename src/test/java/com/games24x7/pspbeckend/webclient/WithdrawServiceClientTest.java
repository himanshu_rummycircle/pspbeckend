package com.games24x7.pspbeckend.webclient;

import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.games24x7.frameworks.config.Configuration;
import com.games24x7.frameworks.exception.ServiceException;
import com.games24x7.pspbeckend.Util.RestCoreUtil;
import com.games24x7.pspbeckend.pojo.WebResponse;

import junit.framework.Assert;

@ContextConfiguration(locations = { "classpath:test_pspbeckend_context.xml" })
public class WithdrawServiceClientTest {
	private static final Logger logger = LoggerFactory.getLogger(WithdrawServiceClientTest.class);
	@Mock
	RestCoreUtil restUtil;

	@Mock
	Configuration config;

	@InjectMocks
	WithdrawServiceClient withdrawServiceClient;

	@BeforeTest
	public void testSetup() {
		try {
			MockitoAnnotations.initMocks(this);
			Mockito.when(config.get(Mockito.anyString())).thenReturn("abc");
		} catch (Exception e) {
			logger.error("Exception in testSetup", e);
		}
	}

	@Test
	public void postCallTest() {

		try {
			Mockito.when(restUtil.callPost(Mockito.anyString(), Mockito.any(), Mockito.any()))
					.thenReturn(new WebResponse());
			String res = withdrawServiceClient.postCall(null, null);
			Assert.assertNotNull(res);

			Mockito.when(restUtil.callPost(Mockito.anyString(), Mockito.any(), Mockito.any()))
					.thenThrow(new ServiceException("Testing....", 400));
			try {
				res = withdrawServiceClient.postCall(null, null);
			} catch (ServiceException e) {
				Assert.assertEquals(e.getErrorCode(), 400);
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	@Test
	public void getCallTest() {
		Mockito.when(restUtil.getCall(Mockito.anyString())).thenReturn(null);
		JSONArray arr = withdrawServiceClient.getCall(null);
		Assert.assertNotNull(arr);

		Mockito.when(restUtil.getCall(Mockito.anyString())).thenReturn(new JSONArray().toString());
		arr = withdrawServiceClient.getCall(null);
		Assert.assertNotNull(arr);

		Mockito.when(restUtil.getCall(Mockito.anyString())).thenReturn("abc");
		String resp = withdrawServiceClient.getStringResGetCall(null);
		Assert.assertNotNull(resp);

		Mockito.doThrow(new ServiceException("Testing...", Status.NOT_FOUND)).when(restUtil)
				.getCall(Mockito.anyString());
		try {
			arr = withdrawServiceClient.getCall(null);
		} catch (ServiceException e) {
			Assert.assertEquals(e.getErrorStatus(), Status.NOT_FOUND);
		}

		Mockito.doThrow(new ServiceException("Testing...", Status.BAD_REQUEST)).when(restUtil)
				.getCall(Mockito.anyString());
		try {
			arr = withdrawServiceClient.getCall(null);
		} catch (ServiceException e) {
			Assert.assertEquals(e.getErrorStatus(), Status.BAD_REQUEST);
		}

		Mockito.doThrow(new ServiceException("Testing...", Status.NOT_FOUND)).when(restUtil)
				.getCall(Mockito.anyString());
		try {
			resp = withdrawServiceClient.getStringResGetCall(null);
		} catch (ServiceException e) {
			Assert.assertEquals(e.getErrorStatus(), Status.NOT_FOUND);
		}

		Mockito.doThrow(new ServiceException("Testing...", Status.BAD_REQUEST)).when(restUtil)
				.getCall(Mockito.anyString());
		try {
			resp = withdrawServiceClient.getStringResGetCall(null);
		} catch (ServiceException e) {
			Assert.assertEquals(e.getErrorStatus(), Status.BAD_REQUEST);
		}
	}
}
